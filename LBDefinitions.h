#ifndef _PEANO_APPLICATIONS_LATTICEBOLTZMANN_OPTIMISED_OPTLBDEFINITIONS_H_
#define _PEANO_APPLICATIONS_LATTICEBOLTZMANN_OPTIMISED_OPTLBDEFINITIONS_H_

#include "peano/utils/Globals.h"

#define LB_DIMENSION 32

#define COARSE_REFINEMENT_LEVEL 2
// #define COARSE_REFINEMENT_LEVEL 2
#define FINE_REFINEMENT_LEVEL 3
// #define FINE_REFINEMENT_LEVEL   3

#define INIT_VEL_X 0.0
#define INIT_VEL_Y 0.0
#define INIT_VEL_Z 0.0

#define MOVING_WALL_X 0.07
#define MOVING_WALL_Y 0.0
#define MOVING_WALL_Z 0.0

#define NUMBER_OF_ITERATIONS 8000
#define PLOTTING_TIMESTEP    500

#define TAU_ON_COARSEST_LEVEL 0.70

#if defined (Dim2)
  #define LB_DIR          9
  #define CELL_BLOCK_SIZE (LB_DIMENSION * LB_DIMENSION)

  #define VERTEX_BLOCK_SIZE ((LB_DIMENSION - 1) * 2)

  #define SOUTH_WEST 0
  #define SOUTH      1
  #define SOUTH_EAST 2
  #define WEST       3
  #define CENTER     4
  #define EAST       5
  #define NORTH_WEST 6
  #define NORTH      7
  #define NORTH_EAST 8

#elif defined (Dim3)
  #define LB_DIR            19
  #define CELL_BLOCK_SIZE   (LB_DIMENSION * LB_DIMENSION * LB_DIMENSION)
  #define VERTEX_BLOCK_SIZE ((LB_DIMENSION - 1) * (LB_DIMENSION - 1) * 3)

  #define BOTTOM_SOUTH 0
  #define BOTTOM_WEST  1
  #define BOTTOM       2
  #define BOTTOM_EAST  3
  #define BOTTOM_NORTH 4

  #define SOUTH_WEST 5
  #define SOUTH      6
  #define SOUTH_EAST 7
  #define WEST       8
  #define CENTER     9
  #define EAST       10
  #define NORTH_WEST 11
  #define NORTH      12
  #define NORTH_EAST 13

  #define TOP_SOUTH 14
  #define TOP_WEST  15
  #define TOP       16
  #define TOP_EAST  17
  #define TOP_NORTH 18

#endif

#define LB_DIR_BY_CELL_BLOCK_SIZE (LB_DIR * CELL_BLOCK_SIZE)

#if (LB_DIR == 19) && (DIMENSIONS == 3)
static const int    VELOCITIES[19][3] = { { 0,  -1,  -1   },
                                          { -1, 0,   -1   },
                                          { 0,  0,   -1   },
                                          { 1,  0,   -1   },
                                          { 0,  1,   -1   },
                                          { -1, -1,  0    },
                                          { 0,  -1,  0    },
                                          { 1,  -1,  0    },
                                          { -1, 0,   0    },
                                          { 0,  0,   0    },
                                          { 1,  0,   0    },
                                          { -1, 1,   0    },
                                          { 0,  1,   0    },
                                          { 1,  1,   0    },
                                          { 0,  -1,  1    },
                                          { -1, 0,   1    },
                                          { 0,  0,   1    },
                                          { 1,  0,   1    },
                                          { 0,  1,   1    } };
static const double WEIGHTS[19] =
{ 1.0 / 36.0, 1.0 / 36.0, 1.0 / 18.0,  1.0 / 36.0,  1.0 / 36.0,
  1.0 / 36.0, 1.0 / 18.0, 1.0 / 36.0,
  1.0 / 18.0, 1.0 / 3.0,  1.0 / 18.0,
  1.0 / 36.0, 1.0 / 18.0, 1.0 / 36.0,
  1.0 / 36.0, 1.0 / 36.0, 1.0 / 18.0,  1.0 / 36.0,  1.0 / 36.0 };

#elif (LB_DIR == 9) && (DIMENSIONS == 2)

static const int VELOCITIES[9][2] = { { -1, -1 }, { 0, -1 }, { 1, -1 },
                                      { -1, 0  }, { 0, 0  }, { 1, 0  },
                                      { -1, 1  }, { 0, 1  }, { 1, 1  } };

static const double WEIGHTS[9] = { 1.0 / 36.0, 1.0 / 9.0,  1.0 / 36.0,
                                   1.0 / 9.0,  4.0 / 9.0,  1.0 / 9.0,
                                   1.0 / 36.0, 1.0 / 9.0,  1.0 / 36.0 };
#else
  #error "Not implemented yet!"
#endif
#endif // _PEANO_APPLICATIONS_LATTICEBOLTZMANN_OPTIMISED_OPTLBDEFINITIONS_H_
