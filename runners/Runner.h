// This file is part of the Peano project. For conditions of distribution and
// use, please see the copyright notice at www.peano-framework.org
#ifndef _LATTICEBOLTZMANN_RUNNERS_RUNNER_H_
#define _LATTICEBOLTZMANN_RUNNERS_RUNNER_H_

namespace latticeboltzmann {
namespace runners {
class Runner;
}

namespace repositories {
class Repository;
}
}

/**
 * Runner
 *
 */
class latticeboltzmann::runners::Runner
{
private:
  int
  runAsMaster(latticeboltzmann::repositories::Repository& repository);

#ifdef Parallel
  int
  runAsWorker(latticeboltzmann::repositories::Repository& repository);

#endif

public:
  Runner();
  virtual
  ~Runner();

  /**
   * Run
   */
  int
  run();
};

#endif
