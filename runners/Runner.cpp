#include "latticeboltzmann/runners/Runner.h"

#include "latticeboltzmann/repositories/Repository.h"
#include "latticeboltzmann/repositories/RepositoryFactory.h"

#include "peano/utils/UserInterface.h"

#include "tarch/Assertions.h"

#include "tarch/logging/CommandLineLogger.h"
#include "tarch/parallel/Node.h"
#include "tarch/parallel/NodePool.h"

#include "latticeboltzmann/LBDefinitions.h"

// @todo Remove this include as soon as you've created your real-world geometry
#include "peano/geometry/Hexahedron.h"

#include <papi.h>
latticeboltzmann::runners::Runner::
Runner()
{
  // @todo Insert your code here
}

latticeboltzmann::runners::Runner::~Runner()
{
  // @todo Insert your code here
}

int
latticeboltzmann::runners::Runner::
run()
{
  // @todo Insert your geometry generation here and adopt the repository
  //       generation to your needs. There is a dummy implementation to allow
  //       for a quick start, but this is really very dummy (it generates
  //       solely a sphere computational domain and basically does nothing with
  //       it).

  tarch::logging::CommandLineLogger::getInstance().clearFilterList();
  tarch::logging::CommandLineLogger::getInstance().addFilterListEntry(
    ::tarch::logging::CommandLineLogger::FilterListEntry("", -1,
                                                         "peano::grid", true));

  // Start of dummy implementation
  peano::geometry::Hexahedron geometry(
    tarch::la::Vector<DIMENSIONS, double>(1.0),
    tarch::la::Vector<DIMENSIONS, double>(0.0));

  latticeboltzmann::repositories::Repository* repository =
    latticeboltzmann::repositories::RepositoryFactory::getInstance().
    createWithSTDStackImplementation(
      geometry,
      tarch::la::Vector<DIMENSIONS, double>(1.0),   // domainSize,
      tarch::la::Vector<DIMENSIONS, double>(0.0)    // computationalDomainOffset
      );
  // End of dummy implementation

  int result = 0;

  if (tarch::parallel::Node::getInstance().isGlobalMaster())
    result = runAsMaster(*repository);

#ifdef Parallel
  else
    result = runAsWorker(*repository);

#endif

  delete repository;

  return result;
}

int
latticeboltzmann::runners::Runner::
runAsMaster(latticeboltzmann::repositories::Repository& repository)
{
  peano::utils::UserInterface userInterface;
  userInterface.writeHeader();

  repository.switchToSetupExperiment(); repository.iterate();

  float     real_time, proc_time, mflops;
  long long flpins;
  PAPI_flops(&real_time, &proc_time, &flpins, &mflops);

  int       numIterations        = NUMBER_OF_ITERATIONS;
  const int iterationsMultiplier = FINE_REFINEMENT_LEVEL - COARSE_REFINEMENT_LEVEL;

  for (int i = 0; i < iterationsMultiplier; ++i)
    numIterations *= 3;

  std::cout << "num iterations" << numIterations << std::endl;

  int i = 0;

  while (i < numIterations) {
    // streaming
    repository.switchToLBMImplementation(); repository.iterate();

    repository.switchToInterpolationRestriction(); repository.iterate();

    if ((i + 1) % PLOTTING_TIMESTEP == 0) {
      // if ((i + 1) % 1 == 0) {
      std::cout << i << std::endl;
      repository.switchToPlot(); repository.iterate();
    }

    ++i;
  }

  PAPI_flops(&real_time, &proc_time, &flpins, &mflops);

  printf("MFLOPS : %f\n", mflops);

  printf("MLUPS : %f\n",  3 * 3 * CELL_BLOCK_SIZE  * numIterations / real_time / 1e6);

  repository.logIterationStatistics();
  repository.terminate();
  // End of dummy implementation

  return 0;
}
