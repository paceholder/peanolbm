#include "latticeboltzmann/Vertex.h"
#include "peano/utils/Loop.h"
#include "peano/grid/Checkpoint.h"

#ifdef Dim2
#include "latticeboltzmann/heap2D/VertexHeap.h"
#endif

#ifdef Dim3
#include "latticeboltzmann/heap3D/VertexHeap.h"
#endif

#include "peano/heap/Heap.h"

#include <tuple>


std::map<tarch::la::Vector<DIMENSIONS + 1, double>, int, tarch::la::VectorCompare<DIMENSIONS+1> > latticeboltzmann::Vertex::_vertices;

std::map<tarch::la::Vector<DIMENSIONS, double>, int, tarch::la::VectorCompare<DIMENSIONS> >
latticeboltzmann::Vertex::processedHangingVertices;

latticeboltzmann::Vertex::Vertex():
  Base() { 
  // @todo Insert your code here
}


latticeboltzmann::Vertex::Vertex(const Base::DoNotCallStandardConstructor& value):
  Base(value) { 
  // Please do not insert anything here
}


latticeboltzmann::Vertex::Vertex(const Base::PersistentVertex& argument):
  Base(argument) {
  // @todo Insert your code here
}


int
latticeboltzmann::Vertex::
getAggregateWithArbitraryCardinality() const
{
  return Base::_vertexData.getAggregateWithArbitraryCardinality();
}

void
latticeboltzmann::Vertex::
setAggregateWithArbitraryCardinality(const int cardinality)
{
  Base::_vertexData.setAggregateWithArbitraryCardinality(cardinality);
}



bool
latticeboltzmann::Vertex::
isComputed() const 
{
  return Base::_vertexData.getComputed();
}

void
latticeboltzmann::Vertex::
setComputed(const bool computed)
{
  Base::_vertexData.setComputed(computed);
}


int
latticeboltzmann::Vertex::
getVertexIndex(const tarch::la::Vector<DIMENSIONS, double>& fineGridX,
               int level)
{
//  auto tuple = std::make_tuple(fineGridX(0), fineGridX(1), level);

  tarch::la::Vector<DIMENSIONS+1, double> tuple(fineGridX(0), fineGridX(1), double(level));

//  std::map<std::tuple<double, double, int>, int>::iterator it =
  std::map<tarch::la::Vector<DIMENSIONS+1, double>, int>::iterator it =
    latticeboltzmann::Vertex::_vertices.find(tuple);

  int result = -1;

  // if we do not have allocated vertex yet
  if (it == _vertices.end()) {
    // allocation
    int index = peano::heap::Heap<VertexHeap>::getInstance().createData(2);

    // store in map
    Vertex::_vertices[tuple] = index;

    // return result
    result = index;
  } else
    // return old stored value as a result
    result = it->second;

  return result;
}



















