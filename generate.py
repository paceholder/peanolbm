#! /usr/bin/env python

import subprocess


if __name__ == "__main__":
    subprocess.call(["java",
                     "-jar",
                     "../../pdt/pdt.jar",
                     "./latticeboltzmann.peano-specification", "."],
                     shell=False)

    heap_structs = ["VertexHeap.def", "CellHeap.def"]

    for struct in heap_structs:
        subprocess.call(["java",
                         "-jar",
                         "../../pdt/lib/DaStGen.jar",
                         "--plugin",
                         "PeanoHeapSnippetGenerator",
                         "--naming",
                         "PeanoHeapNameTranslator",
                         struct,
                         "heap2D"],
                         shell=False)

    heap_structs = ["VertexHeap3D.def", "CellHeap3D.def"]

    for struct in heap_structs:
        subprocess.call(["java",
                         "-jar",
                         "../../pdt/lib/DaStGen.jar",
                         "--plugin",
                         "PeanoHeapSnippetGenerator",
                         "--naming",
                         "PeanoHeapNameTranslator",
                         struct,
                         "heap3D"],
                         shell=False)
