// This file is part of the Peano project. For conditions of distribution and
// use, please see the copyright notice at www.peano-framework.org
#ifndef _LATTICEBOLTZMANN_CELL_STEPS_H_
#define _LATTICEBOLTZMANN_CELL_STEPS_H_

#include <set>
#include <tuple>
#include <vector>

#include "latticeboltzmann/lbmsteps/Interpolation.h"
#include "latticeboltzmann/records/Cell.h"
#include "tarch/la/Vector.h"

using namespace tarch::la;

namespace peano {
namespace grid {
class VertexEnumerator;
}
}

namespace latticeboltzmann {
class CellHeap;
class Vertex;

class CellSteps
{
public:
  /** Cell is overlapping if the number of refined vertices is in ]0;3[   */
  static
  bool
  isOverlappingCell(latticeboltzmann::Vertex* const      fineGridVertices,
                    const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  /** Computes density in every LB-node in the cell */
  static
  void
  computeDensity(latticeboltzmann::CellHeap& dirs,
                 Vector<CELL_BLOCK_SIZE, double>& density);

  /** Computes velocity in every LB-node in the cell */
  static
  void
  computeVelocity(latticeboltzmann::CellHeap& dirs,
                  Vector<CELL_BLOCK_SIZE, double>& density,
                  Vector<CELL_BLOCK_SIZE, Vector<DIMENSIONS, double> >&  velocity);

  /** Computes equilibrium state distribitions for given velocity and density */
  static
  void
  computeFeq(Vector<CELL_BLOCK_SIZE, double>& density,
             Vector<CELL_BLOCK_SIZE, Vector<DIMENSIONS, double> >& velocity,
             Vector<LB_DIR, Vector<CELL_BLOCK_SIZE, double> >& feq);

  /** Applies collision state equation */
  static
  void
  computePostCollisionDistribution(std::vector<tarch::la::Vector<CELL_BLOCK_SIZE, double> >& dirs,
                                   double tau,
                                   const Vector<LB_DIR, Vector<CELL_BLOCK_SIZE, double> >& feq);

  // A group of functions below for copying vertex distributions to the cell boundary
  // Here we use the distributions pointing to the inner part of the cell
#ifdef Dim2
  static
  void
  copy2DVertex0ToCell(int vertexArrayIndex, latticeboltzmann::CellHeap& cell,
                      Vertex* const fineGridVertices,
                      const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  copy2DVertex1ToCell(int vertexArrayIndex, latticeboltzmann::CellHeap& cell,
                      Vertex* const fineGridVertices,
                      const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  copy2DVertex2ToCell(int vertexArrayIndex, latticeboltzmann::CellHeap& cell,
                      Vertex* const fineGridVertices,
                      const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  copy2DVertex3ToCell(int vertexArrayIndex, latticeboltzmann::CellHeap& cell,
                      Vertex* const fineGridVertices,
                      const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  // The group of four functions for copying all distributions from vertex to boundary
  static
  void
  copy2DFullVertex0ToCell(int                                  vertexArrayIndex,
                          latticeboltzmann::CellHeap&          cell,
                          Vertex* const                        fineGridVertices,
                          const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  copy2DFullVertex1ToCell(int                                  vertexArrayIndex,
                          latticeboltzmann::CellHeap&          cell,
                          Vertex* const                        fineGridVertices,
                          const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  copy2DFullVertex2ToCell(int                                  vertexArrayIndex,
                          latticeboltzmann::CellHeap&          cell,
                          Vertex* const                        fineGridVertices,
                          const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  copy2DFullVertex3ToCell(int                                  vertexArrayIndex,
                          latticeboltzmann::CellHeap&          cell,
                          Vertex* const                        fineGridVertices,
                          const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

#endif // Dim2

  // Copy full distributions set from vertices to the cell boundary

#ifdef Dim3
  static
  void
  copy3DFullVertex0ToCell(int                                  vertexArrayIndex,
                          latticeboltzmann::CellHeap&          cell,
                          Vertex* const                        fineGridVertices,
                          const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  copy3DFullVertex1ToCell(int                                  vertexArrayIndex,
                          latticeboltzmann::CellHeap&          cell,
                          Vertex* const                        fineGridVertices,
                          const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  copy3DFullVertex2ToCell(int                                  vertexArrayIndex,
                          latticeboltzmann::CellHeap&          cell,
                          Vertex* const                        fineGridVertices,
                          const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  copy3DFullVertex3ToCell(int                                  vertexArrayIndex,
                          latticeboltzmann::CellHeap&          cell,
                          Vertex* const                        fineGridVertices,
                          const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  copy3DFullVertex4ToCell(int                                  vertexArrayIndex,
                          latticeboltzmann::CellHeap&          cell,
                          Vertex* const                        fineGridVertices,
                          const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  copy3DFullVertex5ToCell(int                                  vertexArrayIndex,
                          latticeboltzmann::CellHeap&          cell,
                          Vertex* const                        fineGridVertices,
                          const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  copy3DFullVertex6ToCell(int                                  vertexArrayIndex,
                          latticeboltzmann::CellHeap&          cell,
                          Vertex* const                        fineGridVertices,
                          const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  copy3DFullVertex7ToCell(int                                  vertexArrayIndex,
                          latticeboltzmann::CellHeap&          cell,
                          Vertex* const                        fineGridVertices,
                          const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

#endif // Dim3

  /** Copies all distributions from vertices into the cell boundary */
  static
  void
  completeDirs(int                                  level,
               latticeboltzmann::CellHeap&          dirs,
               latticeboltzmann::Vertex* const      fineGridVertices,
               const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  // 3D version of functions copying "inner" distributions from vertex to cell boundary

#ifdef Dim3
  static
  void
  copy3DVertex0ToCell(int vertexArrayIndex, latticeboltzmann::CellHeap& cell,
                      Vertex* const fineGridVertices,
                      const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  copy3DVertex1ToCell(int vertexArrayIndex, latticeboltzmann::CellHeap& cell,
                      Vertex* const fineGridVertices,
                      const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  copy3DVertex2ToCell(int vertexArrayIndex, latticeboltzmann::CellHeap& cell,
                      Vertex* const fineGridVertices,
                      const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  copy3DVertex3ToCell(int vertexArrayIndex, latticeboltzmann::CellHeap& cell,
                      Vertex* const fineGridVertices,
                      const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  copy3DVertex4ToCell(int vertexArrayIndex, latticeboltzmann::CellHeap& cell,
                      Vertex* const fineGridVertices,
                      const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  copy3DVertex5ToCell(int vertexArrayIndex, latticeboltzmann::CellHeap& cell,
                      Vertex* const fineGridVertices,
                      const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  copy3DVertex6ToCell(int vertexArrayIndex, latticeboltzmann::CellHeap& cell,
                      Vertex* const fineGridVertices,
                      const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  copy3DVertex7ToCell(int vertexArrayIndex, latticeboltzmann::CellHeap& cell,
                      Vertex* const fineGridVertices,
                      const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

#endif // Dim3

  /** Copies INNER distributions from vertices into the cell boundary */
  static
  void
  fillCellBoundary(int                                  vertexArrayIndex,
                   latticeboltzmann::CellHeap&          cell,
                   latticeboltzmann::Vertex* const      fineGridVertices,
                   const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

#ifdef Dim2
  static
  void
  fill2DVertex0(int vertexArrayIndex, CellHeap& cell, Vertex* fineGridVertices,
                const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  fill2DVertex1(int vertexArrayIndex, CellHeap& cell, Vertex* fineGridVertices,
                const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  fill2DVertex2(int vertexArrayIndex, CellHeap& cell, Vertex* fineGridVertices,
                const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  fill2DVertex3(int vertexArrayIndex, CellHeap& cell, Vertex* fineGridVertices,
                const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

#endif

  // Functions to fill neighbouring vertices with cell's boundary "inner" distributions

#ifdef Dim3

  static
  void
  fill3DVertex0(int vertexArrayIndex, CellHeap& cell, Vertex* fineGridVertices,
                const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  fill3DVertex1(int vertexArrayIndex, CellHeap& cell, Vertex* fineGridVertices,
                const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  fill3DVertex2(int vertexArrayIndex, CellHeap& cell, Vertex* fineGridVertices,
                const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  fill3DVertex3(int vertexArrayIndex, CellHeap& cell, Vertex* fineGridVertices,
                const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  fill3DVertex4(int vertexArrayIndex, CellHeap& cell, Vertex* fineGridVertices,
                const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  fill3DVertex5(int vertexArrayIndex, CellHeap& cell, Vertex* fineGridVertices,
                const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  fill3DVertex6(int vertexArrayIndex, CellHeap& cell, Vertex* fineGridVertices,
                const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  static
  void
  fill3DVertex7(int vertexArrayIndex, CellHeap& cell, Vertex* fineGridVertices,
                const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

#endif // Dim3

  static
  void
  fillVertices(int                                  vertexArrayIndex,
               latticeboltzmann::CellHeap&          cell,
               Vertex*                              fineGridVertices,
               const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  /** Swaps localy oposite distributions, makes collision step,
   * makes streaming in (left + bottom + center) zone */
  static
  void
  swapCollisionStreamCell(latticeboltzmann::CellHeap& cell,
                          double                      tau);

  /** Streaming in the right-most nodes in the cell */
  static
  void
  streamCellRight(latticeboltzmann::CellHeap& cell);

  /** Streaming in the top-most nodes in the cell */
  static
  void
  streamCellTop(latticeboltzmann::CellHeap& cell);

#ifdef Dim3
  static
  void
  streamCellTop3D(latticeboltzmann::CellHeap& cell);

  static
  void
  streamCellRight3D(latticeboltzmann::CellHeap& cell);

  static
  void
  streamCellBack3D(latticeboltzmann::CellHeap& cell);

#endif

  /** Takes an array of vertices for certain cell and returns "edges" of hanging nodes
   * - position of left-lower corner of the cell
   * - hanging vertex 1
   * - hanging vertex 2
   * - direction horizontal or vertical (BELOW, ABOVE, LEFT, RIGHT) */
  static
  std::vector<std::tuple<tarch::la::Vector<DIMENSIONS, double>,
                         Vertex*,
                         Vertex*,
                         latticeboltzmann::Interpolation::Direction> >
  collectPairsOfFineVertices(Vertex* const                        fineGridVertices,
                             const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);

  /** Does the same as previous functinon returns just direction of "hanging" nodes */
  static std::set<latticeboltzmann::Interpolation::Direction>
  collectRestrictionPositions(Vertex* const                        fineGridVertices,
                              const peano::grid::VertexEnumerator& fineGridVerticesEnumerator);
};
}

#endif
