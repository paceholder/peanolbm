#include "Interpolation.h"

#include "CellSteps.h"
#include "latticeboltzmann/splines/Splines.h"

#include "latticeboltzmann/lbmsteps/VertexSteps.h"

namespace latticeboltzmann {
void
Interpolation::
interpolateHangingVerticesCoarseToFine(int                                  level,
                                       Vertex* const                        fineGridVertices,
                                       const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
                                       Vertex* const                        coarseGridVertices,
                                       const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator)
{
  std::vector<std::tuple<tarch::la::Vector<DIMENSIONS, double>,
                         Vertex*,
                         Vertex*,
                         latticeboltzmann::Interpolation::Direction> > pairsOfFineVertices =
    latticeboltzmann::CellSteps::collectPairsOfFineVertices(fineGridVertices, fineGridVerticesEnumerator);

  for (auto pair : pairsOfFineVertices) {
    // get information about coarse grid and vertices
    tarch::la::Vector<DIMENSIONS, double> coarseVertex1Position;

    std::tuple<Vertex*, Vertex*> coarseVertices =
      Interpolation::getPairOfCoarseVertices(std::get<3>(pair), // direction
                                             std::get<0>(pair), // fineGridX
                                             coarseGridVertices,
                                             coarseGridVerticesEnumerator,
                                             coarseVertex1Position);

    Interpolation::interpolateVertices(std::get<3>(pair),
                                       level,
                                       std::get<1>(pair),         // first vertex
                                       std::get<2>(pair),         // second vertex
                                       std::get<0>(pair),         // fineGridX
                                       coarseVertices,
                                       coarseVertex1Position);
  }
}

void
Interpolation::
interpolateVertices(Direction direction,
                    int level,
                    latticeboltzmann::Vertex* fineGridVertex1,
                    latticeboltzmann::Vertex* fineGridVertex2,
                    const tarch::la::Vector<DIMENSIONS, double>& fineGridX,
                    std::tuple<latticeboltzmann::Vertex*, latticeboltzmann::Vertex*> coarseVertices,
                    tarch::la::Vector<DIMENSIONS, double> coarseVertex1Position)
{
  // container to store interpolated data
  tarch::la::Vector<LB_DIMENSION, double> v(0.0);

  tarch::la::Vector<LB_DIR, tarch::la::Vector<LB_DIMENSION, double> > dirs(LB_DIR, v);

  // first, we do time interpolation
  double timestep = State::getTimeStep(level);

  timeInterpolation(level,
                    coarseVertices,
                    timestep, dirs, direction);

  std::vector<VertexHeap>& fineVertexData1 =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      fineGridVertex1->getAggregateWithArbitraryCardinality());

  VertexHeap& fineArray1 = fineVertexData1[(State::lastArray[level] + 1) % 2];

  std::vector<VertexHeap>& fineVertexData2 =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      fineGridVertex2->getAggregateWithArbitraryCardinality());

  VertexHeap& fineArray2 = fineVertexData2[(State::lastArray[level] + 1) % 2];

  double tauFine   = State::getTau(level);
  double tauCoarse = State::getTau(level - 1);

  rescaleFromCoarseToFine(dirs, tauFine, tauCoarse);

  // for current fine cell
  double peanoCellSize = 1.0 / pow(3.0, level - 1);
  double dyFine        = peanoCellSize / double(LB_DIMENSION - 1);

  // for coarse Cell
  double coarseCellHeight = 1.0 / pow(3.0, level - 2);
  double dyCoarse         = coarseCellHeight / double(LB_DIMENSION - 1);

  if (direction == LEFT || direction == RIGHT)
    interpolateSpaciallyVertical(fineArray1,
                                 fineArray2,
                                 dyFine, dyCoarse,
                                 fineGridX,
                                 coarseVertex1Position,
                                 dirs,
                                 direction);

  if (direction == ABOVE || direction == BELOW)
    interpolateSpaciallyHorizontal(fineArray1,
                                   fineArray2,
                                   dyFine, dyCoarse,
                                   fineGridX,
                                   coarseVertex1Position,
                                   dirs,
                                   direction);
}

// ---------------------------------------------------------------------------------------------

void
Interpolation::
rescaleFromCoarseToFine(Vector<LB_DIR, Vector<LB_DIMENSION, double> >& dirs,
                        double tauFine, double tauCoarse)
{
  // f_fine = f_eq + (f_coase - f_eq ) * 3 * tau_f / tau_c
  for (int i = 0; i < LB_DIMENSION; ++i) {
    Vector<LB_DIR, double> feq;
    PointSteps::computeFeq(dirs[SOUTH_WEST],
                           dirs[SOUTH],
                           dirs[SOUTH_EAST],
                           dirs[WEST],
                           dirs[CENTER],
                           dirs[EAST],
                           dirs[NORTH_WEST],
                           dirs[NORTH],
                           dirs[NORTH_EAST],
#ifdef Dim3
                           dirs[BOTTOM_SOUTH],
                           dirs[BOTTOM_WEST],
                           dirs[BOTTOM],
                           dirs[BOTTOM_EAST],
                           dirs[BOTTOM_NORTH],

                           dirs[TOP_SOUTH],
                           dirs[TOP_WEST],
                           dirs[TOP],
                           dirs[TOP_EAST],
                           dirs[TOP_NORTH],
#endif
                           i,
                           feq);

    // ratio for rescaling
    // double s_cf = 3. * (tauCoarse - 1.) / (tauFine - 1.);

    // rescale for each direction
    // f_fine = f_eq + (f_coase - f_eq ) * 3 * tau_f / tau_c
    for (int direction = 0; direction < LB_DIR; ++direction) {
      dirs[direction][i] = feq[direction] + (dirs[direction][i] - feq[direction]) / 3.0 * tauFine / tauCoarse;
      assert(dirs[direction][i] > 0.0);
    }

    // dirs[direction][i] = feq[direction] + (dirs[direction][i] - feq[direction]) / s_cf;
  }
}

// ---------------------------------------------------------------------------------------------

void
Interpolation::
interpolateSpaciallyVertical(
  VertexHeap& array1,
  VertexHeap& array2,
  double dyFine,
  double dyCoarse,
  const tarch::la::Vector<DIMENSIONS, double>& fineGridX,
  tarch::la::Vector<DIMENSIONS, double>& coarseVertex1Position,
  tarch::la::Vector<LB_DIR, tarch::la::Vector<LB_DIMENSION, double> >& dirs,
  int direction)
{
  std::vector<int> directions({
                                SOUTH_WEST, SOUTH, SOUTH_EAST,
                                WEST, CENTER, EAST,
                                NORTH_WEST, NORTH, NORTH_EAST
                              }
                              );

  int offset = LB_DIMENSION - 1;

  for (int i = 0; i < LB_DIMENSION - 1; ++i) {
    double y = fineGridX(1) + i * dyFine;         // here we interpolate

    double k     = (y - coarseVertex1Position(1)) / dyCoarse;
    int    point = int(k);
    double ratio = k - point;

    if (point >= LB_DIMENSION - 1) {
      point -= 1;
      ratio  = 1.0;
    }

    // interpolate all directions
    for (auto it = directions.begin(); it != directions.end(); ++it)
      array1.getDir(*it)[offset + i] = splines::linearInterpolation(dirs[*it][point],
                                                                    dirs[*it][point + 1],
                                                                    ratio);
  }

  // fill zero-point
  for (auto it = directions.begin(); it != directions.end(); ++it)
    array1.getDir(*it)[0] = array1.getDir(*it)[offset];

  // ----
  // second vertex(just first element)
  int i = LB_DIMENSION - 1;

  double y = fineGridX(1) + i * dyFine;         // here we interpolate

  double k     = (y - coarseVertex1Position(1)) / dyCoarse;
  int    point = int(k);
  double ratio = k - point;

  if (point >= LB_DIMENSION - 1) {
    point -= 1;
    ratio  = 1.0;
  }

  for (auto it = directions.begin(); it != directions.end(); ++it)
    array2.getDir(*it)[0] = splines::linearInterpolation(dirs[*it][point],
                                                         dirs[*it][point + 1],
                                                         ratio);
}

// ---------------------------------------------------------------------------------------------

void
Interpolation::
interpolateSpaciallyHorizontal(VertexHeap& array1,
                               VertexHeap& array2,
                               double dxFine,
                               double dxCoarse,
                               const tarch::la::Vector<DIMENSIONS, double>& fineGridX,
                               tarch::la::Vector<DIMENSIONS, double>& coarseVertex1Position,
                               tarch::la::Vector<LB_DIR, tarch::la::Vector<LB_DIMENSION, double> >& dirs,
                               int direction)
{
  std::vector<int> directions({
                                SOUTH_WEST, SOUTH, SOUTH_EAST,
                                WEST, CENTER, EAST,
                                NORTH_WEST, NORTH, NORTH_EAST
                              }
                              );

  for (int i = 0; i < LB_DIMENSION - 1; ++i) {
    double x = fineGridX(0) + i * dxFine;           // here we interpolate

    double k     = (x - coarseVertex1Position(0)) / dxCoarse;
    int    point = int(k);
    double ratio = k - point;

    if (point >= LB_DIMENSION - 1) {
      point = LB_DIMENSION - 2;
      ratio = 1.0;
    }

    assert(point >= 0);

    for (auto it = directions.begin(); it != directions.end(); ++it) {
      array1.getDir(*it)[i] = splines::linearInterpolation(dirs[*it][point],
                                                           dirs[*it][point + 1],
                                                           ratio);

      assert(ratio >= 0.0);
      assert(ratio <= 1.0);
      assert(array1.getDir(*it)[i] > 0.0);
      assert(array1.getDir(*it)[i] < 1.0);
    }
  }

  // ----
  // second vertex(just first element)
  int i = LB_DIMENSION - 1;

  double x = fineGridX(0) + i * dxFine;

  double k     = (x - coarseVertex1Position(0)) / dxCoarse;
  int    point = int(k);
  double ratio = k - point;

  if (point >= LB_DIMENSION - 1) {
    point = LB_DIMENSION - 2;
    ratio = 1.0;
  }

  for (auto it = directions.begin(); it != directions.end(); ++it)
    array2.getDir(*it)[0] = splines::linearInterpolation(dirs[*it][point],
                                                         dirs[*it][point + 1],
                                                         ratio);
}

// ---------------------------------------------------------------------------------------------

void
Interpolation::
timeInterpolation(int level,
                  std::tuple<latticeboltzmann::Vertex*, latticeboltzmann::Vertex*> coarseVertices,
                  double timestep,
                  tarch::la::Vector<LB_DIR, tarch::la::Vector<LB_DIMENSION, double> >& dirs,
                  Direction                          direction)
{
  // indices for arrays for two times
  // important: we use here coarser level, i.e. level - 1
  int vertexArrayLastIndex    = (State::lastArray[level - 1] + 1) % 2;
  int vertexArrayCurrentIndex = State::lastArray[level - 1];

  // take heap data structure
  std::vector<VertexHeap>& vertexData1 =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      std::get<0>(coarseVertices)->getAggregateWithArbitraryCardinality());

  std::vector<VertexHeap>& vertexData2 =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      std::get<1>(coarseVertices)->getAggregateWithArbitraryCardinality());

  // first time
  tarch::la::Vector<LB_DIR, tarch::la::Vector<LB_DIMENSION, double> > dirsT0;
  {
    VertexHeap& array1 = vertexData1[vertexArrayLastIndex];
    VertexHeap& array2 = vertexData2[vertexArrayLastIndex];

    if (direction == LEFT || direction == RIGHT)
      stitchVerticalArrays(array1, array2, dirsT0);

    if (direction == ABOVE || direction == BELOW)
      stitchHorizontalArrays(array1, array2, dirsT0);
  }

  // second time
  tarch::la::Vector<LB_DIR, tarch::la::Vector<LB_DIMENSION, double> > dirsT1;
  {
    VertexHeap& array1 = vertexData1[vertexArrayCurrentIndex];
    VertexHeap& array2 = vertexData2[vertexArrayCurrentIndex];

    if (direction == LEFT || direction == RIGHT)
      stitchVerticalArrays(array1, array2, dirsT1);

    if (direction == ABOVE || direction == BELOW)
      stitchHorizontalArrays(array1, array2, dirsT1);
  }

  // linear time interpolation =)
  for (int i = 0; i < LB_DIR; ++i)
    for (int j = 0; j < LB_DIMENSION; ++j) {
      dirs[i][j] = (1.0 - timestep) * dirsT0[i][j] + timestep * dirsT1[i][j];
      assert(dirs[i][j] > 0.0);
    }

}

// ---------------------------------------------------------------------------------------------

void
Interpolation::
stitchVerticalArrays(VertexHeap& array1,
                     VertexHeap& array2,
                     Vector<LB_DIR, Vector<LB_DIMENSION, double> >& dirs)
{
  for (int direction = 0; direction < LB_DIR; ++direction)
    dirs[direction][0] = array1.getDir(direction)[0];

  const int offset = LB_DIMENSION - 1;

  for (int i = 1; i < LB_DIMENSION - 1; ++i)
    for (int direction = 0; direction < LB_DIR; ++direction)
      dirs[direction][i] = array1.getDir(direction)[offset + i];



  for (int direction = 0; direction < LB_DIR; ++direction)
    dirs[direction][LB_DIMENSION - 1] = array2.getDir(direction)[0];
}

// ---------------------------------------------------------------------------------------------

void
Interpolation::
stitchHorizontalArrays(VertexHeap& array1,
                       VertexHeap& array2,
                       tarch::la::Vector<LB_DIR, tarch::la::Vector<LB_DIMENSION, double> >& dirs)
{
  // part of horizontal array of vertex1
  //   *--*--*--* - 0
  for (int i = 0; i < LB_DIMENSION - 1; ++i)
    for (int direction = 0; direction < LB_DIR; ++direction)
      dirs[direction][i] = array1.getDir(direction)[i];

  // last point

  // 0 - 0 - 0 - 0--*
  for (int direction = 0; direction < LB_DIR; ++direction)
    dirs[direction][LB_DIMENSION - 1] = array2.getDir(direction)[0];
}

// ---------------------------------------------------------------------------------------------

std::tuple<latticeboltzmann::Vertex*, latticeboltzmann::Vertex*>
Interpolation::
getPairOfCoarseVertices(Interpolation::Direction direction,
                        const tarch::la::Vector<DIMENSIONS, double>& fineGridX,
                        latticeboltzmann::Vertex* const      coarseGridVertices,
                        const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
                        tarch::la::Vector<DIMENSIONS, double>& vertex1Position)
{
  latticeboltzmann::Vertex* vertex1;
  latticeboltzmann::Vertex* vertex2;

  if (direction == ABOVE) {
    vertex1         =  &(coarseGridVertices[coarseGridVerticesEnumerator(2)]);
    vertex1Position = coarseGridVerticesEnumerator.getVertexPosition(2);
    vertex2         =  &(coarseGridVertices[coarseGridVerticesEnumerator(3)]);
  }

  if (direction == BELOW) {
    vertex1         =  &coarseGridVertices[coarseGridVerticesEnumerator(0)];
    vertex1Position = coarseGridVerticesEnumerator.getVertexPosition(0);
    vertex2         =  &coarseGridVertices[coarseGridVerticesEnumerator(1)];
  }

  if (direction == RIGHT) {
    vertex1         =  &coarseGridVertices[coarseGridVerticesEnumerator(1)];
    vertex1Position = coarseGridVerticesEnumerator.getVertexPosition(1);
    vertex2         =  &coarseGridVertices[coarseGridVerticesEnumerator(3)];
  }

  if (direction == LEFT) {
    vertex1         =  &coarseGridVertices[coarseGridVerticesEnumerator(0)];
    vertex1Position = coarseGridVerticesEnumerator.getVertexPosition(0);
    vertex2         =  &coarseGridVertices[coarseGridVerticesEnumerator(2)];
  }

  return std::make_tuple(vertex1, vertex2);
}
}
