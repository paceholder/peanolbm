#ifndef _LATTICEBOLTZMANN_RESTRICTION_H_
#define _LATTICEBOLTZMANN_RESTRICTION_H_

#include "latticeboltzmann/records/Vertex.h"
#include "peano/grid/Vertex.h"
#include "peano/grid/VertexEnumerator.h"
#include "peano/utils/Globals.h"

#ifdef Dim2
  #include "latticeboltzmann/heap2D/CellHeap.h"
  #include "latticeboltzmann/heap2D/VertexHeap.h"
#endif

#ifdef Dim3
  #include "latticeboltzmann/heap3D/CellHeap.h"
  #include "latticeboltzmann/heap3D/VertexHeap.h"
#endif

#include "latticeboltzmann/State.h"

#include "peano/grid/VertexEnumerator.h"
#include "peano/heap/Heap.h"

#include "latticeboltzmann/lbmsteps/PointSteps.h"

#include "latticeboltzmann/lbmsteps/Interpolation.h"

#include <vector>

using namespace tarch::la;

namespace latticeboltzmann {
class Vertex;

class Restriction
{
public:
  // "Starting" function for restriction. It is called from the fine grid
  static
  void
  restrictHangingVerticesCoarseToFine(int                                  level,
                                      Vertex* const                        fineGridVertices,
                                      const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
                                      latticeboltzmann::Cell&              fineGridCell,
                                      Vertex* const                        coarseGridVertices,
                                      const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
                                      latticeboltzmann::Cell&              coarseGridCell);

  static
  void
  restrictVertices(
    int                                                  fineLevel,
    std::set<latticeboltzmann::Interpolation::Direction> restrictionPositions,
    Vertex* const                                        fineGridVertices,
    const peano::grid::VertexEnumerator&                 fineGridVerticesEnumerator,
    latticeboltzmann::Cell&                              fineGridCell,
    Vertex* const                                        coarseGridVertices,
    const peano::grid::VertexEnumerator&                 coarseGridVerticesEnumerator,
    latticeboltzmann::Cell&                              coarseGridCell);

  /** takes two horizonal or vertical parts of arrays of length LB_DIMENSION-1 and 1
   * and stitches them to a vector of lenght LB_DIMENSION */
  static
  void
  stitchArrays(int level,
               latticeboltzmann::Vertex* fineVertex1,
               latticeboltzmann::Vertex* fineVertex2,
               tarch::la::Vector<LB_DIR, tarch::la::Vector<LB_DIMENSION, double> >& stitchedDirs,
               Interpolation::Direction                          direction);

  /** If restriction node coinsides with coarse cell vertical edge */
  static
  void
  restrictEdgeVertical(int       fineLevel,
                       Vertex&   fineVertex,
                       int       restrictionRow,
                       CellHeap& coarseCellArray,
                       Vertex&   coarseVertex,
                       int       xc,
                       int       yc);

  /** If restriction node coinsides with coarse cell horizontal edge */
  static
  void
  restrictEdgeHorizontal(int       fineLevel,
                         Vertex&   fineVertex,
                         int       restrictionColumn,
                         CellHeap& coarseCellArray,
                         Vertex&   coarseVertex,
                         int       xc,
                         int       yc);

  /** Restrict all the distributions from the central part of the fine grid */
  static
  void
  restrictCenter(int fineLevel,
                 latticeboltzmann::CellHeap& fineCellData,
                 Vector<DIMENSIONS, double> finePos,
                 latticeboltzmann::CellHeap& coarseCellData,
                 Vector<DIMENSIONS, double> coarsePos);

  /** distributions rescaling according to the equation from the article */
  static
  void
  rescaleFromFineToCoarse(Vector<LB_DIR, Vector<LB_DIMENSION, double> >& dirs,
                          double tauFine, double tauCoarse);
};
}
#endif // RESTRICTION_H
