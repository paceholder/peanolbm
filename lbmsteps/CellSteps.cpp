#include "CellSteps.h"

#include "peano/utils/Globals.h"

#include "peano/grid/VertexEnumerator.h"
#include "peano/heap/Heap.h"

#include "latticeboltzmann/State.h"
#include "latticeboltzmann/Vertex.h"

#ifdef Dim2
  #include "latticeboltzmann/heap2D/CellHeap.h"
  #include "latticeboltzmann/heap2D/VertexHeap.h"
#endif

#ifdef Dim3
  #include "latticeboltzmann/heap3D/CellHeap.h"
  #include "latticeboltzmann/heap3D/VertexHeap.h"
#endif

#include "latticeboltzmann/lbmsteps/PointSteps.h"

#ifdef ISPC_BUILD
  #include "latticeboltzmann/ispc_sources/CellSteps_ispc.h"
#endif

#ifdef Dim2
  #define DOT(a, b) (a[0] * b[0] + a[1] * b[1])
#endif
#ifdef Dim3
  #define DOT(a, b) (a[0] * b[0] + a[1] * b[1] + a[2] * b[2])
#endif

namespace latticeboltzmann {
bool
CellSteps::
isOverlappingCell(latticeboltzmann::Vertex* const      fineGridVertices,
                  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  int refinedVertices = 0;
#ifdef Dim2

  for (int i = 0; i < TWO_POWER_D; ++i) {
    Vertex& v = fineGridVertices[fineGridVerticesEnumerator(i)];

    if (v.getRefinementControl() == latticeboltzmann::records::Vertex::Refined)
      ++refinedVertices;
  }

#endif

#ifdef Dim3
  assert(false);
#endif
  //

  return refinedVertices > 0 && refinedVertices < TWO_POWER_D;
}

void
CellSteps::
computeDensity(latticeboltzmann::CellHeap& dirs, Vector<CELL_BLOCK_SIZE, double>& density)
{
  for (int index = 0; index < CELL_BLOCK_SIZE; ++index) {
    density[index] = 0.0;

    for (int k = 0; k < LB_DIR; ++k)
      density[index] += dirs.getDir(k)[index];
  }
}

void
CellSteps::
computeVelocity(latticeboltzmann::CellHeap& dirs,
                Vector<CELL_BLOCK_SIZE, double>& density,
                Vector<CELL_BLOCK_SIZE, Vector<DIMENSIONS, double> >&  velocity)
{
  for (int index = 0; index < CELL_BLOCK_SIZE; ++index) {
#ifdef Dim2
    velocity[index][0] = -dirs.getDir(SOUTH_WEST)[index]
                         + dirs.getDir(SOUTH_EAST)[index]
                         - dirs.getDir(WEST)[index]
                         + dirs.getDir(EAST)[index]
                         - dirs.getDir(NORTH_WEST)[index]
                         + dirs.getDir(NORTH_EAST)[index];

    velocity[index][1] = -dirs.getDir(SOUTH_WEST)[index]
                         - dirs.getDir(SOUTH)[index]
                         - dirs.getDir(SOUTH_EAST)[index]
                         + dirs.getDir(NORTH_WEST)[index]
                         + dirs.getDir(NORTH)[index]
                         + dirs.getDir(NORTH_EAST)[index];
#endif

#ifdef Dim3
    velocity[index][0] = -dirs.getDir(BOTTOM_WEST)[index]
                         + dirs.getDir(BOTTOM_EAST)[index]
                         - dirs.getDir(SOUTH_WEST)[index]
                         + dirs.getDir(SOUTH_EAST)[index]
                         - dirs.getDir(WEST)[index]
                         + dirs.getDir(EAST)[index]
                         - dirs.getDir(NORTH_WEST)[index]
                         + dirs.getDir(NORTH_EAST)[index]
                         - dirs.getDir(TOP_WEST)[index]
                         + dirs.getDir(TOP_EAST)[index];

    velocity[index][1] = -dirs.getDir(BOTTOM_SOUTH)[index]
                         + dirs.getDir(BOTTOM_NORTH)[index]
                         - dirs.getDir(SOUTH_WEST)[index]
                         - dirs.getDir(SOUTH)[index]
                         - dirs.getDir(SOUTH_EAST)[index]
                         + dirs.getDir(NORTH_WEST)[index]
                         + dirs.getDir(NORTH)[index]
                         + dirs.getDir(NORTH_EAST)[index]
                         - dirs.getDir(TOP_SOUTH)[index]
                         + dirs.getDir(TOP_NORTH)[index];

    velocity[index][2] = -dirs.getDir(BOTTOM_SOUTH)[index]
                         - dirs.getDir(BOTTOM_EAST)[index]
                         - dirs.getDir(BOTTOM)[index]
                         - dirs.getDir(BOTTOM_WEST)[index]
                         - dirs.getDir(BOTTOM_NORTH)[index]
                         + dirs.getDir(TOP_SOUTH)[index]
                         + dirs.getDir(TOP_EAST)[index]
                         + dirs.getDir(TOP)[index]
                         + dirs.getDir(TOP_WEST)[index]
                         + dirs.getDir(TOP_NORTH)[index];
#endif

    velocity[index][0] /= density[index];
    velocity[index][1] /= density[index];
#ifdef Dim3
    velocity[index][2] /= density[index];
#endif
  }
}

void
CellSteps::
computeFeq(Vector<CELL_BLOCK_SIZE, double>& density,
           Vector<CELL_BLOCK_SIZE, Vector<DIMENSIONS, double> >& velocity,
           Vector<LB_DIR, Vector<CELL_BLOCK_SIZE, double> >& feq)
{
  const double CS_2     = 1.0 / 3.0;
  const double CS_4_x_2 = 2 * CS_2 * CS_2;
  const double CS_2_x_2 = 2 * CS_2;

  for (int index = 0; index < CELL_BLOCK_SIZE; ++index) {
    double CU_SW = DOT(velocity[index], VELOCITIES[SOUTH_WEST]);
    double CU_S  = DOT(velocity[index], VELOCITIES[SOUTH]);
    double CU_SE = DOT(velocity[index], VELOCITIES[SOUTH_EAST]);
    double CU_W  = DOT(velocity[index], VELOCITIES[WEST]);

#ifdef Dim3
    double CU_BS = DOT(velocity[index], VELOCITIES[BOTTOM_SOUTH]);
    double CU_BE = DOT(velocity[index], VELOCITIES[BOTTOM_EAST]);
    double CU_B  = DOT(velocity[index], VELOCITIES[BOTTOM]);
    double CU_BW = DOT(velocity[index], VELOCITIES[BOTTOM_WEST]);
    double CU_BN = DOT(velocity[index], VELOCITIES[BOTTOM_NORTH]);
#endif

    double UU = 1.0 - DOT(velocity[index], velocity[index]) / CS_2_x_2;

#define FEQ(a) ((a) / CS_2 + (a) * (a) / CS_4_x_2 + UU)

    double d = density[index];

    feq[SOUTH_WEST][index] = WEIGHTS[SOUTH_WEST]* d* FEQ(CU_SW);

    feq[SOUTH][index] = WEIGHTS[SOUTH]* d* FEQ(CU_S);

    feq[SOUTH_EAST][index] = WEIGHTS[SOUTH_EAST]* d* FEQ(CU_SE);

    feq[WEST][index] = WEIGHTS[WEST]* d* FEQ(CU_W);

    feq[CENTER][index] = WEIGHTS[CENTER] * d * (UU);

    feq[EAST][index] = WEIGHTS[EAST]* d* FEQ(-CU_W);

    feq[NORTH_WEST][index] = WEIGHTS[NORTH_WEST]* d* FEQ(-CU_SE);

    feq[NORTH][index] = WEIGHTS[NORTH]* d* FEQ(-CU_S);

    feq[NORTH_EAST][index] = WEIGHTS[NORTH_EAST]* d* FEQ(-CU_SW);

#ifdef Dim3
    feq[BOTTOM_SOUTH][index] = WEIGHTS[BOTTOM_SOUTH]* d* FEQ(CU_BS);

    feq[BOTTOM_EAST][index] = WEIGHTS[BOTTOM_EAST]* d* FEQ(CU_BE);

    feq[BOTTOM][index] = WEIGHTS[BOTTOM]* d* FEQ(CU_B);

    feq[BOTTOM_WEST][index] = WEIGHTS[BOTTOM_WEST]* d* FEQ(CU_BW);

    feq[BOTTOM_NORTH][index] = WEIGHTS[BOTTOM_NORTH]* d* FEQ(CU_BN);

    feq[TOP_SOUTH][index] = WEIGHTS[TOP_SOUTH]* d* FEQ(-CU_BN);

    feq[TOP_EAST][index] = WEIGHTS[TOP_EAST]* d* FEQ(-CU_BW);

    feq[TOP][index] = WEIGHTS[TOP]* d* FEQ(-CU_B);

    feq[TOP_WEST][index] = WEIGHTS[TOP_WEST]* d* FEQ(-CU_BE);

    feq[TOP_NORTH][index] = WEIGHTS[TOP_NORTH]* d* FEQ(-CU_BS);

#endif
  }
}

void
CellSteps::
computePostCollisionDistribution(
  std::vector<tarch::la::Vector<CELL_BLOCK_SIZE, double> >& dirs,
  double tau,
  const Vector<LB_DIR, Vector<CELL_BLOCK_SIZE, double> >& feq)
{
  const double tauRecp = 1.0 / tau;

  for (int i = 0; i < LB_DIMENSION * LB_DIMENSION; ++i)
    for (int l = 0; l < LB_DIR; ++l)
      dirs[l][i] -= tauRecp * (dirs[l][i] - feq[l][i]);

}

// helping function which goes through all chosen direction
// and copies data from vertex to cell heap structure
inline
void
copyPointFromVertexToCell(std::vector<int>& directions,
                          VertexHeap&       vertex,
                          int               indexFrom,
                          CellHeap&         cell,
                          int               indexTo)
{
  for (auto it = directions.begin(); it != directions.end(); ++it) {
    cell.getDir(*it)[indexTo] = vertex.getDir(*it)[indexFrom];
    assert(cell.getDir(*it)[indexTo] > 0.0);
    assert(cell.getDir(*it)[indexTo] < 1.0);
  }
}

// same as previous function but acts in opposite direction
inline
void
copyPointFromCellToVertex(std::vector<int>& directions,
                          CellHeap&         cell,
                          int               indexFrom,
                          VertexHeap&       vertex,
                          int               indexTo)
{
  for (auto it = directions.begin(); it != directions.end(); ++it)
    vertex.getDir(*it)[indexTo] = cell.getDir(*it)[indexFrom];
}

#ifdef Dim2

void
CellSteps::
copy2DVertex0ToCell(int                                  vertexArrayIndex,
                    latticeboltzmann::CellHeap&          cell,
                    Vertex* const                        fineGridVertices,
                    const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vector<DIMENSIONS, int> index(0, 0);

  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(index)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // origin point
  {
    std::vector<int> directionsToCopy({ NORTH, CENTER, NORTH_EAST, EAST }
                                      );

    copyPointFromVertexToCell(directionsToCopy, array, 0, cell, 0);
  }

  // bottom
  {
    std::vector<int> directionsToCopy({ WEST, CENTER, EAST, NORTH_WEST, NORTH, NORTH_EAST }
                                      );

    for (int i = 1; i < LB_DIMENSION - 1; ++i)
      copyPointFromVertexToCell(directionsToCopy, array, i, cell, i);
  }

  // left
  {
    std::vector<int> directionsToCopy({ SOUTH, SOUTH_EAST, CENTER, EAST, NORTH_EAST, NORTH }
                                      );

    for (int j = 1; j < LB_DIMENSION - 1; ++j) {
      int indexFrom = LB_DIMENSION - 1 + j;
      int indexTo   = LB_DIMENSION * j;
      copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
    }
  }
}

void
CellSteps::
copy2DVertex1ToCell(int                                  vertexArrayIndex,
                    latticeboltzmann::CellHeap&          cell,
                    Vertex* const                        fineGridVertices,
                    const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vector<DIMENSIONS, int> index(1, 0);

  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(index)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // origin point
  {
    std::vector<int> directionsToCopy({ WEST, CENTER, NORTH_WEST, NORTH }
                                      );

    int indexTo = LB_DIMENSION - 1;
    copyPointFromVertexToCell(directionsToCopy, array, 0, cell, indexTo);
  }
  // right edge
  {
    std::vector<int> directionsToCopy({ SOUTH, SOUTH_WEST, WEST, CENTER, NORTH_WEST, NORTH }
                                      );

    for (int j = 1; j < LB_DIMENSION - 1; ++j) {
      int indexFrom = LB_DIMENSION - 1 + j;
      int indexTo   = LB_DIMENSION * j + LB_DIMENSION - 1;
      copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
    }
  }
}

void
CellSteps::
copy2DVertex2ToCell(int                                  vertexArrayIndex,
                    latticeboltzmann::CellHeap&          cell,
                    Vertex* const                        fineGridVertices,
                    const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vector<DIMENSIONS, int> index(0, 1);

  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(index)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // origin point
  {
    std::vector<int> directionsToCopy({ SOUTH, SOUTH_EAST, CENTER, EAST }
                                      );

    int indexTo = LB_DIMENSION * (LB_DIMENSION - 1);
    copyPointFromVertexToCell(directionsToCopy, array, 0, cell, indexTo);
  }

  // top
  {
    std::vector<int> directionsToCopy({ SOUTH_WEST, SOUTH, SOUTH_EAST, WEST, CENTER, EAST }
                                      );

    for (int i = 1; i < LB_DIMENSION - 1; ++i) {
      int indexFrom = i;
      int indexTo   = LB_DIMENSION * (LB_DIMENSION - 1) + i;
      copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
    }
  }
}

void
CellSteps::
copy2DVertex3ToCell(int                                  vertexArrayIndex,
                    latticeboltzmann::CellHeap&          cell,
                    Vertex* const                        fineGridVertices,
                    const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vector<DIMENSIONS, int> index(1, 1);

  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(index)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // origin point
  {
    std::vector<int> directionsToCopy({ SOUTH_WEST, SOUTH, WEST, CENTER }
                                      );

    int indexTo = LB_DIMENSION * LB_DIMENSION - 1;
    copyPointFromVertexToCell(directionsToCopy, array, 0, cell, indexTo);
  }
}

void
CellSteps::
fillCellBoundary(int                                  vertexArrayIndex,
                 latticeboltzmann::CellHeap&          cell,
                 Vertex* const                        fineGridVertices,
                 const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  copy2DVertex0ToCell(vertexArrayIndex,
                      cell,
                      fineGridVertices,
                      fineGridVerticesEnumerator);

  copy2DVertex1ToCell(vertexArrayIndex,
                      cell,
                      fineGridVertices,
                      fineGridVerticesEnumerator);

  copy2DVertex2ToCell(vertexArrayIndex,
                      cell,
                      fineGridVertices,
                      fineGridVerticesEnumerator);

  copy2DVertex3ToCell(vertexArrayIndex,
                      cell,
                      fineGridVertices,
                      fineGridVerticesEnumerator);
}

#endif

#ifdef Dim2

void
CellSteps::
copy2DFullVertex0ToCell(int                                  vertexArrayIndex,
                        latticeboltzmann::CellHeap&          cell,
                        Vertex* const                        fineGridVertices,
                        const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  // take vertex
  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(0)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // horizontal part
  for (int i = 0; i < LB_DIMENSION - 1; ++i)
    for (int direction = 0; direction < LB_DIR; ++direction)
      cell.getDir(direction)[i] = array.getDir(direction)[i];

  int offset = LB_DIMENSION - 1;

  // vertical part
  for (int j = 1; j < LB_DIMENSION - 1; ++j) {
    int index = LB_DIMENSION * j;

    for (int direction = 0; direction < LB_DIR; ++direction)
      cell.getDir(direction)[index] = array.getDir(direction)[offset + j];
  }
}

void
CellSteps::
copy2DFullVertex1ToCell(int                                  vertexArrayIndex,
                        latticeboltzmann::CellHeap&          cell,
                        Vertex* const                        fineGridVertices,
                        const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(1)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  for (int direction = 0; direction < LB_DIR; ++direction)
    cell.getDir(direction)[LB_DIMENSION - 1] = array.getDir(direction)[0];

  const int offset = LB_DIMENSION - 1;

  for (int j = 1; j < LB_DIMENSION - 1; ++j) {
    int index = LB_DIMENSION * j + LB_DIMENSION - 1;

    for (int direction = 0; direction < LB_DIR; ++direction)
      cell.getDir(direction)[index] = array.getDir(direction)[offset + j];
  }
}

void
CellSteps::
copy2DFullVertex2ToCell(int                                  vertexArrayIndex,
                        latticeboltzmann::CellHeap&          cell,
                        Vertex* const                        fineGridVertices,
                        const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(2)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  const int index = LB_DIMENSION * (LB_DIMENSION - 1);

  for (int i = 0; i < LB_DIMENSION - 1; ++i)
    for (int direction = 0; direction < LB_DIR; ++direction)
      cell.getDir(direction)[index + i] = array.getDir(direction)[i];

}

void
CellSteps::
copy2DFullVertex3ToCell(int                                  vertexArrayIndex,
                        latticeboltzmann::CellHeap&          cell,
                        Vertex* const                        fineGridVertices,
                        const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(3)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  int offset = LB_DIMENSION * LB_DIMENSION - 1;

  for (int direction = 0; direction < LB_DIR; ++direction)
    cell.getDir(direction)[offset] = array.getDir(direction)[0];
}

void
CellSteps::
completeDirs(int                                  vertexArrayIndex,
             latticeboltzmann::CellHeap&          dirs,
             Vertex* const                        fineGridVertices,
             const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  copy2DFullVertex0ToCell(vertexArrayIndex, dirs, fineGridVertices, fineGridVerticesEnumerator);
  copy2DFullVertex1ToCell(vertexArrayIndex, dirs, fineGridVertices, fineGridVerticesEnumerator);
  copy2DFullVertex2ToCell(vertexArrayIndex, dirs, fineGridVertices, fineGridVerticesEnumerator);
  copy2DFullVertex3ToCell(vertexArrayIndex, dirs, fineGridVertices, fineGridVerticesEnumerator);
}
#endif // 2D

#ifdef Dim3

void
CellSteps::
copy3DFullVertex0ToCell(int                                  vertexArrayIndex,
                        latticeboltzmann::CellHeap&          cell,
                        Vertex* const                        fineGridVertices,
                        const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(0)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData = peano::heap::Heap<VertexHeap>::getInstance().getData(
    v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  std::vector<int> directions({ BOTTOM_SOUTH, BOTTOM_WEST, BOTTOM, BOTTOM_EAST, BOTTOM_NORTH,
                                SOUTH_WEST, SOUTH, SOUTH_EAST, WEST, CENTER, EAST, NORTH_WEST, NORTH, NORTH_EAST,
                                TOP_SOUTH, TOP_WEST, TOP, TOP_EAST, TOP_NORTH }
                              );

  // bottom
  for (int j = 0; j < LB_DIMENSION - 1; ++j)
    for (int i = 0; i < LB_DIMENSION - 1; ++i) {
      int indexFrom = (LB_DIMENSION - 1) * j + i;
      int indexTo   = LB_DIMENSION  * j + i;
      copyPointFromVertexToCell(directions,  array, indexFrom, cell, indexTo);
    }

  // left

  for (int k = 1; k < LB_DIMENSION - 1; ++k)
    for (int j = 0; j < LB_DIMENSION - 1; ++j) {
      int indexFrom = (LB_DIMENSION - 1) * (LB_DIMENSION - 1) + (LB_DIMENSION - 1) * k + j;
      int indexTo   = LB_DIMENSION * LB_DIMENSION  * k +  LB_DIMENSION * j;
      copyPointFromVertexToCell(directions,  array, indexFrom, cell, indexTo);
    }

  // front

  for (int k = 1; k < LB_DIMENSION - 1; ++k)
    for (int i = 1; i < LB_DIMENSION - 1; ++i) {
      int indexFrom = 2 * (LB_DIMENSION - 1) * (LB_DIMENSION - 1) + (LB_DIMENSION - 1) * k + i;
      int indexTo   = LB_DIMENSION * LB_DIMENSION  * k + i;
      copyPointFromVertexToCell(directions,  array, indexFrom, cell, indexTo);
    }

}

void
CellSteps::
copy3DFullVertex1ToCell(int                                  vertexArrayIndex,
                        latticeboltzmann::CellHeap&          cell,
                        Vertex* const                        fineGridVertices,
                        const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(1)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData = peano::heap::Heap<VertexHeap>::getInstance().getData(
    v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // full set of directions
  std::vector<int> directions({ BOTTOM_SOUTH, BOTTOM_WEST, BOTTOM, BOTTOM_EAST, BOTTOM_NORTH,
                                SOUTH_WEST, SOUTH, SOUTH_EAST, WEST, CENTER, EAST, NORTH_WEST, NORTH, NORTH_EAST,
                                TOP_SOUTH, TOP_WEST, TOP, TOP_EAST, TOP_NORTH }
                              );

  // right bottom edge
  for (int j = 0; j < LB_DIMENSION - 1; ++j) {
    int indexFrom = (LB_DIMENSION - 1) * j;
    int indexTo   = LB_DIMENSION  * j + LB_DIMENSION - 1;
    copyPointFromVertexToCell(directions,  array, indexFrom, cell, indexTo);
  }

  // right

  for (int k = 1; k < LB_DIMENSION - 1; ++k)
    for (int j = 0; j < LB_DIMENSION - 1; ++j) {
      int indexFrom = (LB_DIMENSION - 1) * (LB_DIMENSION - 1) + (LB_DIMENSION - 1) * k + j;
      int indexTo   = LB_DIMENSION * LB_DIMENSION * k + LB_DIMENSION * j + LB_DIMENSION - 1;
      copyPointFromVertexToCell(directions,  array, indexFrom, cell, indexTo);
    }

}

void
CellSteps::
copy3DFullVertex2ToCell(int                                  vertexArrayIndex,
                        latticeboltzmann::CellHeap&          cell,
                        Vertex* const                        fineGridVertices,
                        const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(2)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData = peano::heap::Heap<VertexHeap>::getInstance().getData(
    v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // full set of directions
  std::vector<int> directions({ BOTTOM_SOUTH, BOTTOM_WEST, BOTTOM, BOTTOM_EAST, BOTTOM_NORTH,
                                SOUTH_WEST, SOUTH, SOUTH_EAST, WEST, CENTER, EAST, NORTH_WEST, NORTH, NORTH_EAST,
                                TOP_SOUTH, TOP_WEST, TOP, TOP_EAST, TOP_NORTH }
                              );

  // back bottom edge
  for (int i = 0; i < LB_DIMENSION - 1; ++i) {
    int indexFrom = i;
    int indexTo   = LB_DIMENSION  * (LB_DIMENSION - 1) + i;
    copyPointFromVertexToCell(directions,  array, indexFrom, cell, indexTo);
  }

  // left back edge
  for (int k = 1; k < LB_DIMENSION - 1; ++k) {
    int indexFrom = (LB_DIMENSION - 1) * (LB_DIMENSION - 1) + k * (LB_DIMENSION - 1);
    int indexTo   = LB_DIMENSION * LB_DIMENSION  * k  + LB_DIMENSION *  (LB_DIMENSION - 1);
    copyPointFromVertexToCell(directions,  array, indexFrom, cell, indexTo);
  }

  // back

  for (int k = 1; k < LB_DIMENSION - 1; ++k)
    for (int i = 1; i < LB_DIMENSION - 1; ++i) {
      int indexFrom = 2 * (LB_DIMENSION - 1) * (LB_DIMENSION - 1) + (LB_DIMENSION - 1) * k + i;
      int indexTo   = LB_DIMENSION * LB_DIMENSION * k + LB_DIMENSION * (LB_DIMENSION - 1)  + i;
      copyPointFromVertexToCell(directions,  array, indexFrom, cell, indexTo);
    }

}

void
CellSteps::
copy3DFullVertex3ToCell(int                                  vertexArrayIndex,
                        latticeboltzmann::CellHeap&          cell,
                        Vertex* const                        fineGridVertices,
                        const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(3)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData = peano::heap::Heap<VertexHeap>::getInstance().getData(
    v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // full set of directions
  std::vector<int> directions({ BOTTOM_SOUTH, BOTTOM_WEST, BOTTOM, BOTTOM_EAST, BOTTOM_NORTH,
                                SOUTH_WEST, SOUTH, SOUTH_EAST, WEST, CENTER, EAST, NORTH_WEST, NORTH, NORTH_EAST,
                                TOP_SOUTH, TOP_WEST, TOP, TOP_EAST, TOP_NORTH }
                              );

  // right back bottom corner
  {
    int indexFrom = 0;
    int indexTo   = LB_DIMENSION  * LB_DIMENSION - 1;
    copyPointFromVertexToCell(directions,  array, indexFrom, cell, indexTo);
  }

  // right back edge
  for (int k = 1; k < LB_DIMENSION - 1; ++k) {
    int indexFrom = (LB_DIMENSION - 1) * (LB_DIMENSION - 1) + k * (LB_DIMENSION - 1);
    int indexTo   = LB_DIMENSION * LB_DIMENSION  * k  + LB_DIMENSION *  LB_DIMENSION - 1;
    copyPointFromVertexToCell(directions,  array, indexFrom, cell, indexTo);
  }
}

void
CellSteps::
copy3DFullVertex4ToCell(int                                  vertexArrayIndex,
                        latticeboltzmann::CellHeap&          cell,
                        Vertex* const                        fineGridVertices,
                        const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(4)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData = peano::heap::Heap<VertexHeap>::getInstance().getData(
    v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // full set of directions
  std::vector<int> directions({ BOTTOM_SOUTH, BOTTOM_WEST, BOTTOM, BOTTOM_EAST, BOTTOM_NORTH,
                                SOUTH_WEST, SOUTH, SOUTH_EAST, WEST, CENTER, EAST, NORTH_WEST, NORTH, NORTH_EAST,
                                TOP_SOUTH, TOP_WEST, TOP, TOP_EAST, TOP_NORTH }
                              );

  // top
  for (int j = 0; j < LB_DIMENSION - 1; ++j)
    for (int i = 0; i < LB_DIMENSION - 1; ++i) {
      int indexFrom = j * (LB_DIMENSION - 1) + i;
      int indexTo   = LB_DIMENSION * LB_DIMENSION  * (LB_DIMENSION - 1)  + LB_DIMENSION * j + i;
      copyPointFromVertexToCell(directions,  array, indexFrom, cell, indexTo);
    }

}

void
CellSteps::
copy3DFullVertex5ToCell(int                                  vertexArrayIndex,
                        latticeboltzmann::CellHeap&          cell,
                        Vertex* const                        fineGridVertices,
                        const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(5)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData = peano::heap::Heap<VertexHeap>::getInstance().getData(
    v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // full set of directions
  std::vector<int> directions({ BOTTOM_SOUTH, BOTTOM_WEST, BOTTOM, BOTTOM_EAST, BOTTOM_NORTH,
                                SOUTH_WEST, SOUTH, SOUTH_EAST, WEST, CENTER, EAST, NORTH_WEST, NORTH, NORTH_EAST,
                                TOP_SOUTH, TOP_WEST, TOP, TOP_EAST, TOP_NORTH }
                              );

  // top right
  for (int j = 0; j < LB_DIMENSION - 1; ++j) {
    int indexFrom = j * (LB_DIMENSION - 1);
    int indexTo   = LB_DIMENSION * LB_DIMENSION  * (LB_DIMENSION - 1)  +
                    j * LB_DIMENSION + LB_DIMENSION - 1;
    copyPointFromVertexToCell(directions,  array, indexFrom, cell, indexTo);
  }
}

void
CellSteps::
copy3DFullVertex6ToCell(int                                  vertexArrayIndex,
                        latticeboltzmann::CellHeap&          cell,
                        Vertex* const                        fineGridVertices,
                        const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(6)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData = peano::heap::Heap<VertexHeap>::getInstance().getData(
    v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // full set of directions
  std::vector<int> directions({ BOTTOM_SOUTH, BOTTOM_WEST, BOTTOM, BOTTOM_EAST, BOTTOM_NORTH,
                                SOUTH_WEST, SOUTH, SOUTH_EAST, WEST, CENTER, EAST, NORTH_WEST, NORTH, NORTH_EAST,
                                TOP_SOUTH, TOP_WEST, TOP, TOP_EAST, TOP_NORTH }
                              );

  // top back edge
  for (int i = 0; i < LB_DIMENSION - 1; ++i) {
    int indexFrom = i;
    int indexTo   = LB_DIMENSION * LB_DIMENSION  * (LB_DIMENSION - 1) +
                    LB_DIMENSION * (LB_DIMENSION - 1) + i;
    copyPointFromVertexToCell(directions,  array, indexFrom, cell, indexTo);
  }
}

void
CellSteps::
copy3DFullVertex7ToCell(int                                  vertexArrayIndex,
                        latticeboltzmann::CellHeap&          cell,
                        Vertex* const                        fineGridVertices,
                        const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(7)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData = peano::heap::Heap<VertexHeap>::getInstance().getData(
    v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // full set of directions
  std::vector<int> directions({ BOTTOM_SOUTH, BOTTOM_WEST, BOTTOM, BOTTOM_EAST, BOTTOM_NORTH,
                                SOUTH_WEST, SOUTH, SOUTH_EAST, WEST, CENTER, EAST, NORTH_WEST, NORTH, NORTH_EAST,
                                TOP_SOUTH, TOP_WEST, TOP, TOP_EAST, TOP_NORTH }
                              );

  // top back edge
  int indexFrom = 0;
  int indexTo   = LB_DIMENSION * LB_DIMENSION  * LB_DIMENSION - 1;
  copyPointFromVertexToCell(directions,  array, indexFrom, cell, indexTo);
}

void
CellSteps::
completeDirs(int                                  vertexArrayIndex,
             latticeboltzmann::CellHeap&          dirs,
             Vertex* const                        fineGridVertices,
             const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  copy3DFullVertex0ToCell(vertexArrayIndex, dirs, fineGridVertices, fineGridVerticesEnumerator);
  copy3DFullVertex1ToCell(vertexArrayIndex, dirs, fineGridVertices, fineGridVerticesEnumerator);
  copy3DFullVertex2ToCell(vertexArrayIndex, dirs, fineGridVertices, fineGridVerticesEnumerator);
  copy3DFullVertex3ToCell(vertexArrayIndex, dirs, fineGridVertices, fineGridVerticesEnumerator);
  copy3DFullVertex4ToCell(vertexArrayIndex, dirs, fineGridVertices, fineGridVerticesEnumerator);
  copy3DFullVertex5ToCell(vertexArrayIndex, dirs, fineGridVertices, fineGridVerticesEnumerator);
  copy3DFullVertex6ToCell(vertexArrayIndex, dirs, fineGridVertices, fineGridVerticesEnumerator);
  copy3DFullVertex7ToCell(vertexArrayIndex, dirs, fineGridVertices, fineGridVerticesEnumerator);
}
#endif // Dim3

// ASCII-art madskillz
//
//                       /|\  Z
//           6     top    |7
//            *-----------*
//           /|          /|
//          / |   back  / |  / Y
//       4 /  |       5/  | /
//        *-----------*   |/
// left   |  2|_______|__3*    right
//        |  /        |  /
//        | / front   | /
//        |/          |/
//        *-----------*----->  X
//       0    bottom   1
//

#ifdef Dim3
void
CellSteps::
copy3DVertex0ToCell(int                                  vertexArrayIndex,
                    latticeboltzmann::CellHeap&          cell,
                    Vertex* const                        fineGridVertices,
                    const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  // vertex #0 -- left front bottom
  Vector<DIMENSIONS, int> index(0, 0, 0);

  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(index)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // origin point
  {
    std::vector<int> directionsToCopy({ CENTER, EAST, NORTH,
                                        NORTH_EAST, TOP, TOP_EAST,
                                        TOP_NORTH }
                                      );

    copyPointFromVertexToCell(directionsToCopy, array, 0, cell, 0);
  }

  // front bottom edge
  {
    std::vector<int> directionsToCopy({ WEST, CENTER, EAST,
                                        NORTH_WEST, NORTH, NORTH_EAST,
                                        TOP_WEST, TOP, TOP_EAST,
                                        TOP_NORTH }
                                      );

    for (int i = 1; i < LB_DIMENSION - 1; ++i)
      copyPointFromVertexToCell(directionsToCopy, array, i, cell, i);
  }

  // left bottom edge
  {
    std::vector<int> directionsToCopy({ SOUTH, SOUTH_EAST,
                                        CENTER, EAST,
                                        NORTH, NORTH_EAST,
                                        TOP_SOUTH, TOP, TOP_EAST,
                                        TOP_NORTH }
                                      );

    for (int j = 1; j < LB_DIMENSION - 1; ++j) {
      int indexFrom = (LB_DIMENSION - 1) * j;
      int indexTo   = LB_DIMENSION * j;

      copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
    }
  }

  // left front edge
  {
    std::vector<int> directionsToCopy({ BOTTOM, BOTTOM_EAST, BOTTOM_NORTH,
                                        CENTER, EAST, NORTH, NORTH_EAST,
                                        TOP, TOP_EAST, TOP_NORTH }
                                      );

    for (int k = 1; k < LB_DIMENSION - 1; ++k) {
      int indexFrom = (LB_DIMENSION - 1) * (LB_DIMENSION - 1) + k * (LB_DIMENSION - 1);
      int indexTo   = k * LB_DIMENSION * LB_DIMENSION;

      copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
    }
  }

  // bottom
  {
    std::vector<int> directionsToCopy({ SOUTH_WEST, SOUTH, SOUTH_EAST,
                                        WEST, CENTER, EAST,
                                        NORTH_WEST, NORTH, NORTH_EAST,
                                        TOP_SOUTH, TOP_WEST, TOP, TOP_EAST, TOP_NORTH }
                                      );

    for (int j = 1; j < LB_DIMENSION - 1; ++j)
      for (int i = 1; i < LB_DIMENSION - 1; ++i) {
        int indexFrom = i + j * (LB_DIMENSION - 1);
        int indexTo   = i + j * LB_DIMENSION;
        copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
      }

  }

  // left
  {
    std::vector<int> directionsToCopy({ BOTTOM_SOUTH, BOTTOM, BOTTOM_EAST, BOTTOM_NORTH,
                                        SOUTH, SOUTH_EAST, CENTER, WEST, NORTH, NORTH_EAST,
                                        TOP_SOUTH, TOP, TOP_EAST, TOP_NORTH }
                                      );

    for (int k = 1; k < LB_DIMENSION - 1; ++k)
      for (int j = 1; j < LB_DIMENSION - 1; ++j) {
        int indexFrom = (LB_DIMENSION - 1) * (LB_DIMENSION - 1) + k * (LB_DIMENSION - 1) + j;
        int indexTo   = k * LB_DIMENSION * LB_DIMENSION + j * LB_DIMENSION;

        copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
      }

  }

  // front
  {
    std::vector<int> directionsToCopy({ BOTTOM_WEST, BOTTOM, BOTTOM_EAST, BOTTOM_NORTH,
                                        WEST, CENTER, EAST,
                                        NORTH_WEST, NORTH, NORTH_EAST,
                                        TOP_WEST, TOP, TOP_EAST, TOP_NORTH }
                                      );

    for (int k = 1; k < LB_DIMENSION - 1; ++k)
      for (int i = 1; i < LB_DIMENSION - 1; ++i) {
        int indexFrom = 2 * (LB_DIMENSION - 1) * (LB_DIMENSION - 1) +
                        k * (LB_DIMENSION - 1) + i;
        int indexTo = k * LB_DIMENSION * LB_DIMENSION + i;
        copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
      }

  }
}

void
CellSteps::
copy3DVertex1ToCell(int                                  vertexArrayIndex,
                    latticeboltzmann::CellHeap&          cell,
                    Vertex* const                        fineGridVertices,
                    const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vector<DIMENSIONS, int> index(1, 0, 0);

  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(index)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // fill point in the corner
  {
    std::vector<int> directionsToCopy({ WEST, CENTER, NORTH_WEST, NORTH, TOP_WEST, TOP, TOP_NORTH }
                                      );

    int indexFrom = 0;
    int indexTo   = (LB_DIMENSION - 1);
    copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
  }

  // right bottom edge
  {
    std::vector<int> directionsToCopy({ SOUTH, SOUTH_WEST,
                                        WEST, CENTER,
                                        NORTH_WEST, NORTH,
                                        TOP_SOUTH, TOP_WEST, TOP, TOP_NORTH }
                                      );

    for (int j = 1; j < LB_DIMENSION - 1; ++j) {
      int indexFrom = j * (LB_DIMENSION - 1);
      int indexTo   = (LB_DIMENSION - 1) + j * LB_DIMENSION;
      copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
    }
  }

  // right front edge
  {
    std::vector<int> directionsToCopy({ BOTTOM_WEST, BOTTOM,  BOTTOM_NORTH,
                                        WEST, CENTER,
                                        NORTH_WEST, NORTH,
                                        TOP_WEST, TOP, TOP_NORTH }
                                      );

    for (int k = 1; k < LB_DIMENSION - 1; ++k) {
      int indexFrom = (LB_DIMENSION - 1) * (LB_DIMENSION - 1) +  k * (LB_DIMENSION - 1);
      int indexTo   = LB_DIMENSION * LB_DIMENSION  * k + LB_DIMENSION - 1;
      copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
    }
  }

  // right
  {
    std::vector<int> directionsToCopy({ BOTTOM_SOUTH, BOTTOM_WEST,  BOTTOM, BOTTOM_NORTH,
                                        SOUTH_WEST, SOUTH,
                                        WEST, CENTER,
                                        NORTH_WEST, NORTH,
                                        TOP_SOUTH, TOP_WEST, TOP, TOP_NORTH }
                                      );

    for (int k = 1; k < LB_DIMENSION - 1; ++k)
      for (int j = 1; j < LB_DIMENSION - 1; ++j) {
        int indexFrom = (LB_DIMENSION - 1) * (LB_DIMENSION - 1) +  k * (LB_DIMENSION - 1) + j;
        int indexTo   = LB_DIMENSION * LB_DIMENSION  * k + LB_DIMENSION * j + LB_DIMENSION - 1;
        copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
      }

  }
}

void
CellSteps::
copy3DVertex2ToCell(int                                  vertexArrayIndex,
                    latticeboltzmann::CellHeap&          cell,
                    Vertex* const                        fineGridVertices,
                    const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vector<DIMENSIONS, int> index(0, 1, 0);

  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(index)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // fill point in the corner
  {
    std::vector<int> directionsToCopy({ SOUTH, SOUTH_EAST, CENTER, EAST,
                                        TOP_SOUTH, TOP, TOP_EAST }
                                      );

    int indexFrom = 0;
    int indexTo   = LB_DIMENSION * (LB_DIMENSION - 1);
    copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
  }

  // back bottom ebge
  {
    std::vector<int> directionsToCopy({ SOUTH_WEST, SOUTH, SOUTH_EAST,
                                        WEST, CENTER, EAST,
                                        TOP_SOUTH,
                                        TOP_WEST, TOP, TOP_EAST }
                                      );

    for (int i = 1; i < LB_DIMENSION - 1; ++i) {
      int indexFrom = i;
      int indexTo   = LB_DIMENSION * (LB_DIMENSION - 1) + i;
      copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
    }
  }

  // back left edge
  {
    std::vector<int> directionsToCopy({ BOTTOM_SOUTH, BOTTOM, BOTTOM_EAST,
                                        SOUTH, SOUTH_EAST,
                                        CENTER, EAST,
                                        TOP_SOUTH, TOP, TOP_EAST }
                                      );

    for (int k = 1; k < LB_DIMENSION - 1; ++k) {
      int indexFrom = (LB_DIMENSION - 1) * (LB_DIMENSION - 1) + k * (LB_DIMENSION - 1);
      int indexTo   = k * LB_DIMENSION * LB_DIMENSION + LB_DIMENSION * (LB_DIMENSION - 1);
      copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
    }
  }

  // back
  {
    std::vector<int> directionsToCopy({ BOTTOM_SOUTH, BOTTOM_WEST, BOTTOM, BOTTOM_EAST,
                                        SOUTH_WEST, SOUTH, SOUTH_EAST,
                                        WEST, CENTER, EAST,
                                        TOP_SOUTH, TOP_WEST, TOP, TOP_EAST }
                                      );

    for (int k = 1; k < LB_DIMENSION - 1; ++k)
      for (int i = 1; i < LB_DIMENSION - 1; ++i) {
        int indexFrom = 2 * (LB_DIMENSION - 1) * (LB_DIMENSION - 1) + k * (LB_DIMENSION - 1) + i;
        int indexTo   = k * LB_DIMENSION * LB_DIMENSION + LB_DIMENSION * (LB_DIMENSION - 1) + i;
        copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
      }

  }
}

void
CellSteps::
copy3DVertex3ToCell(int                                  vertexArrayIndex,
                    latticeboltzmann::CellHeap&          cell,
                    Vertex* const                        fineGridVertices,
                    const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vector<DIMENSIONS, int> index(1, 1, 0);

  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(index)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // fill point in the corner
  {
    std::vector<int> directionsToCopy({ SOUTH_WEST,  SOUTH,
                                        WEST, CENTER,
                                        TOP_SOUTH, TOP_WEST, TOP }
                                      );

    int indexFrom = 0;
    int indexTo   = LB_DIMENSION * LB_DIMENSION - 1;
    copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
  }

  // right back edge
  {
    std::vector<int> directionsToCopy({ BOTTOM_SOUTH, BOTTOM_WEST, BOTTOM,
                                        SOUTH_WEST, SOUTH,
                                        WEST, CENTER,
                                        TOP_SOUTH, TOP_WEST, TOP }
                                      );

    for (int k = 1; k < LB_DIMENSION - 1; ++k) {
      int indexFrom = (LB_DIMENSION - 1) * (LB_DIMENSION - 1) + k * (LB_DIMENSION - 1);
      int indexTo   = LB_DIMENSION * LB_DIMENSION * k + LB_DIMENSION * LB_DIMENSION - 1;
      copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
    }
  }
}

void
CellSteps::
copy3DVertex4ToCell(int                                  vertexArrayIndex,
                    latticeboltzmann::CellHeap&          cell,
                    Vertex* const                        fineGridVertices,
                    const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vector<DIMENSIONS, int> index(0, 0, 1);

  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(index)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // fill point in the corner
  {
    std::vector<int> directionsToCopy({ BOTTOM, BOTTOM_EAST, BOTTOM_NORTH,
                                        CENTER, EAST,
                                        NORTH, NORTH_EAST }
                                      );

    int indexFrom = 0;
    int indexTo   = LB_DIMENSION * LB_DIMENSION * (LB_DIMENSION - 1);
    copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
  }

  // front top edge
  {
    std::vector<int> directionsToCopy({ BOTTOM_WEST, BOTTOM, BOTTOM_EAST, BOTTOM_NORTH,
                                        WEST, CENTER, EAST,
                                        NORTH_WEST, NORTH, NORTH_EAST }
                                      );

    for (int i = 1; i < LB_DIMENSION - 1; ++i) {
      int indexFrom = i;
      int indexTo   = LB_DIMENSION * LB_DIMENSION * (LB_DIMENSION - 1) + i;
      copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
    }
  }

  // left top edge
  {
    std::vector<int> directionsToCopy({ BOTTOM_SOUTH, BOTTOM, BOTTOM_EAST, BOTTOM_NORTH,
                                        SOUTH, SOUTH_EAST,
                                        CENTER, EAST,
                                        NORTH, NORTH_EAST }
                                      );

    for (int j = 1; j < LB_DIMENSION - 1; ++j) {
      int indexFrom = (LB_DIMENSION - 1) * j;
      int indexTo   = LB_DIMENSION * LB_DIMENSION * (LB_DIMENSION - 1) + j * LB_DIMENSION;
      copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
    }
  }

  // top
  {
    std::vector<int> directionsToCopy({ BOTTOM_SOUTH, BOTTOM_WEST, BOTTOM, BOTTOM_EAST, BOTTOM_NORTH,
                                        SOUTH_WEST, SOUTH, SOUTH_EAST,
                                        WEST, CENTER, EAST,
                                        NORTH_WEST, NORTH, NORTH_EAST }
                                      );

    for (int j = 1; j < LB_DIMENSION - 1; ++j)
      for (int i = 1; i < LB_DIMENSION - 1; ++i) {
        int indexFrom = (LB_DIMENSION - 1) * j + i;
        int indexTo   = LB_DIMENSION * LB_DIMENSION * (LB_DIMENSION - 1) + j * LB_DIMENSION + i;
        copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
      }

  }
}

void
CellSteps::
copy3DVertex5ToCell(int                                  vertexArrayIndex,
                    latticeboltzmann::CellHeap&          cell,
                    Vertex* const                        fineGridVertices,
                    const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vector<DIMENSIONS, int> index(1, 0, 1);

  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(index)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // fill point in the corner
  {
    std::vector<int> directionsToCopy({ BOTTOM_WEST, BOTTOM, BOTTOM_NORTH,
                                        WEST, CENTER,
                                        NORTH_WEST, NORTH }
                                      );

    int indexFrom = 0;
    int indexTo   = LB_DIMENSION * LB_DIMENSION * (LB_DIMENSION - 1) + (LB_DIMENSION - 1);
    copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
  }

  // right top edge
  {
    std::vector<int> directionsToCopy({ BOTTOM_SOUTH,  BOTTOM_WEST, BOTTOM, BOTTOM_NORTH,
                                        SOUTH_WEST, SOUTH,
                                        WEST, CENTER,
                                        NORTH_WEST, NORTH }
                                      );

    for (int j = 1; j < LB_DIMENSION - 1; ++j) {
      int indexFrom = j * (LB_DIMENSION - 1);
      int indexTo   = LB_DIMENSION * LB_DIMENSION * (LB_DIMENSION - 1) +
                      j * LB_DIMENSION + LB_DIMENSION - 1;
      copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
    }
  }
}

void
CellSteps::
copy3DVertex6ToCell(int                                  vertexArrayIndex,
                    latticeboltzmann::CellHeap&          cell,
                    Vertex* const                        fineGridVertices,
                    const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vector<DIMENSIONS, int> index(0, 1, 1);

  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(index)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // fill point in the corner
  {
    std::vector<int> directionsToCopy({ BOTTOM_SOUTH, BOTTOM, BOTTOM_EAST,
                                        SOUTH, SOUTH_EAST,
                                        CENTER, EAST }
                                      );

    int indexFrom = 0;
    int indexTo   = LB_DIMENSION * LB_DIMENSION * (LB_DIMENSION - 1) + LB_DIMENSION * (LB_DIMENSION - 1);
    copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
  }

  // back top edge
  {
    std::vector<int> directionsToCopy({ BOTTOM_SOUTH, BOTTOM_WEST, BOTTOM, BOTTOM_EAST,
                                        SOUTH_WEST, SOUTH, SOUTH_EAST,
                                        WEST, CENTER, EAST }
                                      );

    for (int i = 1; i < LB_DIMENSION - 1; ++i) {
      int indexFrom = i;
      int indexTo   = LB_DIMENSION * LB_DIMENSION * (LB_DIMENSION - 1) +
                      LB_DIMENSION * (LB_DIMENSION - 1) + i;
      copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
    }
  }
}

void
CellSteps::
copy3DVertex7ToCell(int                                  vertexArrayIndex,
                    latticeboltzmann::CellHeap&          cell,
                    Vertex* const                        fineGridVertices,
                    const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vector<DIMENSIONS, int> index(1, 1, 1);

  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(index)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // fill point in the corner
  {
    std::vector<int> directionsToCopy({ BOTTOM_SOUTH, BOTTOM_WEST, BOTTOM,
                                        SOUTH_WEST, SOUTH,
                                        WEST, CENTER }
                                      );

    int indexFrom = 0;
    int indexTo   = LB_DIMENSION * LB_DIMENSION * LB_DIMENSION - 1;
    copyPointFromVertexToCell(directionsToCopy, array, indexFrom, cell, indexTo);
  }
}

/* **********************************************************************************
* **********************************************************************************
* **********************************************************************************/

void
CellSteps::
fillCellBoundary(int                         vertexArrayIndex,
                 latticeboltzmann::CellHeap& cell,
                 Vertex* const               fineGridVertices,
                 const peano::grid::VertexEnumerator&
                 fineGridVerticesEnumerator)
{
  // left front bottom
  copy3DVertex0ToCell(vertexArrayIndex, cell, fineGridVertices,
                      fineGridVerticesEnumerator);

  // right front bottom
  copy3DVertex1ToCell(vertexArrayIndex, cell, fineGridVertices,
                      fineGridVerticesEnumerator);

  // left back bottom
  copy3DVertex2ToCell(vertexArrayIndex, cell, fineGridVertices,
                      fineGridVerticesEnumerator);

  // right back bottom
  copy3DVertex3ToCell(vertexArrayIndex, cell, fineGridVertices,
                      fineGridVerticesEnumerator);

  // left front top
  copy3DVertex4ToCell(vertexArrayIndex, cell, fineGridVertices,
                      fineGridVerticesEnumerator);

  // right front top
  copy3DVertex5ToCell(vertexArrayIndex, cell, fineGridVertices,
                      fineGridVerticesEnumerator);

  // left back top
  copy3DVertex6ToCell(vertexArrayIndex, cell, fineGridVertices,
                      fineGridVerticesEnumerator);

  // right back top
  copy3DVertex7ToCell(vertexArrayIndex, cell, fineGridVertices,
                      fineGridVerticesEnumerator);
}
#endif

#ifdef Dim2
void
CellSteps::
fill2DVertex0(int                                  vertexArrayIndex,
              CellHeap&                            cell,
              Vertex*                              fineGridVertices,
              const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vector<DIMENSIONS, int> index(0, 0);

  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(index)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // corner point
  {
    std::vector<int> directions({ NORTH, NORTH_EAST, EAST, CENTER }
                                );

    copyPointFromCellToVertex(directions, cell, 0, array, 0);
  }

  // horizontal
  {
    std::vector<int> directions({ WEST, NORTH_WEST,
                                  NORTH, NORTH_EAST,
                                  EAST, CENTER }
                                );

    for (int i = 1; i < LB_DIMENSION - 1; ++i)
      copyPointFromCellToVertex(directions, cell, i, array, i);
  }

  // vertical
  {
    std::vector<int> directions({ NORTH, NORTH_EAST,
                                  EAST, CENTER,
                                  SOUTH_EAST, SOUTH }
                                );

    // vertical part

    for (int j = 1; j < LB_DIMENSION - 1; ++j) {
      int indexFrom = j * LB_DIMENSION;
      int indexTo   = LB_DIMENSION - 1 + j;
      copyPointFromCellToVertex(directions, cell, indexFrom, array, indexTo);
    }
  }
}

void
CellSteps::
fill2DVertex1(int                                  vertexArrayIndex,
              CellHeap&                            cell,
              Vertex*                              fineGridVertices,
              const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vector<DIMENSIONS, int> index(1, 0);

  Vertex&                    v   = fineGridVertices[fineGridVerticesEnumerator(index)];
  Vector<DIMENSIONS, double> pos = fineGridVerticesEnumerator.getVertexPosition(index);

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // corner
  {
    int indexFrom = LB_DIMENSION - 1;

    std::vector<int> directions({ WEST, NORTH_WEST }
                                );

    if (pos(0) > 0.9999) {
      directions.push_back(NORTH);
      directions.push_back(CENTER);
    }

    copyPointFromCellToVertex(directions, cell, indexFrom, array, 0);
  }

  {
    std::vector<int> directions({ NORTH_WEST, WEST, SOUTH_WEST }
                                );

    if (pos(0) > 0.9999) {
      directions.push_back(NORTH);
      directions.push_back(CENTER);
      directions.push_back(SOUTH);
    }

    // vertical part
    for (int j = 1; j < LB_DIMENSION - 1; ++j) {
      int indexFrom = j * LB_DIMENSION + LB_DIMENSION - 1;
      int indexTo   = LB_DIMENSION - 1 + j;
      copyPointFromCellToVertex(directions, cell, indexFrom, array, indexTo);
    }
  }
}

void
CellSteps::
fill2DVertex2(int                                  vertexArrayIndex,
              CellHeap&                            cell,
              Vertex*                              fineGridVertices,
              const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vector<DIMENSIONS, int> index(0, 1);

  Vertex&                    v   = fineGridVertices[fineGridVerticesEnumerator(index)];
  Vector<DIMENSIONS, double> pos = fineGridVerticesEnumerator.getVertexPosition(index);

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // corner point
  {
    int              indexFrom = LB_DIMENSION * (LB_DIMENSION - 1);
    std::vector<int> directions({ SOUTH, SOUTH_EAST }
                                );

    if (pos(1) > 0.999) {
      directions.push_back(EAST);
      directions.push_back(CENTER);
    }

    copyPointFromCellToVertex(directions, cell, indexFrom, array, 0);
  }

  {
    std::vector<int> directions({ SOUTH_WEST, SOUTH, SOUTH_EAST }
                                );

    if (pos(1) > 0.999) {
      directions.push_back(WEST);
      directions.push_back(CENTER);
      directions.push_back(EAST);
    }

    for (int i = 1; i < LB_DIMENSION - 1; ++i) {
      int indexFrom = LB_DIMENSION * (LB_DIMENSION - 1) + i;
      copyPointFromCellToVertex(directions, cell, indexFrom, array, i);
    }
  }
}

void
CellSteps::
fill2DVertex3(int                                  vertexArrayIndex,
              CellHeap&                            cell,
              Vertex*                              fineGridVertices,
              const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vector<DIMENSIONS, int> index(1, 1);

  Vertex&                    v   = fineGridVertices[fineGridVerticesEnumerator(index)];
  Vector<DIMENSIONS, double> pos = fineGridVerticesEnumerator.getVertexPosition(index);

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  const int indexFrom = LB_DIMENSION * LB_DIMENSION - 1;

  // TODO think about which directions to copy
  std::vector<int> directions({ SOUTH_WEST, CENTER }
                              );

  if (pos(0) > 0.999)
    directions.push_back(SOUTH);

  if (pos(1) > 0.999)
    directions.push_back(WEST);

  copyPointFromCellToVertex(directions, cell, indexFrom, array, 0);
}

void
CellSteps::
fillVertices(int                                  vertexArrayIndex,
             CellHeap&                            cell,
             Vertex*                              fineGridVertices,
             const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  fill2DVertex0(vertexArrayIndex,
                cell,
                fineGridVertices,
                fineGridVerticesEnumerator);
  fill2DVertex1(vertexArrayIndex,
                cell,
                fineGridVertices,
                fineGridVerticesEnumerator);
  fill2DVertex2(vertexArrayIndex,
                cell,
                fineGridVertices,
                fineGridVerticesEnumerator);
  fill2DVertex3(vertexArrayIndex,
                cell,
                fineGridVertices,
                fineGridVerticesEnumerator);
}

#endif // Dim2

#ifdef Dim3

void
CellSteps::
fill3DVertex0(int                                  vertexArrayIndex,
              CellHeap&                            cell,
              Vertex*                              fineGridVertices,
              const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  // vertex #0 -- left front bottom
  Vector<DIMENSIONS, int> index(0, 0, 0);

  Vertex& v = fineGridVertices[fineGridVerticesEnumerator(index)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // origin point
  {
    std::vector<int> directionsToCopy({ CENTER, EAST, NORTH,
                                        NORTH_EAST, TOP, TOP_EAST,
                                        TOP_NORTH }
                                      );

    copyPointFromCellToVertex(directionsToCopy, cell, 0, array, 0);
  }

  // front bottom edge
  {
    std::vector<int> directionsToCopy({ WEST, CENTER, EAST,
                                        NORTH_WEST, NORTH, NORTH_EAST,
                                        TOP_WEST, TOP, TOP_EAST,
                                        TOP_NORTH }
                                      );

    for (int i = 1; i < LB_DIMENSION - 1; ++i)
      copyPointFromCellToVertex(directionsToCopy, cell, i, array, i);
  }

  // left bottom edge
  {
    std::vector<int> directionsToCopy({ SOUTH, SOUTH_EAST,
                                        CENTER, EAST,
                                        NORTH, NORTH_EAST,
                                        TOP_SOUTH, TOP, TOP_EAST,
                                        TOP_NORTH }
                                      );

    for (int j = 1; j < LB_DIMENSION - 1; ++j) {
      int indexFrom = LB_DIMENSION * j;
      int indexTo   = (LB_DIMENSION - 1) * j;

      copyPointFromCellToVertex(directionsToCopy, cell, indexFrom,  array, indexTo);
    }
  }

  // left front edge
  {
    std::vector<int> directionsToCopy({ BOTTOM, BOTTOM_EAST, BOTTOM_NORTH,
                                        CENTER, EAST, NORTH, NORTH_EAST,
                                        TOP, TOP_EAST, TOP_NORTH }
                                      );

    for (int k = 1; k < LB_DIMENSION - 1; ++k) {
      int indexFrom = k * LB_DIMENSION * LB_DIMENSION;
      int indexTo   = (LB_DIMENSION - 1) * (LB_DIMENSION - 1) + k * (LB_DIMENSION - 1);

      copyPointFromCellToVertex(directionsToCopy, cell, indexFrom,  array, indexTo);
    }
  }

  // bottom
  {
    std::vector<int> directionsToCopy({ SOUTH_WEST, SOUTH, SOUTH_EAST,
                                        WEST, CENTER, EAST,
                                        NORTH_WEST, NORTH, NORTH_EAST,
                                        TOP_SOUTH, TOP_WEST, TOP, TOP_EAST, TOP_NORTH }
                                      );

    for (int j = 1; j < LB_DIMENSION - 1; ++j)
      for (int i = 1; i < LB_DIMENSION - 1; ++i) {
        int indexFrom = i + j * LB_DIMENSION;
        int indexTo   = i + j * (LB_DIMENSION - 1);
        copyPointFromCellToVertex(directionsToCopy, cell, indexFrom, array, indexTo);
      }

  }

  // left
  {
    std::vector<int> directionsToCopy({ BOTTOM_SOUTH, BOTTOM, BOTTOM_EAST, BOTTOM_NORTH,
                                        SOUTH, SOUTH_EAST, CENTER, WEST, NORTH, NORTH_EAST,
                                        TOP_SOUTH, TOP, TOP_EAST, TOP_NORTH }
                                      );

    for (int k = 1; k < LB_DIMENSION - 1; ++k)
      for (int j = 1; j < LB_DIMENSION - 1; ++j) {
        int indexFrom = k * LB_DIMENSION * LB_DIMENSION + j * LB_DIMENSION;
        int indexTo   = (LB_DIMENSION - 1) * (LB_DIMENSION - 1) + k * (LB_DIMENSION - 1) + j;

        copyPointFromCellToVertex(directionsToCopy, cell, indexFrom, array, indexTo);
      }

  }

  // front

  {
    std::vector<int> directionsToCopy({ BOTTOM_WEST, BOTTOM, BOTTOM_EAST, BOTTOM_NORTH,
                                        WEST, CENTER, EAST,
                                        NORTH_WEST, NORTH, NORTH_EAST,
                                        TOP_WEST, TOP, TOP_EAST, TOP_NORTH }
                                      );

    for (int k = 1; k < LB_DIMENSION - 1; ++k)
      for (int i = 1; i < LB_DIMENSION - 1; ++i) {
        int indexFrom = k * LB_DIMENSION * LB_DIMENSION + i;
        int indexTo   = 2 * (LB_DIMENSION - 1) * (LB_DIMENSION - 1) + k * (LB_DIMENSION - 1) + i;
        copyPointFromCellToVertex(directionsToCopy, cell, indexFrom, array, indexTo);
      }

  }
  //
}

void
CellSteps::
fill3DVertex1(int                                  vertexArrayIndex,
              CellHeap&                            cell,
              Vertex*                              fineGridVertices,
              const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vector<DIMENSIONS, int> index(1, 0, 0);

  Vector<DIMENSIONS, double> pos = fineGridVerticesEnumerator.getVertexPosition(index);
  Vertex&                    v   = fineGridVertices[fineGridVerticesEnumerator(index)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // fill point in the corner
  {
    std::vector<int> directionsToCopy({ WEST, NORTH_WEST, TOP_WEST }
                                      );

    if (pos(0) > 0.999) {
      directionsToCopy.push_back(CENTER);
      directionsToCopy.push_back(NORTH);
      directionsToCopy.push_back(TOP);
      directionsToCopy.push_back(TOP_NORTH);
    }

    int indexFrom = (LB_DIMENSION - 1);
    int indexTo   = 0;
    copyPointFromCellToVertex(directionsToCopy, cell, indexFrom, array, indexTo);
  }

  // right bottom edge
  {
    std::vector<int> directionsToCopy({ SOUTH_WEST,
                                        WEST,
                                        NORTH_WEST,
                                        TOP_WEST }
                                      );

    if (pos(0) > 0.999) {
      directionsToCopy.push_back(SOUTH);
      directionsToCopy.push_back(CENTER);
      directionsToCopy.push_back(NORTH);
      directionsToCopy.push_back(TOP_SOUTH);
      directionsToCopy.push_back(TOP);
      directionsToCopy.push_back(TOP_NORTH);
    }

    for (int j = 1; j < LB_DIMENSION - 1; ++j) {
      int indexFrom =  j * LB_DIMENSION + (LB_DIMENSION - 1);
      int indexTo   = j * (LB_DIMENSION - 1);
      copyPointFromCellToVertex(directionsToCopy, cell, indexFrom, array, indexTo);
    }
  }

  // right front edge
  {
    std::vector<int> directionsToCopy({ BOTTOM_WEST,
                                        WEST,
                                        NORTH_WEST,
                                        TOP_WEST }
                                      );

    if (pos(0) > 0.999) {
      directionsToCopy.push_back(BOTTOM);
      directionsToCopy.push_back(BOTTOM_NORTH);
      directionsToCopy.push_back(CENTER);
      directionsToCopy.push_back(NORTH);
      directionsToCopy.push_back(TOP);
      directionsToCopy.push_back(TOP_NORTH);
    }

    for (int k = 1; k < LB_DIMENSION - 1; ++k) {
      int indexFrom = LB_DIMENSION * LB_DIMENSION  * k + LB_DIMENSION - 1;
      int indexTo   = (LB_DIMENSION - 1) * (LB_DIMENSION - 1) +  k * (LB_DIMENSION - 1);
      copyPointFromCellToVertex(directionsToCopy, cell, indexFrom, array, indexTo);
    }
  }

  // right
  {
    std::vector<int> directionsToCopy({ BOTTOM_WEST,
                                        SOUTH_WEST,
                                        WEST,
                                        NORTH_WEST,
                                        TOP_WEST }
                                      );

    if (pos(0) > 0.999) {
      directionsToCopy.push_back(BOTTOM_SOUTH);
      directionsToCopy.push_back(BOTTOM);
      directionsToCopy.push_back(BOTTOM_NORTH);
      directionsToCopy.push_back(SOUTH);
      directionsToCopy.push_back(CENTER);
      directionsToCopy.push_back(NORTH);
      directionsToCopy.push_back(TOP_SOUTH);
      directionsToCopy.push_back(TOP);
      directionsToCopy.push_back(TOP_NORTH);
    }

    for (int k = 1; k < LB_DIMENSION - 1; ++k)
      for (int j = 1; j < LB_DIMENSION - 1; ++j) {
        int indexFrom = LB_DIMENSION * LB_DIMENSION  * k + LB_DIMENSION * j + LB_DIMENSION - 1;
        int indexTo   = (LB_DIMENSION - 1) * (LB_DIMENSION - 1) +  k * (LB_DIMENSION - 1) + j;
        copyPointFromCellToVertex(directionsToCopy, cell, indexFrom, array, indexTo);
      }

  }
  //
}

void
CellSteps::
fill3DVertex2(int                                  vertexArrayIndex,
              CellHeap&                            cell,
              Vertex*                              fineGridVertices,
              const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vector<DIMENSIONS, int> index(0, 1, 0);

  Vector<DIMENSIONS, double> pos = fineGridVerticesEnumerator.getVertexPosition(index);
  Vertex&                    v   = fineGridVertices[fineGridVerticesEnumerator(index)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // fill point in the corner
  {
    std::vector<int> directionsToCopy({ SOUTH, SOUTH_EAST,
                                        TOP_SOUTH }
                                      );

    if (pos(1) > 0.999) {
      directionsToCopy.push_back(CENTER);
      directionsToCopy.push_back(EAST);
      directionsToCopy.push_back(TOP);
      directionsToCopy.push_back(TOP_EAST);
    }

    int indexFrom = LB_DIMENSION * (LB_DIMENSION - 1);
    int indexTo   = 0;
    copyPointFromCellToVertex(directionsToCopy, cell, indexFrom, array, indexTo);
  }

  // back bottom ebge
  {
    std::vector<int> directionsToCopy({ SOUTH_WEST, SOUTH, SOUTH_EAST, TOP_SOUTH }
                                      );

    if (pos(1) > 0.999) {
      directionsToCopy.push_back(WEST);
      directionsToCopy.push_back(CENTER);
      directionsToCopy.push_back(EAST);
      directionsToCopy.push_back(TOP_WEST);
      directionsToCopy.push_back(TOP);
      directionsToCopy.push_back(TOP_EAST);
    }

    for (int i = 1; i < LB_DIMENSION - 1; ++i) {
      int indexFrom = LB_DIMENSION * (LB_DIMENSION - 1) + i;
      int indexTo   = i;
      copyPointFromCellToVertex(directionsToCopy, cell, indexFrom, array, indexTo);
    }
  }

  // back left edge
  {
    std::vector<int> directionsToCopy({ BOTTOM_SOUTH,
                                        SOUTH, SOUTH_EAST,
                                        TOP_SOUTH }
                                      );

    if (pos(1) > 0.999) {
      directionsToCopy.push_back(BOTTOM);
      directionsToCopy.push_back(BOTTOM_EAST);
      directionsToCopy.push_back(CENTER);
      directionsToCopy.push_back(EAST);
      directionsToCopy.push_back(TOP);
      directionsToCopy.push_back(TOP_EAST);
    }

    for (int k = 1; k < LB_DIMENSION - 1; ++k) {
      int indexFrom = k * LB_DIMENSION * LB_DIMENSION + LB_DIMENSION * (LB_DIMENSION - 1);
      int indexTo   = (LB_DIMENSION - 1) * (LB_DIMENSION - 1) + k * (LB_DIMENSION - 1);
      copyPointFromCellToVertex(directionsToCopy, cell, indexFrom, array, indexTo);
    }
  }

  // back
  {
    std::vector<int> directionsToCopy({ BOTTOM_SOUTH,
                                        SOUTH_WEST, SOUTH, SOUTH_EAST,
                                        TOP_SOUTH }
                                      );

    if (pos(1) > 0.999) {
      directionsToCopy.push_back(BOTTOM_WEST);
      directionsToCopy.push_back(BOTTOM);
      directionsToCopy.push_back(BOTTOM_EAST);
      directionsToCopy.push_back(WEST);
      directionsToCopy.push_back(CENTER);
      directionsToCopy.push_back(EAST);
      directionsToCopy.push_back(TOP_WEST);
      directionsToCopy.push_back(TOP);
      directionsToCopy.push_back(TOP_EAST);
    }

    for (int k = 1; k < LB_DIMENSION - 1; ++k)
      for (int i = 1; i < LB_DIMENSION - 1; ++i) {
        int indexFrom = k * LB_DIMENSION * LB_DIMENSION + LB_DIMENSION * (LB_DIMENSION - 1) + i;
        int indexTo   = 2 * (LB_DIMENSION - 1) * (LB_DIMENSION - 1) + k * (LB_DIMENSION - 1) + i;
        copyPointFromCellToVertex(directionsToCopy, cell, indexFrom, array, indexTo);
      }

  }
}

void
CellSteps::
fill3DVertex3(int                                  vertexArrayIndex,
              CellHeap&                            cell,
              Vertex*                              fineGridVertices,
              const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vector<DIMENSIONS, int> index(1, 1, 0);

  Vector<DIMENSIONS, double> pos = fineGridVerticesEnumerator.getVertexPosition(index);
  Vertex&                    v   = fineGridVertices[fineGridVerticesEnumerator(index)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // fill point in the corner
  {
    std::vector<int> directionsToCopy({ SOUTH_WEST }
                                      );

    if (pos(0) > 0.999) {
      directionsToCopy.push_back(SOUTH);
      directionsToCopy.push_back(TOP_SOUTH);
    }

    if (pos(1) > 0.999) {
      directionsToCopy.push_back(WEST);
      directionsToCopy.push_back(TOP_WEST);
    }

    if (pos(0) > 0.999 && pos(1) > 0.999) {
      directionsToCopy.push_back(CENTER);
      directionsToCopy.push_back(TOP);
    }

    int indexFrom = LB_DIMENSION * LB_DIMENSION - 1;
    int indexTo   = 0;
    copyPointFromCellToVertex(directionsToCopy, cell, indexFrom, array, indexTo);
  }

  // right back edge
  {
    std::vector<int> directionsToCopy({ SOUTH_WEST }
                                      );

    if (pos(0) > 0.999) {
      directionsToCopy.push_back(BOTTOM_SOUTH);
      directionsToCopy.push_back(SOUTH);
      directionsToCopy.push_back(TOP_SOUTH);
    }

    if (pos(1) > 0.999) {
      directionsToCopy.push_back(BOTTOM_WEST);
      directionsToCopy.push_back(WEST);
      directionsToCopy.push_back(TOP_WEST);
    }

    if (pos(0) > 0.999 && pos(1) > 0.999) {
      directionsToCopy.push_back(BOTTOM);
      directionsToCopy.push_back(CENTER);
      directionsToCopy.push_back(TOP);
    }

    for (int k = 1; k < LB_DIMENSION - 1; ++k) {
      int indexFrom = LB_DIMENSION * LB_DIMENSION * k + LB_DIMENSION * LB_DIMENSION - 1;
      int indexTo   = (LB_DIMENSION - 1) * (LB_DIMENSION - 1) + k * (LB_DIMENSION - 1);
      copyPointFromCellToVertex(directionsToCopy, cell, indexFrom, array, indexTo);
    }
  }
  //
}

void
CellSteps::
fill3DVertex4(int                                  vertexArrayIndex,
              CellHeap&                            cell,
              Vertex*                              fineGridVertices,
              const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vector<DIMENSIONS, int> index(0, 0, 1);

  Vector<DIMENSIONS, double> pos = fineGridVerticesEnumerator.getVertexPosition(index);
  Vertex&                    v   = fineGridVertices[fineGridVerticesEnumerator(index)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // fill point in the corner
  {
    std::vector<int> directionsToCopy({ BOTTOM, BOTTOM_EAST, BOTTOM_NORTH }
                                      );

    if (pos(2) > 0.999) {
      directionsToCopy.push_back(CENTER);
      directionsToCopy.push_back(EAST);
      directionsToCopy.push_back(NORTH);
      directionsToCopy.push_back(NORTH_EAST);
    }

    int indexFrom = LB_DIMENSION * LB_DIMENSION * (LB_DIMENSION - 1);
    int indexTo   = 0;
    copyPointFromCellToVertex(directionsToCopy, cell, indexFrom, array, indexTo);
  }

  // front top edge
  {
    std::vector<int> directionsToCopy({ BOTTOM_WEST, BOTTOM, BOTTOM_EAST, BOTTOM_NORTH }
                                      );

    if (pos(2) > 0.999) {
      directionsToCopy.push_back(WEST);
      directionsToCopy.push_back(CENTER);
      directionsToCopy.push_back(EAST);
      directionsToCopy.push_back(NORTH_WEST);
      directionsToCopy.push_back(NORTH);
      directionsToCopy.push_back(NORTH_EAST);
    }

    for (int i = 1; i < LB_DIMENSION - 1; ++i) {
      int indexFrom = LB_DIMENSION * LB_DIMENSION * (LB_DIMENSION - 1) + i;
      int indexTo   = i;
      copyPointFromCellToVertex(directionsToCopy, cell, indexFrom, array, indexTo);
    }
  }

  // left top edge
  {
    std::vector<int> directionsToCopy({ BOTTOM_SOUTH, BOTTOM, BOTTOM_EAST, BOTTOM_NORTH }
                                      );

    if (pos(2) > 0.999) {
      directionsToCopy.push_back(SOUTH);
      directionsToCopy.push_back(SOUTH_EAST);
      directionsToCopy.push_back(CENTER);
      directionsToCopy.push_back(EAST);
      directionsToCopy.push_back(NORTH);
      directionsToCopy.push_back(NORTH_EAST);
    }

    for (int j = 1; j < LB_DIMENSION - 1; ++j) {
      int indexFrom = LB_DIMENSION * LB_DIMENSION * (LB_DIMENSION - 1) + j * LB_DIMENSION;
      int indexTo   = (LB_DIMENSION - 1) * j;
      copyPointFromCellToVertex(directionsToCopy, cell, indexFrom, array, indexTo);
    }
  }

  // top
  {
    std::vector<int> directionsToCopy({ BOTTOM_SOUTH, BOTTOM_WEST,
                                        BOTTOM,
                                        BOTTOM_EAST, BOTTOM_NORTH }
                                      );

    if (pos(2) > 0.999) {
      directionsToCopy.push_back(SOUTH_WEST);
      directionsToCopy.push_back(SOUTH);
      directionsToCopy.push_back(SOUTH_EAST);
      directionsToCopy.push_back(WEST);
      directionsToCopy.push_back(CENTER);
      directionsToCopy.push_back(EAST);
      directionsToCopy.push_back(NORTH_WEST);
      directionsToCopy.push_back(NORTH);
      directionsToCopy.push_back(NORTH_EAST);
    }

    for (int j = 1; j < LB_DIMENSION - 1; ++j)
      for (int i = 1; i < LB_DIMENSION - 1; ++i) {
        int indexFrom = LB_DIMENSION * LB_DIMENSION * (LB_DIMENSION - 1) + j * LB_DIMENSION + i;
        int indexTo   = (LB_DIMENSION - 1) * j + i;
        copyPointFromCellToVertex(directionsToCopy, cell, indexFrom, array, indexTo);
      }

  }
}

void
CellSteps::
fill3DVertex5(int                                  vertexArrayIndex,
              CellHeap&                            cell,
              Vertex*                              fineGridVertices,
              const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vector<DIMENSIONS, int> index(1, 0, 1);

  Vector<DIMENSIONS, double> pos = fineGridVerticesEnumerator.getVertexPosition(index);
  Vertex&                    v   = fineGridVertices[fineGridVerticesEnumerator(index)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // fill point in the corner
  {
    std::vector<int> directionsToCopy({ BOTTOM_WEST }
                                      );

    if (pos(0) > 0.999) {
      directionsToCopy.push_back(BOTTOM);
      directionsToCopy.push_back(BOTTOM_NORTH);
    }

    if (pos(2) > 0.999) {
      directionsToCopy.push_back(WEST);
      directionsToCopy.push_back(NORTH_WEST);
    }

    if (pos(0) > 0.999 && pos(2) > 0.999) {
      directionsToCopy.push_back(CENTER);
      directionsToCopy.push_back(NORTH);
    }

    int indexFrom = LB_DIMENSION * LB_DIMENSION * (LB_DIMENSION - 1) + (LB_DIMENSION - 1);
    int indexTo   = 0;
    copyPointFromCellToVertex(directionsToCopy, cell, indexFrom, array, indexTo);
  }

  // right top edge
  {
    std::vector<int> directionsToCopy({ BOTTOM_WEST }
                                      );

    if (pos(0) > 0.999) {
      directionsToCopy.push_back(BOTTOM_SOUTH);
      directionsToCopy.push_back(BOTTOM);
      directionsToCopy.push_back(BOTTOM_NORTH);
    }

    if (pos(2) > 0.999) {
      directionsToCopy.push_back(SOUTH_WEST);
      directionsToCopy.push_back(WEST);
      directionsToCopy.push_back(NORTH_WEST);
    }

    if (pos(0) > 0.999 && pos(2) > 0.999) {
      directionsToCopy.push_back(SOUTH);
      directionsToCopy.push_back(CENTER);
      directionsToCopy.push_back(NORTH);
    }

    for (int j = 1; j < LB_DIMENSION - 1; ++j) {
      // TODO check inder From
      int indexFrom = LB_DIMENSION * LB_DIMENSION * (LB_DIMENSION - 1) + j * LB_DIMENSION + LB_DIMENSION - 1;
      int indexTo   = j * (LB_DIMENSION - 1);
      copyPointFromCellToVertex(directionsToCopy, cell, indexFrom, array, indexTo);
    }
  }
}

void
CellSteps::
fill3DVertex6(int                                  vertexArrayIndex,
              CellHeap&                            cell,
              Vertex*                              fineGridVertices,
              const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vector<DIMENSIONS, int> index(0, 1, 1);

  Vector<DIMENSIONS, double> pos = fineGridVerticesEnumerator.getVertexPosition(index);
  Vertex&                    v   = fineGridVertices[fineGridVerticesEnumerator(index)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // fill point in the corner
  {
    std::vector<int> directionsToCopy({ BOTTOM_SOUTH }
                                      );

    if (pos(1) > 0.999) {
      directionsToCopy.push_back(BOTTOM);
      directionsToCopy.push_back(BOTTOM_EAST);
    }

    if (pos(2) > 0.999) {
      directionsToCopy.push_back(SOUTH);
      directionsToCopy.push_back(SOUTH_EAST);
    }

    if (pos(1) > 0.999 && pos(2) > 0.999) {
      directionsToCopy.push_back(CENTER);
      directionsToCopy.push_back(EAST);
    }

    int indexFrom = LB_DIMENSION * LB_DIMENSION * (LB_DIMENSION - 1) + LB_DIMENSION * (LB_DIMENSION - 1);
    int indexTo   = 0;

    copyPointFromCellToVertex(directionsToCopy, cell, indexFrom, array, indexTo);
  }

  // back top edge
  {
    std::vector<int> directionsToCopy({ BOTTOM_SOUTH }
                                      );

    if (pos(1) > 0.999) {
      directionsToCopy.push_back(BOTTOM_WEST);
      directionsToCopy.push_back(BOTTOM);
      directionsToCopy.push_back(BOTTOM_EAST);
    }

    if (pos(2) > 0.999) {
      directionsToCopy.push_back(SOUTH_WEST);
      directionsToCopy.push_back(SOUTH);
      directionsToCopy.push_back(SOUTH_EAST);
    }

    if (pos(1) > 0.999 && pos(2) > 0.999) {
      directionsToCopy.push_back(WEST);
      directionsToCopy.push_back(CENTER);
      directionsToCopy.push_back(EAST);
    }

    for (int i = 1; i < LB_DIMENSION - 1; ++i) {
      int indexFrom = LB_DIMENSION * LB_DIMENSION * (LB_DIMENSION - 1) +
                      LB_DIMENSION * (LB_DIMENSION - 1) + i;
      int indexTo = i;
      copyPointFromCellToVertex(directionsToCopy, cell, indexFrom, array, indexTo);
    }
  }
}

void
CellSteps::
fill3DVertex7(int                                  vertexArrayIndex,
              CellHeap&                            cell,
              Vertex*                              fineGridVertices,
              const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  Vector<DIMENSIONS, int> index(1, 1, 1);

  Vector<DIMENSIONS, double> pos = fineGridVerticesEnumerator.getVertexPosition(index);
  Vertex&                    v   = fineGridVertices[fineGridVerticesEnumerator(index)];

  // take heap data structure
  std::vector<VertexHeap>& vertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      v.getAggregateWithArbitraryCardinality());

  // take arrays
  VertexHeap& array = vertexData[vertexArrayIndex];

  // fill point in the corner
  {
    std::vector<int> directionsToCopy;

    if (pos(0) > 0.999)
      directionsToCopy.push_back(BOTTOM_SOUTH);

    if (pos(1) > 0.999)
      directionsToCopy.push_back(BOTTOM_WEST);

    if (pos(2) > 0.999)
      directionsToCopy.push_back(SOUTH_WEST);

    if (pos(0) > 0.999 && pos(1) > 0.999)
      directionsToCopy.push_back(BOTTOM);

    if (pos(1) > 0.999 && pos(2) > 0.999)
      directionsToCopy.push_back(WEST);

    if (pos(2) > 0.999 && pos(0) > 0.999)
      directionsToCopy.push_back(SOUTH);

    if (pos(0) > 0.999 && pos(1) > 0.999 && pos(2) > 0.999)
      directionsToCopy.push_back(CENTER);

    int indexFrom = LB_DIMENSION * LB_DIMENSION * LB_DIMENSION - 1;
    int indexTo   = 0;
    copyPointFromCellToVertex(directionsToCopy, cell, indexFrom, array, indexTo);
  }
}

void
CellSteps::
fillVertices(int                                  vertexArrayIndex,
             CellHeap&                            cell,
             Vertex*                              fineGridVertices,
             const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  fill3DVertex0(vertexArrayIndex,
                cell,
                fineGridVertices,
                fineGridVerticesEnumerator);

  fill3DVertex1(vertexArrayIndex,
                cell,
                fineGridVertices,
                fineGridVerticesEnumerator);

  fill3DVertex2(vertexArrayIndex,
                cell,
                fineGridVertices,
                fineGridVerticesEnumerator);

  fill3DVertex3(vertexArrayIndex,
                cell,
                fineGridVertices,
                fineGridVerticesEnumerator);

  fill3DVertex4(vertexArrayIndex,
                cell,
                fineGridVertices,
                fineGridVerticesEnumerator);

  fill3DVertex5(vertexArrayIndex,
                cell,
                fineGridVertices,
                fineGridVerticesEnumerator);

  fill3DVertex6(vertexArrayIndex,
                cell,
                fineGridVertices,
                fineGridVerticesEnumerator);

  fill3DVertex7(vertexArrayIndex,
                cell,
                fineGridVertices,
                fineGridVerticesEnumerator);
}
#endif // Dim3

inline
void
swap(CellHeap& cell, int indexOne, int directionOne, int indexTwo, int directionTwo)
{
  // we must be sure that direction are opposite
  assert(directionOne == (LB_DIR - 1 - directionTwo));

  double temp = cell.getDir(directionOne)[indexOne];
  cell.getDir(directionOne)[indexOne] = cell.getDir(directionTwo)[indexTwo];
  cell.getDir(directionTwo)[indexTwo] = temp;
}

void
CellSteps::
swapCollisionStreamCell(latticeboltzmann::CellHeap& cell, double tau)
{
  double buffer;

#ifdef Dim3

  for (int k = 1; k < LB_DIMENSION - 1; ++k) {
    int offsetK = LB_DIMENSION * LB_DIMENSION * k;
#endif

  for (int j = 1; j < LB_DIMENSION - 1; ++j) {
    int offsetJ =
#ifdef Dim3
      offsetK +
#endif
      LB_DIMENSION * j;

#ifdef ISPC_BUILD

    for (int direction = 0; direction < LB_DIR / 2; ++direction) {
      int opposite = LB_DIR - 1 - direction;
      ispc::swapCell(&cell.getDir(direction)[offsetJ + 1],
                     &cell.getDir(opposite)[offsetJ + 1],
                     LB_DIMENSION - 2);
    }

#endif

    for (int i = 1; i < LB_DIMENSION - 1; ++i) {
      int index = offsetJ + i;

      // swap opposite directions

#ifndef ISPC_BUILD

      for (int direction = 0; direction < LB_DIR / 2; ++direction) {
        int opposite = LB_DIR - 1 - direction;

        buffer                        = cell.getDir(direction)[index];
        cell.getDir(direction)[index] = cell.getDir(opposite)[index];
        cell.getDir(opposite)[index]  = buffer;
        assert(cell.getDir(opposite)[index] > 0.0);
        assert(cell.getDir(opposite)[index] < 1.0);
      }

#endif

      // now collision
      Vector<LB_DIR, double> feq;
      PointSteps::computeFeq(cell, index, feq);

      PointSteps::computePostCollisionDistribution(cell, index, tau, feq);
    }
  }

#ifdef Dim3
}

#endif

#ifdef Dim3

  for (int k = 1; k < LB_DIMENSION - 1; ++k) {
    int offsetK = LB_DIMENSION * LB_DIMENSION * k;
#endif

  for (int j = 1; j < LB_DIMENSION - 1; ++j) {
    int offsetJ =
#ifdef Dim3
      offsetK +
#endif
      LB_DIMENSION * j;

    for (int i = 1; i < LB_DIMENSION - 1; ++i) {
      int index = offsetJ + i;
      // now stream available part

#ifdef Dim2
      double indexBelow    = index - LB_DIMENSION;
      double indexLeft     = index - 1;
      double indexDiagonal = index - LB_DIMENSION - 1;

      //  0  *
      //  *  0
      // indexLeft     index
      // indexDiagonal indexBelow

      swap(cell, indexDiagonal, NORTH_EAST, index, SOUTH_WEST);

      // *  0
      // *  0
      swap(cell, indexDiagonal, NORTH, indexLeft, SOUTH);

      // 0  0
      // *  *
      swap(cell, indexDiagonal, EAST, indexBelow, WEST);

      // *  0
      // 0  *
      swap(cell, indexBelow, NORTH_WEST, indexLeft, SOUTH_EAST);
#endif

#ifdef Dim3
      //
      //           indexLeft           index (i, j, k)
      //                 *-----------*
      //                /|          /|
      //               / |         / |
      //      indexLef/tFront     /indexFront
      //             *-----------*   |
      //             |   |_____ _|__ * indexBelow
      //       indexB|el/owLeft  |  /
      //             | /         | /
      //             |/          |/
      //             *-----------*
      //   indexBelowLeftFront     indexBelowFront

      int indexLeft      = index - 1;
      int indexFront     = index - LB_DIMENSION;
      int indexLeftFront = index - LB_DIMENSION - 1;

      int indexBelow          = index - LB_DIMENSION * LB_DIMENSION;
      int indexBelowLeft      = index - LB_DIMENSION * LB_DIMENSION - 1;
      int indexBelowFront     = index - LB_DIMENSION * LB_DIMENSION - LB_DIMENSION;
      int indexBelowLeftFront = index - LB_DIMENSION * LB_DIMENSION - LB_DIMENSION - 1;

      swap(cell, indexBelowLeftFront, TOP_EAST, indexFront, BOTTOM_WEST);

      swap(cell, indexBelowLeftFront, TOP_NORTH, indexLeft, BOTTOM_SOUTH);
      swap(cell, indexBelowLeftFront, NORTH_EAST, indexBelow, SOUTH_WEST);

      swap(cell, indexBelowLeftFront, EAST, indexBelowFront, WEST);
      swap(cell, indexBelowLeftFront, NORTH, indexBelowLeft, SOUTH);
      swap(cell, indexBelowLeftFront, TOP, indexLeftFront, BOTTOM);

      swap(cell, indexBelowFront, NORTH_WEST, indexBelowLeft, SOUTH_EAST);
      swap(cell, indexBelowFront, TOP_WEST, indexLeftFront, BOTTOM_EAST);
      swap(cell, indexBelowLeft, TOP_SOUTH, indexLeftFront, BOTTOM_NORTH);
#endif
    }
  }

#ifdef Dim3
}
#endif
}

// ----------------------------------------------------------------------------------------------

#ifdef Dim2
void
CellSteps::
streamCellTop(latticeboltzmann::CellHeap& cell)
{
  int j = LB_DIMENSION - 1;

  for (int i = 1; i < LB_DIMENSION - 1; ++i) {
    int index = j * LB_DIMENSION + i;

    double indexBelow    = index - LB_DIMENSION;
    double indexLeft     = index - 1;
    double indexDiagonal = index - LB_DIMENSION - 1;

    //  0  *
    //  *  0
    swap(cell, indexDiagonal, NORTH_EAST, index, SOUTH_WEST);

    // *  0
    // *  0
    swap(cell, indexDiagonal, NORTH, indexLeft, SOUTH);

    // 0  0
    // *  *
    swap(cell, indexDiagonal, EAST, indexBelow, WEST);

    // *  0
    // 0  *
    swap(cell, indexBelow, NORTH_WEST, indexLeft, SOUTH_EAST);

    // *  *
    // 0  0
    swap(cell, indexLeft, EAST, index, WEST);
  }
}

// ----------------------------------------------------------------------------------------------

void
CellSteps::
streamCellRight(latticeboltzmann::CellHeap& cell)
{
  int i = LB_DIMENSION - 1;

  for (int j = 1; j < LB_DIMENSION; ++j) {
    int index = j * LB_DIMENSION + i;

    double indexBelow    = index - LB_DIMENSION;
    double indexLeft     = index - 1;
    double indexDiagonal = index - LB_DIMENSION - 1;

    //  0  *
    //  *  0
    swap(cell, indexDiagonal, NORTH_EAST, index, SOUTH_WEST);

    // *  0
    // *  0
    swap(cell, indexDiagonal, NORTH, indexLeft, SOUTH);

    // 0  0
    // *  *
    swap(cell, indexDiagonal, EAST, indexBelow, WEST);

    // *  0
    // 0  *
    swap(cell, indexBelow, NORTH_WEST, indexLeft, SOUTH_EAST);

    // 0  *
    // 0  *
    swap(cell, indexBelow, NORTH, index, SOUTH);
  }

  // last corner element
  int index = LB_DIMENSION * LB_DIMENSION - 1;

  // *  *
  // 0  0
  swap(cell, index - 1, EAST, index, WEST);
}
#endif

#ifdef Dim3
void
CellSteps::
streamCellTop3D(CellHeap& cell)
{
  const int k = LB_DIMENSION - 1;

  for (int j = 1; j < LB_DIMENSION - 1; ++j)
    for (int i = 1; i < LB_DIMENSION - 1; ++i) {
      //
      //           indexLeft           index (i, j, k)
      //                 *-----------*
      //                /|          /|
      //               / |         / |
      //      indexLef/tFront     /indexFront
      //             *-----------*   |
      //             |   |_____ _|__ * indexBelow
      //       indexB|el/owLeft  |  /
      //             | /         | /
      //             |/          |/
      //             *-----------*
      //   indexBelowLeftFront     indexBelowFront

      //
      //
      //                 *-----------*
      //                /________   /|
      //               / |      /| / |
      //              / /|     / |/  |
      //             *-----------*   |
      //             |/__|_____|/|   |
      //             |   |_____ _| _ *
      //             |  /        |  /
      //             | /         | /
      //             |/          |/
      //             *-----------*
      //

      int index          = k * LB_DIMENSION * LB_DIMENSION + j * LB_DIMENSION + i;
      int indexLeft      = index - 1;
      int indexFront     = index - LB_DIMENSION;
      int indexLeftFront = index - LB_DIMENSION - 1;

      int indexBelow          = index - LB_DIMENSION * LB_DIMENSION;
      int indexBelowLeft      = index - LB_DIMENSION * LB_DIMENSION - 1;
      int indexBelowFront     = index - LB_DIMENSION * LB_DIMENSION - LB_DIMENSION;
      int indexBelowLeftFront = index - LB_DIMENSION * LB_DIMENSION - LB_DIMENSION - 1;

      swap(cell, indexBelowLeftFront, TOP_EAST, indexFront, BOTTOM_WEST);
      swap(cell, indexBelowLeftFront, TOP_NORTH, indexLeft, BOTTOM_SOUTH);
      swap(cell, indexBelowLeftFront, NORTH_EAST, indexBelow, SOUTH_WEST);

      swap(cell, indexBelowLeftFront, EAST, indexBelowFront, WEST);
      swap(cell, indexBelowLeftFront, NORTH, indexBelowLeft, SOUTH);
      swap(cell, indexBelowLeftFront, TOP, indexLeftFront, BOTTOM);

      swap(cell, indexBelowFront, NORTH_WEST, indexBelowLeft, SOUTH_EAST);
      swap(cell, indexBelowFront, TOP_WEST, indexLeftFront, BOTTOM_EAST);
      swap(cell, indexBelowLeft, TOP_SOUTH, indexLeftFront, BOTTOM_NORTH);

      //      // top
      swap(cell, indexLeftFront, EAST, indexFront, WEST);
      swap(cell, indexLeftFront, NORTH, indexLeft, SOUTH);
      swap(cell, indexLeftFront, NORTH_EAST, index, SOUTH_WEST);
      swap(cell, indexLeft, SOUTH_EAST, indexFront, NORTH_WEST);
    }

}

void
CellSteps::
streamCellRight3D(CellHeap& cell)
{
  const int i = LB_DIMENSION - 1;

  for (int k = 1; k < LB_DIMENSION; ++k)
    for (int j = 1; j < LB_DIMENSION; ++j) {
      //
      //           indexLeft           index (i, j, k)
      //                 *-----------*
      //                /|          /|
      //               / |         / |
      //      indexLef/tFront     /indexFront
      //             *-----------*   |
      //             |   |_____ _|__ * indexBelow
      //       indexB|el/owLeft  |  /
      //             | /         | /
      //             |/          |/
      //             *-----------*
      //   indexBelowLeftFront     indexBelowFront

      //
      //
      //                 *-----------*
      //                /|       /| /|
      //               / |      / |/ |
      //              /  |     /  /  |
      //             *-----------*|  |
      //             |   |     | ||  |
      //             |   |_____|_||_ *
      //             |  /      | |  /
      //             | /       |/| /
      //             |/        | |/
      //             *-----------*
      //

      int index          = k * LB_DIMENSION * LB_DIMENSION + j * LB_DIMENSION + i;
      int indexLeft      = index - 1;
      int indexFront     = index - LB_DIMENSION;
      int indexLeftFront = index - LB_DIMENSION - 1;

      int indexBelow          = index - LB_DIMENSION * LB_DIMENSION;
      int indexBelowLeft      = index - LB_DIMENSION * LB_DIMENSION - 1;
      int indexBelowFront     = index - LB_DIMENSION * LB_DIMENSION - LB_DIMENSION;
      int indexBelowLeftFront = index - LB_DIMENSION * LB_DIMENSION - LB_DIMENSION - 1;

      swap(cell, indexBelowLeftFront, TOP_EAST, indexFront, BOTTOM_WEST);
      swap(cell, indexBelowLeftFront, TOP_NORTH, indexLeft, BOTTOM_SOUTH);
      swap(cell, indexBelowLeftFront, NORTH_EAST, indexBelow, SOUTH_WEST);

      swap(cell, indexBelowLeftFront, EAST, indexBelowFront, WEST);
      swap(cell, indexBelowLeftFront, NORTH, indexBelowLeft, SOUTH);
      swap(cell, indexBelowLeftFront, TOP, indexLeftFront, BOTTOM);

      swap(cell, indexBelowFront, NORTH_WEST, indexBelowLeft, SOUTH_EAST);
      swap(cell, indexBelowFront, TOP_WEST, indexLeftFront, BOTTOM_EAST);
      swap(cell, indexBelowLeft, TOP_SOUTH, indexLeftFront, BOTTOM_NORTH);

      // right
      swap(cell, indexBelowFront, TOP_NORTH, index, BOTTOM_SOUTH);
      swap(cell, indexBelowFront, NORTH, indexBelow, SOUTH);
      swap(cell, indexBelowFront, TOP, indexFront, BOTTOM);
      swap(cell, indexBelow, TOP_SOUTH, indexFront, BOTTOM_NORTH);
    }

  // last columt of cells
  {
    const int i = LB_DIMENSION - 1;
    const int j = LB_DIMENSION - 1;

    for (int k = 1; k < LB_DIMENSION; ++k) {
      int index          = k * LB_DIMENSION * LB_DIMENSION + j * LB_DIMENSION + i;
      int indexLeft      = index - 1;
      int indexBelow     = index - LB_DIMENSION * LB_DIMENSION;
      int indexBelowLeft = index - LB_DIMENSION * LB_DIMENSION - 1;

      swap(cell, index, BOTTOM_WEST, indexBelowLeft, TOP_EAST);
      swap(cell, index, BOTTOM, indexBelow, TOP);

      swap(cell, indexBelow, WEST, indexBelowLeft, EAST);

      swap(cell, indexLeft, BOTTOM_EAST, indexBelow, TOP_WEST);
      swap(cell, indexLeft, BOTTOM, indexBelowLeft, TOP);
    }
  }
  {
    const int i = LB_DIMENSION - 1;
    const int k = LB_DIMENSION - 1;

    for (int j = 1; j < LB_DIMENSION; ++j) {
      int index          = k * LB_DIMENSION * LB_DIMENSION + j * LB_DIMENSION + i;
      int indexLeft      = index - 1;
      int indexFront     = index - LB_DIMENSION;
      int indexLeftFront = index - LB_DIMENSION - 1;

      swap(cell, index, SOUTH, indexFront, NORTH);
      swap(cell, index, SOUTH_WEST, indexLeftFront, NORTH_EAST);

      swap(cell, indexFront, WEST, indexLeftFront, EAST);

      swap(cell, indexLeft, SOUTH, indexLeftFront, NORTH);
      swap(cell, indexLeft, SOUTH_EAST, indexFront, NORTH_WEST);
    }
  }
  {
    int index     = LB_DIMENSION * LB_DIMENSION * LB_DIMENSION - 1;
    int indexLeft = index - 1;
    swap(cell, index, WEST, indexLeft, EAST);
  }
}

void
CellSteps::
streamCellBack3D(CellHeap& cell)
{
  const int j = LB_DIMENSION - 1;

  for (int k = 1; k < LB_DIMENSION; ++k)
    for (int i = 1; i < LB_DIMENSION - 1; ++i) {
      //
      //           indexLeft           index (i, j, k)
      //                 *-----------*
      //                /|          /|
      //               / |         / |
      //      indexLef/tFront     /indexFront
      //             *-----------*   |
      //             |   |_____ _|__ * indexBelow
      //       indexB|el/owLeft  |  /
      //             | /         | /
      //             |/          |/
      //             *-----------*
      //   indexBelowLeftFront     indexBelowFront

      //
      //
      //                 *-------*---*
      //                /_______/ | /|
      //               /||      | |/ |
      //              / ||      | /  |
      //             *-----------*|  |
      //             |  ||      |||  |
      //             |  ||_____ |||_ *
      //             |  /_______|/  /
      //             | /         | /
      //             |/          |/
      //             *-----------*
      //
      int index          = k * LB_DIMENSION * LB_DIMENSION + j * LB_DIMENSION + i;
      int indexLeft      = index - 1;
      int indexFront     = index - LB_DIMENSION;
      int indexLeftFront = index - LB_DIMENSION - 1;

      int indexBelow          = index - LB_DIMENSION * LB_DIMENSION;
      int indexBelowLeft      = index - LB_DIMENSION * LB_DIMENSION - 1;
      int indexBelowFront     = index - LB_DIMENSION * LB_DIMENSION - LB_DIMENSION;
      int indexBelowLeftFront = index - LB_DIMENSION * LB_DIMENSION - LB_DIMENSION - 1;

      swap(cell, indexBelowLeftFront, TOP_EAST, indexFront, BOTTOM_WEST);
      swap(cell, indexBelowLeftFront, TOP_NORTH, indexLeft, BOTTOM_SOUTH);
      swap(cell, indexBelowLeftFront, NORTH_EAST, indexBelow, SOUTH_WEST);

      swap(cell, indexBelowLeftFront, EAST, indexBelowFront, WEST);
      swap(cell, indexBelowLeftFront, NORTH, indexBelowLeft, SOUTH);
      swap(cell, indexBelowLeftFront, TOP, indexLeftFront, BOTTOM);

      swap(cell, indexBelowFront, NORTH_WEST, indexBelowLeft, SOUTH_EAST);
      swap(cell, indexBelowFront, TOP_WEST, indexLeftFront, BOTTOM_EAST);
      swap(cell, indexBelowLeft, TOP_SOUTH, indexLeftFront, BOTTOM_NORTH);

      // back
      swap(cell, indexBelowLeft, TOP_EAST, index, BOTTOM_WEST);
      swap(cell, indexBelowLeft, EAST, indexBelow, WEST);
      swap(cell, indexBelowLeft, TOP, indexLeft, BOTTOM);
      swap(cell, indexBelow, TOP_WEST, indexLeft, BOTTOM_EAST);
    }

  {
    const int k = LB_DIMENSION - 1;
    const int j = LB_DIMENSION - 1;

    for (int i = 1; i < LB_DIMENSION - 1; ++i) {
      int index          = k * LB_DIMENSION * LB_DIMENSION + j * LB_DIMENSION + i;
      int indexLeft      = index - 1;
      int indexFront     = index - LB_DIMENSION;
      int indexLeftFront = index - LB_DIMENSION - 1;

      swap(cell, index, WEST, indexLeft, EAST);
      swap(cell, index, SOUTH_WEST, indexLeftFront, NORTH_EAST);
      swap(cell, indexLeft, SOUTH, indexLeftFront, NORTH);
      swap(cell, indexLeft, SOUTH_EAST, indexFront, NORTH_WEST);
      swap(cell, indexLeftFront, EAST, indexFront, WEST);
    }
  }
}

#endif

std::vector<std::tuple<tarch::la::Vector<DIMENSIONS, double>,
                       latticeboltzmann::Vertex*,
                       latticeboltzmann::Vertex*,
                       latticeboltzmann::Interpolation::Direction> >
CellSteps::
collectPairsOfFineVertices(latticeboltzmann::Vertex*
                           const fineGridVertices,
                           const peano::grid::VertexEnumerator&
                           fineGridVerticesEnumerator)
{
  std::vector<std::tuple<tarch::la::Vector<DIMENSIONS, double>,
                         latticeboltzmann::Vertex*,
                         latticeboltzmann::Vertex*,
                         latticeboltzmann::Interpolation::Direction> >
  pairsOfFineVertices;

  for (int i = 0; i < 4; ++i) {
    latticeboltzmann::Vertex& fineGridVertex =
      fineGridVertices[fineGridVerticesEnumerator(i)];
    tarch::la::Vector<DIMENSIONS,
                      double> fineGridX =
      fineGridVerticesEnumerator.getVertexPosition(i);

    if (fineGridVertex.isHangingNode()) {
      // now we need to find a second hanging vertex in order to
      switch (i) {
        // edge  0 - 1  or 0 - 2
        case 0: {
          latticeboltzmann::Vertex& anotherVertex1 =
            fineGridVertices[fineGridVerticesEnumerator(1)];

          if (anotherVertex1.isHangingNode())
            pairsOfFineVertices.push_back(std::make_tuple(fineGridX,
                                                          &fineGridVertex,
                                                          &anotherVertex1,
                                                          latticeboltzmann::Interpolation::BELOW));

          latticeboltzmann::Vertex& anotherVertex2 =
            fineGridVertices[fineGridVerticesEnumerator(2)];

          if (anotherVertex2.isHangingNode())
            pairsOfFineVertices.push_back(std::make_tuple(fineGridX,
                                                          &fineGridVertex,
                                                          &anotherVertex2,
                                                          latticeboltzmann::Interpolation::LEFT));

          break;
        }

        // edge  1 - 3
        case 1: {
          latticeboltzmann::Vertex& anotherVertex =
            fineGridVertices[fineGridVerticesEnumerator(3)];

          if (anotherVertex.isHangingNode())
            pairsOfFineVertices.push_back(std::make_tuple(fineGridX,
                                                          &fineGridVertex,
                                                          &anotherVertex,
                                                          latticeboltzmann::Interpolation::RIGHT));

          break;
        }

        // edge  2 - 3
        case 2: {
          latticeboltzmann::Vertex& anotherVertex =
            fineGridVertices[fineGridVerticesEnumerator(3)];

          if (anotherVertex.isHangingNode())
            pairsOfFineVertices.push_back(std::make_tuple(fineGridX,
                                                          &fineGridVertex,
                                                          &anotherVertex,
                                                          latticeboltzmann::Interpolation::ABOVE));

          break;
        }
      }
    }
  }

  return pairsOfFineVertices;
}

std::set<latticeboltzmann::Interpolation::Direction>
CellSteps::
collectRestrictionPositions(Vertex* const                        fineGridVertices,
                            const peano::grid::VertexEnumerator& fineGridVerticesEnumerator)
{
  std::set<latticeboltzmann::Interpolation::Direction> restrictionPositions;

  for (int i = 0; i < 4; ++i) {
    latticeboltzmann::Vertex& fineGridVertex =
      fineGridVertices[fineGridVerticesEnumerator(i)];
    tarch::la::Vector<DIMENSIONS,
                      double> fineGridX = fineGridVerticesEnumerator.getVertexPosition(i);

    if (fineGridVertex.isHangingNode()) {
      // now we need to find a second hanging vertex in order to
      switch (i) {
        // edge  0 - 1  or 0 - 2
        case 0: {
          latticeboltzmann::Vertex& anotherVertex1 =
            fineGridVertices[fineGridVerticesEnumerator(1)];

          if (anotherVertex1.isHangingNode())
            restrictionPositions.insert(latticeboltzmann::Interpolation::BELOW);

          latticeboltzmann::Vertex& anotherVertex2 =
            fineGridVertices[fineGridVerticesEnumerator(2)];

          if (anotherVertex2.isHangingNode())
            restrictionPositions.insert(latticeboltzmann::Interpolation::LEFT);

          break;
        }

        // edge  1 - 3
        case 1: {
          latticeboltzmann::Vertex& anotherVertex =
            fineGridVertices[fineGridVerticesEnumerator(3)];

          if (anotherVertex.isHangingNode())
            restrictionPositions.insert(latticeboltzmann::Interpolation::RIGHT);

          break;
        }

        // edge  2 - 3
        case 2: {
          latticeboltzmann::Vertex& anotherVertex =
            fineGridVertices[fineGridVerticesEnumerator(3)];

          if (anotherVertex.isHangingNode())
            restrictionPositions.insert(latticeboltzmann::Interpolation::ABOVE);

          break;
        }
      }
    }
  }

  return restrictionPositions;
}
}
