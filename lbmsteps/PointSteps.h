// This file is part of the Peano project. For conditions of distribution and
// use, please see the copyright notice at www.peano-framework.org
#ifndef _LATTICEBOLTZMANN_POINT_STEPS_H_
#define _LATTICEBOLTZMANN_POINT_STEPS_H_

#include "latticeboltzmann/records/Cell.h"
#include "peano/grid/Cell.h"

#ifdef Dim2
  #include "latticeboltzmann/heap2D/CellHeap.h"
  #include "latticeboltzmann/heap2D/VertexHeap.h"
#endif

#ifdef Dim3
  #include "latticeboltzmann/heap3D/CellHeap.h"
  #include "latticeboltzmann/heap3D/VertexHeap.h"
#endif

#ifdef ISPC_BUILD
  #include "latticeboltzmann/ispc_sources/PointSteps_ispc.h"
  #include "latticeboltzmann/ispc_sources/VertexSteps_ispc.h"
#endif

#ifdef Dim2
  #define DOT(a, b) (a[0] * b[0] + a[1] * b[1])
#endif
#ifdef Dim3
  #define DOT(a, b) (a[0] * b[0] + a[1] * b[1] + a[2] * b[2])
#endif

#include <vector>

using namespace tarch::la;

namespace latticeboltzmann {
class PointSteps;
}

class latticeboltzmann::PointSteps
{
public:
  template <int Size>
  static
  void
  computeDensity(tarch::la::Vector<Size, double>& SW,
                 tarch::la::Vector<Size, double>& S,
                 tarch::la::Vector<Size, double>& SE,
                 tarch::la::Vector<Size, double>& W,
                 tarch::la::Vector<Size, double>& C,
                 tarch::la::Vector<Size, double>& E,
                 tarch::la::Vector<Size, double>& NW,
                 tarch::la::Vector<Size, double>& N,
                 tarch::la::Vector<Size, double>& NE,
#ifdef Dim3
                 tarch::la::Vector<Size, double>& BS,
                 tarch::la::Vector<Size, double>& BW,
                 tarch::la::Vector<Size, double>& B,
                 tarch::la::Vector<Size, double>& BE,
                 tarch::la::Vector<Size, double>& BN,

                 tarch::la::Vector<Size, double>& TS,
                 tarch::la::Vector<Size, double>& TW,
                 tarch::la::Vector<Size, double>& T,
                 tarch::la::Vector<Size, double>& TE,
                 tarch::la::Vector<Size, double>& TN,

#endif
                 const int                   index,
                 double&                     density)
  {
    density = SW[index]
              + S[index]
              + SE[index]
              + W[index]
              + C[index]
              + E[index]
              + NW[index]
              + N[index]
              + NE[index]
#ifdef Dim3
              + BS[index]
              + BW[index]
              + B[index]
              + BE[index]
              + BN[index]
              + TS[index]
              + TW[index]
              + T[index]
              + TE[index]
              + TN[index]
#endif
    ;
  }

  template <int Size>
  static
  void
  computeVelocity(tarch::la::Vector<Size, double>& SW,
                  tarch::la::Vector<Size, double>& S,
                  tarch::la::Vector<Size, double>& SE,
                  tarch::la::Vector<Size, double>& W,
                  tarch::la::Vector<Size, double>& C,
                  tarch::la::Vector<Size, double>& E,
                  tarch::la::Vector<Size, double>& NW,
                  tarch::la::Vector<Size, double>& N,
                  tarch::la::Vector<Size, double>& NE,
#ifdef Dim3
                  tarch::la::Vector<Size, double>& BS,
                  tarch::la::Vector<Size, double>& BW,
                  tarch::la::Vector<Size, double>& B,
                  tarch::la::Vector<Size, double>& BE,
                  tarch::la::Vector<Size, double>& BN,

                  tarch::la::Vector<Size, double>& TS,
                  tarch::la::Vector<Size, double>& TW,
                  tarch::la::Vector<Size, double>& T,
                  tarch::la::Vector<Size, double>& TE,
                  tarch::la::Vector<Size, double>& TN,

#endif
                  const int index,
                  const double density,
                  Vector<DIMENSIONS, double>& velocity)
  {
    velocity[0] = (-SW[index]
                   + SE[index]
                   - W[index]
                   + E[index]
                   - NW[index]
                   + NE[index]
#ifdef Dim3
                   - BW[index]
                   + BE[index]
                   - TW[index]
                   + TE[index]
#endif

                   ) / density;

    velocity[1] = (-SW[index]
                   - S[index]
                   - SE[index]
                   + NW[index]
                   + N[index]
                   + NE[index]
#ifdef Dim3
                   - BS[index]
                   + BN[index]
                   - TS[index]
                   + TN[index]
#endif
                   ) / density;

#ifdef Dim3
    velocity[2] = (-BS[index]
                   - BW[index]
                   - B[index]
                   - BE[index]
                   - BN[index]
                   + TS[index]
                   + TW[index]
                   + T[index]
                   + TE[index]
                   + TN[index]) / density;
#endif
  }

  // -------------------------------------------------------

  static
  void
  computeFeq(const double density,
             const Vector<DIMENSIONS, double>& velocity,
             Vector<LB_DIR, double>& feq);

  template <int Size>
  static
  void
  computeFeq(tarch::la::Vector<Size, double>& SW,
             tarch::la::Vector<Size, double>& S,
             tarch::la::Vector<Size, double>& SE,
             tarch::la::Vector<Size, double>& W,
             tarch::la::Vector<Size, double>& C,
             tarch::la::Vector<Size, double>& E,
             tarch::la::Vector<Size, double>& NW,
             tarch::la::Vector<Size, double>& N,
             tarch::la::Vector<Size, double>& NE,
#ifdef Dim3
             tarch::la::Vector<Size, double>& BS,
             tarch::la::Vector<Size, double>& BW,
             tarch::la::Vector<Size, double>& B,
             tarch::la::Vector<Size, double>& BE,
             tarch::la::Vector<Size, double>& BN,

             tarch::la::Vector<Size, double>& TS,
             tarch::la::Vector<Size, double>& TW,
             tarch::la::Vector<Size, double>& T,
             tarch::la::Vector<Size, double>& TE,
             tarch::la::Vector<Size, double>& TN,

#endif
             int index,
             Vector<LB_DIR, double>& feq)
  {
    double density;
    PointSteps::computeDensity(SW, S, SE,
                               W, C, E,
                               NW, N, NE,
#ifdef Dim3
                               BS, BW, B, BE, BN,
                               TS, TW, T, TE, TN,
#endif
                               index, density);

#ifdef Dim2
    Vector<DIMENSIONS, double> velocity(0.0, 0.0);

#endif

#ifdef Dim3
    Vector<DIMENSIONS, double> velocity(0.0, 0.0, 0.0);

#endif

    PointSteps::computeVelocity(SW, S, SE,
                                W, C, E,
                                NW, N, NE,
#ifdef Dim3
                                BS, BW, B, BE, BN,
                                TS, TW, T, TE, TN,
#endif
                                index, density, velocity);

    PointSteps::computeFeq(density, velocity, feq);
  }

  /** Compute equilibrium distributions at node "index" */
  static
  void
  computeFeq(latticeboltzmann::CellHeap& cell,
             int index,
             Vector<LB_DIR, double>& feq);

  /** Compute equilibrium distributions at node "index" for vertex */
  static
  void
  computeFeq(latticeboltzmann::VertexHeap& vertex,
             int index,
             Vector<LB_DIR, double>& feq);

  static
  void
  computePostCollisionDistribution(CellHeap& cell,
                                   const int index,
                                   const double tau,
                                   const Vector<LB_DIR, double>& feq);
};

#endif
