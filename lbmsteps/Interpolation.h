// This file is part of the Peano project. For conditions of distribution and
// use, please see the copyright notice at www.peano-framework.org
#ifndef _LATTICEBOLTZMANN_INTERPOLATION_H_
#define _LATTICEBOLTZMANN_INTERPOLATION_H_

#include "latticeboltzmann/records/Vertex.h"
#include "peano/grid/Vertex.h"
#include "peano/grid/VertexEnumerator.h"
#include "peano/utils/Globals.h"

#include "latticeboltzmann/Vertex.h"
#ifdef Dim2
  #include "latticeboltzmann/heap2D/VertexHeap.h"
#endif

#ifdef Dim3
  #include "latticeboltzmann/heap3D/VertexHeap.h"
#endif

#include "latticeboltzmann/State.h"

#include "peano/grid/VertexEnumerator.h"
#include "peano/heap/Heap.h"

#include "latticeboltzmann/lbmsteps/PointSteps.h"

#include <tuple>
#include <vector>

using namespace tarch::la;

namespace latticeboltzmann {
class Interpolation
{
public:
  typedef tarch::la::Vector<VERTEX_BLOCK_SIZE, double> VertexArray;

  enum Direction { RIGHT, ABOVE, LEFT, BELOW };

  static
  void
  interpolateHangingVerticesCoarseToFine(int                                  level,
                                         latticeboltzmann::Vertex* const      fineGridVertices,
                                         const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
                                         latticeboltzmann::Vertex* const      coarseGridVertices,
                                         const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator);

  static
  void
  interpolateVertices(Direction direction,
                      int level,
                      latticeboltzmann::Vertex* fineGridVertex1,
                      latticeboltzmann::Vertex* fineGridVertex2,
                      const tarch::la::Vector<DIMENSIONS, double>& fineGridX,
                      std::tuple<latticeboltzmann::Vertex*, latticeboltzmann::Vertex*> coarseVertices,
                      tarch::la::Vector<DIMENSIONS, double> coarseVertex1Position);

  static
  void
  rescaleFromCoarseToFine(Vector<LB_DIR, Vector<LB_DIMENSION, double> >& dirs,
                          double tauFine, double tauCoarse);

  static
  void
  interpolateSpaciallyVertical(VertexHeap& array1,
                               VertexHeap& array2,
                               double dyFine,
                               double dyCoarse,
                               const tarch::la::Vector<DIMENSIONS, double>& fineGridX,
                               tarch::la::Vector<DIMENSIONS, double>& coarseVertex1Position,
                               tarch::la::Vector<LB_DIR, tarch::la::Vector<LB_DIMENSION, double> >& dirs,
                               int direction);

  static
  void
  interpolateSpaciallyHorizontal(VertexHeap& array1,
                                 VertexHeap& array2,
                                 double dxFine,
                                 double dxCoarse,
                                 const tarch::la::Vector<DIMENSIONS, double>& fineGridX,
                                 tarch::la::Vector<DIMENSIONS, double>& coarseVertex1Position,
                                 tarch::la::Vector<LB_DIR, tarch::la::Vector<LB_DIMENSION, double> >& dirs,
                                 int direction);

  static
  void
  timeInterpolation(int level,
                    std::tuple<latticeboltzmann::Vertex*, latticeboltzmann::Vertex*> coarseVertices,
                    double timestep,
                    tarch::la::Vector<LB_DIR, tarch::la::Vector<LB_DIMENSION, double> >& dirs,
                    Direction                          direction);

  static
  void
  stitchVerticalArrays(VertexHeap& array1,
                       VertexHeap& array2,
                       Vector<LB_DIR, Vector<LB_DIMENSION, double> >& dirs);

  static
  void
  stitchHorizontalArrays(VertexHeap& array1,
                         VertexHeap& array2,
                         tarch::la::Vector<LB_DIR, tarch::la::Vector<LB_DIMENSION, double> >& dirs);

  static
  std::tuple<latticeboltzmann::Vertex*, latticeboltzmann::Vertex*>
  getPairOfCoarseVertices(Direction direction,
                          const tarch::la::Vector<DIMENSIONS, double>& fineGridX,
                          latticeboltzmann::Vertex* const      coarseGridVertices,
                          const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
                          tarch::la::Vector<DIMENSIONS, double>& vertex1Position);
};
}

#endif
