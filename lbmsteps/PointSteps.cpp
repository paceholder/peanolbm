#include "PointSteps.h"

namespace latticeboltzmann {
void
PointSteps::
computeFeq(const double density,
           const Vector<DIMENSIONS, double>& velocity,
           Vector<LB_DIR, double>& feq)
{
  const double CS_2     = 1.0 / 3.0;
  const double CS_4_x_2 = 2 * CS_2 * CS_2;
  const double CS_2_x_2 = 2 * CS_2;

  double CU_SW = DOT(velocity, VELOCITIES[SOUTH_WEST]);
  double CU_S  = DOT(velocity, VELOCITIES[SOUTH]);
  double CU_SE = DOT(velocity, VELOCITIES[SOUTH_EAST]);
  double CU_W  = DOT(velocity, VELOCITIES[WEST]);

#ifdef Dim3
  double CU_BS = DOT(velocity, VELOCITIES[SOUTH_WEST]);
  double CU_BE = DOT(velocity, VELOCITIES[SOUTH]);
  double CU_B  = DOT(velocity, VELOCITIES[SOUTH_EAST]);
  double CU_BW = DOT(velocity, VELOCITIES[WEST]);
  double CU_BN = DOT(velocity, VELOCITIES[WEST]);
#endif

#ifdef Dim2
  std::vector<double> feqCoefficients({ CU_SW, CU_S, CU_SE,
                                        CU_W, 0, -CU_W,
                                        -CU_SE, -CU_S, -CU_SW }
                                      );

#endif
#ifdef Dim3
  std::vector<double> feqCoefficients({ CU_BS, CU_BW, CU_B, CU_BE, CU_BN,
                                        CU_SW, CU_S, CU_SE,
                                        CU_W, 0, -CU_W,
                                        -CU_SE, -CU_S, -CU_SW,
                                        -CU_BN, -CU_BE, -CU_B, -CU_BW, -CU_BS }
                                      );

#endif

  double UU = 1.0 - DOT(velocity, velocity) / CS_2_x_2;

#define FEQ(a) ((a) / CS_2 + (a) * (a) / CS_4_x_2 + UU)

#ifdef ISPC_BUILD

  ispc::computeFeq(&feq[0],
                   feqCoefficients.data(),
                   LB_DIR,
                   WEIGHTS,
                   density,
                   UU);

#else

  for (int dir = 0; dir < LB_DIR; ++dir)
    feq[dir] = WEIGHTS[dir]* density* FEQ(feqCoefficients[dir]);

#endif
}

void
PointSteps::
computeFeq(latticeboltzmann::CellHeap& cell,
           int index,
           Vector<LB_DIR, double>& feq)
{
  computeFeq(cell.getDir(SOUTH_WEST),
             cell.getDir(SOUTH),
             cell.getDir(SOUTH_EAST),
             cell.getDir(WEST),
             cell.getDir(CENTER),
             cell.getDir(EAST),
             cell.getDir(NORTH_WEST),
             cell.getDir(NORTH),
             cell.getDir(NORTH_EAST),
#ifdef Dim3
             cell.getDir(BOTTOM_SOUTH),
             cell.getDir(BOTTOM_WEST),
             cell.getDir(BOTTOM),
             cell.getDir(BOTTOM_EAST),
             cell.getDir(BOTTOM_NORTH),
             cell.getDir(TOP_SOUTH),
             cell.getDir(TOP_WEST),
             cell.getDir(TOP),
             cell.getDir(TOP_EAST),
             cell.getDir(TOP_NORTH),
#endif
             index,
             feq);
}

void
PointSteps::
computeFeq(latticeboltzmann::VertexHeap& vertex,
           int index,
           Vector<LB_DIR, double>& feq)
{
  computeFeq(vertex.getDir(SOUTH_WEST),
             vertex.getDir(SOUTH),
             vertex.getDir(SOUTH_EAST),
             vertex.getDir(WEST),
             vertex.getDir(CENTER),
             vertex.getDir(EAST),
             vertex.getDir(NORTH_WEST),
             vertex.getDir(NORTH),
             vertex.getDir(NORTH_EAST),
#ifdef Dim3
             vertex.getDir(BOTTOM_SOUTH),
             vertex.getDir(BOTTOM_WEST),
             vertex.getDir(BOTTOM),
             vertex.getDir(BOTTOM_EAST),
             vertex.getDir(BOTTOM_NORTH),
             vertex.getDir(TOP_SOUTH),
             vertex.getDir(TOP_WEST),
             vertex.getDir(TOP),
             vertex.getDir(TOP_EAST),
             vertex.getDir(TOP_NORTH),
#endif
             index,
             feq);
}

void
PointSteps::
computePostCollisionDistribution(CellHeap& cell,
                                 const int index,
                                 const double tau,
                                 const Vector<LB_DIR, double>& feq)
{
  const double tauRecp = 1.0 / tau;

  for (int direction = 0; direction < LB_DIR; ++direction) {
    cell.getDir(direction)[index] -= tauRecp * (cell.getDir(direction)[index] - feq[direction]);
    assert(cell.getDir(direction)[index] > 0.0);
    assert(cell.getDir(direction)[index]  < 1.0);
  }
}
}
