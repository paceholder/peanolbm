#include "Restriction.h"

#include "latticeboltzmann/Vertex.h"
#include "lbmsteps/CellSteps.h"
#include "lbmsteps/PointSteps.h"

#include "latticeboltzmann/Cell.h"

#include "latticeboltzmann/LBDefinitions.h"

namespace latticeboltzmann {
void
Restriction::
restrictHangingVerticesCoarseToFine(int                                  level,
                                    Vertex* const                        fineGridVertices,
                                    const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
                                    latticeboltzmann::Cell&              fineGridCell,
                                    Vertex* const                        coarseGridVertices,
                                    const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
                                    latticeboltzmann::Cell&              coarseGridCell)
{
  std::set<latticeboltzmann::Interpolation::Direction> restrictionPositions =
    latticeboltzmann::CellSteps::collectRestrictionPositions(fineGridVertices,
                                                             fineGridVerticesEnumerator);

  // nothing to restrict
  if (restrictionPositions.size() == 0)
    return;

  Restriction::restrictVertices(level,
                                restrictionPositions,
                                fineGridVertices,
                                fineGridVerticesEnumerator,
                                fineGridCell,
                                coarseGridVertices,
                                coarseGridVerticesEnumerator,
                                coarseGridCell);
}

void
Restriction::
restrictVertices(int                                                  fineLevel,
                 std::set<latticeboltzmann::Interpolation::Direction> restrictionPositions,
                 Vertex* const                                        fineGridVertices,
                 const peano::grid::VertexEnumerator&                 fineGridVerticesEnumerator,
                 latticeboltzmann::Cell&                              fineGridCell,
                 Vertex* const                                        coarseGridVertices,
                 const peano::grid::VertexEnumerator&                 coarseGridVerticesEnumerator,
                 latticeboltzmann::Cell&                              coarseGridCell)
{
  // container to store interpolated data
  tarch::la::Vector<LB_DIMENSION, double> v(0.0);

  tarch::la::Vector<LB_DIR, tarch::la::Vector<LB_DIMENSION, double> > dirs(LB_DIR, v);

  // for current fine cell
  double peanoCellSize = 1.0 / pow(3.0, fineLevel - 1);
  double dyFine        = peanoCellSize / double(LB_DIMENSION - 1);
  double dxFine        = dyFine;

  // for coarse Cell
  double coarseCellHeight = 1.0 / pow(3.0, fineLevel - 2);
  double dyCoarse         = coarseCellHeight / double(LB_DIMENSION - 1);
  double dxCoarse         = dyCoarse;

  std::vector<CellHeap>& fineCellData =
    peano::heap::Heap<CellHeap>::getInstance().getData(fineGridCell.getAggregateWithArbitraryCardinality());
  CellHeap& fineCellArray = fineCellData[0];

  std::vector<CellHeap>& coarseCellData =
    peano::heap::Heap<CellHeap>::getInstance().getData(coarseGridCell.getAggregateWithArbitraryCardinality());
  CellHeap& coarseCellArray = coarseCellData[0];

  Vector<DIMENSIONS, double> finePos   = fineGridVerticesEnumerator.getVertexPosition(0);
  Vector<DIMENSIONS, double> coarsePos = coarseGridVerticesEnumerator.getVertexPosition(0);

  restrictCenter(fineLevel,
                 fineCellArray,
                 finePos,
                 coarseCellArray,
                 coarsePos);

  double tauFine   = State::getTau(fineLevel);
  double tauCoarse = State::getTau(fineLevel - 1);

  // go through each selected edge "position" and perform restriction
  for (auto position = restrictionPositions.begin(); position != restrictionPositions.end(); ++position) {
    switch (*position) {
      case Interpolation::BELOW: {
        int    restrictionRow = 3; // offset +3 rows inside cell
        double y              = coarsePos(1) + restrictionRow * dyFine;
        int    yk             = int((y - coarsePos(1)) / dyFine + 0.5);
        int    yc             = int((y - coarsePos(1)) / dyCoarse + 0.5);

        // if no left edge to restrict then fine grid continues to the left
        // we must restrict on the left vertex as well
        if (restrictionPositions.find(Interpolation::LEFT) == restrictionPositions.end()) {
          double x  = finePos(0) + 0 * dyFine;
          int    xc = int((x - coarsePos(0)) / dyCoarse + 0.5);

          Vertex& fineVertex   = fineGridVertices[fineGridVerticesEnumerator(0)];
          Vertex& coarseVertex = coarseGridVertices[coarseGridVerticesEnumerator(0)];
          restrictEdgeVertical(fineLevel,
                               fineVertex, restrictionRow,
                               coarseCellArray,
                               coarseVertex,
                               xc, yc);
        }

        // if no right edge to restrict then fine grid continues to the right
        // we must restrict on the right vertex as well
        if (restrictionPositions.find(Interpolation::RIGHT) == restrictionPositions.end()) {
          double x  = finePos(0) + (LB_DIMENSION - 1) * dyFine;
          int    xc = int((x - coarsePos(0)) / dyCoarse + 0.5);

          Vertex& fineVertex   = fineGridVertices[fineGridVerticesEnumerator(1)];
          Vertex& coarseVertex = coarseGridVertices[coarseGridVerticesEnumerator(1)];
          restrictEdgeVertical(fineLevel,
                               fineVertex, restrictionRow,
                               coarseCellArray,
                               coarseVertex,
                               xc, yc);
        }

        break;
      }

      case Interpolation::ABOVE: {
        // row of fine nodes in coarse cell
        int    restrictionRow = 3 * (LB_DIMENSION - 1) - 3; // offset 3 rows towards grid center
        double y              = coarsePos(1) + restrictionRow * dyFine;
        int    yk             = int((y - coarsePos(1)) / dyFine + 0.5);
        int    yc             = int((y - coarsePos(1)) / dyCoarse + 0.5);

        // check left edge
        if (restrictionPositions.find(Interpolation::LEFT) == restrictionPositions.end()) {
          double x  = finePos(0) + 0 * dyFine;
          int    xc = int((x - coarsePos(0)) / dyCoarse + 0.5);

          Vertex& fineVertex   = fineGridVertices[fineGridVerticesEnumerator(0)];
          Vertex& coarseVertex = coarseGridVertices[coarseGridVerticesEnumerator(0)];
          restrictEdgeVertical(fineLevel,
                               fineVertex,
                               restrictionRow - 2 * (LB_DIMENSION - 1),
                               coarseCellArray,
                               coarseVertex,
                               xc, yc
                               );
        }

        if (restrictionPositions.find(Interpolation::RIGHT) == restrictionPositions.end()) {
          double x  = finePos(0) + (LB_DIMENSION - 1) * dyFine;
          int    xc = int((x - coarsePos(0)) / dyCoarse + 0.5);

          Vertex& fineVertex   = fineGridVertices[fineGridVerticesEnumerator(1)];
          Vertex& coarseVertex = coarseGridVertices[coarseGridVerticesEnumerator(1)];
          restrictEdgeVertical(fineLevel,
                               fineVertex,
                               restrictionRow - 2 * (LB_DIMENSION - 1),
                               coarseCellArray,
                               coarseVertex,
                               xc, yc
                               );
        }

        break;
      } // ABOVE

      case Interpolation::LEFT: {
        int    restrictionColumn = 3;
        double x                 = coarsePos(0) + restrictionColumn * dxFine;
        int    xk                = int((x - coarsePos(0)) / dxFine + 0.5);
        int    xc                = int((x - coarsePos(0)) / dxCoarse + 0.5);

        if (restrictionPositions.find(Interpolation::BELOW) == restrictionPositions.end()) {
          double y  = finePos(1) + 0 * dyFine;
          double yc = int((y - coarsePos(1)) / dyCoarse + 0.5);

          Vertex& fineVertex   = fineGridVertices[fineGridVerticesEnumerator(0)];
          Vertex& coarseVertex = coarseGridVertices[coarseGridVerticesEnumerator(0)];
          restrictEdgeHorizontal(fineLevel,
                                 fineVertex, restrictionColumn,
                                 coarseCellArray,
                                 coarseVertex,
                                 xc, yc);
        }

        if (restrictionPositions.find(Interpolation::ABOVE) == restrictionPositions.end()) {
          double y  = finePos(1) + (LB_DIMENSION - 1) * dyFine;
          double yc = int((y - coarsePos(1)) / dyCoarse + 0.5);

          Vertex& fineVertex   = fineGridVertices[fineGridVerticesEnumerator(2)];
          Vertex& coarseVertex = coarseGridVertices[coarseGridVerticesEnumerator(2)];
          restrictEdgeHorizontal(fineLevel,
                                 fineVertex, restrictionColumn,
                                 coarseCellArray,
                                 coarseVertex,
                                 xc, yc);
        }

        break;
      }

      case Interpolation::RIGHT: {
        int    restrictionColumn = 3 * (LB_DIMENSION - 1) - 3;
        double x                 = coarsePos(0) + restrictionColumn * dxFine;
        int    xk                = int((x - coarsePos(0)) / dxFine + 0.5);
        int    xc                = int((x - coarsePos(0)) / dxCoarse + 0.5);

        if (restrictionPositions.find(Interpolation::BELOW) == restrictionPositions.end()) {
          double y  = finePos(1) + 0 * dyFine;
          double yc = int((y - coarsePos(1)) / dyCoarse + 0.5);

          Vertex& fineVertex   = fineGridVertices[fineGridVerticesEnumerator(0)];
          Vertex& coarseVertex = coarseGridVertices[coarseGridVerticesEnumerator(0)];
          restrictEdgeHorizontal(fineLevel,
                                 fineVertex,
                                 restrictionColumn - 2 * (LB_DIMENSION - 1),
                                 coarseCellArray,
                                 coarseVertex,
                                 xc, yc);
        }

        if (restrictionPositions.find(Interpolation::ABOVE) == restrictionPositions.end()) {
          double y  = finePos(1) + (LB_DIMENSION - 1) * dyFine;
          double yc = int((y - coarsePos(1)) / dyCoarse + 0.5);

          Vertex& fineVertex   = fineGridVertices[fineGridVerticesEnumerator(2)];
          Vertex& coarseVertex = coarseGridVertices[coarseGridVerticesEnumerator(2)];
          restrictEdgeHorizontal(fineLevel,
                                 fineVertex,
                                 restrictionColumn - 2 * (LB_DIMENSION - 1),
                                 coarseCellArray,
                                 coarseVertex,
                                 xc, yc);
        }

        break;
      }
    } // switch
  }
}

void
Restriction::
restrictEdgeHorizontal(int       fineLevel,
                       Vertex&   fineVertex,
                       int       restrictionColumn,
                       CellHeap& coarseCellArray,
                       Vertex&   coarseVertex,
                       int       xc,
                       int       yc)
{
  int fineVertexArrayIndex   = (State::lastArray[fineLevel] + 1) % 2;
  int coarseVertexArrayIndex = State::lastArray[fineLevel - 1];

  std::vector<VertexHeap>& fineVertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(fineVertex.getAggregateWithArbitraryCardinality());
  VertexHeap& fineVertexArray = fineVertexData[fineVertexArrayIndex];

  std::vector<VertexHeap>& coarseVertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(coarseVertex.getAggregateWithArbitraryCardinality());
  VertexHeap& coarseVertexArray = coarseVertexData[coarseVertexArrayIndex];

  Vector<LB_DIR, double> feq;
  PointSteps::computeFeq(fineVertexArray,
                         restrictionColumn,
                         feq);

  double tauFine   = State::getTau(fineLevel);
  double tauCoarse = State::getTau(fineLevel - 1);

  // distributions from left or right edge go to coarse vertices
  if (yc == 0 || yc == LB_DIMENSION - 1)
    for (int dir = 0; dir < LB_DIR; ++dir) {
      double rescaledValue = feq[dir] +
                             (fineVertexArray.getDir(dir)[restrictionColumn] -
                              feq[dir]) * 3 * tauCoarse / tauFine;
      assert(rescaledValue > 0.0);
      coarseVertexArray.getDir(dir)[xc] = rescaledValue;
    }

  // distributions from the center go to cell array
  else {
    int destinationIndex = xc + yc * LB_DIMENSION;

    for (int dir = 0; dir < LB_DIR; ++dir) {
      double rescaledValue = feq[dir] +
                             (fineVertexArray.getDir(dir)[restrictionColumn] -
                              feq[dir]) * 3 * tauCoarse / tauFine;
      //       rescaledValue = 0.2;
      assert(rescaledValue > 0.0);
      coarseCellArray.getDir(dir)[destinationIndex] = rescaledValue;
    }
  }
}

void
Restriction::
restrictEdgeVertical(int       fineLevel,
                     Vertex&   fineVertex,
                     int       restrictionRow,
                     CellHeap& coarseCellArray,
                     Vertex&   coarseVertex,
                     int       xc,
                     int       yc)
{
  int fineVertexArrayIndex   = (State::lastArray[fineLevel] + 1) % 2;
  int coarseVertexArrayIndex = State::lastArray[fineLevel - 1];

  std::vector<VertexHeap>& fineVertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(fineVertex.
                                                         getAggregateWithArbitraryCardinality());
  VertexHeap& fineVertexArray = fineVertexData[fineVertexArrayIndex];

  std::vector<VertexHeap>& coarseVertexData =
    peano::heap::Heap<VertexHeap>::getInstance().getData(coarseVertex.
                                                         getAggregateWithArbitraryCardinality());
  VertexHeap& coarseVertexArray = coarseVertexData[coarseVertexArrayIndex];

  // offset to access vertical part of an array
  int offset = LB_DIMENSION - 1;

  Vector<LB_DIR, double> feq;
  PointSteps::computeFeq(fineVertexArray,
                         offset + restrictionRow,
                         feq);

  double tauFine   = State::getTau(fineLevel);
  double tauCoarse = State::getTau(fineLevel - 1);

  // distributions from left or right edge go to coarse vertices
  if (xc == 0 || xc == LB_DIMENSION - 1) {
    int destinationIndex = offset + yc;

    for (int dir = 0; dir < LB_DIR; ++dir) {
      double rescaledValue = feq[dir] +
                             (fineVertexArray.getDir(dir)[offset + restrictionRow] -
                              feq[dir]) * 3 * tauCoarse / tauFine;
      assert(rescaledValue > 0.0);
      coarseVertexArray.getDir(dir)[destinationIndex] = rescaledValue;
    }

    // distributions from the center go to cell array
  } else {
    int destinationIndex = xc + yc * LB_DIMENSION;

    for (int dir = 0; dir < LB_DIR; ++dir) {
      double rescaledValue = feq[dir] +
                             (fineVertexArray.getDir(dir)[offset + restrictionRow] -
                              feq[dir]) * 3 * tauCoarse / tauFine;
      //       rescaledValue = 0.2;
      assert(rescaledValue > 0.0);
      coarseCellArray.getDir(dir)[destinationIndex] = rescaledValue;
    }
  }
}

void
Restriction::
restrictCenter(int fineLevel,
               latticeboltzmann::CellHeap& fineCellData,
               Vector<DIMENSIONS, double> finePos,
               latticeboltzmann::CellHeap& coarseCellData,
               Vector<DIMENSIONS, double> coarsePos)
{
  double fineCellSize = 1.0 / pow(3.0, fineLevel - 1);
  double dyFine       = fineCellSize / double(LB_DIMENSION - 1);

  // for coarse Cell
  double coarseCellSize = 1.0 / pow(3.0, fineLevel - 2);
  double dyCoarse       = coarseCellSize / double(LB_DIMENSION - 1);

  double tauFine   = State::getTau(fineLevel);
  double tauCoarse = State::getTau(fineLevel - 1);

  // do not process boundary
  for (int j = 1; j < LB_DIMENSION - 1; ++j)
    for (int i = 1; i < LB_DIMENSION - 1; ++i) {
      // position of current fine LB point
      double x = finePos(0) + i * dyFine;
      double y = finePos(1) + j * dyFine;

      // round position
      int xk = int((x - coarsePos(0)) / dyFine + 0.5);
      int xc = int((x - coarsePos(0)) / dyCoarse + 0.5);
      int yk = int((y - coarsePos(1)) / dyFine + 0.5);
      int yc = int((y - coarsePos(1)) / dyCoarse + 0.5);

      // assume we have LB_DIMENSION = 4
      //
      // 0       1       2       3       4       5       6       7       8
      // *-------*-------*-------*-------*-------*-------*-------*-------*-------*
      //                    fineGridx(1)
      // 0                       1                       2                       3
      // *-----------------------*-----------------------*-----------------------*
      //  \
      //   coarsePoint()                 /\
      //                                  y
      //  in this case xk = 4; xc = 1

      // it corresponds to the coarse point
      if (xk % 3 == 0 && yk % 3 == 0) {
        int fineIndex   = i + j * LB_DIMENSION;
        int coarseIndex = xc + yc * LB_DIMENSION;

        Vector<LB_DIR, double> feq;
        PointSteps::computeFeq(fineCellData,
                               fineIndex,
                               feq);

        for (int dir = 0; dir < LB_DIR; ++dir) {
          double rescaledValue = feq[dir] +
                                 (fineCellData.getDir(dir)[fineIndex] -
                                  feq[dir]) * 3 * tauCoarse / tauFine;
          assert(rescaledValue > 0.0);
          coarseCellData.getDir(dir)[coarseIndex] = rescaledValue;
        }
      }
    }

}

void
Restriction::
stitchArrays(int level,
             latticeboltzmann::Vertex* fineVertex1,
             latticeboltzmann::Vertex* fineVertex2,
             tarch::la::Vector<LB_DIR,
                               tarch::la::Vector<LB_DIMENSION, double> >& stitchedDirs,
             Interpolation::Direction                          direction)
{
  int vertexArrayLastIndex = (State::lastArray[level] + 1) % 2;

  // take heap data structure
  std::vector<VertexHeap>& vertexData1 =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      fineVertex1->getAggregateWithArbitraryCardinality());

  std::vector<VertexHeap>& vertexData2 =
    peano::heap::Heap<VertexHeap>::getInstance().getData(
      fineVertex2->getAggregateWithArbitraryCardinality());

  latticeboltzmann::VertexHeap& array1 = vertexData1[vertexArrayLastIndex];
  latticeboltzmann::VertexHeap& array2 = vertexData2[vertexArrayLastIndex];

  if (direction == Interpolation::LEFT || direction == Interpolation::RIGHT)
    Interpolation::stitchVerticalArrays(array1, array2, stitchedDirs);

  if (direction == Interpolation::ABOVE || direction == Interpolation::BELOW)
    Interpolation::stitchHorizontalArrays(array1, array2, stitchedDirs);
}

void
Restriction::
rescaleFromFineToCoarse(Vector<LB_DIR, Vector<LB_DIMENSION, double> >& dirs,
                        double tauFine, double tauCoarse)
{
  for (int i = 0; i < LB_DIMENSION; ++i) {
    Vector<LB_DIR, double> feq;
    PointSteps::computeFeq(dirs[SOUTH_WEST],
                           dirs[SOUTH],
                           dirs[SOUTH_EAST],
                           dirs[WEST],
                           dirs[CENTER],
                           dirs[EAST],
                           dirs[NORTH_WEST],
                           dirs[NORTH],
                           dirs[NORTH_EAST],
#ifdef Dim3
                           dirs[BOTTOM_SOUTH],
                           dirs[BOTTOM_WEST],
                           dirs[BOTTOM],
                           dirs[BOTTOM_EAST],
                           dirs[BOTTOM_NORTH],

                           dirs[TOP_SOUTH],
                           dirs[TOP_WEST],
                           dirs[TOP],
                           dirs[TOP_EAST],
                           dirs[TOP_NORTH],
#endif
                           i,
                           feq);

    for (int direction = 0; direction < LB_DIR; ++direction) {
      dirs[direction][i] = feq[direction] +
                           (dirs[direction][i] - feq[direction]) * 3 * tauCoarse / tauFine;
      assert(dirs[direction][i] > 0.0);
    }
  }
}
}
