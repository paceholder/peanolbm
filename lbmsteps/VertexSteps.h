// This file is part of the Peano project. For conditions of distribution and
// use, please see the copyright notice at www.peano-framework.org
#ifndef _LATTICEBOLTZMANN_VERTEX_STEPS_H_
#define _LATTICEBOLTZMANN_VERTEX_STEPS_H_

#include "latticeboltzmann/records/Vertex.h"
#include "peano/grid/Vertex.h"
#include "peano/grid/VertexEnumerator.h"
#include "peano/utils/Globals.h"

#include "latticeboltzmann/Vertex.h"
#ifdef Dim2
  #include "latticeboltzmann/heap2D/VertexHeap.h"
#endif

#ifdef Dim3
  #include "latticeboltzmann/heap3D/VertexHeap.h"
#endif

#include "latticeboltzmann/State.h"

#include "peano/grid/VertexEnumerator.h"
#include "peano/heap/Heap.h"

#ifdef ISPC_BUILD
  #include "latticeboltzmann/ispc_sources/VertexSteps_ispc.h"
#endif

#include <vector>

#ifdef Dim2
  #define DOT(a, b) (a[0] * b[0] + a[1] * b[1])
#endif
#ifdef Dim3
  #define DOT(a, b) (a[0] * b[0] + a[1] * b[1] + a[2] * b[2])
#endif

using namespace tarch::la;

namespace latticeboltzmann {
class VertexSteps;
}

class latticeboltzmann::VertexSteps
{
public:
  typedef tarch::la::Vector<VERTEX_BLOCK_SIZE, double> VertexArray;

  /** Universal function for processing hanging and normal nodes.
   * It swaps opposite distributions, makes collision step, applies boundary conditions (moving wall)*/
  static
  void
  computeVertex(latticeboltzmann::Vertex& fineGridVertex,
                const tarch::la::Vector<DIMENSIONS, double>& fineGridX,
                const tarch::la::Vector<DIMENSIONS, double>& fineGridH,
                latticeboltzmann::Vertex* const   coarseGridVertices,
                const peano::grid::VertexEnumerator&      coarseGridVerticesEnumerator,
                latticeboltzmann::Cell&       coarseGridCell,
                const tarch::la::Vector<DIMENSIONS, int>&   fineGridPositionOfVertex)
  {
    if (fineGridX(0) < -0.001 || fineGridX(0) > 1.001 ||
        fineGridX(1) < -0.001 || fineGridX(1) > 1.001)
      return;

    int level = coarseGridVerticesEnumerator.getLevel() + 1;

    std::vector<VertexHeap>& vertexData =
      peano::heap::Heap<VertexHeap>::getInstance().getData(
        fineGridVertex.getAggregateWithArbitraryCardinality());

    int srcArrayIndex  = State::lastArray[level];
    int destArrayIndex = (State::lastArray[level] + 1) % 2;

    VertexHeap& srcArray  = vertexData[srcArrayIndex];
    VertexHeap& destArray = vertexData[destArrayIndex];

    // copy data from src to dest array with swapping the direcitons
    latticeboltzmann::VertexSteps::swapVertex(srcArray, destArray);

    // !!!! THIS IS CRITICAL for adaptive grid configuration
    // If there is just finest grid near the moving wall then we use "if (level == FINE_REFINEMENT_LEVEL)"
    // If there is coarse grid near the moving wall then we use "if (level == COARSE_REFINEMENT_LEVEL)"
//    if (level == FINE_REFINEMENT_LEVEL)
      latticeboltzmann::VertexSteps::boundaryConditionVertex(destArray, fineGridX);

    // returns tau for current level
    double tau = State::getTau(level);

    latticeboltzmann::VertexSteps::collideVertex(destArray, tau);
  }

  // ----------------------------------------------------------

  /** modifies "inner" distributions near the moving wall */
  static
  void
  boundaryConditionVertex(latticeboltzmann::VertexHeap& vertex,
                          const tarch::la::Vector<DIMENSIONS, double>& fineGridX)
  {
#ifdef Dim2
    const double wallVelocity[] = { MOVING_WALL_X, MOVING_WALL_Y };

    std::vector<int> dirs({ SOUTH_EAST, SOUTH, SOUTH_WEST }
                          );

    if (fineGridX(1) > 0.9999) {
#endif
#ifdef Dim3
    const double wallVelocity[] = { MOVING_WALL_X,
                                    MOVING_WALL_Y,
                                    MOVING_WALL_Z }

    ;
    // directions to process
    std::vector<int> dirs({ BOTTOM_SOUTH, BOTTOM_EAST, BOTTOM, BOTTOM_WEST, BOTTOM_NORTH }
                          );

#endif
    Vector<VERTEX_BLOCK_SIZE, double> density;
    latticeboltzmann::VertexSteps::computeDensity(vertex, density);

    const double C_S2 = 1.0 / 3.0;

#ifdef ISPC_BUILD

    for (auto it = dirs.begin(); it != dirs.end(); ++it) {
      double p = DOT(VELOCITIES[*it], wallVelocity);
      ispc::boundaryConditionVertex(&(vertex.getDir(*it)[0]),
                                    VERTEX_BLOCK_SIZE / DIMENSIONS,
                                    &density[0],
                                    p,
                                    WEIGHTS[*it]);
    }

#else

    // move in x direction
    for (auto it = dirs.begin(); it != dirs.end(); ++it) {
      double p = DOT(VELOCITIES[*it], wallVelocity);

      // VERTEX_BLOCK_SIZE / DIMENSIONS  gives  lenght of horizontal part of vertex structure (lower in 3D)
      for (int i = 0; i < VERTEX_BLOCK_SIZE / DIMENSIONS; ++i) {
        double d =  2 * WEIGHTS[*it] * density[i]  * p / C_S2;
        vertex.getDir(*it)[i] += d;
      }
    }

#endif              // ISPC_BUILD
  }
}

static
void
swapVertex(latticeboltzmann::VertexHeap& srcArray,
           latticeboltzmann::VertexHeap& destArray)
{
#ifdef ISPC_BUILD

  for (int direction = 0; direction < LB_DIR; ++direction) {
    int directionFrom = (LB_DIR - 1 - direction);

    ispc::swapVertex(&(srcArray.getDir(directionFrom)[0]),
                     &(destArray.getDir(direction)[0]),
                     VERTEX_BLOCK_SIZE);
  }

#else

  for (int direction = 0; direction < LB_DIR; ++direction)
    for (int i = 0; i < VERTEX_BLOCK_SIZE; ++i) {
      int directionFrom = (LB_DIR - 1 - direction);
      assert(srcArray.getDir(directionFrom)[i] > 0.0);
      assert(srcArray.getDir(directionFrom)[i] < 1.0);
      destArray.getDir(direction)[i] = srcArray.getDir(directionFrom)[i];
    }



#endif
}

static
void
collideVertex(latticeboltzmann::VertexHeap& dirs,
              double                        tau)
{
  Vector<VERTEX_BLOCK_SIZE, Vector<LB_DIR, double> > feq;
  VertexSteps::computeFeq(dirs, feq);

  VertexSteps::computePostCollisionDistribution(dirs, tau, feq);
}

static
void
initializeVertex(latticeboltzmann::VertexHeap& vertex,
                 Vector<VERTEX_BLOCK_SIZE, Vector<LB_DIR, double> >& feq)
{
  for (int i = 0; i < VERTEX_BLOCK_SIZE; ++i)
    for (int direction = 0; direction < LB_DIR; ++direction)
      vertex.getDir(direction)[i] = feq[i][direction];

}

static
void
initializeVertexArray(std::vector<latticeboltzmann::VertexHeap>& vertexData,
                      Vector<DIMENSIONS, double> v = Vector<DIMENSIONS, double>(0.0, 0.0))
{
  const int numberOfArraysPerVertex = 2;

  Vector<VERTEX_BLOCK_SIZE, double> density(1.0);

  Vector<VERTEX_BLOCK_SIZE, Vector<DIMENSIONS, double> > velocity(v);

  // all knots
  Vector<VERTEX_BLOCK_SIZE, Vector<LB_DIR, double> > feq;
  VertexSteps::computeFeq(density, velocity, feq);

  for (int i = 0; i < numberOfArraysPerVertex; ++i)
    VertexSteps::initializeVertex(vertexData[i], feq);
}

// --------------------------------------------------------------------------------------------------------

static
inline
void
computeDensity(latticeboltzmann::VertexHeap& dirs,
               Vector<VERTEX_BLOCK_SIZE,
                      double>&               density)
{
  for (int i = 0; i < VERTEX_BLOCK_SIZE; ++i) {
    density[i] = 0.0;

    for (int direction = 0; direction < LB_DIR; ++direction)
      density[i] += dirs.getDir(direction)[i];
  }
}

// ------------------------------------------------------------------

template <int Size>
static
void
computeVelocity(latticeboltzmann::VertexHeap& dirs,
                Vector<Size, double>& density,
                Vector<Size, Vector<DIMENSIONS, double> >& velocity)
{
  for (int index = 0; index < Size; ++index) {
#ifdef Dim2

    // I added getDir() manually for better using of VertexHeap class
    velocity[index][0] = -dirs.getDir(SOUTH_WEST)[index]
                         + dirs.getDir(SOUTH_EAST)[index]
                         - dirs.getDir(WEST)[index]
                         + dirs.getDir(EAST)[index]
                         - dirs.getDir(NORTH_WEST)[index]
                         + dirs.getDir(NORTH_EAST)[index];

    velocity[index][1] = -dirs.getDir(SOUTH_WEST)[index]
                         - dirs.getDir(SOUTH)[index]
                         - dirs.getDir(SOUTH_EAST)[index]
                         + dirs.getDir(NORTH_WEST)[index]
                         + dirs.getDir(NORTH)[index]
                         + dirs.getDir(NORTH_EAST)[index];
#endif

#ifdef Dim3
    velocity[index][0] = -dirs.getDir(BOTTOM_WEST)[index]
                         + dirs.getDir(BOTTOM_EAST)[index]
                         - dirs.getDir(SOUTH_WEST)[index]
                         + dirs.getDir(SOUTH_EAST)[index]
                         - dirs.getDir(WEST)[index]
                         + dirs.getDir(EAST)[index]
                         - dirs.getDir(NORTH_WEST)[index]
                         + dirs.getDir(NORTH_EAST)[index]
                         - dirs.getDir(TOP_WEST)[index]
                         + dirs.getDir(TOP_EAST)[index];

    velocity[index][1] = -dirs.getDir(BOTTOM_SOUTH)[index]
                         + dirs.getDir(BOTTOM_NORTH)[index]
                         - dirs.getDir(SOUTH_WEST)[index]
                         - dirs.getDir(SOUTH)[index]
                         - dirs.getDir(SOUTH_EAST)[index]
                         + dirs.getDir(NORTH_WEST)[index]
                         + dirs.getDir(NORTH)[index]
                         + dirs.getDir(NORTH_EAST)[index]
                         - dirs.getDir(TOP_SOUTH)[index]
                         + dirs.getDir(TOP_NORTH)[index];

    velocity[index][2] = -dirs.getDir(BOTTOM_SOUTH)[index]
                         - dirs.getDir(BOTTOM_EAST)[index]
                         - dirs.getDir(BOTTOM)[index]
                         - dirs.getDir(BOTTOM_WEST)[index]
                         - dirs.getDir(BOTTOM_NORTH)[index]
                         + dirs.getDir(TOP_SOUTH)[index]
                         + dirs.getDir(TOP_EAST)[index]
                         + dirs.getDir(TOP)[index]
                         + dirs.getDir(TOP_WEST)[index]
                         + dirs.getDir(TOP_NORTH)[index];
#endif

    velocity[index][0] /= density[index];
    velocity[index][1] /= density[index];
#ifdef Dim3
    velocity[index][2] /= density[index];
#endif
  }
}

// --------------------------------------------------

static
void
computeFeq(Vector<VERTEX_BLOCK_SIZE, double>& density,
           Vector<VERTEX_BLOCK_SIZE, Vector<DIMENSIONS, double> >& velocity,
           Vector<VERTEX_BLOCK_SIZE, Vector<LB_DIR, double> >& feq)
{
  const double CS_2     = 1.0 / 3.0;
  const double CS_4_x_2 = 2 * CS_2 * CS_2;
  const double CS_2_x_2 = 2 * CS_2;

  for (int i = 0; i < VERTEX_BLOCK_SIZE; ++i) {
    double CU_SW = DOT(velocity[i], VELOCITIES[SOUTH_WEST]);
    double CU_S  = DOT(velocity[i], VELOCITIES[SOUTH]);
    double CU_SE = DOT(velocity[i], VELOCITIES[SOUTH_EAST]);
    double CU_W  = DOT(velocity[i], VELOCITIES[WEST]);

#ifdef Dim3
    double CU_BS = DOT(velocity[i], VELOCITIES[BOTTOM_SOUTH]);
    double CU_BE = DOT(velocity[i], VELOCITIES[BOTTOM_EAST]);
    double CU_B  = DOT(velocity[i], VELOCITIES[BOTTOM]);
    double CU_BW = DOT(velocity[i], VELOCITIES[BOTTOM_WEST]);
    double CU_BN = DOT(velocity[i], VELOCITIES[BOTTOM_NORTH]);
#endif

#ifdef Dim2
    std::vector<double> feqCoefficients({ CU_SW, CU_S, CU_SE,
                                          CU_W, 0, -CU_W,
                                          -CU_SE, -CU_S, -CU_SW }
                                        );

#endif
#ifdef Dim3
    std::vector<double> feqCoefficients({ CU_BS, CU_BW, CU_B, CU_BE, CU_BN,
                                          CU_SW, CU_S, CU_SE,
                                          CU_W, 0, -CU_W,
                                          -CU_SE, -CU_S, -CU_SW,
                                          -CU_BN, -CU_BE, -CU_B, -CU_BW, -CU_BS }
                                        );

#endif

    double UU = 1.0 - DOT(velocity[i], velocity[i]) / CS_2_x_2;

#define FEQ(a) ((a) / CS_2 + (a) * (a) / CS_4_x_2 + UU)
    double d = density[i];

#ifdef ISPC_BUILD
    ispc::computeFeq(&feq[i][0],
                     feqCoefficients.data(),
                     LB_DIR,
                     WEIGHTS,
                     d,
                     UU);
#else

    for (int dir = 0; dir < LB_DIR; ++dir)
      feq[i][dir] = WEIGHTS[dir]* d* FEQ(feqCoefficients[dir]);

#endif
  }
}

static
void
computeFeq(VertexHeap& dirs,
           Vector<VERTEX_BLOCK_SIZE, Vector<LB_DIR, double> >& feq)
{
  Vector<VERTEX_BLOCK_SIZE, double> density(0.0);

  VertexSteps::computeDensity(dirs, density);

#ifdef Dim2
  Vector<DIMENSIONS, double> v(0.0, 0.0);

#endif

#ifdef Dim3
  Vector<DIMENSIONS, double> v(0.0, 0.0, 0.0);

#endif

  Vector<VERTEX_BLOCK_SIZE, Vector<DIMENSIONS, double> > velocity(v);

  VertexSteps::computeVelocity(dirs, density, velocity);

  VertexSteps::computeFeq(density, velocity, feq);
}

// ------------------------------------------------------------------------------------------

static
void
computePostCollisionDistribution(latticeboltzmann::VertexHeap& vertexDirs,
                                 double tau,
                                 const Vector<VERTEX_BLOCK_SIZE, Vector<LB_DIR, double> >& feq)
{
  const double tauRecp = 1.0 / tau;

  for (int direction = 0; direction < LB_DIR; ++direction)
    for (int i = 0; i < VERTEX_BLOCK_SIZE; ++i) {
      vertexDirs.getDir(direction)[i] -= tauRecp *
                                         (vertexDirs.getDir(direction)[i] - feq[i][direction]);
      assert(vertexDirs.getDir(direction)[i] > 0.0);
    }

}
};

#endif
