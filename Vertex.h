// This file is part of the Peano project. For conditions of distribution and 
// use, please see the copyright notice at www.peano-framework.org
#ifndef _LATTICEBOLTZMANN_VERTEX_H_ 
#define _LATTICEBOLTZMANN_VERTEX_H_


#include "latticeboltzmann/records/Vertex.h"
#include "peano/grid/Vertex.h"
#include "peano/grid/VertexEnumerator.h"
#include "peano/utils/Globals.h"


#include "tarch/la/VectorCompare.h"

#include <map>
#include <vector>

namespace latticeboltzmann {
class Vertex;
class VertexHeap;
}


/**
 * Blueprint for grid vertex.
 * 
 * This file has originally been created by the PDT and may be manually extended to 
 * the needs of your application. We do not recommend to remove anything!
 */
class latticeboltzmann::Vertex: public peano::grid::Vertex< latticeboltzmann::records::Vertex > { 
  private: 
    typedef class peano::grid::Vertex< latticeboltzmann::records::Vertex >  Base;

  static
  std::map<tarch::la::Vector<DIMENSIONS + 1, double>, int, tarch::la::VectorCompare<DIMENSIONS+1> > _vertices;

public:
  static
  std::map<tarch::la::Vector<DIMENSIONS, double>, int,
                             tarch::la::VectorCompare<DIMENSIONS> >
  processedHangingVertices;


  public:
    /**
     * Default Constructor
     *
     * This constructor is required by the framework's data container. Do not 
     * remove it.
     */
    Vertex();
    
    /**
     * This constructor should not set any attributes. It is used by the 
     * traversal algorithm whenever it allocates an array whose elements 
     * will be overwritten later anyway.  
     */
    Vertex(const Base::DoNotCallStandardConstructor&);
    
    /**
     * Constructor
     *
     * This constructor is required by the framework's data container. Do not 
     * remove it. It is kind of a copy constructor that converts an object which 
     * comprises solely persistent attributes into a full attribute. This very 
     * functionality is implemented within the super type, i.e. this constructor 
     * has to invoke the correponsing super type's constructor and not the super 
     * type standard constructor.
     */
    Vertex(const Base::PersistentVertex& argument);


  int
  getAggregateWithArbitraryCardinality() const;

  void
  setAggregateWithArbitraryCardinality(const int);

  bool
  isComputed() const;

  void
  setComputed(const bool computed);

  static
  int
  getVertexIndex(const tarch::la::Vector<DIMENSIONS, double>& fineGridX,
                 int level);
};


#endif
