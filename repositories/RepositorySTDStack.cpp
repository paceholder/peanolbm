#include "latticeboltzmann/repositories/RepositorySTDStack.h"

#include "tarch/Assertions.h"
#include "tarch/timing/Watch.h"

#ifdef Parallel
#include "tarch/parallel/Node.h"
#include "tarch/parallel/NodePool.h"
#include "peano/parallel/SendReceiveBufferPool.h"
#include "peano/parallel/loadbalancing/Oracle.h"
#endif

#include "peano/datatraversal/autotuning/Oracle.h"


tarch::logging::Log latticeboltzmann::repositories::RepositorySTDStack::_log( "latticeboltzmann::repositories::RepositorySTDStack" );


latticeboltzmann::repositories::RepositorySTDStack::RepositorySTDStack(
  peano::geometry::Geometry&                   geometry,
  const tarch::la::Vector<DIMENSIONS,double>&  domainSize,
  const tarch::la::Vector<DIMENSIONS,double>&  computationalDomainOffset
):
  _geometry(geometry),
  _cellStack(),
  _vertexStack(),
  _solverState(),
  _gridWithSetupExperiment(_vertexStack,_cellStack,_geometry,_solverState,domainSize,computationalDomainOffset,_regularGridContainer,_traversalOrderOnTopLevel),
  _gridWithLBMImplementation(_vertexStack,_cellStack,_geometry,_solverState,domainSize,computationalDomainOffset,_regularGridContainer,_traversalOrderOnTopLevel),
  _gridWithInterpolationRestriction(_vertexStack,_cellStack,_geometry,_solverState,domainSize,computationalDomainOffset,_regularGridContainer,_traversalOrderOnTopLevel),
  _gridWithPlot(_vertexStack,_cellStack,_geometry,_solverState,domainSize,computationalDomainOffset,_regularGridContainer,_traversalOrderOnTopLevel),

  _repositoryState() {
  logTraceIn( "RepositorySTDStack(...)" );
  _repositoryState.setAction( latticeboltzmann::records::RepositoryState::Terminate );

  peano::datatraversal::autotuning::Oracle::getInstance().setNumberOfOracles(4 +3);
  #ifdef Parallel
  peano::parallel::loadbalancing::Oracle::getInstance().setNumberOfOracles(4 +3 );
  #endif
  
  
  logTraceOut( "RepositorySTDStack(...)" );
}



latticeboltzmann::repositories::RepositorySTDStack::RepositorySTDStack(
  peano::geometry::Geometry&                   geometry
):
  _geometry(geometry),
  _cellStack(),
  _vertexStack(),
  _solverState(),
  _gridWithSetupExperiment(_vertexStack,_cellStack,_geometry,_solverState,_regularGridContainer,_traversalOrderOnTopLevel),
  _gridWithLBMImplementation(_vertexStack,_cellStack,_geometry,_solverState,_regularGridContainer,_traversalOrderOnTopLevel),
  _gridWithInterpolationRestriction(_vertexStack,_cellStack,_geometry,_solverState,_regularGridContainer,_traversalOrderOnTopLevel),
  _gridWithPlot(_vertexStack,_cellStack,_geometry,_solverState,_regularGridContainer,_traversalOrderOnTopLevel),

  _repositoryState() {
  logTraceIn( "RepositorySTDStack(Geometry&)" );

  _repositoryState.setAction( latticeboltzmann::records::RepositoryState::Terminate );
  
  peano::datatraversal::autotuning::Oracle::getInstance().setNumberOfOracles(4 +3);
  #ifdef Parallel
  peano::parallel::loadbalancing::Oracle::getInstance().setNumberOfOracles(4 +3 );
  #endif
  
  
  logTraceOut( "RepositorySTDStack(Geometry&)" );
}
    
   
latticeboltzmann::repositories::RepositorySTDStack::~RepositorySTDStack() {
  assertionMsg( _repositoryState.getAction() == latticeboltzmann::records::RepositoryState::Terminate, "terminate() must be called before destroying repository." );
}


void latticeboltzmann::repositories::RepositorySTDStack::restart(
  const tarch::la::Vector<DIMENSIONS,double>&  domainSize,
  const tarch::la::Vector<DIMENSIONS,double>&  domainOffset,
  int                                          domainLevel,
  const tarch::la::Vector<DIMENSIONS,int>&     positionOfCentralElementWithRespectToCoarserRemoteLevel
) {
  logTraceInWith4Arguments( "restart(...)", domainSize, domainOffset, domainLevel, positionOfCentralElementWithRespectToCoarserRemoteLevel );
  #ifdef Parallel
  assertion( !tarch::parallel::Node::getInstance().isGlobalMaster());
  #endif
  
  logInfo( "restart(...)", "start node for subdomain " << domainOffset << "x" << domainSize << " on level " << domainLevel );
  
  assertion( _repositoryState.getAction() == latticeboltzmann::records::RepositoryState::Terminate );

  _vertexStack.clear();
  _cellStack.clear();

  _gridWithSetupExperiment.restart(domainSize,domainOffset,domainLevel, positionOfCentralElementWithRespectToCoarserRemoteLevel);
  _gridWithLBMImplementation.restart(domainSize,domainOffset,domainLevel, positionOfCentralElementWithRespectToCoarserRemoteLevel);
  _gridWithInterpolationRestriction.restart(domainSize,domainOffset,domainLevel, positionOfCentralElementWithRespectToCoarserRemoteLevel);
  _gridWithPlot.restart(domainSize,domainOffset,domainLevel, positionOfCentralElementWithRespectToCoarserRemoteLevel);


  _solverState.restart();

  logTraceOut( "restart(...)" );
}


void latticeboltzmann::repositories::RepositorySTDStack::terminate() {
  logTraceIn( "terminate()" );
  
  _repositoryState.setAction( latticeboltzmann::records::RepositoryState::Terminate );
  
  #ifdef Parallel
  if (tarch::parallel::Node::getInstance().isGlobalMaster()) {
    tarch::parallel::NodePool::getInstance().broadcastToWorkingNodes(
      _repositoryState,
      peano::parallel::SendReceiveBufferPool::getInstance().getIterationManagementTag()
    );
  }
  peano::parallel::SendReceiveBufferPool::getInstance().terminate();
  #endif

  _gridWithSetupExperiment.terminate();
  _gridWithLBMImplementation.terminate();
  _gridWithInterpolationRestriction.terminate();
  _gridWithPlot.terminate();


  logInfo( "terminate()", "rank has terminated" );
  logTraceOut( "terminate()" );
}


latticeboltzmann::State& latticeboltzmann::repositories::RepositorySTDStack::getState() {
  return _solverState;
}


void latticeboltzmann::repositories::RepositorySTDStack::iterate(bool reduceState) {
  tarch::timing::Watch watch( "latticeboltzmann::repositories::RepositorySTDStack", "iterate(bool)", false);
  
  #ifdef Parallel
  if (tarch::parallel::Node::getInstance().isGlobalMaster()) {
    _repositoryState.setReduceState(reduceState);
    tarch::parallel::NodePool::getInstance().broadcastToWorkingNodes(
      _repositoryState,
      peano::parallel::SendReceiveBufferPool::getInstance().getIterationManagementTag()
    );
  }
  else {
    reduceState = _repositoryState.getReduceState();
  }
  #endif
  
  peano::datatraversal::autotuning::Oracle::getInstance().switchToOracle(_repositoryState.getAction());
  #ifdef Parallel
  peano::parallel::loadbalancing::Oracle::getInstance().switchToOracle(_repositoryState.getAction());
  #endif
  
  
  switch ( _repositoryState.getAction()) {
    case latticeboltzmann::records::RepositoryState::UseAdapterSetupExperiment: watch.startTimer(); _gridWithSetupExperiment.iterate(reduceState); watch.stopTimer(); _measureSetupExperimentCPUTime.setValue( watch.getCPUTime() ); _measureSetupExperimentCalendarTime.setValue( watch.getCalendarTime() ); break;
    case latticeboltzmann::records::RepositoryState::UseAdapterLBMImplementation: watch.startTimer(); _gridWithLBMImplementation.iterate(reduceState); watch.stopTimer(); _measureLBMImplementationCPUTime.setValue( watch.getCPUTime() ); _measureLBMImplementationCalendarTime.setValue( watch.getCalendarTime() ); break;
    case latticeboltzmann::records::RepositoryState::UseAdapterInterpolationRestriction: watch.startTimer(); _gridWithInterpolationRestriction.iterate(reduceState); watch.stopTimer(); _measureInterpolationRestrictionCPUTime.setValue( watch.getCPUTime() ); _measureInterpolationRestrictionCalendarTime.setValue( watch.getCalendarTime() ); break;
    case latticeboltzmann::records::RepositoryState::UseAdapterPlot: watch.startTimer(); _gridWithPlot.iterate(reduceState); watch.stopTimer(); _measurePlotCPUTime.setValue( watch.getCPUTime() ); _measurePlotCalendarTime.setValue( watch.getCalendarTime() ); break;

    case latticeboltzmann::records::RepositoryState::Terminate:
      assertionMsg( false, "this branch/state should never be reached" ); 
      break;
    case latticeboltzmann::records::RepositoryState::ReadCheckpoint:
      assertionMsg( false, "not implemented yet" );
      break;
    case latticeboltzmann::records::RepositoryState::WriteCheckpoint:
      assertionMsg( false, "not implemented yet" );
      break;
  }
}

 void latticeboltzmann::repositories::RepositorySTDStack::switchToSetupExperiment() { _repositoryState.setAction(latticeboltzmann::records::RepositoryState::UseAdapterSetupExperiment); }
 void latticeboltzmann::repositories::RepositorySTDStack::switchToLBMImplementation() { _repositoryState.setAction(latticeboltzmann::records::RepositoryState::UseAdapterLBMImplementation); }
 void latticeboltzmann::repositories::RepositorySTDStack::switchToInterpolationRestriction() { _repositoryState.setAction(latticeboltzmann::records::RepositoryState::UseAdapterInterpolationRestriction); }
 void latticeboltzmann::repositories::RepositorySTDStack::switchToPlot() { _repositoryState.setAction(latticeboltzmann::records::RepositoryState::UseAdapterPlot); }



 bool latticeboltzmann::repositories::RepositorySTDStack::isActiveAdapterSetupExperiment() const { return _repositoryState.getAction() == latticeboltzmann::records::RepositoryState::UseAdapterSetupExperiment; }
 bool latticeboltzmann::repositories::RepositorySTDStack::isActiveAdapterLBMImplementation() const { return _repositoryState.getAction() == latticeboltzmann::records::RepositoryState::UseAdapterLBMImplementation; }
 bool latticeboltzmann::repositories::RepositorySTDStack::isActiveAdapterInterpolationRestriction() const { return _repositoryState.getAction() == latticeboltzmann::records::RepositoryState::UseAdapterInterpolationRestriction; }
 bool latticeboltzmann::repositories::RepositorySTDStack::isActiveAdapterPlot() const { return _repositoryState.getAction() == latticeboltzmann::records::RepositoryState::UseAdapterPlot; }



peano::grid::Checkpoint<latticeboltzmann::Vertex, latticeboltzmann::Cell>* latticeboltzmann::repositories::RepositorySTDStack::createEmptyCheckpoint() {
  return new peano::grid::Checkpoint<latticeboltzmann::Vertex, latticeboltzmann::Cell>();
} 


void latticeboltzmann::repositories::RepositorySTDStack::writeCheckpoint(peano::grid::Checkpoint<latticeboltzmann::Vertex, latticeboltzmann::Cell> * const checkpoint) {
  _solverState.writeToCheckpoint( *checkpoint );
  _vertexStack.writeToCheckpoint( *checkpoint );
  _cellStack.writeToCheckpoint( *checkpoint );
} 


void latticeboltzmann::repositories::RepositorySTDStack::readCheckpoint( peano::grid::Checkpoint<latticeboltzmann::Vertex, latticeboltzmann::Cell> const * const checkpoint ) {
  assertionMsg( checkpoint->isValid(), "checkpoint has to be valid if you call this operation" );

  _solverState.readFromCheckpoint( *checkpoint );
  _vertexStack.readFromCheckpoint( *checkpoint );
  _cellStack.readFromCheckpoint( *checkpoint );
}


void latticeboltzmann::repositories::RepositorySTDStack::setMaximumMemoryFootprintForTemporaryRegularGrids(double value) {
  _regularGridContainer.setMaximumMemoryFootprintForTemporaryRegularGrids(value);
}


#ifdef Parallel
bool latticeboltzmann::repositories::RepositorySTDStack::continueToIterate() {
  logTraceIn( "continueToIterate()" );

  assertion( !tarch::parallel::Node::getInstance().isGlobalMaster());

  bool result;
  if ( _solverState.hasJoinedWithMaster() ) {
    result = false;
  }
  else {
    int masterNode = tarch::parallel::Node::getInstance().getGlobalMasterRank();
    assertion( masterNode != -1 );

    _repositoryState.receive( masterNode, peano::parallel::SendReceiveBufferPool::getInstance().getIterationManagementTag(), true );

    result = _repositoryState.getAction()!=latticeboltzmann::records::RepositoryState::Terminate;
  }
   
  logTraceOutWith1Argument( "continueToIterate()", result );
  return result;
}
#endif


void latticeboltzmann::repositories::RepositorySTDStack::logIterationStatistics() const {
  logInfo( "logIterationStatistics()", "|| adapter name \t || iterations \t || total CPU time [t]=s \t || average CPU time [t]=s \t || total user time [t]=s \t || average user time [t]=s  || CPU time properties  || user time properties " );  
   logInfo( "logIterationStatistics()", "| SetupExperiment \t |  " << _measureSetupExperimentCPUTime.getNumberOfMeasurements() << " \t |  " << _measureSetupExperimentCPUTime.getAccumulatedValue() << " \t |  " << _measureSetupExperimentCPUTime.getValue()  << " \t |  " << _measureSetupExperimentCalendarTime.getAccumulatedValue() << " \t |  " << _measureSetupExperimentCalendarTime.getValue() << " \t |  " << _measureSetupExperimentCPUTime.toString() << " \t |  " << _measureSetupExperimentCalendarTime.toString() );
   logInfo( "logIterationStatistics()", "| LBMImplementation \t |  " << _measureLBMImplementationCPUTime.getNumberOfMeasurements() << " \t |  " << _measureLBMImplementationCPUTime.getAccumulatedValue() << " \t |  " << _measureLBMImplementationCPUTime.getValue()  << " \t |  " << _measureLBMImplementationCalendarTime.getAccumulatedValue() << " \t |  " << _measureLBMImplementationCalendarTime.getValue() << " \t |  " << _measureLBMImplementationCPUTime.toString() << " \t |  " << _measureLBMImplementationCalendarTime.toString() );
   logInfo( "logIterationStatistics()", "| InterpolationRestriction \t |  " << _measureInterpolationRestrictionCPUTime.getNumberOfMeasurements() << " \t |  " << _measureInterpolationRestrictionCPUTime.getAccumulatedValue() << " \t |  " << _measureInterpolationRestrictionCPUTime.getValue()  << " \t |  " << _measureInterpolationRestrictionCalendarTime.getAccumulatedValue() << " \t |  " << _measureInterpolationRestrictionCalendarTime.getValue() << " \t |  " << _measureInterpolationRestrictionCPUTime.toString() << " \t |  " << _measureInterpolationRestrictionCalendarTime.toString() );
   logInfo( "logIterationStatistics()", "| Plot \t |  " << _measurePlotCPUTime.getNumberOfMeasurements() << " \t |  " << _measurePlotCPUTime.getAccumulatedValue() << " \t |  " << _measurePlotCPUTime.getValue()  << " \t |  " << _measurePlotCalendarTime.getAccumulatedValue() << " \t |  " << _measurePlotCalendarTime.getValue() << " \t |  " << _measurePlotCPUTime.toString() << " \t |  " << _measurePlotCalendarTime.toString() );

}


void latticeboltzmann::repositories::RepositorySTDStack::clearIterationStatistics() {
   _measureSetupExperimentCPUTime.erase();
   _measureLBMImplementationCPUTime.erase();
   _measureInterpolationRestrictionCPUTime.erase();
   _measurePlotCPUTime.erase();

   _measureSetupExperimentCalendarTime.erase();
   _measureLBMImplementationCalendarTime.erase();
   _measureInterpolationRestrictionCalendarTime.erase();
   _measurePlotCalendarTime.erase();

}
