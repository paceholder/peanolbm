// This file is part of the Peano project. For conditions of distribution and 
// use, please see the copyright notice at www.peano-framework.org
#ifndef _LATTICEBOLTZMANN_REPOSITORIES_REPOSITORY_ARRAY_STACK_H_ 
#define _LATTICEBOLTZMANN_REPOSITORIES_REPOSITORY_ARRAY_STACK_H_ 


#include "latticeboltzmann/repositories/Repository.h"
#include "latticeboltzmann/records/RepositoryState.h"

#include "latticeboltzmann/State.h"
#include "latticeboltzmann/Vertex.h"
#include "latticeboltzmann/Cell.h"

#include "peano/grid/Grid.h"
#include "peano/stacks/CellArrayStack.h"
#include "peano/stacks/VertexArrayStack.h"


 #include "latticeboltzmann/adapters/SetupExperiment.h" 
 #include "latticeboltzmann/adapters/LBMImplementation.h" 
 #include "latticeboltzmann/adapters/InterpolationRestriction.h" 
 #include "latticeboltzmann/adapters/Plot.h" 



namespace latticeboltzmann {
      namespace repositories {
        class RepositoryArrayStack;  
      }
}


class latticeboltzmann::repositories::RepositoryArrayStack: public latticeboltzmann::repositories::Repository {
  private:
    static tarch::logging::Log _log;
  
    peano::geometry::Geometry& _geometry;
    
    typedef peano::stacks::CellArrayStack<latticeboltzmann::Cell>       CellStack;
    typedef peano::stacks::VertexArrayStack<latticeboltzmann::Vertex>   VertexStack;

    CellStack    _cellStack;
    VertexStack  _vertexStack;
    latticeboltzmann::State          _solverState;
    peano::grid::RegularGridContainer<latticeboltzmann::Vertex,latticeboltzmann::Cell>  _regularGridContainer;
    peano::grid::TraversalOrderOnTopLevel                                         _traversalOrderOnTopLevel;

    peano::grid::Grid<latticeboltzmann::Vertex,latticeboltzmann::Cell,latticeboltzmann::State,VertexStack,CellStack,latticeboltzmann::adapters::SetupExperiment> _gridWithSetupExperiment;
    peano::grid::Grid<latticeboltzmann::Vertex,latticeboltzmann::Cell,latticeboltzmann::State,VertexStack,CellStack,latticeboltzmann::adapters::LBMImplementation> _gridWithLBMImplementation;
    peano::grid::Grid<latticeboltzmann::Vertex,latticeboltzmann::Cell,latticeboltzmann::State,VertexStack,CellStack,latticeboltzmann::adapters::InterpolationRestriction> _gridWithInterpolationRestriction;
    peano::grid::Grid<latticeboltzmann::Vertex,latticeboltzmann::Cell,latticeboltzmann::State,VertexStack,CellStack,latticeboltzmann::adapters::Plot> _gridWithPlot;

  
   latticeboltzmann::records::RepositoryState               _repositoryState;
   
    tarch::timing::Measurement _measureSetupExperimentCPUTime;
    tarch::timing::Measurement _measureLBMImplementationCPUTime;
    tarch::timing::Measurement _measureInterpolationRestrictionCPUTime;
    tarch::timing::Measurement _measurePlotCPUTime;

    tarch::timing::Measurement _measureSetupExperimentCalendarTime;
    tarch::timing::Measurement _measureLBMImplementationCalendarTime;
    tarch::timing::Measurement _measureInterpolationRestrictionCalendarTime;
    tarch::timing::Measurement _measurePlotCalendarTime;


  public:
    RepositoryArrayStack(
      peano::geometry::Geometry&                   geometry,
      const tarch::la::Vector<DIMENSIONS,double>&  domainSize,
      const tarch::la::Vector<DIMENSIONS,double>&  computationalDomainOffset,
      int                                          maximumSizeOfCellInOutStack,
      int                                          maximumSizeOfVertexInOutStack,
      int                                          maximumSizeOfVertexTemporaryStack
    );
    
    /**
     * Parallel Constructor
     *
     * Used in parallel mode only where the size of the domain is not known 
     * when the type of repository is determined.  
     */
    RepositoryArrayStack(
      peano::geometry::Geometry&                   geometry,
      int                                          maximumSizeOfCellInOutStack,
      int                                          maximumSizeOfVertexInOutStack,
      int                                          maximumSizeOfVertexTemporaryStack
    );
    
    virtual ~RepositoryArrayStack();

    virtual void restart(
      const tarch::la::Vector<DIMENSIONS,double>&  domainSize,
      const tarch::la::Vector<DIMENSIONS,double>&  domainOffset,
      int                                          domainLevel,
      const tarch::la::Vector<DIMENSIONS,int>&     positionOfCentralElementWithRespectToCoarserRemoteLevel
    );
         
    virtual void terminate();
        
    virtual latticeboltzmann::State& getState();

    virtual void iterate( bool reduceState = true );
    
    virtual void writeCheckpoint(peano::grid::Checkpoint<latticeboltzmann::Vertex, latticeboltzmann::Cell> * const checkpoint); 
    virtual void readCheckpoint( peano::grid::Checkpoint<latticeboltzmann::Vertex, latticeboltzmann::Cell> const * const checkpoint );
    virtual peano::grid::Checkpoint<latticeboltzmann::Vertex, latticeboltzmann::Cell>* createEmptyCheckpoint(); 

    virtual void switchToSetupExperiment();    
    virtual void switchToLBMImplementation();    
    virtual void switchToInterpolationRestriction();    
    virtual void switchToPlot();    

    virtual bool isActiveAdapterSetupExperiment() const;
    virtual bool isActiveAdapterLBMImplementation() const;
    virtual bool isActiveAdapterInterpolationRestriction() const;
    virtual bool isActiveAdapterPlot() const;

     
    #ifdef Parallel
    virtual bool continueToIterate();
    #endif

    virtual void setMaximumMemoryFootprintForTemporaryRegularGrids(double value);
    virtual void logIterationStatistics() const;
    virtual void clearIterationStatistics();
};


#endif
