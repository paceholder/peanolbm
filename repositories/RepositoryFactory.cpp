#include "latticeboltzmann/repositories/RepositoryFactory.h"

#include "latticeboltzmann/repositories/RepositoryArrayStack.h"
#include "latticeboltzmann/repositories/RepositorySTDStack.h"

#include "latticeboltzmann/records/RepositoryState.h"

#ifdef Parallel
#include "tarch/parallel/NodePool.h"
#include "peano/parallel/Partitioner.h"
#endif


latticeboltzmann::repositories::RepositoryFactory::RepositoryFactory() {
  #ifdef Parallel
  peano::parallel::Partitioner::initDatatypes();

  latticeboltzmann::State::initDatatype();
  latticeboltzmann::Vertex::initDatatype();
  latticeboltzmann::Cell::initDatatype();

  if (latticeboltzmann::records::RepositoryState::Datatype==0) {
    latticeboltzmann::records::RepositoryState::initDatatype();
  }
  #endif
}


latticeboltzmann::repositories::RepositoryFactory::~RepositoryFactory() {
}


void latticeboltzmann::repositories::RepositoryFactory::shutdownAllParallelDatatypes() {
  #ifdef Parallel
  peano::parallel::Partitioner::shutdownDatatypes();

  latticeboltzmann::State::shutdownDatatype();
  latticeboltzmann::Vertex::shutdownDatatype();
  latticeboltzmann::Cell::shutdownDatatype();

  if (latticeboltzmann::records::RepositoryState::Datatype!=0) {
    latticeboltzmann::records::RepositoryState::shutdownDatatype();
    latticeboltzmann::records::RepositoryState::Datatype = 0;
  }
  #endif
}


latticeboltzmann::repositories::RepositoryFactory& latticeboltzmann::repositories::RepositoryFactory::getInstance() {
  static latticeboltzmann::repositories::RepositoryFactory singleton;
  return singleton;
}

    
latticeboltzmann::repositories::Repository* 
latticeboltzmann::repositories::RepositoryFactory::createWithArrayStackImplementation(
  peano::geometry::Geometry&                   geometry,
  const tarch::la::Vector<DIMENSIONS,double>&  domainSize,
  const tarch::la::Vector<DIMENSIONS,double>&  computationalDomainOffset,
  int                                          maxCellStackSize,    
  int                                          maxVertexStackSize,    
  int                                          maxTemporaryVertexStackSize    
) {
  #ifdef Parallel
  if (!tarch::parallel::Node::getInstance().isGlobalMaster()) {
    return new latticeboltzmann::repositories::RepositoryArrayStack(geometry, domainSize, computationalDomainOffset,maxCellStackSize,maxVertexStackSize,maxTemporaryVertexStackSize);
  }
  else
  #endif
  return new latticeboltzmann::repositories::RepositoryArrayStack(geometry, domainSize, computationalDomainOffset,maxCellStackSize,maxVertexStackSize,maxTemporaryVertexStackSize);
}    


latticeboltzmann::repositories::Repository* 
latticeboltzmann::repositories::RepositoryFactory::createWithSTDStackImplementation(
  peano::geometry::Geometry&                   geometry,
  const tarch::la::Vector<DIMENSIONS,double>&  domainSize,
  const tarch::la::Vector<DIMENSIONS,double>&  computationalDomainOffset
) {
  #ifdef Parallel
  if (!tarch::parallel::Node::getInstance().isGlobalMaster()) {
    return new latticeboltzmann::repositories::RepositorySTDStack(geometry);
  }
  else
  #endif
  return new latticeboltzmann::repositories::RepositorySTDStack(geometry, domainSize, computationalDomainOffset);
}
