// This file is part of the Peano project. For conditions of distribution and
// use, please see the copyright notice at www.peano-framework.org
#ifndef _LATTICEBOLTZMANN_TESTS_RESCALING_H_
#define _LATTICEBOLTZMANN_TESTS_RESCALING_H_

#include "tarch/tests/TestCase.h"

namespace latticeboltzmann {
namespace tests {
class TestRescaling;
}
}

/**
 * This is just a default test case that demonstrated how to write unit tests
 * in Peano. Feel free to rename, remove, or duplicate it.
 */
class latticeboltzmann::tests::TestRescaling: public tarch::tests::TestCase
{
private:
  /**
   * These operation usually implement the real tests.
   */
  void
  testFineToCoarse();

  /**
   * These operation usually implement the real tests.
   */
  void
  test2();

  /**
   * These operation usually implement the real tests.
   */
  void
  test3();

public:
  TestRescaling();
  virtual
  ~TestRescaling();

  virtual void
  run();
};

#endif
