// This file is part of the Peano project. For conditions of distribution and
// use, please see the copyright notice at www.peano-framework.org
#ifndef _LATTICEBOLTZMANN_TESTS_TEST_CELL_STEPS_H_
#define _LATTICEBOLTZMANN_TESTS_TEST_CELL_STEPS_H_

#include "tarch/tests/TestCase.h"

namespace latticeboltzmann {
namespace tests {
class TestCellSteps;
}
}

/**
 * This is just a default test case that demonstrated how to write unit tests
 * in Peano. Feel free to rename, remove, or duplicate it.
 */
class latticeboltzmann::tests::TestCellSteps: public tarch::tests::TestCase
{
private:
  /**
   * Here we compare the momentum before and after collision
   */
  void
  testComputeDensity();

  /**
   * Test whether we initialize distirbution correctly according to the intial velocity
   */
  void
  testComputeVelocity();

  /**
   * These operation usually implement the real tests.
   */
  void
  testFillCellBoundary();

public:
  TestCellSteps();
  virtual
  ~TestCellSteps();

  virtual void
  run() override;
};

#endif
