// This file is part of the Peano project. For conditions of distribution and
// use, please see the copyright notice at www.peano-framework.org
#ifndef _LATTICEBOLTZMANN_TESTS_TEST_COLLISION_H_
#define _LATTICEBOLTZMANN_TESTS_TEST_COLLISION_H_

#include "tarch/tests/TestCase.h"

namespace latticeboltzmann {
namespace tests {
class TestCollision;
}
}

/**
 * This is just a default test case that demonstrated how to write unit tests
 * in Peano. Feel free to rename, remove, or duplicate it.
 */
class latticeboltzmann::tests::TestCollision: public tarch::tests::TestCase
{
private:
  /**
   * Here we compare the momentum before and after collision
   */
  void
  testMomentumConservation();

  /**
   * Test whether we initialize distirbution correctly according to the intial velocity
   */
  void
  testVelocityInitialization();

  /**
   * These operation usually implement the real tests.
   */
  void
  test3();

public:
  TestCollision();
  virtual
  ~TestCollision();

  virtual void
  run() override;
};

#endif
