#include "latticeboltzmann/tests/TestCellSteps.h"

#include "tarch/compiler/CompilerSpecificSettings.h"
#include "tarch/tests/TestCaseFactory.h"

#include "peano/grid/VertexEnumerator.h"

#include <vector>

#ifdef Dim2
  #include "latticeboltzmann/heap2D/CellHeap.h"
#endif

#ifdef Dim3
  #include "latticeboltzmann/heap3D/CellHeap.h"
#endif

#include "latticeboltzmann/lbmsteps/CellSteps.h"

// registerTest(latticeboltzmann::tests::TestCellSteps)
static tarch::tests::TestCaseFactory<latticeboltzmann::tests::TestCellSteps>
thisTestCaseFactoryInstance(tarch::tests::TestCaseFactory<latticeboltzmann::tests::TestCellSteps>::UnitTest,
                            "latticeboltzmann::tests::TestCellSteps");

class TestVertexEnumerator: public peano::grid::VertexEnumerator
{
  int
  operator()(int localVertexNumber) const { return 0; }
  int
  operator()(const LocalVertexIntegerIndex& localVertexNumber) const { return 0; }
  int
  operator()(const LocalVertexBitsetIndex& localVertexNumber) const { return 0; }
  Vector
  getVertexPosition(int localVertexNumber) const { return Vector(); }
  Vector
  getVertexPosition(const LocalVertexIntegerIndex& localVertexNumber) const { return Vector(); }
  Vector
  getVertexPosition(const LocalVertexBitsetIndex& localVertexNumber) const { return Vector(); }
  Vector
  getVertexPosition() const { return Vector(); }
  Vector
  getCellCenter() const { return Vector(); }
  int
  getLevel() const { return 0; }
  Vector
  getCellSize() const { return Vector(); }
  std::string
  toString() const { return std::string(); }
  peano::grid::CellFlags
  getCellFlags() const { return peano::grid::CellFlags::Undefined; }
  int
  cell(const LocalVertexIntegerIndex& localVertexNumber) const { return 0; }
  bool
  overlaps(const Vector& offset, const Vector& size) const { return true; }
  bool
  isVertexAtPatchBoundaryWithinRegularSubtree(const LocalVertexIntegerIndex& localVertexNumber) const  { return true; }
};

#ifdef UseTestSpecificCompilerSettings
  #pragma optimize("",off)
#endif
latticeboltzmann::tests::TestCellSteps::
TestCellSteps():
  tarch::tests::TestCase("latticeboltzmann::tests::TestCase") {}

latticeboltzmann::tests::TestCellSteps::~TestCellSteps() {}

void
latticeboltzmann::tests::TestCellSteps::
run()
{
  // @todo If you have further tests, add them here
  testMethod(testComputeDensity);
  testMethod(testComputeVelocity);
  testMethod(testFillCellBoundary);
}

void
latticeboltzmann::tests::TestCellSteps::
testComputeDensity()
{
  latticeboltzmann::CellHeap* cell = new latticeboltzmann::CellHeap;
  // vector of density
  Vector<CELL_BLOCK_SIZE, double>* density = new Vector<CELL_BLOCK_SIZE, double>(1.0);

#ifdef Dim2
  Vector<DIMENSIONS, double> v(1.0, 1.0);

#endif

#ifdef Dim3
  // Vector<DIMENSIONS, double> v(1.0, 1.0, 1.0);
  Vector<DIMENSIONS, double> v(.0, .1, .1);

#endif

  // vector of velocity
  Vector<CELL_BLOCK_SIZE, Vector<DIMENSIONS, double> >* velocity =
    new Vector<CELL_BLOCK_SIZE, Vector<DIMENSIONS, double> >(v);

  // vector of equilibrium state distributions
  Vector<LB_DIR, Vector<CELL_BLOCK_SIZE, double> >* feq =
    new Vector<LB_DIR, Vector<CELL_BLOCK_SIZE, double> >;

  CellSteps::computeFeq(*density, *velocity, *feq);

  for (int direction = 0; direction < LB_DIR; ++direction)
    cell->getDir(direction) = feq->operator()(direction);

  Vector<CELL_BLOCK_SIZE, double>* den = new Vector<CELL_BLOCK_SIZE, double>;
  CellSteps::computeDensity(*cell, *den);

  // density is preserved after computing equilibrium state
  for (int i = 0; i < CELL_BLOCK_SIZE; ++i)
    validateNumericalEqualsWithEps(density->operator()(i),
                                   den->    operator()(i),
                                   0.0001);

  delete cell;
  delete velocity;
  delete den;
  delete feq;
  delete density;
}

// ----------------------------------------------------------------------------------

/** All variables are allocated in the heap to prevent stack overflow with large sizes of arrays */
void
latticeboltzmann::tests::TestCellSteps::
testComputeVelocity()
{
  latticeboltzmann::CellHeap* cell = new latticeboltzmann::CellHeap;

  Vector<CELL_BLOCK_SIZE, double>* density = new Vector<CELL_BLOCK_SIZE, double>(1.0);

  Vector<DIMENSIONS, double> v(1.0, 1.0);

  Vector<CELL_BLOCK_SIZE, Vector<DIMENSIONS, double> >* velocity =
    new Vector<CELL_BLOCK_SIZE, Vector<DIMENSIONS, double> >(v);

  Vector<LB_DIR, Vector<CELL_BLOCK_SIZE, double> >* feq = new Vector<LB_DIR, Vector<CELL_BLOCK_SIZE, double> >;

  CellSteps::computeFeq(*density, *velocity, *feq);

  // use new FEQ values
  for (int direction = 0; direction < LB_DIR; ++direction)
    cell->getDir(direction) = feq->operator()(direction);

  Vector<CELL_BLOCK_SIZE, Vector<DIMENSIONS, double> >* vel =
    new Vector<CELL_BLOCK_SIZE, Vector<DIMENSIONS, double> >;
  CellSteps::computeVelocity(*cell, *density, *vel);

  // check if velocity is the same for FEQ distributions
  for (int i = 0; i < CELL_BLOCK_SIZE; ++i) {
    validateNumericalEqualsWithEps(velocity->operator()(i)[0], v[0], 0.0001);
    validateNumericalEqualsWithEps(velocity->operator()(i)[1], v[1], 0.0001);
  }

  delete cell;
  delete density;
  delete velocity;
  delete feq;
  delete vel;
}

void
latticeboltzmann::tests::TestCellSteps::
testFillCellBoundary()
{
  // CellSteps::fillCellBoundary(0,)
}

#ifdef UseTestSpecificCompilerSettings
  #pragma optimize("",on)
#endif
