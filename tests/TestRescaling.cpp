#include "latticeboltzmann/tests/TestRescaling.h"

#include "tarch/compiler/CompilerSpecificSettings.h"
#include "tarch/tests/TestCaseFactory.h"

#include "tarch/la/Vector.h"

#include "latticeboltzmann/LBDefinitions.h"
#include "latticeboltzmann/State.h"

#include "latticeboltzmann/lbmsteps/Interpolation.h"
#include "latticeboltzmann/lbmsteps/PointSteps.h"
#include "latticeboltzmann/lbmsteps/Restriction.h"
#include "latticeboltzmann/lbmsteps/VertexSteps.h"

#include <cstdlib>
#include <time.h>

#ifdef Dim2
  #include "latticeboltzmann/heap2D/VertexHeap.h"
#endif //  Dim2
#ifdef Dim3
  #include "latticeboltzmann/heap3D/VertexHeap.h"
#endif //  Dim3

#include <tuple>

registerTest(latticeboltzmann::tests::TestRescaling)

#ifdef UseTestSpecificCompilerSettings
  #pragma optimize("",off)
#endif
latticeboltzmann::tests::TestRescaling::
TestRescaling():
  tarch::tests::TestCase("latticeboltzmann::tests::TestRescaling") {}

latticeboltzmann::tests::TestRescaling::~TestRescaling() {}

void
latticeboltzmann::tests::TestRescaling::
run()
{
  // @todo If you have further tests, add them here
  testMethod(testFineToCoarse);
  testMethod(test2);
  testMethod(test3);
}

void
latticeboltzmann::tests::TestRescaling::
testFineToCoarse()
{
  return;
  tarch::la::Vector<LB_DIMENSION, double> v(0.0);

  tarch::la::Vector<LB_DIR, tarch::la::Vector<LB_DIMENSION, double> > dirs(LB_DIR, v);

  VertexHeap array1;
  VertexHeap array2;
  VertexHeap coarseArray1;
  VertexHeap coarseArray2;

  srand(time(0));

  for (int dir = 0; dir < LB_DIR; ++dir) {
    double r =  (rand() % 20) / 100.0;
    array1.getDir(dir)[0] = r;
    dirs[dir][0]          = r;
  }

  int    level     = 3;
  double tauFine   = State::getTau(level);
  double tauCoarse = State::getTau(level - 1);

  Restriction::rescaleFromFineToCoarse(dirs, tauFine, tauCoarse);

  double                     dyFine   = 1 / 3.;
  double                     dyCoarse = 1.;
  Vector<DIMENSIONS, double> fineGridX;
  Vector<DIMENSIONS, double> coarseVertex1Position;
  Interpolation::Direction   direction = Interpolation::BELOW;

  // "restriction". We check just first element (0)
  for (int dir = 0; dir < LB_DIR; ++dir)
    coarseArray1.getDir(dir)[0] = dirs[dir][0];

  // ----------------------------------------------------

  double stressCoarse[2][2];
  double coeffCoarse = (1. -  1 / (2 * tauCoarse));

  Vector<VERTEX_BLOCK_SIZE, Vector<LB_DIR, double> > feq;
  VertexSteps::computeFeq(coarseArray1, feq);

  for (int j = 0; j < 2; ++j)
    for (int i = 0; i < 2; ++i) {
      double m = 0;

      for (int dir = 0; dir < LB_DIR; ++dir)
        m += (coarseArray1.getDir(dir)[0] - feq[0][dir]) *
             (VELOCITIES[dir][i] * VELOCITIES[dir][j] - (i == j) ? .5 * DOT(VELOCITIES[dir], VELOCITIES[dir]) : 0.0);

      stressCoarse[i][j] = coeffCoarse * m;
    }

  double stressFine[2][2];
  double coeffFine = (1. -  1 / (2 * tauFine));

  Vector<VERTEX_BLOCK_SIZE, Vector<LB_DIR, double> > feqFine;

  VertexSteps::computeFeq(array1, feqFine);

  for (int j = 0; j < 2; ++j)
    for (int i = 0; i < 2; ++i) {
      double m = 0;

      for (int dir = 0; dir < LB_DIR; ++dir)
        m += (array1.getDir(dir)[0] - feqFine[0][dir]) *
             (VELOCITIES[dir][i] * VELOCITIES[dir][j] - (i == j) ? .5 * DOT(VELOCITIES[dir], VELOCITIES[dir]) : 0.0);

      stressFine[i][j] = coeffFine * m;
    }



  for (int j = 0; j < 2; ++j)
    for (int i = 0; i < 2; ++i)
      validateNumericalEqualsWithEps(stressFine[i][j], stressCoarse[i][j], 0.0001);

  std::cout << "stresses computed" << std::endl;
}

void
latticeboltzmann::tests::TestRescaling::
test2()
{
  // @todo Add your test here
  validateEquals(2, 2);
}

void
latticeboltzmann::tests::TestRescaling::
test3()
{
  // @todo Add your test here
  validateEquals(3, 3);
}

#ifdef UseTestSpecificCompilerSettings
  #pragma optimize("",on)
#endif
