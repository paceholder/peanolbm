#include "latticeboltzmann/tests/TestInterpolation.h"

#include "tarch/compiler/CompilerSpecificSettings.h"
#include "tarch/tests/TestCaseFactory.h"

#include "peano/grid/VertexEnumerator.h"

#include <vector>

#ifdef Dim2
  #include "latticeboltzmann/heap2D/CellHeap.h"
#endif

#ifdef Dim3
  #include "latticeboltzmann/heap3D/CellHeap.h"
#endif

#include "latticeboltzmann/lbmsteps/CellSteps.h"

namespace latticeboltzmann {
namespace tests {
static tarch::tests::TestCaseFactory<latticeboltzmann::tests::TestInterpolation>
thisTestCaseFactoryInstance(tarch::tests::TestCaseFactory<latticeboltzmann::tests::TestInterpolation>::UnitTest,
                            "latticeboltzmann::tests::TestInterpolation");

#ifdef UseTestSpecificCompilerSettings
  #pragma optimize("",off)
#endif
latticeboltzmann::tests::TestInterpolation::
TestInterpolation():
  tarch::tests::TestCase("latticeboltzmann::tests::TestCase") {}

latticeboltzmann::tests::TestInterpolation::~TestInterpolation() {}

void
latticeboltzmann::tests::TestInterpolation::
run()
{
  // @todo If you have further tests, add them here
  testMethod(testInterpolationHorizontal);
  testMethod(testInterpolationVertical);
}

void
latticeboltzmann::tests::TestInterpolation::
testInterpolationHorizontal()
{
  // VertexHeap vertex1;
  // VertexHeap vertex2;

  // int level = 2;
  //// for current fine cell
  // double peanoCellSize = 1.0 / pow(3.0, level - 1);
  // double dxFine        = peanoCellSize / double(LB_DIMENSION - 1);

  //// for coarse Cell
  // double coarseCellWidth = 1.0 / pow(3.0, level - 2);
  // double dxCoarse        = coarseCellWidth / double(LB_DIMENSION - 1);

  // tarch::la::Vector<DIMENSIONS, double> fineGridX(0.22222, 0.666666);

  // tarch::la::Vector<DIMENSIONS, double> coarseVertex1Position(0.0, 0.66666);

  // tarch::la::Vector<LB_DIR, tarch::la::Vector<LB_DIMENSION, double> > dirs;

  ///// fill dirs -- data from coarse grid
  // for (int direction = 0; direction < LB_DIR; ++direction)
  // for (int i = 0; i < LB_DIMENSION; ++i)
  // dirs[direction][i] = dxCoarse * i / coarseCellWidth;

  // Interpolation::interpolateSpaciallyHorizontal(vertex1,
  // vertex2,
  // dxFine,
  // dxCoarse,
  // fineGridX,
  // coarseVertex1Position,
  // dirs,
  // Interpolation::BOTTOM);

  ////  double baseValue = 2 / 3.0;
  // for (int direction = 0; direction < LB_DIR; ++direction)
  // for (int i = 0; i < LB_DIMENSION - 1; ++i) {
  // double a = vertex1.getDir(direction)[i];
  // double b = (fineGridX(0) + i * dxFine) / coarseCellWidth;
  // validateNumericalEqualsWithEps(a, b, 0.0001);
  // }
}

// ----------------------------------------------------------------------------------

void
latticeboltzmann::tests::TestInterpolation::
testInterpolationVertical()
{
  // VertexHeap vertex1;
  // VertexHeap vertex2;

  // int level = 2;
  //// for current fine cell
  // double peanoCellSize = 1.0 / pow(3.0, level - 1);
  // double dxFine        = peanoCellSize / double(LB_DIMENSION - 1);

  //// for coarse Cell
  // double coarseCellWidth = 1.0 / pow(3.0, level - 2);
  // double dxCoarse        = coarseCellWidth / double(LB_DIMENSION - 1);

  // tarch::la::Vector<DIMENSIONS, double> fineGridX(0.666666, 0.22222);

  // tarch::la::Vector<DIMENSIONS, double> coarseVertex1Position(0.66666, 0.0);

  // tarch::la::Vector<LB_DIR, tarch::la::Vector<LB_DIMENSION, double> > dirs;

  ///// fill dirs -- data from coarse grid
  // for (int direction = 0; direction < LB_DIR; ++direction)
  // for (int i = 0; i < LB_DIMENSION; ++i)
  // dirs[direction][i] = dxCoarse * i / coarseCellWidth;

  // Interpolation::interpolateSpaciallyVertical(vertex1,
  // vertex2,
  // dxFine,
  // dxCoarse,
  // fineGridX,
  // coarseVertex1Position,
  // dirs,
  // Interpolation::LEFT);

  ////  double baseValue = 2 / 3.0;
  // for (int direction = 0; direction < LB_DIR; ++direction) {
  // int    i = 0;
  // double a = vertex1.getDir(direction)[i];
  // double b = (fineGridX(1) + i * dxFine) / coarseCellWidth;
  // validateNumericalEqualsWithEps(a, b, 0.0001);

  // int offset = LB_DIMENSION - 1;

  // for (int i = 1; i < LB_DIMENSION - 1; ++i) {
  // double a = vertex1.getDir(direction)[i + offset];
  // double b = (fineGridX(1) + i * dxFine) / coarseCellWidth;
  // validateNumericalEqualsWithEps(a, b, 0.0001);
  // }
  // }
}
}
}

#ifdef UseTestSpecificCompilerSettings
  #pragma optimize("",on)
#endif
