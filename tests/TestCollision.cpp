#include "latticeboltzmann/tests/TestCollision.h"

#include "tarch/compiler/CompilerSpecificSettings.h"
#include "tarch/tests/TestCaseFactory.h"

#include <vector>

#ifdef Dim2
  #include "latticeboltzmann/heap2D/VertexHeap.h"
#endif

#ifdef Dim3
  #include "latticeboltzmann/heap3D/VertexHeap.h"
#endif

#include "latticeboltzmann/lbmsteps/VertexSteps.h"

// registerTest(latticeboltzmann::tests::TestCollision)
static tarch::tests::TestCaseFactory<latticeboltzmann::tests::TestCollision>
thisTestCaseFactoryInstance(tarch::tests::TestCaseFactory<latticeboltzmann::tests::TestCollision>::UnitTest,
                            "latticeboltzmann::tests::TestCollision");

#ifdef UseTestSpecificCompilerSettings
  #pragma optimize("",off)
#endif
latticeboltzmann::tests::TestCollision::
TestCollision():
  tarch::tests::TestCase("latticeboltzmann::tests::TestCase") {}

latticeboltzmann::tests::TestCollision::~TestCollision() {}

void
latticeboltzmann::tests::TestCollision::
run()
{
  // @todo If you have further tests, add them here
  testMethod(testMomentumConservation);
  testMethod(testVelocityInitialization);
  testMethod(test3);
}

void
latticeboltzmann::tests::TestCollision::
testMomentumConservation()
{
  // create one vertex
  latticeboltzmann::VertexHeap vertex;

  // initialize it
  Vector<VERTEX_BLOCK_SIZE, double> density(1.0);

#ifdef Dim2
  // initial velocity
  Vector<DIMENSIONS, double> v(0.0, 0.0);

#endif
#ifdef Dim3
  Vector<DIMENSIONS, double> v(0.0, 0.0, 0.0);

#endif

  Vector<VERTEX_BLOCK_SIZE, Vector<DIMENSIONS, double> > velocity1(v);

  // compute distibutions for given velocity
  Vector<VERTEX_BLOCK_SIZE, Vector<LB_DIR, double> > feq;
  VertexSteps::computeFeq(density, velocity1, feq);

#ifdef Dim2
  const Vector<DIMENSIONS, double> fineGridX(0.0, 1.0);

#endif
#ifdef Dim3
  const Vector<DIMENSIONS, double> fineGridX(0.0, 0.0, 1.0);

#endif

  latticeboltzmann::VertexSteps::initializeVertex(vertex, feq);

  // apply boundary condition
  latticeboltzmann::VertexSteps::boundaryConditionVertex(vertex, fineGridX);

  // check velocity before collision
  latticeboltzmann::VertexSteps::computeDensity(vertex, density);
  latticeboltzmann::VertexSteps::computeVelocity(vertex, density, velocity1);

  // collision
  //  latticeboltzmann::VertexSteps::collideVertex(vertex, 1.0 /*tau*/);
  //  Vector<LB_DIR, Vector<VERTEX_BLOCK_SIZE, double> > feq;
  latticeboltzmann::VertexSteps::computeFeq(density, velocity1, feq);
  latticeboltzmann::VertexSteps::computePostCollisionDistribution(vertex, 1.0, feq);

  Vector<VERTEX_BLOCK_SIZE, Vector<DIMENSIONS, double> > velocity2(v);

  // compute velocity after collision
  latticeboltzmann::VertexSteps::computeDensity(vertex, density);
  latticeboltzmann::VertexSteps::computeVelocity(vertex, density, velocity2);

  // check
  for (int i = 0; i < VERTEX_BLOCK_SIZE; ++i) {
    validateNumericalEqualsWithEps(velocity1[0][0], velocity2[0][0], 0.0001);
    validateNumericalEqualsWithEps(velocity1[0][1], velocity2[0][1], 0.0001);
#ifdef Dim3
    validateNumericalEqualsWithEps(velocity1[0][2], velocity2[0][2], 0.0001);
#endif
  }
}

// ----------------------------------------------------------------------------------

void
latticeboltzmann::tests::TestCollision::
testVelocityInitialization()
{
  // create one vertex
  latticeboltzmann::VertexHeap vertex;

  // initialize it
  Vector<VERTEX_BLOCK_SIZE, double> density(1.0);

  // initial velocity
#ifdef Dim2
  Vector<DIMENSIONS, double> v(1.0, 0.5);

#endif
#ifdef Dim3
  Vector<DIMENSIONS, double> v(1.0, 0.5, 0.3);

#endif

  Vector<VERTEX_BLOCK_SIZE, Vector<DIMENSIONS, double> > velocity(v);

  // compute distibutions for given velocity
  Vector<VERTEX_BLOCK_SIZE, Vector<LB_DIR, double> > feq;
  VertexSteps::computeFeq(density, velocity, feq);

  // assign distributions to vertex
  latticeboltzmann::VertexSteps::initializeVertex(vertex, feq);
  latticeboltzmann::VertexSteps::computeVelocity(vertex, density, velocity);

  // check
  for (int i = 0; i < VERTEX_BLOCK_SIZE; ++i) {
    validateNumericalEqualsWithEps(v[0], velocity[i][0], 0.0001);
    validateNumericalEqualsWithEps(v[1], velocity[i][1], 0.0001);
#ifdef Dim3
    validateNumericalEqualsWithEps(v[2], velocity[i][2], 0.0001);
#endif
  }
}

void
latticeboltzmann::tests::TestCollision::
test3()
{
  // @todo Add your test here
  validateEquals(3, 3);
}

#ifdef UseTestSpecificCompilerSettings
  #pragma optimize("",on)
#endif
