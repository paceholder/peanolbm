#ifndef _SPLINES_H_
#define _SPLINES_H_

#include <assert.h>
#include <iostream>
#include <vector>

namespace latticeboltzmann {
namespace splines {
double
linearInterpolation(double a, double b, double x)
{
  return a * (1 - x) + b * x;
}

double
cubicInterpolate(std::vector<double> p, int a, double x)
{
  return p[a] + 0.5 * x * (p[a + 1] - p[a - 1]
                           + x * (2.0 * p[a - 1] - 5.0 * p[a] +
                                  4.0 * p[a + 1] - p[a + 2] +
                                  x * (3.0 * (p[a] - p[a + 1]) + p[a + 2] - p[a - 1])));
}

double
// bicubicInterpolate(double p[4][4], double x, double y)
bicubicInterpolate(std::vector<std::vector<double> > p, int a, int b, double x, double y)
{
  std::vector<double> arr(4);

  arr[0] = cubicInterpolate(p[a - 1], b, y);
  arr[1] = cubicInterpolate(p[a + 0], b, y);
  arr[2] = cubicInterpolate(p[a + 1], b, y);
  arr[3] = cubicInterpolate(p[a + 2], b, y);
  return cubicInterpolate(arr, a, x);
}

// double
// tricubicInterpolate(double p[4][4][4], double x, double y, double z)
// {
//  double arr[4];
//  arr[0] = bicubicInterpolate(p[0], y, z);
//  arr[1] = bicubicInterpolate(p[1], y, z);
//  arr[2] = bicubicInterpolate(p[2], y, z);
//  arr[3] = bicubicInterpolate(p[3], y, z);
//  return cubicInterpolate(arr, x);
// }

// double
// nCubicInterpolate(int n, double* p, double coordinates[])
// {
//  assert(n > 0);

//  if (n == 1)
//    return cubicInterpolate(p, *coordinates);

//  else {
//    double arr[4];
//    int    skip = 1 << (n - 1) * 2;
//    arr[0] = nCubicInterpolate(n - 1, p, coordinates + 1);
//    arr[1] = nCubicInterpolate(n - 1, p + skip, coordinates + 1);
//    arr[2] = nCubicInterpolate(n - 1, p + 2 * skip, coordinates + 1);
//    arr[3] = nCubicInterpolate(n - 1, p + 3 * skip, coordinates + 1);
//    return cubicInterpolate(arr, *coordinates);
//  }
// }
}
}

#endif
