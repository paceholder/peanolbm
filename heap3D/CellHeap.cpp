#include "CellHeap.h"
latticeboltzmann::CellHeap::PersistentRecords::
PersistentRecords() {}

latticeboltzmann::CellHeap::PersistentRecords::
PersistentRecords(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BS,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BE,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& B,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BW,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BN,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& SE,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& S,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& SW,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& E,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& C,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& W,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& NE,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& N,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& NW,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TS,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TE,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& T,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TW,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TN):
  _BS(BS),
  _BE(BE),
  _B(B),
  _BW(BW),
  _BN(BN),
  _SE(SE),
  _S(S),
  _SW(SW),
  _E(E),
  _C(C),
  _W(W),
  _NE(NE),
  _N(N),
  _NW(NW),
  _TS(TS),
  _TE(TE),
  _T(T),
  _TW(TW),
  _TN(TN) {}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::PersistentRecords::
getBS() const
{
  return _BS;
}

void
latticeboltzmann::CellHeap::PersistentRecords::
setBS(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BS)
{
  _BS = (BS);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::PersistentRecords::
getBE() const
{
  return _BE;
}

void
latticeboltzmann::CellHeap::PersistentRecords::
setBE(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BE)
{
  _BE = (BE);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::PersistentRecords::
getB() const
{
  return _B;
}

void
latticeboltzmann::CellHeap::PersistentRecords::
setB(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& B)
{
  _B = (B);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::PersistentRecords::
getBW() const
{
  return _BW;
}

void
latticeboltzmann::CellHeap::PersistentRecords::
setBW(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BW)
{
  _BW = (BW);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::PersistentRecords::
getBN() const
{
  return _BN;
}

void
latticeboltzmann::CellHeap::PersistentRecords::
setBN(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BN)
{
  _BN = (BN);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::PersistentRecords::
getSE() const
{
  return _SE;
}

void
latticeboltzmann::CellHeap::PersistentRecords::
setSE(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& SE)
{
  _SE = (SE);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::PersistentRecords::
getS() const
{
  return _S;
}

void
latticeboltzmann::CellHeap::PersistentRecords::
setS(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& S)
{
  _S = (S);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::PersistentRecords::
getSW() const
{
  return _SW;
}

void
latticeboltzmann::CellHeap::PersistentRecords::
setSW(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& SW)
{
  _SW = (SW);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::PersistentRecords::
getE() const
{
  return _E;
}

void
latticeboltzmann::CellHeap::PersistentRecords::
setE(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& E)
{
  _E = (E);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::PersistentRecords::
getC() const
{
  return _C;
}

void
latticeboltzmann::CellHeap::PersistentRecords::
setC(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& C)
{
  _C = (C);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::PersistentRecords::
getW() const
{
  return _W;
}

void
latticeboltzmann::CellHeap::PersistentRecords::
setW(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& W)
{
  _W = (W);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::PersistentRecords::
getNE() const
{
  return _NE;
}

void
latticeboltzmann::CellHeap::PersistentRecords::
setNE(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& NE)
{
  _NE = (NE);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::PersistentRecords::
getN() const
{
  return _N;
}

void
latticeboltzmann::CellHeap::PersistentRecords::
setN(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& N)
{
  _N = (N);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::PersistentRecords::
getNW() const
{
  return _NW;
}

void
latticeboltzmann::CellHeap::PersistentRecords::
setNW(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& NW)
{
  _NW = (NW);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::PersistentRecords::
getTS() const
{
  return _TS;
}

void
latticeboltzmann::CellHeap::PersistentRecords::
setTS(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TS)
{
  _TS = (TS);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::PersistentRecords::
getTE() const
{
  return _TE;
}

void
latticeboltzmann::CellHeap::PersistentRecords::
setTE(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TE)
{
  _TE = (TE);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::PersistentRecords::
getT() const
{
  return _T;
}

void
latticeboltzmann::CellHeap::PersistentRecords::
setT(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& T)
{
  _T = (T);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::PersistentRecords::
getTW() const
{
  return _TW;
}

void
latticeboltzmann::CellHeap::PersistentRecords::
setTW(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TW)
{
  _TW = (TW);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::PersistentRecords::
getTN() const
{
  return _TN;
}

void
latticeboltzmann::CellHeap::PersistentRecords::
setTN(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TN)
{
  _TN = (TN);
}

latticeboltzmann::CellHeap::
CellHeap() {}

latticeboltzmann::CellHeap::
CellHeap(const PersistentRecords& persistentRecords):
  _persistentRecords(persistentRecords._BS,
                     persistentRecords._BE,
                     persistentRecords._B,
                     persistentRecords._BW,
                     persistentRecords._BN,
                     persistentRecords._SE,
                     persistentRecords._S,
                     persistentRecords._SW,
                     persistentRecords._E,
                     persistentRecords._C,
                     persistentRecords._W,
                     persistentRecords._NE,
                     persistentRecords._N,
                     persistentRecords._NW,
                     persistentRecords._TS,
                     persistentRecords._TE,
                     persistentRecords._T,
                     persistentRecords._TW,
                     persistentRecords._TN) {}

latticeboltzmann::CellHeap::
CellHeap(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BS,
         const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BE,
         const tarch::la::Vector<CELL_BLOCK_SIZE, double>& B,
         const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BW,
         const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BN,
         const tarch::la::Vector<CELL_BLOCK_SIZE, double>& SE,
         const tarch::la::Vector<CELL_BLOCK_SIZE, double>& S,
         const tarch::la::Vector<CELL_BLOCK_SIZE, double>& SW,
         const tarch::la::Vector<CELL_BLOCK_SIZE, double>& E,
         const tarch::la::Vector<CELL_BLOCK_SIZE, double>& C,
         const tarch::la::Vector<CELL_BLOCK_SIZE, double>& W,
         const tarch::la::Vector<CELL_BLOCK_SIZE, double>& NE,
         const tarch::la::Vector<CELL_BLOCK_SIZE, double>& N,
         const tarch::la::Vector<CELL_BLOCK_SIZE, double>& NW,
         const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TS,
         const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TE,
         const tarch::la::Vector<CELL_BLOCK_SIZE, double>& T,
         const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TW,
         const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TN):
  _persistentRecords(BS, BE, B, BW, BN, SE, S, SW, E, C, W, NE, N, NW, TS, TE, T, TW, TN) {}

latticeboltzmann::CellHeap::~CellHeap() {}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::
getBS() const
{
  return _persistentRecords._BS;
}

void
latticeboltzmann::CellHeap::
setBS(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BS)
{
  _persistentRecords._BS = (BS);
}

double
latticeboltzmann::CellHeap::
getBS(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._BS[elementIndex];
}

void
latticeboltzmann::CellHeap::
setBS(int elementIndex, const double& BS)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._BS[elementIndex] = BS;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::
getBE() const
{
  return _persistentRecords._BE;
}

void
latticeboltzmann::CellHeap::
setBE(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BE)
{
  _persistentRecords._BE = (BE);
}

double
latticeboltzmann::CellHeap::
getBE(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._BE[elementIndex];
}

void
latticeboltzmann::CellHeap::
setBE(int elementIndex, const double& BE)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._BE[elementIndex] = BE;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::
getB() const
{
  return _persistentRecords._B;
}

void
latticeboltzmann::CellHeap::
setB(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& B)
{
  _persistentRecords._B = (B);
}

double
latticeboltzmann::CellHeap::
getB(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._B[elementIndex];
}

void
latticeboltzmann::CellHeap::
setB(int elementIndex, const double& B)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._B[elementIndex] = B;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::
getBW() const
{
  return _persistentRecords._BW;
}

void
latticeboltzmann::CellHeap::
setBW(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BW)
{
  _persistentRecords._BW = (BW);
}

double
latticeboltzmann::CellHeap::
getBW(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._BW[elementIndex];
}

void
latticeboltzmann::CellHeap::
setBW(int elementIndex, const double& BW)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._BW[elementIndex] = BW;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::
getBN() const
{
  return _persistentRecords._BN;
}

void
latticeboltzmann::CellHeap::
setBN(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BN)
{
  _persistentRecords._BN = (BN);
}

double
latticeboltzmann::CellHeap::
getBN(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._BN[elementIndex];
}

void
latticeboltzmann::CellHeap::
setBN(int elementIndex, const double& BN)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._BN[elementIndex] = BN;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::
getSE() const
{
  return _persistentRecords._SE;
}

void
latticeboltzmann::CellHeap::
setSE(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& SE)
{
  _persistentRecords._SE = (SE);
}

double
latticeboltzmann::CellHeap::
getSE(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._SE[elementIndex];
}

void
latticeboltzmann::CellHeap::
setSE(int elementIndex, const double& SE)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._SE[elementIndex] = SE;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::
getS() const
{
  return _persistentRecords._S;
}

void
latticeboltzmann::CellHeap::
setS(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& S)
{
  _persistentRecords._S = (S);
}

double
latticeboltzmann::CellHeap::
getS(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._S[elementIndex];
}

void
latticeboltzmann::CellHeap::
setS(int elementIndex, const double& S)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._S[elementIndex] = S;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::
getSW() const
{
  return _persistentRecords._SW;
}

void
latticeboltzmann::CellHeap::
setSW(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& SW)
{
  _persistentRecords._SW = (SW);
}

double
latticeboltzmann::CellHeap::
getSW(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._SW[elementIndex];
}

void
latticeboltzmann::CellHeap::
setSW(int elementIndex, const double& SW)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._SW[elementIndex] = SW;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::
getE() const
{
  return _persistentRecords._E;
}

void
latticeboltzmann::CellHeap::
setE(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& E)
{
  _persistentRecords._E = (E);
}

double
latticeboltzmann::CellHeap::
getE(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._E[elementIndex];
}

void
latticeboltzmann::CellHeap::
setE(int elementIndex, const double& E)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._E[elementIndex] = E;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::
getC() const
{
  return _persistentRecords._C;
}

void
latticeboltzmann::CellHeap::
setC(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& C)
{
  _persistentRecords._C = (C);
}

double
latticeboltzmann::CellHeap::
getC(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._C[elementIndex];
}

void
latticeboltzmann::CellHeap::
setC(int elementIndex, const double& C)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._C[elementIndex] = C;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::
getW() const
{
  return _persistentRecords._W;
}

void
latticeboltzmann::CellHeap::
setW(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& W)
{
  _persistentRecords._W = (W);
}

double
latticeboltzmann::CellHeap::
getW(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._W[elementIndex];
}

void
latticeboltzmann::CellHeap::
setW(int elementIndex, const double& W)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._W[elementIndex] = W;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::
getNE() const
{
  return _persistentRecords._NE;
}

void
latticeboltzmann::CellHeap::
setNE(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& NE)
{
  _persistentRecords._NE = (NE);
}

double
latticeboltzmann::CellHeap::
getNE(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._NE[elementIndex];
}

void
latticeboltzmann::CellHeap::
setNE(int elementIndex, const double& NE)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._NE[elementIndex] = NE;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::
getN() const
{
  return _persistentRecords._N;
}

void
latticeboltzmann::CellHeap::
setN(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& N)
{
  _persistentRecords._N = (N);
}

double
latticeboltzmann::CellHeap::
getN(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._N[elementIndex];
}

void
latticeboltzmann::CellHeap::
setN(int elementIndex, const double& N)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._N[elementIndex] = N;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::
getNW() const
{
  return _persistentRecords._NW;
}

void
latticeboltzmann::CellHeap::
setNW(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& NW)
{
  _persistentRecords._NW = (NW);
}

double
latticeboltzmann::CellHeap::
getNW(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._NW[elementIndex];
}

void
latticeboltzmann::CellHeap::
setNW(int elementIndex, const double& NW)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._NW[elementIndex] = NW;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::
getTS() const
{
  return _persistentRecords._TS;
}

void
latticeboltzmann::CellHeap::
setTS(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TS)
{
  _persistentRecords._TS = (TS);
}

double
latticeboltzmann::CellHeap::
getTS(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._TS[elementIndex];
}

void
latticeboltzmann::CellHeap::
setTS(int elementIndex, const double& TS)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._TS[elementIndex] = TS;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::
getTE() const
{
  return _persistentRecords._TE;
}

void
latticeboltzmann::CellHeap::
setTE(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TE)
{
  _persistentRecords._TE = (TE);
}

double
latticeboltzmann::CellHeap::
getTE(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._TE[elementIndex];
}

void
latticeboltzmann::CellHeap::
setTE(int elementIndex, const double& TE)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._TE[elementIndex] = TE;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::
getT() const
{
  return _persistentRecords._T;
}

void
latticeboltzmann::CellHeap::
setT(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& T)
{
  _persistentRecords._T = (T);
}

double
latticeboltzmann::CellHeap::
getT(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._T[elementIndex];
}

void
latticeboltzmann::CellHeap::
setT(int elementIndex, const double& T)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._T[elementIndex] = T;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::
getTW() const
{
  return _persistentRecords._TW;
}

void
latticeboltzmann::CellHeap::
setTW(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TW)
{
  _persistentRecords._TW = (TW);
}

double
latticeboltzmann::CellHeap::
getTW(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._TW[elementIndex];
}

void
latticeboltzmann::CellHeap::
setTW(int elementIndex, const double& TW)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._TW[elementIndex] = TW;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeap::
getTN() const
{
  return _persistentRecords._TN;
}

void
latticeboltzmann::CellHeap::
setTN(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TN)
{
  _persistentRecords._TN = (TN);
}

double
latticeboltzmann::CellHeap::
getTN(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._TN[elementIndex];
}

void
latticeboltzmann::CellHeap::
setTN(int elementIndex, const double& TN)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._TN[elementIndex] = TN;
}

std::string
latticeboltzmann::CellHeap::
toString() const
{
  std::ostringstream stringstr;
  toString(stringstr);
  return stringstr.str();
}

void
latticeboltzmann::CellHeap::
toString(std::ostream& out) const
{
  out << "(";
  out << "BS:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getBS(i) << ",";

  out << getBS(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "BE:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getBE(i) << ",";

  out << getBE(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "B:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getB(i) << ",";

  out << getB(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "BW:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getBW(i) << ",";

  out << getBW(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "BN:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getBN(i) << ",";

  out << getBN(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "SE:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getSE(i) << ",";

  out << getSE(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "S:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getS(i) << ",";

  out << getS(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "SW:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getSW(i) << ",";

  out << getSW(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "E:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getE(i) << ",";

  out << getE(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "C:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getC(i) << ",";

  out << getC(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "W:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getW(i) << ",";

  out << getW(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "NE:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getNE(i) << ",";

  out << getNE(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "N:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getN(i) << ",";

  out << getN(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "NW:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getNW(i) << ",";

  out << getNW(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "TS:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getTS(i) << ",";

  out << getTS(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "TE:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getTE(i) << ",";

  out << getTE(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "T:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getT(i) << ",";

  out << getT(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "TW:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getTW(i) << ",";

  out << getTW(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "TN:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getTN(i) << ",";

  out << getTN(CELL_BLOCK_SIZE - 1) << "]";
  out <<  ")";
}

latticeboltzmann::CellHeap::PersistentRecords
latticeboltzmann::CellHeap::
getPersistentRecords() const
{
  return _persistentRecords;
}

latticeboltzmann::CellHeapPacked
latticeboltzmann::CellHeap::
convert() const
{
  return CellHeapPacked(
    getBS(),
    getBE(),
    getB(),
    getBW(),
    getBN(),
    getSE(),
    getS(),
    getSW(),
    getE(),
    getC(),
    getW(),
    getNE(),
    getN(),
    getNW(),
    getTS(),
    getTE(),
    getT(),
    getTW(),
    getTN()
    );
}

#ifdef Parallel
tarch::logging::Log latticeboltzmann::CellHeap::
_log("latticeboltzmann::CellHeap");

MPI_Datatype latticeboltzmann::CellHeap::Datatype     = 0;
MPI_Datatype latticeboltzmann::CellHeap::FullDatatype = 0;

void
latticeboltzmann::CellHeap::
initDatatype()
{
  {
    CellHeap dummyCellHeap[2];

    const int    Attributes           = 1;
    MPI_Datatype subtypes[Attributes] = {
      MPI_UB           // end/displacement flag
    };

    int blocklen[Attributes] = {
      1          // end/displacement flag
    };

    MPI_Aint disp[Attributes];

    MPI_Aint base;
    MPI_Address(const_cast<void*>(static_cast<const void*>(&(dummyCellHeap[0]))), &base);

    for (int i = 1; i < Attributes; i++)
      assertion1(disp[i] > disp[i - 1], i);

    for (int i = 0; i < Attributes; i++)
      disp[i] -= base;

    MPI_Type_struct(Attributes, blocklen, disp, subtypes, &CellHeap::Datatype);
    MPI_Type_commit(&CellHeap::Datatype);
  }
  {
    CellHeap dummyCellHeap[2];

    const int    Attributes           = 20;
    MPI_Datatype subtypes[Attributes] = {
      MPI_DOUBLE,          // BS
      MPI_DOUBLE,          // BE
      MPI_DOUBLE,          // B
      MPI_DOUBLE,          // BW
      MPI_DOUBLE,          // BN
      MPI_DOUBLE,          // SE
      MPI_DOUBLE,          // S
      MPI_DOUBLE,          // SW
      MPI_DOUBLE,          // E
      MPI_DOUBLE,          // C
      MPI_DOUBLE,          // W
      MPI_DOUBLE,          // NE
      MPI_DOUBLE,          // N
      MPI_DOUBLE,          // NW
      MPI_DOUBLE,          // TS
      MPI_DOUBLE,          // TE
      MPI_DOUBLE,          // T
      MPI_DOUBLE,          // TW
      MPI_DOUBLE,          // TN
      MPI_UB           // end/displacement flag
    };

    int blocklen[Attributes] = {
      CELL_BLOCK_SIZE,           // BS
      CELL_BLOCK_SIZE,           // BE
      CELL_BLOCK_SIZE,           // B
      CELL_BLOCK_SIZE,           // BW
      CELL_BLOCK_SIZE,           // BN
      CELL_BLOCK_SIZE,           // SE
      CELL_BLOCK_SIZE,           // S
      CELL_BLOCK_SIZE,           // SW
      CELL_BLOCK_SIZE,           // E
      CELL_BLOCK_SIZE,           // C
      CELL_BLOCK_SIZE,           // W
      CELL_BLOCK_SIZE,           // NE
      CELL_BLOCK_SIZE,           // N
      CELL_BLOCK_SIZE,           // NW
      CELL_BLOCK_SIZE,           // TS
      CELL_BLOCK_SIZE,           // TE
      CELL_BLOCK_SIZE,           // T
      CELL_BLOCK_SIZE,           // TW
      CELL_BLOCK_SIZE,           // TN
      1          // end/displacement flag
    };

    MPI_Aint disp[Attributes];

    MPI_Aint base;
    MPI_Address(const_cast<void*>(static_cast<const void*>(&(dummyCellHeap[0]))), &base);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeap[0]._persistentRecords._BS[0]))),     &disp[0]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeap[0]._persistentRecords._BE[0]))),     &disp[1]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(&(dummyCellHeap[0]._persistentRecords._B[0]))),
                &disp[2]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeap[0]._persistentRecords._BW[0]))),     &disp[3]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeap[0]._persistentRecords._BN[0]))),     &disp[4]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeap[0]._persistentRecords._SE[0]))),     &disp[5]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(&(dummyCellHeap[0]._persistentRecords._S[0]))),
                &disp[6]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeap[0]._persistentRecords._SW[0]))),     &disp[7]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(&(dummyCellHeap[0]._persistentRecords._E[0]))),
                &disp[8]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(&(dummyCellHeap[0]._persistentRecords._C[0]))),
                &disp[9]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(&(dummyCellHeap[0]._persistentRecords._W[0]))),
                &disp[10]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(&(dummyCellHeap[0]._persistentRecords._NE[0]))),
                &disp[11]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(&(dummyCellHeap[0]._persistentRecords._N[0]))),
                &disp[12]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(&(dummyCellHeap[0]._persistentRecords._NW[0]))),
                &disp[13]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(&(dummyCellHeap[0]._persistentRecords._TS[0]))),
                &disp[14]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(&(dummyCellHeap[0]._persistentRecords._TE[0]))),
                &disp[15]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(&(dummyCellHeap[0]._persistentRecords._T[0]))),
                &disp[16]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(&(dummyCellHeap[0]._persistentRecords._TW[0]))),
                &disp[17]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(&(dummyCellHeap[0]._persistentRecords._TN[0]))),
                &disp[18]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(&dummyCellHeap[1]._persistentRecords._BS[0])),
                &disp[19]);

    for (int i = 1; i < Attributes; i++)
      assertion1(disp[i] > disp[i - 1], i);

    for (int i = 0; i < Attributes; i++)
      disp[i] -= base;

    MPI_Type_struct(Attributes, blocklen, disp, subtypes, &CellHeap::FullDatatype);
    MPI_Type_commit(&CellHeap::FullDatatype);
  }
}

void
latticeboltzmann::CellHeap::
shutdownDatatype()
{
  MPI_Type_free(&CellHeap::Datatype);
  MPI_Type_free(&CellHeap::FullDatatype);
}

void
latticeboltzmann::CellHeap::
send(int destination, int tag, bool exchangeOnlyAttributesMarkedWithParallelise)
{
  MPI_Request* sendRequestHandle = new MPI_Request();

  MPI_Status status;
  int        flag = 0;
  int        result;

  clock_t timeOutWarning          = -1;
  clock_t timeOutShutdown         = -1;
  bool    triggeredTimeoutWarning = false;

  if (exchangeOnlyAttributesMarkedWithParallelise)
    result = MPI_Isend(
      this, 1, Datatype, destination,
      tag, tarch::parallel::Node::getInstance().getCommunicator(),
      sendRequestHandle
      );

  else
    result = MPI_Isend(
      this, 1, FullDatatype, destination,
      tag, tarch::parallel::Node::getInstance().getCommunicator(),
      sendRequestHandle
      );

  if  (result != MPI_SUCCESS) {
    std::ostringstream msg;
    msg << "was not able to send message latticeboltzmann::CellHeap "
        << toString()
        << " to node " << destination
        << ": " << tarch::parallel::MPIReturnValueToString(result);
    _log.error("send(int)", msg.str());
  }

  result = MPI_Test(sendRequestHandle, &flag, &status);

  while (!flag) {
    if (timeOutWarning == -1)
      timeOutWarning = tarch::parallel::Node::getInstance().getDeadlockWarningTimeStamp();

    if (timeOutShutdown == -1)
      timeOutShutdown = tarch::parallel::Node::getInstance().getDeadlockTimeOutTimeStamp();

    result = MPI_Test(sendRequestHandle, &flag, &status);

    if (result != MPI_SUCCESS) {
      std::ostringstream msg;
      msg << "testing for finished send task for latticeboltzmann::CellHeap "
          << toString()
          << " sent to node " << destination
          << " failed: " << tarch::parallel::MPIReturnValueToString(result);
      _log.error("send(int)", msg.str());
    }

    // deadlock aspect
    if (
      tarch::parallel::Node::getInstance().isTimeOutWarningEnabled() &&
      (clock() > timeOutWarning) &&
      (!triggeredTimeoutWarning)
      ) {
      tarch::parallel::Node::getInstance().writeTimeOutWarning(
        "latticeboltzmann::CellHeap",
        "send(int)", destination, tag, 1
        );
      triggeredTimeoutWarning = true;
    }

    if (
      tarch::parallel::Node::getInstance().isTimeOutDeadlockEnabled() &&
      (clock() > timeOutShutdown)
      )
      tarch::parallel::Node::getInstance().triggerDeadlockTimeOut(
        "latticeboltzmann::CellHeap",
        "send(int)", destination, tag, 1
        );

    tarch::parallel::Node::getInstance().receiveDanglingMessages();
  }

  delete sendRequestHandle;
  #ifdef Debug
  _log.debug("send(int,int)", "sent " + toString());
  #endif
}

void
latticeboltzmann::CellHeap::
receive(int source, int tag, bool exchangeOnlyAttributesMarkedWithParallelise)
{
  MPI_Request* sendRequestHandle = new MPI_Request();

  MPI_Status status;
  int        flag = 0;
  int        result;

  clock_t timeOutWarning          = -1;
  clock_t timeOutShutdown         = -1;
  bool    triggeredTimeoutWarning = false;

  if (exchangeOnlyAttributesMarkedWithParallelise)
    result = MPI_Irecv(
      this, 1, Datatype, source, tag,
      tarch::parallel::Node::getInstance().getCommunicator(), sendRequestHandle
      );

  else
    result = MPI_Irecv(
      this, 1, FullDatatype, source, tag,
      tarch::parallel::Node::getInstance().getCommunicator(), sendRequestHandle
      );

  if (result != MPI_SUCCESS) {
    std::ostringstream msg;
    msg << "failed to start to receive latticeboltzmann::CellHeap from node "
        << source << ": " << tarch::parallel::MPIReturnValueToString(result);
    _log.error("receive(int)", msg.str());
  }

  result = MPI_Test(sendRequestHandle, &flag, &status);

  while (!flag) {
    if (timeOutWarning == -1)
      timeOutWarning = tarch::parallel::Node::getInstance().getDeadlockWarningTimeStamp();

    if (timeOutShutdown == -1)
      timeOutShutdown = tarch::parallel::Node::getInstance().getDeadlockTimeOutTimeStamp();

    result = MPI_Test(sendRequestHandle, &flag, &status);

    if (result != MPI_SUCCESS) {
      std::ostringstream msg;
      msg << "testing for finished receive task for latticeboltzmann::CellHeap failed: "
          << tarch::parallel::MPIReturnValueToString(result);
      _log.error("receive(int)", msg.str());
    }

    // deadlock aspect
    if (
      tarch::parallel::Node::getInstance().isTimeOutWarningEnabled() &&
      (clock() > timeOutWarning) &&
      (!triggeredTimeoutWarning)
      ) {
      tarch::parallel::Node::getInstance().writeTimeOutWarning(
        "latticeboltzmann::CellHeap",
        "receive(int)", source, tag, 1
        );
      triggeredTimeoutWarning = true;
    }

    if (
      tarch::parallel::Node::getInstance().isTimeOutDeadlockEnabled() &&
      (clock() > timeOutShutdown)
      )
      tarch::parallel::Node::getInstance().triggerDeadlockTimeOut(
        "latticeboltzmann::CellHeap",
        "receive(int)", source, tag, 1
        );

    tarch::parallel::Node::getInstance().receiveDanglingMessages();
  }

  delete sendRequestHandle;

  #ifdef Debug
  _log.debug("receive(int,int)", "received " + toString());
  #endif
}

bool
latticeboltzmann::CellHeap::
isMessageInQueue(int tag, bool exchangeOnlyAttributesMarkedWithParallelise)
{
  MPI_Status status;
  int        flag = 0;
  MPI_Iprobe(
    MPI_ANY_SOURCE, tag,
    tarch::parallel::Node::getInstance().getCommunicator(), &flag, &status
    );

  if (flag) {
    int messageCounter;

    if (exchangeOnlyAttributesMarkedWithParallelise)
      MPI_Get_count(&status, Datatype, &messageCounter);
    else
      MPI_Get_count(&status, FullDatatype, &messageCounter);

    return messageCounter > 0;
  } else
    return false;
}

#endif
latticeboltzmann::CellHeapPacked::PersistentRecords::
PersistentRecords() {}

latticeboltzmann::CellHeapPacked::PersistentRecords::
PersistentRecords(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BS,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BE,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& B,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BW,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BN,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& SE,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& S,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& SW,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& E,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& C,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& W,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& NE,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& N,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& NW,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TS,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TE,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& T,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TW,
                  const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TN):
  _BS(BS),
  _BE(BE),
  _B(B),
  _BW(BW),
  _BN(BN),
  _SE(SE),
  _S(S),
  _SW(SW),
  _E(E),
  _C(C),
  _W(W),
  _NE(NE),
  _N(N),
  _NW(NW),
  _TS(TS),
  _TE(TE),
  _T(T),
  _TW(TW),
  _TN(TN) {}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::PersistentRecords::
getBS() const
{
  return _BS;
}

void
latticeboltzmann::CellHeapPacked::PersistentRecords::
setBS(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BS)
{
  _BS = (BS);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::PersistentRecords::
getBE() const
{
  return _BE;
}

void
latticeboltzmann::CellHeapPacked::PersistentRecords::
setBE(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BE)
{
  _BE = (BE);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::PersistentRecords::
getB() const
{
  return _B;
}

void
latticeboltzmann::CellHeapPacked::PersistentRecords::
setB(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& B)
{
  _B = (B);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::PersistentRecords::
getBW() const
{
  return _BW;
}

void
latticeboltzmann::CellHeapPacked::PersistentRecords::
setBW(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BW)
{
  _BW = (BW);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::PersistentRecords::
getBN() const
{
  return _BN;
}

void
latticeboltzmann::CellHeapPacked::PersistentRecords::
setBN(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BN)
{
  _BN = (BN);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::PersistentRecords::
getSE() const
{
  return _SE;
}

void
latticeboltzmann::CellHeapPacked::PersistentRecords::
setSE(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& SE)
{
  _SE = (SE);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::PersistentRecords::
getS() const
{
  return _S;
}

void
latticeboltzmann::CellHeapPacked::PersistentRecords::
setS(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& S)
{
  _S = (S);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::PersistentRecords::
getSW() const
{
  return _SW;
}

void
latticeboltzmann::CellHeapPacked::PersistentRecords::
setSW(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& SW)
{
  _SW = (SW);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::PersistentRecords::
getE() const
{
  return _E;
}

void
latticeboltzmann::CellHeapPacked::PersistentRecords::
setE(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& E)
{
  _E = (E);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::PersistentRecords::
getC() const
{
  return _C;
}

void
latticeboltzmann::CellHeapPacked::PersistentRecords::
setC(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& C)
{
  _C = (C);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::PersistentRecords::
getW() const
{
  return _W;
}

void
latticeboltzmann::CellHeapPacked::PersistentRecords::
setW(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& W)
{
  _W = (W);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::PersistentRecords::
getNE() const
{
  return _NE;
}

void
latticeboltzmann::CellHeapPacked::PersistentRecords::
setNE(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& NE)
{
  _NE = (NE);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::PersistentRecords::
getN() const
{
  return _N;
}

void
latticeboltzmann::CellHeapPacked::PersistentRecords::
setN(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& N)
{
  _N = (N);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::PersistentRecords::
getNW() const
{
  return _NW;
}

void
latticeboltzmann::CellHeapPacked::PersistentRecords::
setNW(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& NW)
{
  _NW = (NW);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::PersistentRecords::
getTS() const
{
  return _TS;
}

void
latticeboltzmann::CellHeapPacked::PersistentRecords::
setTS(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TS)
{
  _TS = (TS);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::PersistentRecords::
getTE() const
{
  return _TE;
}

void
latticeboltzmann::CellHeapPacked::PersistentRecords::
setTE(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TE)
{
  _TE = (TE);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::PersistentRecords::
getT() const
{
  return _T;
}

void
latticeboltzmann::CellHeapPacked::PersistentRecords::
setT(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& T)
{
  _T = (T);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::PersistentRecords::
getTW() const
{
  return _TW;
}

void
latticeboltzmann::CellHeapPacked::PersistentRecords::
setTW(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TW)
{
  _TW = (TW);
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::PersistentRecords::
getTN() const
{
  return _TN;
}

void
latticeboltzmann::CellHeapPacked::PersistentRecords::
setTN(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TN)
{
  _TN = (TN);
}

latticeboltzmann::CellHeapPacked::
CellHeapPacked() {}

latticeboltzmann::CellHeapPacked::
CellHeapPacked(const PersistentRecords& persistentRecords):
  _persistentRecords(persistentRecords._BS,
                     persistentRecords._BE,
                     persistentRecords._B,
                     persistentRecords._BW,
                     persistentRecords._BN,
                     persistentRecords._SE,
                     persistentRecords._S,
                     persistentRecords._SW,
                     persistentRecords._E,
                     persistentRecords._C,
                     persistentRecords._W,
                     persistentRecords._NE,
                     persistentRecords._N,
                     persistentRecords._NW,
                     persistentRecords._TS,
                     persistentRecords._TE,
                     persistentRecords._T,
                     persistentRecords._TW,
                     persistentRecords._TN) {}

latticeboltzmann::CellHeapPacked::
CellHeapPacked(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BS,
               const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BE,
               const tarch::la::Vector<CELL_BLOCK_SIZE, double>& B,
               const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BW,
               const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BN,
               const tarch::la::Vector<CELL_BLOCK_SIZE, double>& SE,
               const tarch::la::Vector<CELL_BLOCK_SIZE, double>& S,
               const tarch::la::Vector<CELL_BLOCK_SIZE, double>& SW,
               const tarch::la::Vector<CELL_BLOCK_SIZE, double>& E,
               const tarch::la::Vector<CELL_BLOCK_SIZE, double>& C,
               const tarch::la::Vector<CELL_BLOCK_SIZE, double>& W,
               const tarch::la::Vector<CELL_BLOCK_SIZE, double>& NE,
               const tarch::la::Vector<CELL_BLOCK_SIZE, double>& N,
               const tarch::la::Vector<CELL_BLOCK_SIZE, double>& NW,
               const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TS,
               const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TE,
               const tarch::la::Vector<CELL_BLOCK_SIZE, double>& T,
               const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TW,
               const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TN):
  _persistentRecords(BS, BE, B, BW, BN, SE, S, SW, E, C, W, NE, N, NW, TS, TE, T, TW, TN) {}

latticeboltzmann::CellHeapPacked::~CellHeapPacked() {}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::
getBS() const
{
  return _persistentRecords._BS;
}

void
latticeboltzmann::CellHeapPacked::
setBS(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BS)
{
  _persistentRecords._BS = (BS);
}

double
latticeboltzmann::CellHeapPacked::
getBS(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._BS[elementIndex];
}

void
latticeboltzmann::CellHeapPacked::
setBS(int elementIndex, const double& BS)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._BS[elementIndex] = BS;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::
getBE() const
{
  return _persistentRecords._BE;
}

void
latticeboltzmann::CellHeapPacked::
setBE(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BE)
{
  _persistentRecords._BE = (BE);
}

double
latticeboltzmann::CellHeapPacked::
getBE(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._BE[elementIndex];
}

void
latticeboltzmann::CellHeapPacked::
setBE(int elementIndex, const double& BE)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._BE[elementIndex] = BE;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::
getB() const
{
  return _persistentRecords._B;
}

void
latticeboltzmann::CellHeapPacked::
setB(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& B)
{
  _persistentRecords._B = (B);
}

double
latticeboltzmann::CellHeapPacked::
getB(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._B[elementIndex];
}

void
latticeboltzmann::CellHeapPacked::
setB(int elementIndex, const double& B)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._B[elementIndex] = B;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::
getBW() const
{
  return _persistentRecords._BW;
}

void
latticeboltzmann::CellHeapPacked::
setBW(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BW)
{
  _persistentRecords._BW = (BW);
}

double
latticeboltzmann::CellHeapPacked::
getBW(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._BW[elementIndex];
}

void
latticeboltzmann::CellHeapPacked::
setBW(int elementIndex, const double& BW)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._BW[elementIndex] = BW;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::
getBN() const
{
  return _persistentRecords._BN;
}

void
latticeboltzmann::CellHeapPacked::
setBN(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& BN)
{
  _persistentRecords._BN = (BN);
}

double
latticeboltzmann::CellHeapPacked::
getBN(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._BN[elementIndex];
}

void
latticeboltzmann::CellHeapPacked::
setBN(int elementIndex, const double& BN)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._BN[elementIndex] = BN;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::
getSE() const
{
  return _persistentRecords._SE;
}

void
latticeboltzmann::CellHeapPacked::
setSE(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& SE)
{
  _persistentRecords._SE = (SE);
}

double
latticeboltzmann::CellHeapPacked::
getSE(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._SE[elementIndex];
}

void
latticeboltzmann::CellHeapPacked::
setSE(int elementIndex, const double& SE)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._SE[elementIndex] = SE;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::
getS() const
{
  return _persistentRecords._S;
}

void
latticeboltzmann::CellHeapPacked::
setS(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& S)
{
  _persistentRecords._S = (S);
}

double
latticeboltzmann::CellHeapPacked::
getS(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._S[elementIndex];
}

void
latticeboltzmann::CellHeapPacked::
setS(int elementIndex, const double& S)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._S[elementIndex] = S;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::
getSW() const
{
  return _persistentRecords._SW;
}

void
latticeboltzmann::CellHeapPacked::
setSW(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& SW)
{
  _persistentRecords._SW = (SW);
}

double
latticeboltzmann::CellHeapPacked::
getSW(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._SW[elementIndex];
}

void
latticeboltzmann::CellHeapPacked::
setSW(int elementIndex, const double& SW)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._SW[elementIndex] = SW;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::
getE() const
{
  return _persistentRecords._E;
}

void
latticeboltzmann::CellHeapPacked::
setE(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& E)
{
  _persistentRecords._E = (E);
}

double
latticeboltzmann::CellHeapPacked::
getE(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._E[elementIndex];
}

void
latticeboltzmann::CellHeapPacked::
setE(int elementIndex, const double& E)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._E[elementIndex] = E;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::
getC() const
{
  return _persistentRecords._C;
}

void
latticeboltzmann::CellHeapPacked::
setC(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& C)
{
  _persistentRecords._C = (C);
}

double
latticeboltzmann::CellHeapPacked::
getC(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._C[elementIndex];
}

void
latticeboltzmann::CellHeapPacked::
setC(int elementIndex, const double& C)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._C[elementIndex] = C;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::
getW() const
{
  return _persistentRecords._W;
}

void
latticeboltzmann::CellHeapPacked::
setW(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& W)
{
  _persistentRecords._W = (W);
}

double
latticeboltzmann::CellHeapPacked::
getW(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._W[elementIndex];
}

void
latticeboltzmann::CellHeapPacked::
setW(int elementIndex, const double& W)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._W[elementIndex] = W;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::
getNE() const
{
  return _persistentRecords._NE;
}

void
latticeboltzmann::CellHeapPacked::
setNE(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& NE)
{
  _persistentRecords._NE = (NE);
}

double
latticeboltzmann::CellHeapPacked::
getNE(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._NE[elementIndex];
}

void
latticeboltzmann::CellHeapPacked::
setNE(int elementIndex, const double& NE)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._NE[elementIndex] = NE;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::
getN() const
{
  return _persistentRecords._N;
}

void
latticeboltzmann::CellHeapPacked::
setN(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& N)
{
  _persistentRecords._N = (N);
}

double
latticeboltzmann::CellHeapPacked::
getN(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._N[elementIndex];
}

void
latticeboltzmann::CellHeapPacked::
setN(int elementIndex, const double& N)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._N[elementIndex] = N;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::
getNW() const
{
  return _persistentRecords._NW;
}

void
latticeboltzmann::CellHeapPacked::
setNW(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& NW)
{
  _persistentRecords._NW = (NW);
}

double
latticeboltzmann::CellHeapPacked::
getNW(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._NW[elementIndex];
}

void
latticeboltzmann::CellHeapPacked::
setNW(int elementIndex, const double& NW)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._NW[elementIndex] = NW;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::
getTS() const
{
  return _persistentRecords._TS;
}

void
latticeboltzmann::CellHeapPacked::
setTS(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TS)
{
  _persistentRecords._TS = (TS);
}

double
latticeboltzmann::CellHeapPacked::
getTS(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._TS[elementIndex];
}

void
latticeboltzmann::CellHeapPacked::
setTS(int elementIndex, const double& TS)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._TS[elementIndex] = TS;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::
getTE() const
{
  return _persistentRecords._TE;
}

void
latticeboltzmann::CellHeapPacked::
setTE(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TE)
{
  _persistentRecords._TE = (TE);
}

double
latticeboltzmann::CellHeapPacked::
getTE(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._TE[elementIndex];
}

void
latticeboltzmann::CellHeapPacked::
setTE(int elementIndex, const double& TE)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._TE[elementIndex] = TE;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::
getT() const
{
  return _persistentRecords._T;
}

void
latticeboltzmann::CellHeapPacked::
setT(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& T)
{
  _persistentRecords._T = (T);
}

double
latticeboltzmann::CellHeapPacked::
getT(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._T[elementIndex];
}

void
latticeboltzmann::CellHeapPacked::
setT(int elementIndex, const double& T)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._T[elementIndex] = T;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::
getTW() const
{
  return _persistentRecords._TW;
}

void
latticeboltzmann::CellHeapPacked::
setTW(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TW)
{
  _persistentRecords._TW = (TW);
}

double
latticeboltzmann::CellHeapPacked::
getTW(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._TW[elementIndex];
}

void
latticeboltzmann::CellHeapPacked::
setTW(int elementIndex, const double& TW)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._TW[elementIndex] = TW;
}

tarch::la::Vector<CELL_BLOCK_SIZE, double> latticeboltzmann::CellHeapPacked::
getTN() const
{
  return _persistentRecords._TN;
}

void
latticeboltzmann::CellHeapPacked::
setTN(const tarch::la::Vector<CELL_BLOCK_SIZE, double>& TN)
{
  _persistentRecords._TN = (TN);
}

double
latticeboltzmann::CellHeapPacked::
getTN(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  return _persistentRecords._TN[elementIndex];
}

void
latticeboltzmann::CellHeapPacked::
setTN(int elementIndex, const double& TN)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < CELL_BLOCK_SIZE);
  _persistentRecords._TN[elementIndex] = TN;
}

std::string
latticeboltzmann::CellHeapPacked::
toString() const
{
  std::ostringstream stringstr;
  toString(stringstr);
  return stringstr.str();
}

void
latticeboltzmann::CellHeapPacked::
toString(std::ostream& out) const
{
  out << "(";
  out << "BS:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getBS(i) << ",";

  out << getBS(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "BE:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getBE(i) << ",";

  out << getBE(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "B:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getB(i) << ",";

  out << getB(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "BW:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getBW(i) << ",";

  out << getBW(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "BN:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getBN(i) << ",";

  out << getBN(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "SE:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getSE(i) << ",";

  out << getSE(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "S:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getS(i) << ",";

  out << getS(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "SW:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getSW(i) << ",";

  out << getSW(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "E:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getE(i) << ",";

  out << getE(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "C:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getC(i) << ",";

  out << getC(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "W:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getW(i) << ",";

  out << getW(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "NE:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getNE(i) << ",";

  out << getNE(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "N:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getN(i) << ",";

  out << getN(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "NW:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getNW(i) << ",";

  out << getNW(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "TS:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getTS(i) << ",";

  out << getTS(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "TE:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getTE(i) << ",";

  out << getTE(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "T:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getT(i) << ",";

  out << getT(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "TW:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getTW(i) << ",";

  out << getTW(CELL_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "TN:[";

  for (int i = 0; i < CELL_BLOCK_SIZE - 1; i++)
    out << getTN(i) << ",";

  out << getTN(CELL_BLOCK_SIZE - 1) << "]";
  out <<  ")";
}

latticeboltzmann::CellHeapPacked::PersistentRecords
latticeboltzmann::CellHeapPacked::
getPersistentRecords() const
{
  return _persistentRecords;
}

latticeboltzmann::CellHeap
latticeboltzmann::CellHeapPacked::
convert() const
{
  return CellHeap(
    getBS(),
    getBE(),
    getB(),
    getBW(),
    getBN(),
    getSE(),
    getS(),
    getSW(),
    getE(),
    getC(),
    getW(),
    getNE(),
    getN(),
    getNW(),
    getTS(),
    getTE(),
    getT(),
    getTW(),
    getTN()
    );
}

#ifdef Parallel
tarch::logging::Log latticeboltzmann::CellHeapPacked::
_log("latticeboltzmann::CellHeapPacked");

MPI_Datatype latticeboltzmann::CellHeapPacked::Datatype     = 0;
MPI_Datatype latticeboltzmann::CellHeapPacked::FullDatatype = 0;

void
latticeboltzmann::CellHeapPacked::
initDatatype()
{
  {
    CellHeapPacked dummyCellHeapPacked[2];

    const int    Attributes           = 1;
    MPI_Datatype subtypes[Attributes] = {
      MPI_UB           // end/displacement flag
    };

    int blocklen[Attributes] = {
      1          // end/displacement flag
    };

    MPI_Aint disp[Attributes];

    MPI_Aint base;
    MPI_Address(const_cast<void*>(static_cast<const void*>(&(dummyCellHeapPacked[0]))), &base);

    for (int i = 1; i < Attributes; i++)
      assertion1(disp[i] > disp[i - 1], i);

    for (int i = 0; i < Attributes; i++)
      disp[i] -= base;

    MPI_Type_struct(Attributes, blocklen, disp, subtypes, &CellHeapPacked::Datatype);
    MPI_Type_commit(&CellHeapPacked::Datatype);
  }
  {
    CellHeapPacked dummyCellHeapPacked[2];

    const int    Attributes           = 20;
    MPI_Datatype subtypes[Attributes] = {
      MPI_DOUBLE,          // BS
      MPI_DOUBLE,          // BE
      MPI_DOUBLE,          // B
      MPI_DOUBLE,          // BW
      MPI_DOUBLE,          // BN
      MPI_DOUBLE,          // SE
      MPI_DOUBLE,          // S
      MPI_DOUBLE,          // SW
      MPI_DOUBLE,          // E
      MPI_DOUBLE,          // C
      MPI_DOUBLE,          // W
      MPI_DOUBLE,          // NE
      MPI_DOUBLE,          // N
      MPI_DOUBLE,          // NW
      MPI_DOUBLE,          // TS
      MPI_DOUBLE,          // TE
      MPI_DOUBLE,          // T
      MPI_DOUBLE,          // TW
      MPI_DOUBLE,          // TN
      MPI_UB           // end/displacement flag
    };

    int blocklen[Attributes] = {
      CELL_BLOCK_SIZE,           // BS
      CELL_BLOCK_SIZE,           // BE
      CELL_BLOCK_SIZE,           // B
      CELL_BLOCK_SIZE,           // BW
      CELL_BLOCK_SIZE,           // BN
      CELL_BLOCK_SIZE,           // SE
      CELL_BLOCK_SIZE,           // S
      CELL_BLOCK_SIZE,           // SW
      CELL_BLOCK_SIZE,           // E
      CELL_BLOCK_SIZE,           // C
      CELL_BLOCK_SIZE,           // W
      CELL_BLOCK_SIZE,           // NE
      CELL_BLOCK_SIZE,           // N
      CELL_BLOCK_SIZE,           // NW
      CELL_BLOCK_SIZE,           // TS
      CELL_BLOCK_SIZE,           // TE
      CELL_BLOCK_SIZE,           // T
      CELL_BLOCK_SIZE,           // TW
      CELL_BLOCK_SIZE,           // TN
      1          // end/displacement flag
    };

    MPI_Aint disp[Attributes];

    MPI_Aint base;
    MPI_Address(const_cast<void*>(static_cast<const void*>(&(dummyCellHeapPacked[0]))), &base);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeapPacked[0]._persistentRecords._BS[0]))),     &disp[0]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeapPacked[0]._persistentRecords._BE[0]))),     &disp[1]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeapPacked[0]._persistentRecords._B[0]))),    &disp[2]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeapPacked[0]._persistentRecords._BW[0]))),     &disp[3]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeapPacked[0]._persistentRecords._BN[0]))),     &disp[4]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeapPacked[0]._persistentRecords._SE[0]))),     &disp[5]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeapPacked[0]._persistentRecords._S[0]))),    &disp[6]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeapPacked[0]._persistentRecords._SW[0]))),     &disp[7]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeapPacked[0]._persistentRecords._E[0]))),    &disp[8]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeapPacked[0]._persistentRecords._C[0]))),    &disp[9]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeapPacked[0]._persistentRecords._W[0]))),    &disp[10]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeapPacked[0]._persistentRecords._NE[0]))),     &disp[11]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeapPacked[0]._persistentRecords._N[0]))),    &disp[12]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeapPacked[0]._persistentRecords._NW[0]))),     &disp[13]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeapPacked[0]._persistentRecords._TS[0]))),     &disp[14]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeapPacked[0]._persistentRecords._TE[0]))),     &disp[15]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeapPacked[0]._persistentRecords._T[0]))),    &disp[16]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeapPacked[0]._persistentRecords._TW[0]))),     &disp[17]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyCellHeapPacked[0]._persistentRecords._TN[0]))),     &disp[18]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &dummyCellHeapPacked[1]._persistentRecords._BS[0])),     &disp[19]);

    for (int i = 1; i < Attributes; i++)
      assertion1(disp[i] > disp[i - 1], i);

    for (int i = 0; i < Attributes; i++)
      disp[i] -= base;

    MPI_Type_struct(Attributes, blocklen, disp, subtypes, &CellHeapPacked::FullDatatype);
    MPI_Type_commit(&CellHeapPacked::FullDatatype);
  }
}

void
latticeboltzmann::CellHeapPacked::
shutdownDatatype()
{
  MPI_Type_free(&CellHeapPacked::Datatype);
  MPI_Type_free(&CellHeapPacked::FullDatatype);
}

void
latticeboltzmann::CellHeapPacked::
send(int destination, int tag, bool exchangeOnlyAttributesMarkedWithParallelise)
{
  MPI_Request* sendRequestHandle = new MPI_Request();

  MPI_Status status;
  int        flag = 0;
  int        result;

  clock_t timeOutWarning          = -1;
  clock_t timeOutShutdown         = -1;
  bool    triggeredTimeoutWarning = false;

  if (exchangeOnlyAttributesMarkedWithParallelise)
    result = MPI_Isend(
      this, 1, Datatype, destination,
      tag, tarch::parallel::Node::getInstance().getCommunicator(),
      sendRequestHandle
      );

  else
    result = MPI_Isend(
      this, 1, FullDatatype, destination,
      tag, tarch::parallel::Node::getInstance().getCommunicator(),
      sendRequestHandle
      );

  if  (result != MPI_SUCCESS) {
    std::ostringstream msg;
    msg << "was not able to send message latticeboltzmann::CellHeapPacked "
        << toString()
        << " to node " << destination
        << ": " << tarch::parallel::MPIReturnValueToString(result);
    _log.error("send(int)", msg.str());
  }

  result = MPI_Test(sendRequestHandle, &flag, &status);

  while (!flag) {
    if (timeOutWarning == -1)
      timeOutWarning = tarch::parallel::Node::getInstance().getDeadlockWarningTimeStamp();

    if (timeOutShutdown == -1)
      timeOutShutdown = tarch::parallel::Node::getInstance().getDeadlockTimeOutTimeStamp();

    result = MPI_Test(sendRequestHandle, &flag, &status);

    if (result != MPI_SUCCESS) {
      std::ostringstream msg;
      msg << "testing for finished send task for latticeboltzmann::CellHeapPacked "
          << toString()
          << " sent to node " << destination
          << " failed: " << tarch::parallel::MPIReturnValueToString(result);
      _log.error("send(int)", msg.str());
    }

    // deadlock aspect
    if (
      tarch::parallel::Node::getInstance().isTimeOutWarningEnabled() &&
      (clock() > timeOutWarning) &&
      (!triggeredTimeoutWarning)
      ) {
      tarch::parallel::Node::getInstance().writeTimeOutWarning(
        "latticeboltzmann::CellHeapPacked",
        "send(int)", destination, tag, 1
        );
      triggeredTimeoutWarning = true;
    }

    if (
      tarch::parallel::Node::getInstance().isTimeOutDeadlockEnabled() &&
      (clock() > timeOutShutdown)
      )
      tarch::parallel::Node::getInstance().triggerDeadlockTimeOut(
        "latticeboltzmann::CellHeapPacked",
        "send(int)", destination, tag, 1
        );

    tarch::parallel::Node::getInstance().receiveDanglingMessages();
  }

  delete sendRequestHandle;
  #ifdef Debug
  _log.debug("send(int,int)", "sent " + toString());
  #endif
}

void
latticeboltzmann::CellHeapPacked::
receive(int source, int tag, bool exchangeOnlyAttributesMarkedWithParallelise)
{
  MPI_Request* sendRequestHandle = new MPI_Request();

  MPI_Status status;
  int        flag = 0;
  int        result;

  clock_t timeOutWarning          = -1;
  clock_t timeOutShutdown         = -1;
  bool    triggeredTimeoutWarning = false;

  if (exchangeOnlyAttributesMarkedWithParallelise)
    result = MPI_Irecv(
      this, 1, Datatype, source, tag,
      tarch::parallel::Node::getInstance().getCommunicator(), sendRequestHandle
      );

  else
    result = MPI_Irecv(
      this, 1, FullDatatype, source, tag,
      tarch::parallel::Node::getInstance().getCommunicator(), sendRequestHandle
      );

  if (result != MPI_SUCCESS) {
    std::ostringstream msg;
    msg << "failed to start to receive latticeboltzmann::CellHeapPacked from node "
        << source << ": " << tarch::parallel::MPIReturnValueToString(result);
    _log.error("receive(int)", msg.str());
  }

  result = MPI_Test(sendRequestHandle, &flag, &status);

  while (!flag) {
    if (timeOutWarning == -1)
      timeOutWarning = tarch::parallel::Node::getInstance().getDeadlockWarningTimeStamp();

    if (timeOutShutdown == -1)
      timeOutShutdown = tarch::parallel::Node::getInstance().getDeadlockTimeOutTimeStamp();

    result = MPI_Test(sendRequestHandle, &flag, &status);

    if (result != MPI_SUCCESS) {
      std::ostringstream msg;
      msg << "testing for finished receive task for latticeboltzmann::CellHeapPacked failed: "
          << tarch::parallel::MPIReturnValueToString(result);
      _log.error("receive(int)", msg.str());
    }

    // deadlock aspect
    if (
      tarch::parallel::Node::getInstance().isTimeOutWarningEnabled() &&
      (clock() > timeOutWarning) &&
      (!triggeredTimeoutWarning)
      ) {
      tarch::parallel::Node::getInstance().writeTimeOutWarning(
        "latticeboltzmann::CellHeapPacked",
        "receive(int)", source, tag, 1
        );
      triggeredTimeoutWarning = true;
    }

    if (
      tarch::parallel::Node::getInstance().isTimeOutDeadlockEnabled() &&
      (clock() > timeOutShutdown)
      )
      tarch::parallel::Node::getInstance().triggerDeadlockTimeOut(
        "latticeboltzmann::CellHeapPacked",
        "receive(int)", source, tag, 1
        );

    tarch::parallel::Node::getInstance().receiveDanglingMessages();
  }

  delete sendRequestHandle;

  #ifdef Debug
  _log.debug("receive(int,int)", "received " + toString());
  #endif
}

bool
latticeboltzmann::CellHeapPacked::
isMessageInQueue(int tag, bool exchangeOnlyAttributesMarkedWithParallelise)
{
  MPI_Status status;
  int        flag = 0;
  MPI_Iprobe(
    MPI_ANY_SOURCE, tag,
    tarch::parallel::Node::getInstance().getCommunicator(), &flag, &status
    );

  if (flag) {
    int messageCounter;

    if (exchangeOnlyAttributesMarkedWithParallelise)
      MPI_Get_count(&status, Datatype, &messageCounter);
    else
      MPI_Get_count(&status, FullDatatype, &messageCounter);

    return messageCounter > 0;
  } else
    return false;
}

#endif
