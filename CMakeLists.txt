cmake_minimum_required(VERSION 2.8.10)

project(peano.latticeboltzmann C CXX)

file(GLOB_RECURSE CPP_FILES     ./*.cpp)

include_directories(${peano_INCLUDE_DIRS}
                    ${PROJECT_SOURCE_DIR}
                    ${PROJECT_SOURCE_DIR}/..)

add_library(${PROJECT_NAME} STATIC
            ${CPP_FILES})

