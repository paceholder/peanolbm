#include "latticeboltzmann/adapters/SetupExperiment.h"



peano::MappingSpecification   latticeboltzmann::adapters::SetupExperiment::touchVertexLastTimeSpecification() {
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::SetupExperiment::touchVertexLastTimeSpecification()


;
}


peano::MappingSpecification   latticeboltzmann::adapters::SetupExperiment::touchVertexFirstTimeSpecification() { 
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::SetupExperiment::touchVertexFirstTimeSpecification()


;
}


peano::MappingSpecification   latticeboltzmann::adapters::SetupExperiment::enterCellSpecification() {
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::SetupExperiment::enterCellSpecification()


;
}


peano::MappingSpecification   latticeboltzmann::adapters::SetupExperiment::leaveCellSpecification() {
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::SetupExperiment::leaveCellSpecification()


;
}


peano::MappingSpecification   latticeboltzmann::adapters::SetupExperiment::ascendSpecification() {
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::SetupExperiment::ascendSpecification()


;
}


peano::MappingSpecification   latticeboltzmann::adapters::SetupExperiment::descendSpecification() {
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::SetupExperiment::descendSpecification()


;
}


latticeboltzmann::adapters::SetupExperiment::SetupExperiment() {
}


latticeboltzmann::adapters::SetupExperiment::~SetupExperiment() {
}


#if defined(SharedMemoryParallelisation)
latticeboltzmann::adapters::SetupExperiment::SetupExperiment(const SetupExperiment&  masterThread):
  _map2SetupExperiment(masterThread._map2SetupExperiment) 

 

{
}


void latticeboltzmann::adapters::SetupExperiment::mergeWithWorkerThread(const SetupExperiment& workerThread) {

  _map2SetupExperiment.mergeWithWorkerThread(workerThread._map2SetupExperiment);


}
#endif


void latticeboltzmann::adapters::SetupExperiment::createHangingVertex(
      latticeboltzmann::Vertex&     fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                fineGridH,
      latticeboltzmann::Vertex * const   coarseGridVertices,
      const peano::grid::VertexEnumerator&      coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&       coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                   fineGridPositionOfVertex
) {

  _map2SetupExperiment.createHangingVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::SetupExperiment::destroyHangingVertex(
      const latticeboltzmann::Vertex&   fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      latticeboltzmann::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {

  _map2SetupExperiment.destroyHangingVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::SetupExperiment::createInnerVertex(
      latticeboltzmann::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {

  _map2SetupExperiment.createInnerVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::SetupExperiment::createBoundaryVertex(
      latticeboltzmann::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {

  _map2SetupExperiment.createBoundaryVertex( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::SetupExperiment::destroyVertex(
      const latticeboltzmann::Vertex&   fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      latticeboltzmann::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {

  _map2SetupExperiment.destroyVertex( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::SetupExperiment::createCell(
      latticeboltzmann::Cell&                 fineGridCell,
      latticeboltzmann::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {

  _map2SetupExperiment.createCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


}


void latticeboltzmann::adapters::SetupExperiment::destroyCell(
      const latticeboltzmann::Cell&           fineGridCell,
      latticeboltzmann::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {

  _map2SetupExperiment.destroyCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


}

#ifdef Parallel
void latticeboltzmann::adapters::SetupExperiment::mergeWithNeighbour(
  latticeboltzmann::Vertex&  vertex,
  const latticeboltzmann::Vertex&  neighbour,
  int                                           fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&   fineGridX,
  const tarch::la::Vector<DIMENSIONS,double>&   fineGridH,
  int                                           level
) {

   _map2SetupExperiment.mergeWithNeighbour( vertex, neighbour, fromRank, fineGridX, fineGridH, level );


}

void latticeboltzmann::adapters::SetupExperiment::prepareSendToNeighbour(
  latticeboltzmann::Vertex&  vertex,
  int                                           toRank,
  const tarch::la::Vector<DIMENSIONS,double>&   x,
  const tarch::la::Vector<DIMENSIONS,double>&   h,
  int                                           level
) {

   _map2SetupExperiment.prepareSendToNeighbour( vertex, toRank, x, h, level );


}

void latticeboltzmann::adapters::SetupExperiment::prepareCopyToRemoteNode(
  latticeboltzmann::Vertex&  localVertex,
  int                                           toRank,
  const tarch::la::Vector<DIMENSIONS,double>&   x,
  const tarch::la::Vector<DIMENSIONS,double>&   h,
  int                                           level
) {

   _map2SetupExperiment.prepareCopyToRemoteNode( localVertex, toRank, x, h, level );


}

void latticeboltzmann::adapters::SetupExperiment::prepareCopyToRemoteNode(
  latticeboltzmann::Cell&  localCell,
      int                                           toRank,
      const tarch::la::Vector<DIMENSIONS,double>&   x,
      const tarch::la::Vector<DIMENSIONS,double>&   h,
      int                                           level
) {

   _map2SetupExperiment.prepareCopyToRemoteNode( localCell, toRank, x, h, level );


}

void latticeboltzmann::adapters::SetupExperiment::mergeWithRemoteDataDueToForkOrJoin(
  latticeboltzmann::Vertex&  localVertex,
  const latticeboltzmann::Vertex&  masterOrWorkerVertex,
  int                                       fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&  x,
  const tarch::la::Vector<DIMENSIONS,double>&  h,
  int                                       level
) {

   _map2SetupExperiment.mergeWithRemoteDataDueToForkOrJoin( localVertex, masterOrWorkerVertex, fromRank, x, h, level );


}

void latticeboltzmann::adapters::SetupExperiment::mergeWithRemoteDataDueToForkOrJoin(
  latticeboltzmann::Cell&  localCell,
  const latticeboltzmann::Cell&  masterOrWorkerCell,
  int                                       fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&  x,
  const tarch::la::Vector<DIMENSIONS,double>&  h,
  int                                       level
) {

   _map2SetupExperiment.mergeWithRemoteDataDueToForkOrJoin( localCell, masterOrWorkerCell, fromRank, x, h, level );


}

void latticeboltzmann::adapters::SetupExperiment::prepareSendToWorker(
  latticeboltzmann::Cell&                 fineGridCell,
  latticeboltzmann::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  latticeboltzmann::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell,
  int                                                                  worker
) {

   _map2SetupExperiment.prepareSendToWorker( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell, worker );


}

void latticeboltzmann::adapters::SetupExperiment::prepareSendToMaster(
  latticeboltzmann::Cell&                       localCell,
  latticeboltzmann::Vertex *                    vertices,
  const peano::grid::VertexEnumerator&       verticesEnumerator, 
  const latticeboltzmann::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&       coarseGridVerticesEnumerator,
  const latticeboltzmann::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&   fineGridPositionOfCell
) {

   _map2SetupExperiment.prepareSendToMaster( localCell, vertices, verticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


}

void latticeboltzmann::adapters::SetupExperiment::mergeWithMaster(
  const latticeboltzmann::Cell&           workerGridCell,
  latticeboltzmann::Vertex * const        workerGridVertices,
  const peano::grid::VertexEnumerator& workerEnumerator,
  latticeboltzmann::Cell&                 fineGridCell,
  latticeboltzmann::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  latticeboltzmann::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell,
  int                                                                  worker,
    const latticeboltzmann::State&          workerState,
  latticeboltzmann::State&                masterState
) {

   _map2SetupExperiment.mergeWithMaster( workerGridCell, workerGridVertices, workerEnumerator, fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell, worker, workerState, masterState );


}

void latticeboltzmann::adapters::SetupExperiment::receiveDataFromMaster(
      latticeboltzmann::Cell&                        receivedCell, 
      latticeboltzmann::Vertex *                     receivedVertices,
      const peano::grid::VertexEnumerator&        receivedVerticesEnumerator,
      latticeboltzmann::Vertex * const               receivedCoarseGridVertices,
      const peano::grid::VertexEnumerator&        receivedCoarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                        receivedCoarseGridCell,
      latticeboltzmann::Vertex * const               workersCoarseGridVertices,
      const peano::grid::VertexEnumerator&        workersCoarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                        workersCoarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&    fineGridPositionOfCell
) {

   _map2SetupExperiment.receiveDataFromMaster( receivedCell, receivedVertices, receivedVerticesEnumerator, receivedCoarseGridVertices, receivedCoarseGridVerticesEnumerator, receivedCoarseGridCell, workersCoarseGridVertices, workersCoarseGridVerticesEnumerator, workersCoarseGridCell, fineGridPositionOfCell );


}


void latticeboltzmann::adapters::SetupExperiment::mergeWithWorker(
  latticeboltzmann::Cell&           localCell, 
  const latticeboltzmann::Cell&     receivedMasterCell,
  const tarch::la::Vector<DIMENSIONS,double>&  cellCentre,
  const tarch::la::Vector<DIMENSIONS,double>&  cellSize,
  int                                          level
) {

   _map2SetupExperiment.mergeWithWorker( localCell, receivedMasterCell, cellCentre, cellSize, level );


}

void latticeboltzmann::adapters::SetupExperiment::mergeWithWorker(
  latticeboltzmann::Vertex&        localVertex,
  const latticeboltzmann::Vertex&  receivedMasterVertex,
  const tarch::la::Vector<DIMENSIONS,double>&   x,
  const tarch::la::Vector<DIMENSIONS,double>&   h,
  int                                           level
) {

   _map2SetupExperiment.mergeWithWorker( localVertex, receivedMasterVertex, x, h, level );


}
#endif

void latticeboltzmann::adapters::SetupExperiment::touchVertexFirstTime(
      latticeboltzmann::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {

  _map2SetupExperiment.touchVertexFirstTime( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::SetupExperiment::touchVertexLastTime(
      latticeboltzmann::Vertex&         fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      latticeboltzmann::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {

  _map2SetupExperiment.touchVertexLastTime( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::SetupExperiment::enterCell(
      latticeboltzmann::Cell&                 fineGridCell,
      latticeboltzmann::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {

  _map2SetupExperiment.enterCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


}


void latticeboltzmann::adapters::SetupExperiment::leaveCell(
      latticeboltzmann::Cell&           fineGridCell,
      latticeboltzmann::Vertex * const  fineGridVertices,
      const peano::grid::VertexEnumerator&          fineGridVerticesEnumerator,
      latticeboltzmann::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfCell
) {

  _map2SetupExperiment.leaveCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


}


void latticeboltzmann::adapters::SetupExperiment::beginIteration(
  latticeboltzmann::State&  solverState
) {

  _map2SetupExperiment.beginIteration( solverState );


}


void latticeboltzmann::adapters::SetupExperiment::endIteration(
  latticeboltzmann::State&  solverState
) {

  _map2SetupExperiment.endIteration( solverState );


}




void latticeboltzmann::adapters::SetupExperiment::descend(
  latticeboltzmann::Cell * const          fineGridCells,
  latticeboltzmann::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  latticeboltzmann::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&                 coarseGridCell
) {

  _map2SetupExperiment.descend( fineGridCells, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell );


}


void latticeboltzmann::adapters::SetupExperiment::ascend(
  latticeboltzmann::Cell * const    fineGridCells,
  latticeboltzmann::Vertex * const  fineGridVertices,
  const peano::grid::VertexEnumerator&          fineGridVerticesEnumerator,
  latticeboltzmann::Vertex * const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&           coarseGridCell
) {

  _map2SetupExperiment.ascend( fineGridCells, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell );


}
