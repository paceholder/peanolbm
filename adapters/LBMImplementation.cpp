#include "latticeboltzmann/adapters/LBMImplementation.h"



peano::MappingSpecification   latticeboltzmann::adapters::LBMImplementation::touchVertexLastTimeSpecification() {
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::LBMImplementation::touchVertexLastTimeSpecification()


;
}


peano::MappingSpecification   latticeboltzmann::adapters::LBMImplementation::touchVertexFirstTimeSpecification() { 
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::LBMImplementation::touchVertexFirstTimeSpecification()


;
}


peano::MappingSpecification   latticeboltzmann::adapters::LBMImplementation::enterCellSpecification() {
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::LBMImplementation::enterCellSpecification()


;
}


peano::MappingSpecification   latticeboltzmann::adapters::LBMImplementation::leaveCellSpecification() {
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::LBMImplementation::leaveCellSpecification()


;
}


peano::MappingSpecification   latticeboltzmann::adapters::LBMImplementation::ascendSpecification() {
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::LBMImplementation::ascendSpecification()


;
}


peano::MappingSpecification   latticeboltzmann::adapters::LBMImplementation::descendSpecification() {
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::LBMImplementation::descendSpecification()


;
}


latticeboltzmann::adapters::LBMImplementation::LBMImplementation() {
}


latticeboltzmann::adapters::LBMImplementation::~LBMImplementation() {
}


#if defined(SharedMemoryParallelisation)
latticeboltzmann::adapters::LBMImplementation::LBMImplementation(const LBMImplementation&  masterThread):
  _map2LBMImplementation(masterThread._map2LBMImplementation) 

 

{
}


void latticeboltzmann::adapters::LBMImplementation::mergeWithWorkerThread(const LBMImplementation& workerThread) {

  _map2LBMImplementation.mergeWithWorkerThread(workerThread._map2LBMImplementation);


}
#endif


void latticeboltzmann::adapters::LBMImplementation::createHangingVertex(
      latticeboltzmann::Vertex&     fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                fineGridH,
      latticeboltzmann::Vertex * const   coarseGridVertices,
      const peano::grid::VertexEnumerator&      coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&       coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                   fineGridPositionOfVertex
) {

  _map2LBMImplementation.createHangingVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::LBMImplementation::destroyHangingVertex(
      const latticeboltzmann::Vertex&   fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      latticeboltzmann::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {

  _map2LBMImplementation.destroyHangingVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::LBMImplementation::createInnerVertex(
      latticeboltzmann::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {

  _map2LBMImplementation.createInnerVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::LBMImplementation::createBoundaryVertex(
      latticeboltzmann::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {

  _map2LBMImplementation.createBoundaryVertex( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::LBMImplementation::destroyVertex(
      const latticeboltzmann::Vertex&   fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      latticeboltzmann::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {

  _map2LBMImplementation.destroyVertex( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::LBMImplementation::createCell(
      latticeboltzmann::Cell&                 fineGridCell,
      latticeboltzmann::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {

  _map2LBMImplementation.createCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


}


void latticeboltzmann::adapters::LBMImplementation::destroyCell(
      const latticeboltzmann::Cell&           fineGridCell,
      latticeboltzmann::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {

  _map2LBMImplementation.destroyCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


}

#ifdef Parallel
void latticeboltzmann::adapters::LBMImplementation::mergeWithNeighbour(
  latticeboltzmann::Vertex&  vertex,
  const latticeboltzmann::Vertex&  neighbour,
  int                                           fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&   fineGridX,
  const tarch::la::Vector<DIMENSIONS,double>&   fineGridH,
  int                                           level
) {

   _map2LBMImplementation.mergeWithNeighbour( vertex, neighbour, fromRank, fineGridX, fineGridH, level );


}

void latticeboltzmann::adapters::LBMImplementation::prepareSendToNeighbour(
  latticeboltzmann::Vertex&  vertex,
  int                                           toRank,
  const tarch::la::Vector<DIMENSIONS,double>&   x,
  const tarch::la::Vector<DIMENSIONS,double>&   h,
  int                                           level
) {

   _map2LBMImplementation.prepareSendToNeighbour( vertex, toRank, x, h, level );


}

void latticeboltzmann::adapters::LBMImplementation::prepareCopyToRemoteNode(
  latticeboltzmann::Vertex&  localVertex,
  int                                           toRank,
  const tarch::la::Vector<DIMENSIONS,double>&   x,
  const tarch::la::Vector<DIMENSIONS,double>&   h,
  int                                           level
) {

   _map2LBMImplementation.prepareCopyToRemoteNode( localVertex, toRank, x, h, level );


}

void latticeboltzmann::adapters::LBMImplementation::prepareCopyToRemoteNode(
  latticeboltzmann::Cell&  localCell,
      int                                           toRank,
      const tarch::la::Vector<DIMENSIONS,double>&   x,
      const tarch::la::Vector<DIMENSIONS,double>&   h,
      int                                           level
) {

   _map2LBMImplementation.prepareCopyToRemoteNode( localCell, toRank, x, h, level );


}

void latticeboltzmann::adapters::LBMImplementation::mergeWithRemoteDataDueToForkOrJoin(
  latticeboltzmann::Vertex&  localVertex,
  const latticeboltzmann::Vertex&  masterOrWorkerVertex,
  int                                       fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&  x,
  const tarch::la::Vector<DIMENSIONS,double>&  h,
  int                                       level
) {

   _map2LBMImplementation.mergeWithRemoteDataDueToForkOrJoin( localVertex, masterOrWorkerVertex, fromRank, x, h, level );


}

void latticeboltzmann::adapters::LBMImplementation::mergeWithRemoteDataDueToForkOrJoin(
  latticeboltzmann::Cell&  localCell,
  const latticeboltzmann::Cell&  masterOrWorkerCell,
  int                                       fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&  x,
  const tarch::la::Vector<DIMENSIONS,double>&  h,
  int                                       level
) {

   _map2LBMImplementation.mergeWithRemoteDataDueToForkOrJoin( localCell, masterOrWorkerCell, fromRank, x, h, level );


}

void latticeboltzmann::adapters::LBMImplementation::prepareSendToWorker(
  latticeboltzmann::Cell&                 fineGridCell,
  latticeboltzmann::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  latticeboltzmann::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell,
  int                                                                  worker
) {

   _map2LBMImplementation.prepareSendToWorker( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell, worker );


}

void latticeboltzmann::adapters::LBMImplementation::prepareSendToMaster(
  latticeboltzmann::Cell&                       localCell,
  latticeboltzmann::Vertex *                    vertices,
  const peano::grid::VertexEnumerator&       verticesEnumerator, 
  const latticeboltzmann::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&       coarseGridVerticesEnumerator,
  const latticeboltzmann::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&   fineGridPositionOfCell
) {

   _map2LBMImplementation.prepareSendToMaster( localCell, vertices, verticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


}

void latticeboltzmann::adapters::LBMImplementation::mergeWithMaster(
  const latticeboltzmann::Cell&           workerGridCell,
  latticeboltzmann::Vertex * const        workerGridVertices,
  const peano::grid::VertexEnumerator& workerEnumerator,
  latticeboltzmann::Cell&                 fineGridCell,
  latticeboltzmann::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  latticeboltzmann::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell,
  int                                                                  worker,
    const latticeboltzmann::State&          workerState,
  latticeboltzmann::State&                masterState
) {

   _map2LBMImplementation.mergeWithMaster( workerGridCell, workerGridVertices, workerEnumerator, fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell, worker, workerState, masterState );


}

void latticeboltzmann::adapters::LBMImplementation::receiveDataFromMaster(
      latticeboltzmann::Cell&                        receivedCell, 
      latticeboltzmann::Vertex *                     receivedVertices,
      const peano::grid::VertexEnumerator&        receivedVerticesEnumerator,
      latticeboltzmann::Vertex * const               receivedCoarseGridVertices,
      const peano::grid::VertexEnumerator&        receivedCoarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                        receivedCoarseGridCell,
      latticeboltzmann::Vertex * const               workersCoarseGridVertices,
      const peano::grid::VertexEnumerator&        workersCoarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                        workersCoarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&    fineGridPositionOfCell
) {

   _map2LBMImplementation.receiveDataFromMaster( receivedCell, receivedVertices, receivedVerticesEnumerator, receivedCoarseGridVertices, receivedCoarseGridVerticesEnumerator, receivedCoarseGridCell, workersCoarseGridVertices, workersCoarseGridVerticesEnumerator, workersCoarseGridCell, fineGridPositionOfCell );


}


void latticeboltzmann::adapters::LBMImplementation::mergeWithWorker(
  latticeboltzmann::Cell&           localCell, 
  const latticeboltzmann::Cell&     receivedMasterCell,
  const tarch::la::Vector<DIMENSIONS,double>&  cellCentre,
  const tarch::la::Vector<DIMENSIONS,double>&  cellSize,
  int                                          level
) {

   _map2LBMImplementation.mergeWithWorker( localCell, receivedMasterCell, cellCentre, cellSize, level );


}

void latticeboltzmann::adapters::LBMImplementation::mergeWithWorker(
  latticeboltzmann::Vertex&        localVertex,
  const latticeboltzmann::Vertex&  receivedMasterVertex,
  const tarch::la::Vector<DIMENSIONS,double>&   x,
  const tarch::la::Vector<DIMENSIONS,double>&   h,
  int                                           level
) {

   _map2LBMImplementation.mergeWithWorker( localVertex, receivedMasterVertex, x, h, level );


}
#endif

void latticeboltzmann::adapters::LBMImplementation::touchVertexFirstTime(
      latticeboltzmann::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {

  _map2LBMImplementation.touchVertexFirstTime( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::LBMImplementation::touchVertexLastTime(
      latticeboltzmann::Vertex&         fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      latticeboltzmann::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {

  _map2LBMImplementation.touchVertexLastTime( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::LBMImplementation::enterCell(
      latticeboltzmann::Cell&                 fineGridCell,
      latticeboltzmann::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {

  _map2LBMImplementation.enterCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


}


void latticeboltzmann::adapters::LBMImplementation::leaveCell(
      latticeboltzmann::Cell&           fineGridCell,
      latticeboltzmann::Vertex * const  fineGridVertices,
      const peano::grid::VertexEnumerator&          fineGridVerticesEnumerator,
      latticeboltzmann::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfCell
) {

  _map2LBMImplementation.leaveCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


}


void latticeboltzmann::adapters::LBMImplementation::beginIteration(
  latticeboltzmann::State&  solverState
) {

  _map2LBMImplementation.beginIteration( solverState );


}


void latticeboltzmann::adapters::LBMImplementation::endIteration(
  latticeboltzmann::State&  solverState
) {

  _map2LBMImplementation.endIteration( solverState );


}




void latticeboltzmann::adapters::LBMImplementation::descend(
  latticeboltzmann::Cell * const          fineGridCells,
  latticeboltzmann::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  latticeboltzmann::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&                 coarseGridCell
) {

  _map2LBMImplementation.descend( fineGridCells, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell );


}


void latticeboltzmann::adapters::LBMImplementation::ascend(
  latticeboltzmann::Cell * const    fineGridCells,
  latticeboltzmann::Vertex * const  fineGridVertices,
  const peano::grid::VertexEnumerator&          fineGridVerticesEnumerator,
  latticeboltzmann::Vertex * const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&           coarseGridCell
) {

  _map2LBMImplementation.ascend( fineGridCells, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell );


}
