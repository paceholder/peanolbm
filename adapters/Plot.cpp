#include "latticeboltzmann/adapters/Plot.h"



peano::MappingSpecification   latticeboltzmann::adapters::Plot::touchVertexLastTimeSpecification() {
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::Plot::touchVertexLastTimeSpecification()


;
}


peano::MappingSpecification   latticeboltzmann::adapters::Plot::touchVertexFirstTimeSpecification() { 
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::Plot::touchVertexFirstTimeSpecification()


;
}


peano::MappingSpecification   latticeboltzmann::adapters::Plot::enterCellSpecification() {
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::Plot::enterCellSpecification()


;
}


peano::MappingSpecification   latticeboltzmann::adapters::Plot::leaveCellSpecification() {
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::Plot::leaveCellSpecification()


;
}


peano::MappingSpecification   latticeboltzmann::adapters::Plot::ascendSpecification() {
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::Plot::ascendSpecification()


;
}


peano::MappingSpecification   latticeboltzmann::adapters::Plot::descendSpecification() {
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::Plot::descendSpecification()


;
}


latticeboltzmann::adapters::Plot::Plot() {
}


latticeboltzmann::adapters::Plot::~Plot() {
}


#if defined(SharedMemoryParallelisation)
latticeboltzmann::adapters::Plot::Plot(const Plot&  masterThread):
  _map2Plot(masterThread._map2Plot) 

 

{
}


void latticeboltzmann::adapters::Plot::mergeWithWorkerThread(const Plot& workerThread) {

  _map2Plot.mergeWithWorkerThread(workerThread._map2Plot);


}
#endif


void latticeboltzmann::adapters::Plot::createHangingVertex(
      latticeboltzmann::Vertex&     fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                fineGridH,
      latticeboltzmann::Vertex * const   coarseGridVertices,
      const peano::grid::VertexEnumerator&      coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&       coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                   fineGridPositionOfVertex
) {

  _map2Plot.createHangingVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::Plot::destroyHangingVertex(
      const latticeboltzmann::Vertex&   fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      latticeboltzmann::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {

  _map2Plot.destroyHangingVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::Plot::createInnerVertex(
      latticeboltzmann::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {

  _map2Plot.createInnerVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::Plot::createBoundaryVertex(
      latticeboltzmann::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {

  _map2Plot.createBoundaryVertex( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::Plot::destroyVertex(
      const latticeboltzmann::Vertex&   fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      latticeboltzmann::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {

  _map2Plot.destroyVertex( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::Plot::createCell(
      latticeboltzmann::Cell&                 fineGridCell,
      latticeboltzmann::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {

  _map2Plot.createCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


}


void latticeboltzmann::adapters::Plot::destroyCell(
      const latticeboltzmann::Cell&           fineGridCell,
      latticeboltzmann::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {

  _map2Plot.destroyCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


}

#ifdef Parallel
void latticeboltzmann::adapters::Plot::mergeWithNeighbour(
  latticeboltzmann::Vertex&  vertex,
  const latticeboltzmann::Vertex&  neighbour,
  int                                           fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&   fineGridX,
  const tarch::la::Vector<DIMENSIONS,double>&   fineGridH,
  int                                           level
) {

   _map2Plot.mergeWithNeighbour( vertex, neighbour, fromRank, fineGridX, fineGridH, level );


}

void latticeboltzmann::adapters::Plot::prepareSendToNeighbour(
  latticeboltzmann::Vertex&  vertex,
  int                                           toRank,
  const tarch::la::Vector<DIMENSIONS,double>&   x,
  const tarch::la::Vector<DIMENSIONS,double>&   h,
  int                                           level
) {

   _map2Plot.prepareSendToNeighbour( vertex, toRank, x, h, level );


}

void latticeboltzmann::adapters::Plot::prepareCopyToRemoteNode(
  latticeboltzmann::Vertex&  localVertex,
  int                                           toRank,
  const tarch::la::Vector<DIMENSIONS,double>&   x,
  const tarch::la::Vector<DIMENSIONS,double>&   h,
  int                                           level
) {

   _map2Plot.prepareCopyToRemoteNode( localVertex, toRank, x, h, level );


}

void latticeboltzmann::adapters::Plot::prepareCopyToRemoteNode(
  latticeboltzmann::Cell&  localCell,
      int                                           toRank,
      const tarch::la::Vector<DIMENSIONS,double>&   x,
      const tarch::la::Vector<DIMENSIONS,double>&   h,
      int                                           level
) {

   _map2Plot.prepareCopyToRemoteNode( localCell, toRank, x, h, level );


}

void latticeboltzmann::adapters::Plot::mergeWithRemoteDataDueToForkOrJoin(
  latticeboltzmann::Vertex&  localVertex,
  const latticeboltzmann::Vertex&  masterOrWorkerVertex,
  int                                       fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&  x,
  const tarch::la::Vector<DIMENSIONS,double>&  h,
  int                                       level
) {

   _map2Plot.mergeWithRemoteDataDueToForkOrJoin( localVertex, masterOrWorkerVertex, fromRank, x, h, level );


}

void latticeboltzmann::adapters::Plot::mergeWithRemoteDataDueToForkOrJoin(
  latticeboltzmann::Cell&  localCell,
  const latticeboltzmann::Cell&  masterOrWorkerCell,
  int                                       fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&  x,
  const tarch::la::Vector<DIMENSIONS,double>&  h,
  int                                       level
) {

   _map2Plot.mergeWithRemoteDataDueToForkOrJoin( localCell, masterOrWorkerCell, fromRank, x, h, level );


}

void latticeboltzmann::adapters::Plot::prepareSendToWorker(
  latticeboltzmann::Cell&                 fineGridCell,
  latticeboltzmann::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  latticeboltzmann::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell,
  int                                                                  worker
) {

   _map2Plot.prepareSendToWorker( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell, worker );


}

void latticeboltzmann::adapters::Plot::prepareSendToMaster(
  latticeboltzmann::Cell&                       localCell,
  latticeboltzmann::Vertex *                    vertices,
  const peano::grid::VertexEnumerator&       verticesEnumerator, 
  const latticeboltzmann::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&       coarseGridVerticesEnumerator,
  const latticeboltzmann::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&   fineGridPositionOfCell
) {

   _map2Plot.prepareSendToMaster( localCell, vertices, verticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


}

void latticeboltzmann::adapters::Plot::mergeWithMaster(
  const latticeboltzmann::Cell&           workerGridCell,
  latticeboltzmann::Vertex * const        workerGridVertices,
  const peano::grid::VertexEnumerator& workerEnumerator,
  latticeboltzmann::Cell&                 fineGridCell,
  latticeboltzmann::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  latticeboltzmann::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell,
  int                                                                  worker,
    const latticeboltzmann::State&          workerState,
  latticeboltzmann::State&                masterState
) {

   _map2Plot.mergeWithMaster( workerGridCell, workerGridVertices, workerEnumerator, fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell, worker, workerState, masterState );


}

void latticeboltzmann::adapters::Plot::receiveDataFromMaster(
      latticeboltzmann::Cell&                        receivedCell, 
      latticeboltzmann::Vertex *                     receivedVertices,
      const peano::grid::VertexEnumerator&        receivedVerticesEnumerator,
      latticeboltzmann::Vertex * const               receivedCoarseGridVertices,
      const peano::grid::VertexEnumerator&        receivedCoarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                        receivedCoarseGridCell,
      latticeboltzmann::Vertex * const               workersCoarseGridVertices,
      const peano::grid::VertexEnumerator&        workersCoarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                        workersCoarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&    fineGridPositionOfCell
) {

   _map2Plot.receiveDataFromMaster( receivedCell, receivedVertices, receivedVerticesEnumerator, receivedCoarseGridVertices, receivedCoarseGridVerticesEnumerator, receivedCoarseGridCell, workersCoarseGridVertices, workersCoarseGridVerticesEnumerator, workersCoarseGridCell, fineGridPositionOfCell );


}


void latticeboltzmann::adapters::Plot::mergeWithWorker(
  latticeboltzmann::Cell&           localCell, 
  const latticeboltzmann::Cell&     receivedMasterCell,
  const tarch::la::Vector<DIMENSIONS,double>&  cellCentre,
  const tarch::la::Vector<DIMENSIONS,double>&  cellSize,
  int                                          level
) {

   _map2Plot.mergeWithWorker( localCell, receivedMasterCell, cellCentre, cellSize, level );


}

void latticeboltzmann::adapters::Plot::mergeWithWorker(
  latticeboltzmann::Vertex&        localVertex,
  const latticeboltzmann::Vertex&  receivedMasterVertex,
  const tarch::la::Vector<DIMENSIONS,double>&   x,
  const tarch::la::Vector<DIMENSIONS,double>&   h,
  int                                           level
) {

   _map2Plot.mergeWithWorker( localVertex, receivedMasterVertex, x, h, level );


}
#endif

void latticeboltzmann::adapters::Plot::touchVertexFirstTime(
      latticeboltzmann::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {

  _map2Plot.touchVertexFirstTime( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::Plot::touchVertexLastTime(
      latticeboltzmann::Vertex&         fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      latticeboltzmann::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {

  _map2Plot.touchVertexLastTime( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::Plot::enterCell(
      latticeboltzmann::Cell&                 fineGridCell,
      latticeboltzmann::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {

  _map2Plot.enterCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


}


void latticeboltzmann::adapters::Plot::leaveCell(
      latticeboltzmann::Cell&           fineGridCell,
      latticeboltzmann::Vertex * const  fineGridVertices,
      const peano::grid::VertexEnumerator&          fineGridVerticesEnumerator,
      latticeboltzmann::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfCell
) {

  _map2Plot.leaveCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


}


void latticeboltzmann::adapters::Plot::beginIteration(
  latticeboltzmann::State&  solverState
) {

  _map2Plot.beginIteration( solverState );


}


void latticeboltzmann::adapters::Plot::endIteration(
  latticeboltzmann::State&  solverState
) {

  _map2Plot.endIteration( solverState );


}




void latticeboltzmann::adapters::Plot::descend(
  latticeboltzmann::Cell * const          fineGridCells,
  latticeboltzmann::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  latticeboltzmann::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&                 coarseGridCell
) {

  _map2Plot.descend( fineGridCells, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell );


}


void latticeboltzmann::adapters::Plot::ascend(
  latticeboltzmann::Cell * const    fineGridCells,
  latticeboltzmann::Vertex * const  fineGridVertices,
  const peano::grid::VertexEnumerator&          fineGridVerticesEnumerator,
  latticeboltzmann::Vertex * const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&           coarseGridCell
) {

  _map2Plot.ascend( fineGridCells, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell );


}
