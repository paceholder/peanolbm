#include "latticeboltzmann/adapters/InterpolationRestriction.h"



peano::MappingSpecification   latticeboltzmann::adapters::InterpolationRestriction::touchVertexLastTimeSpecification() {
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::InterpolationRestriction::touchVertexLastTimeSpecification()


;
}


peano::MappingSpecification   latticeboltzmann::adapters::InterpolationRestriction::touchVertexFirstTimeSpecification() { 
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::InterpolationRestriction::touchVertexFirstTimeSpecification()


;
}


peano::MappingSpecification   latticeboltzmann::adapters::InterpolationRestriction::enterCellSpecification() {
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::InterpolationRestriction::enterCellSpecification()


;
}


peano::MappingSpecification   latticeboltzmann::adapters::InterpolationRestriction::leaveCellSpecification() {
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::InterpolationRestriction::leaveCellSpecification()


;
}


peano::MappingSpecification   latticeboltzmann::adapters::InterpolationRestriction::ascendSpecification() {
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::InterpolationRestriction::ascendSpecification()


;
}


peano::MappingSpecification   latticeboltzmann::adapters::InterpolationRestriction::descendSpecification() {
  return peano::MappingSpecification::getMostGeneralSpecification()
   & latticeboltzmann::mappings::InterpolationRestriction::descendSpecification()


;
}


latticeboltzmann::adapters::InterpolationRestriction::InterpolationRestriction() {
}


latticeboltzmann::adapters::InterpolationRestriction::~InterpolationRestriction() {
}


#if defined(SharedMemoryParallelisation)
latticeboltzmann::adapters::InterpolationRestriction::InterpolationRestriction(const InterpolationRestriction&  masterThread):
  _map2InterpolationRestriction(masterThread._map2InterpolationRestriction) 

 

{
}


void latticeboltzmann::adapters::InterpolationRestriction::mergeWithWorkerThread(const InterpolationRestriction& workerThread) {

  _map2InterpolationRestriction.mergeWithWorkerThread(workerThread._map2InterpolationRestriction);


}
#endif


void latticeboltzmann::adapters::InterpolationRestriction::createHangingVertex(
      latticeboltzmann::Vertex&     fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                fineGridH,
      latticeboltzmann::Vertex * const   coarseGridVertices,
      const peano::grid::VertexEnumerator&      coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&       coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                   fineGridPositionOfVertex
) {

  _map2InterpolationRestriction.createHangingVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::InterpolationRestriction::destroyHangingVertex(
      const latticeboltzmann::Vertex&   fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      latticeboltzmann::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {

  _map2InterpolationRestriction.destroyHangingVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::InterpolationRestriction::createInnerVertex(
      latticeboltzmann::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {

  _map2InterpolationRestriction.createInnerVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::InterpolationRestriction::createBoundaryVertex(
      latticeboltzmann::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {

  _map2InterpolationRestriction.createBoundaryVertex( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::InterpolationRestriction::destroyVertex(
      const latticeboltzmann::Vertex&   fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      latticeboltzmann::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {

  _map2InterpolationRestriction.destroyVertex( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::InterpolationRestriction::createCell(
      latticeboltzmann::Cell&                 fineGridCell,
      latticeboltzmann::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {

  _map2InterpolationRestriction.createCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


}


void latticeboltzmann::adapters::InterpolationRestriction::destroyCell(
      const latticeboltzmann::Cell&           fineGridCell,
      latticeboltzmann::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {

  _map2InterpolationRestriction.destroyCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


}

#ifdef Parallel
void latticeboltzmann::adapters::InterpolationRestriction::mergeWithNeighbour(
  latticeboltzmann::Vertex&  vertex,
  const latticeboltzmann::Vertex&  neighbour,
  int                                           fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&   fineGridX,
  const tarch::la::Vector<DIMENSIONS,double>&   fineGridH,
  int                                           level
) {

   _map2InterpolationRestriction.mergeWithNeighbour( vertex, neighbour, fromRank, fineGridX, fineGridH, level );


}

void latticeboltzmann::adapters::InterpolationRestriction::prepareSendToNeighbour(
  latticeboltzmann::Vertex&  vertex,
  int                                           toRank,
  const tarch::la::Vector<DIMENSIONS,double>&   x,
  const tarch::la::Vector<DIMENSIONS,double>&   h,
  int                                           level
) {

   _map2InterpolationRestriction.prepareSendToNeighbour( vertex, toRank, x, h, level );


}

void latticeboltzmann::adapters::InterpolationRestriction::prepareCopyToRemoteNode(
  latticeboltzmann::Vertex&  localVertex,
  int                                           toRank,
  const tarch::la::Vector<DIMENSIONS,double>&   x,
  const tarch::la::Vector<DIMENSIONS,double>&   h,
  int                                           level
) {

   _map2InterpolationRestriction.prepareCopyToRemoteNode( localVertex, toRank, x, h, level );


}

void latticeboltzmann::adapters::InterpolationRestriction::prepareCopyToRemoteNode(
  latticeboltzmann::Cell&  localCell,
      int                                           toRank,
      const tarch::la::Vector<DIMENSIONS,double>&   x,
      const tarch::la::Vector<DIMENSIONS,double>&   h,
      int                                           level
) {

   _map2InterpolationRestriction.prepareCopyToRemoteNode( localCell, toRank, x, h, level );


}

void latticeboltzmann::adapters::InterpolationRestriction::mergeWithRemoteDataDueToForkOrJoin(
  latticeboltzmann::Vertex&  localVertex,
  const latticeboltzmann::Vertex&  masterOrWorkerVertex,
  int                                       fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&  x,
  const tarch::la::Vector<DIMENSIONS,double>&  h,
  int                                       level
) {

   _map2InterpolationRestriction.mergeWithRemoteDataDueToForkOrJoin( localVertex, masterOrWorkerVertex, fromRank, x, h, level );


}

void latticeboltzmann::adapters::InterpolationRestriction::mergeWithRemoteDataDueToForkOrJoin(
  latticeboltzmann::Cell&  localCell,
  const latticeboltzmann::Cell&  masterOrWorkerCell,
  int                                       fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&  x,
  const tarch::la::Vector<DIMENSIONS,double>&  h,
  int                                       level
) {

   _map2InterpolationRestriction.mergeWithRemoteDataDueToForkOrJoin( localCell, masterOrWorkerCell, fromRank, x, h, level );


}

void latticeboltzmann::adapters::InterpolationRestriction::prepareSendToWorker(
  latticeboltzmann::Cell&                 fineGridCell,
  latticeboltzmann::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  latticeboltzmann::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell,
  int                                                                  worker
) {

   _map2InterpolationRestriction.prepareSendToWorker( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell, worker );


}

void latticeboltzmann::adapters::InterpolationRestriction::prepareSendToMaster(
  latticeboltzmann::Cell&                       localCell,
  latticeboltzmann::Vertex *                    vertices,
  const peano::grid::VertexEnumerator&       verticesEnumerator, 
  const latticeboltzmann::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&       coarseGridVerticesEnumerator,
  const latticeboltzmann::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&   fineGridPositionOfCell
) {

   _map2InterpolationRestriction.prepareSendToMaster( localCell, vertices, verticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


}

void latticeboltzmann::adapters::InterpolationRestriction::mergeWithMaster(
  const latticeboltzmann::Cell&           workerGridCell,
  latticeboltzmann::Vertex * const        workerGridVertices,
  const peano::grid::VertexEnumerator& workerEnumerator,
  latticeboltzmann::Cell&                 fineGridCell,
  latticeboltzmann::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  latticeboltzmann::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell,
  int                                                                  worker,
    const latticeboltzmann::State&          workerState,
  latticeboltzmann::State&                masterState
) {

   _map2InterpolationRestriction.mergeWithMaster( workerGridCell, workerGridVertices, workerEnumerator, fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell, worker, workerState, masterState );


}

void latticeboltzmann::adapters::InterpolationRestriction::receiveDataFromMaster(
      latticeboltzmann::Cell&                        receivedCell, 
      latticeboltzmann::Vertex *                     receivedVertices,
      const peano::grid::VertexEnumerator&        receivedVerticesEnumerator,
      latticeboltzmann::Vertex * const               receivedCoarseGridVertices,
      const peano::grid::VertexEnumerator&        receivedCoarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                        receivedCoarseGridCell,
      latticeboltzmann::Vertex * const               workersCoarseGridVertices,
      const peano::grid::VertexEnumerator&        workersCoarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                        workersCoarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&    fineGridPositionOfCell
) {

   _map2InterpolationRestriction.receiveDataFromMaster( receivedCell, receivedVertices, receivedVerticesEnumerator, receivedCoarseGridVertices, receivedCoarseGridVerticesEnumerator, receivedCoarseGridCell, workersCoarseGridVertices, workersCoarseGridVerticesEnumerator, workersCoarseGridCell, fineGridPositionOfCell );


}


void latticeboltzmann::adapters::InterpolationRestriction::mergeWithWorker(
  latticeboltzmann::Cell&           localCell, 
  const latticeboltzmann::Cell&     receivedMasterCell,
  const tarch::la::Vector<DIMENSIONS,double>&  cellCentre,
  const tarch::la::Vector<DIMENSIONS,double>&  cellSize,
  int                                          level
) {

   _map2InterpolationRestriction.mergeWithWorker( localCell, receivedMasterCell, cellCentre, cellSize, level );


}

void latticeboltzmann::adapters::InterpolationRestriction::mergeWithWorker(
  latticeboltzmann::Vertex&        localVertex,
  const latticeboltzmann::Vertex&  receivedMasterVertex,
  const tarch::la::Vector<DIMENSIONS,double>&   x,
  const tarch::la::Vector<DIMENSIONS,double>&   h,
  int                                           level
) {

   _map2InterpolationRestriction.mergeWithWorker( localVertex, receivedMasterVertex, x, h, level );


}
#endif

void latticeboltzmann::adapters::InterpolationRestriction::touchVertexFirstTime(
      latticeboltzmann::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {

  _map2InterpolationRestriction.touchVertexFirstTime( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::InterpolationRestriction::touchVertexLastTime(
      latticeboltzmann::Vertex&         fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      latticeboltzmann::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {

  _map2InterpolationRestriction.touchVertexLastTime( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void latticeboltzmann::adapters::InterpolationRestriction::enterCell(
      latticeboltzmann::Cell&                 fineGridCell,
      latticeboltzmann::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      latticeboltzmann::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {

  _map2InterpolationRestriction.enterCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


}


void latticeboltzmann::adapters::InterpolationRestriction::leaveCell(
      latticeboltzmann::Cell&           fineGridCell,
      latticeboltzmann::Vertex * const  fineGridVertices,
      const peano::grid::VertexEnumerator&          fineGridVerticesEnumerator,
      latticeboltzmann::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      latticeboltzmann::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfCell
) {

  _map2InterpolationRestriction.leaveCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


}


void latticeboltzmann::adapters::InterpolationRestriction::beginIteration(
  latticeboltzmann::State&  solverState
) {

  _map2InterpolationRestriction.beginIteration( solverState );


}


void latticeboltzmann::adapters::InterpolationRestriction::endIteration(
  latticeboltzmann::State&  solverState
) {

  _map2InterpolationRestriction.endIteration( solverState );


}




void latticeboltzmann::adapters::InterpolationRestriction::descend(
  latticeboltzmann::Cell * const          fineGridCells,
  latticeboltzmann::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  latticeboltzmann::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&                 coarseGridCell
) {

  _map2InterpolationRestriction.descend( fineGridCells, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell );


}


void latticeboltzmann::adapters::InterpolationRestriction::ascend(
  latticeboltzmann::Cell * const    fineGridCells,
  latticeboltzmann::Vertex * const  fineGridVertices,
  const peano::grid::VertexEnumerator&          fineGridVerticesEnumerator,
  latticeboltzmann::Vertex * const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&           coarseGridCell
) {

  _map2InterpolationRestriction.ascend( fineGridCells, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell );


}
