#include "latticeboltzmann/mappings/InterpolationRestriction.h"

#include "latticeboltzmann/lbmsteps/CellSteps.h"
#include "latticeboltzmann/lbmsteps/Interpolation.h"
#include "latticeboltzmann/lbmsteps/Restriction.h"

namespace latticeboltzmann {
namespace mappings {
/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
InterpolationRestriction::
touchVertexLastTimeSpecification()
{
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::AvoidFineGridRaces,
                                     false);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
InterpolationRestriction::
touchVertexFirstTimeSpecification()
{
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::AvoidFineGridRaces,
                                     false);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
InterpolationRestriction::
enterCellSpecification()
{
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::AvoidFineGridRaces,
                                     false);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
InterpolationRestriction::
leaveCellSpecification()
{
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::AvoidFineGridRaces,
                                     false);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
InterpolationRestriction::
ascendSpecification()
{
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::AvoidFineGridRaces,
                                     false);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
InterpolationRestriction::
descendSpecification()
{
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::AvoidFineGridRaces,
                                     false);
}

tarch::logging::Log InterpolationRestriction::
_log("InterpolationRestriction");

InterpolationRestriction::
InterpolationRestriction()
{
  logTraceIn("InterpolationRestriction()");
  // @todo Insert your code here
  logTraceOut("InterpolationRestriction()");
}

InterpolationRestriction::~InterpolationRestriction()
{
  logTraceIn("~InterpolationRestriction()");
  // @todo Insert your code here
  logTraceOut("~InterpolationRestriction()");
}

#if defined (SharedMemoryParallelisation)
InterpolationRestriction::
InterpolationRestriction(const InterpolationRestriction& masterThread)
{
  logTraceIn("InterpolationRestriction(InterpolationRestriction)");
  // @todo Insert your code here
  logTraceOut("InterpolationRestriction(InterpolationRestriction)");
}

void
InterpolationRestriction::
mergeWithWorkerThread(const InterpolationRestriction& workerThread)
{
  logTraceIn("mergeWithWorkerThread(InterpolationRestriction)");
  // @todo Insert your code here
  logTraceOut("mergeWithWorkerThread(InterpolationRestriction)");
}
#endif

void
InterpolationRestriction::
createHangingVertex(
  latticeboltzmann::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                fineGridH,
  latticeboltzmann::Vertex* const   coarseGridVertices,
  const peano::grid::VertexEnumerator&      coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&       coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&                   fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("createHangingVertex(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);

  int level = coarseGridVerticesEnumerator.getLevel() + 1;

  int newIndex = Vertex::getVertexIndex(fineGridX, level);

  // extract data structure from the heap
  fineGridVertex.setAggregateWithArbitraryCardinality(newIndex);

  logTraceOutWith1Argument("createHangingVertex(...)", fineGridVertex);
}

void
InterpolationRestriction::
destroyHangingVertex(
  const latticeboltzmann::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridH,
  latticeboltzmann::Vertex* const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&           coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&                       fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("destroyHangingVertex(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("destroyHangingVertex(...)", fineGridVertex);
}

void
InterpolationRestriction::
createInnerVertex(
  latticeboltzmann::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                          fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                          fineGridH,
  latticeboltzmann::Vertex* const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&                             fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("createInnerVertex(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("createInnerVertex(...)", fineGridVertex);
}

void
InterpolationRestriction::
createBoundaryVertex(
  latticeboltzmann::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                          fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                          fineGridH,
  latticeboltzmann::Vertex* const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&                             fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("createBoundaryVertex(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("createBoundaryVertex(...)", fineGridVertex);
}

void
InterpolationRestriction::
destroyVertex(
  const latticeboltzmann::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridH,
  latticeboltzmann::Vertex* const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&           coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&                       fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("destroyVertex(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("destroyVertex(...)", fineGridVertex);
}

void
InterpolationRestriction::
createCell(
  latticeboltzmann::Cell& fineGridCell,
  latticeboltzmann::Vertex* const fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  latticeboltzmann::Vertex* const coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  latticeboltzmann::Cell& coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&                             fineGridPositionOfCell)
{
  logTraceInWith4Arguments("createCell(...)", fineGridCell,
                           fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell);
  // @todo Insert your code here
  logTraceOutWith1Argument("createCell(...)", fineGridCell);
}

void
InterpolationRestriction::
destroyCell(
  const latticeboltzmann::Cell& fineGridCell,
  latticeboltzmann::Vertex* const fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  latticeboltzmann::Vertex* const coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  latticeboltzmann::Cell& coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&                             fineGridPositionOfCell)
{
  logTraceInWith4Arguments("destroyCell(...)", fineGridCell,
                           fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell);
  // @todo Insert your code here
  logTraceOutWith1Argument("destroyCell(...)", fineGridCell);
}

#ifdef Parallel
void
InterpolationRestriction::
mergeWithNeighbour(
  latticeboltzmann::Vertex& vertex,
  const latticeboltzmann::Vertex& neighbour,
  int fromRank,
  const tarch::la::Vector<DIMENSIONS, double>&   fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&   fineGridH,
  int                                           level)
{
  logTraceInWith6Arguments("mergeWithNeighbour(...)", vertex, neighbour, fromRank, fineGridX, fineGridH, level);
  // @todo Insert your code here
  logTraceOut("mergeWithNeighbour(...)");
}

void
InterpolationRestriction::
prepareSendToNeighbour(
  latticeboltzmann::Vertex& vertex,
  int toRank,
  const tarch::la::Vector<DIMENSIONS, double>&   x,
  const tarch::la::Vector<DIMENSIONS, double>&   h,
  int                                           level)
{
  logTraceInWith3Arguments("prepareSendToNeighbour(...)", vertex, toRank, level);
  // @todo Insert your code here
  logTraceOut("prepareSendToNeighbour(...)");
}

void
InterpolationRestriction::
prepareCopyToRemoteNode(
  latticeboltzmann::Vertex& localVertex,
  int toRank,
  const tarch::la::Vector<DIMENSIONS, double>&   x,
  const tarch::la::Vector<DIMENSIONS, double>&   h,
  int                                           level)
{
  logTraceInWith5Arguments("prepareCopyToRemoteNode(...)", localVertex, toRank, x, h, level);
  // @todo Insert your code here
  logTraceOut("prepareCopyToRemoteNode(...)");
}

void
InterpolationRestriction::
prepareCopyToRemoteNode(
  latticeboltzmann::Cell& localCell,
  int toRank,
  const tarch::la::Vector<DIMENSIONS, double>&   cellCentre,
  const tarch::la::Vector<DIMENSIONS, double>&   cellSize,
  int                                           level)
{
  logTraceInWith5Arguments("prepareCopyToRemoteNode(...)", localCell, toRank, cellCentre, cellSize, level);
  // @todo Insert your code here
  logTraceOut("prepareCopyToRemoteNode(...)");
}

void
InterpolationRestriction::
mergeWithRemoteDataDueToForkOrJoin(
  latticeboltzmann::Vertex& localVertex,
  const latticeboltzmann::Vertex& masterOrWorkerVertex,
  int fromRank,
  const tarch::la::Vector<DIMENSIONS, double>&  x,
  const tarch::la::Vector<DIMENSIONS, double>&  h,
  int                                       level)
{
  logTraceInWith6Arguments("mergeWithRemoteDataDueToForkOrJoin(...)",
                           localVertex,
                           masterOrWorkerVertex,
                           fromRank,
                           x,
                           h,
                           level);
  // @todo Insert your code here
  logTraceOut("mergeWithRemoteDataDueToForkOrJoin(...)");
}

void
InterpolationRestriction::
mergeWithRemoteDataDueToForkOrJoin(
  latticeboltzmann::Cell& localCell,
  const latticeboltzmann::Cell& masterOrWorkerCell,
  int fromRank,
  const tarch::la::Vector<DIMENSIONS, double>&  cellCentre,
  const tarch::la::Vector<DIMENSIONS, double>&  cellSize,
  int                                       level)
{
  logTraceInWith3Arguments("mergeWithRemoteDataDueToForkOrJoin(...)", localCell, masterOrWorkerCell, fromRank);
  // @todo Insert your code here
  logTraceOut("mergeWithRemoteDataDueToForkOrJoin(...)");
}

void
InterpolationRestriction::
prepareSendToWorker(
  latticeboltzmann::Cell& fineGridCell,
  latticeboltzmann::Vertex* const fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  latticeboltzmann::Vertex* const coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  latticeboltzmann::Cell& coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&                             fineGridPositionOfCell,
  int                                                                  worker)
{
  logTraceIn("prepareSendToWorker(...)");
  // @todo Insert your code here
  logTraceOut("prepareSendToWorker(...)");
}

void
InterpolationRestriction::
prepareSendToMaster(
  latticeboltzmann::Cell& localCell,
  latticeboltzmann::Vertex* vertices,
  const peano::grid::VertexEnumerator& verticesEnumerator,
  const latticeboltzmann::Vertex* const coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  const latticeboltzmann::Cell& coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&   fineGridPositionOfCell)
{
  logTraceInWith2Arguments("prepareSendToMaster(...)", localCell, verticesEnumerator.toString());
  // @todo Insert your code here
  logTraceOut("prepareSendToMaster(...)");
}

void
InterpolationRestriction::
mergeWithMaster(
  const latticeboltzmann::Cell& workerGridCell,
  latticeboltzmann::Vertex* const workerGridVertices,
  const peano::grid::VertexEnumerator& workerEnumerator,
  latticeboltzmann::Cell& fineGridCell,
  latticeboltzmann::Vertex* const fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  latticeboltzmann::Vertex* const coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  latticeboltzmann::Cell& coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&                             fineGridPositionOfCell,
  int                                                                  worker,
  const latticeboltzmann::State&          workerState,
  latticeboltzmann::State&                masterState)
{
  logTraceIn("mergeWithMaster(...)");
  // @todo Insert your code here
  logTraceOut("mergeWithMaster(...)");
}

void
InterpolationRestriction::
receiveDataFromMaster(
  latticeboltzmann::Cell& receivedCell,
  latticeboltzmann::Vertex* receivedVertices,
  const peano::grid::VertexEnumerator& receivedVerticesEnumerator,
  latticeboltzmann::Vertex* const receivedCoarseGridVertices,
  const peano::grid::VertexEnumerator& receivedCoarseGridVerticesEnumerator,
  latticeboltzmann::Cell& receivedCoarseGridCell,
  latticeboltzmann::Vertex* const workersCoarseGridVertices,
  const peano::grid::VertexEnumerator& workersCoarseGridVerticesEnumerator,
  latticeboltzmann::Cell& workersCoarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&    fineGridPositionOfCell)
{
  logTraceIn("receiveDataFromMaster(...)");
  // @todo Insert your code here
  logTraceOut("receiveDataFromMaster(...)");
}

void
InterpolationRestriction::
mergeWithWorker(
  latticeboltzmann::Cell& localCell,
  const latticeboltzmann::Cell& receivedMasterCell,
  const tarch::la::Vector<DIMENSIONS, double>&  cellCentre,
  const tarch::la::Vector<DIMENSIONS, double>&  cellSize,
  int                                          level)
{
  logTraceInWith2Arguments("mergeWithWorker(...)", localCell.toString(), receivedMasterCell.toString());
  // @todo Insert your code here
  logTraceOutWith1Argument("mergeWithWorker(...)", localCell.toString());
}

void
InterpolationRestriction::
mergeWithWorker(
  latticeboltzmann::Vertex& localVertex,
  const latticeboltzmann::Vertex& receivedMasterVertex,
  const tarch::la::Vector<DIMENSIONS, double>&   x,
  const tarch::la::Vector<DIMENSIONS, double>&   h,
  int                                           level)
{
  logTraceInWith2Arguments("mergeWithWorker(...)", localVertex.toString(), receivedMasterVertex.toString());
  // @todo Insert your code here
  logTraceOutWith1Argument("mergeWithWorker(...)", localVertex.toString());
}
#endif

void
InterpolationRestriction::
touchVertexFirstTime(
  latticeboltzmann::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                          fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                          fineGridH,
  latticeboltzmann::Vertex* const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&                             fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("touchVertexFirstTime(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("touchVertexFirstTime(...)", fineGridVertex);
}

void
InterpolationRestriction::
touchVertexLastTime(
  latticeboltzmann::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridH,
  latticeboltzmann::Vertex* const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&           coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&                       fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("touchVertexLastTime(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("touchVertexLastTime(...)", fineGridVertex);
}

void
InterpolationRestriction::
enterCell(latticeboltzmann::Cell& fineGridCell,
          latticeboltzmann::Vertex* const fineGridVertices,
          const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
          latticeboltzmann::Vertex* const coarseGridVertices,
          const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
          latticeboltzmann::Cell& coarseGridCell,
          const tarch::la::Vector<DIMENSIONS, int>& fineGridPositionOfCell)
{
  logTraceInWith4Arguments("enterCell(...)", fineGridCell,
                           fineGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfCell);

  int level = fineGridVerticesEnumerator.getLevel();

  if (level > COARSE_REFINEMENT_LEVEL && State::isIterationWithComputing(level)) {
    // if next iteration on the coarser level has some computing
    // we are doing here restriction

    if (State::isIterationWithComputing(level - 1, +1))
      Restriction::restrictHangingVerticesCoarseToFine(level,
                                                       fineGridVertices,
                                                       fineGridVerticesEnumerator,
                                                       fineGridCell,
                                                       coarseGridVertices,
                                                       coarseGridVerticesEnumerator,
                                                       coarseGridCell
                                                       );

    Interpolation::interpolateHangingVerticesCoarseToFine(level,
                                                          fineGridVertices,
                                                          fineGridVerticesEnumerator,
                                                          coarseGridVertices,
                                                          coarseGridVerticesEnumerator);
  }

  logTraceOutWith1Argument("enterCell(...)", fineGridCell);
}

void
InterpolationRestriction::
leaveCell(
  latticeboltzmann::Cell& fineGridCell,
  latticeboltzmann::Vertex* const fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  latticeboltzmann::Vertex* const coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  latticeboltzmann::Cell& coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&                       fineGridPositionOfCell)
{
  logTraceInWith4Arguments("leaveCell(...)", fineGridCell,
                           fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell);
  // @todo Insert your code here
  logTraceOutWith1Argument("leaveCell(...)", fineGridCell);
}

void
InterpolationRestriction::
beginIteration(
  latticeboltzmann::State& solverState)
{
  logTraceInWith1Argument("beginIteration(State)", solverState);
  // @todo Insert your code here
  logTraceOutWith1Argument("beginIteration(State)", solverState);
}

void
InterpolationRestriction::
endIteration(latticeboltzmann::State& solverState)
{
  logTraceInWith1Argument("endIteration(State)", solverState);

  for (int level = COARSE_REFINEMENT_LEVEL; level <= FINE_REFINEMENT_LEVEL; ++level)
    // here we invert last array value
    // 0 -> 1
    // 1 -> 2 == 0
    if (State::isIterationWithComputing(level))
      State::lastArray[level] = (State::lastArray[level] + 1) % 2;

  State::setIteration(State::getIteration() + 1);

  logTraceOutWith1Argument("endIteration(State)", solverState);
}

void
InterpolationRestriction::
descend(
  latticeboltzmann::Cell* const        fineGridCells,
  latticeboltzmann::Vertex* const      fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  latticeboltzmann::Vertex* const      coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&              coarseGridCell)
{
  logTraceInWith2Arguments("descend(...)", coarseGridCell.toString(), coarseGridVerticesEnumerator.toString());
  // @todo Insert your code here
  logTraceOut("descend(...)");
}

void
InterpolationRestriction::
ascend(
  latticeboltzmann::Cell* const        fineGridCells,
  latticeboltzmann::Vertex* const      fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  latticeboltzmann::Vertex* const      coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&              coarseGridCell)
{
  logTraceInWith2Arguments("ascend(...)", coarseGridCell.toString(), coarseGridVerticesEnumerator.toString());
  // @todo Insert your code here
  logTraceOut("ascend(...)");
}
}
}
