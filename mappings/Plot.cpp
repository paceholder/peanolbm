#include "latticeboltzmann/mappings/Plot.h"

#include "latticeboltzmann/LBDefinitions.h"

#ifdef Dim2
  #include "latticeboltzmann/heap2D/CellHeap.h"
  #include "latticeboltzmann/heap2D/VertexHeap.h"
#endif

#ifdef Dim3
  #include "latticeboltzmann/heap3D/CellHeap.h"
  #include "latticeboltzmann/heap3D/VertexHeap.h"
#endif

#include "peano/heap/Heap.h"

#include "latticeboltzmann/lbmsteps/CellSteps.h"

namespace latticeboltzmann {
/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
mappings::Plot::
touchVertexLastTimeSpecification()
{
  return peano::MappingSpecification(
    peano::MappingSpecification::WholeTree,
    peano::MappingSpecification::
    AvoidFineGridRaces,
    false);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
mappings::Plot::
touchVertexFirstTimeSpecification()
{
  return peano::MappingSpecification(
    peano::MappingSpecification::WholeTree,
    peano::MappingSpecification::
    AvoidFineGridRaces,
    false);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
mappings::Plot::
enterCellSpecification()
{
  return peano::MappingSpecification(
    peano::MappingSpecification::WholeTree,
    peano::MappingSpecification::
    AvoidFineGridRaces,
    false);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
mappings::Plot::
leaveCellSpecification()
{
  return peano::MappingSpecification(
    peano::MappingSpecification::WholeTree,
    peano::MappingSpecification::
    AvoidFineGridRaces,
    false);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
mappings::Plot::
ascendSpecification()
{
  return peano::MappingSpecification(
    peano::MappingSpecification::WholeTree,
    peano::MappingSpecification::
    AvoidFineGridRaces,
    false);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
mappings::Plot::
descendSpecification()
{
  return peano::MappingSpecification(
    peano::MappingSpecification::WholeTree,
    peano::MappingSpecification::
    AvoidFineGridRaces,
    false);
}

tarch::logging::Log mappings::Plot::
_log("mappings::Plot");

mappings::Plot::
Plot()
{
  logTraceIn("Plot()");
  // @todo Insert your code here
  logTraceOut("Plot()");
}

mappings::Plot::~Plot()
{
  logTraceIn("~Plot()");
  // @todo Insert your code here
  logTraceOut("~Plot()");
}

#if defined (SharedMemoryParallelisation)
mappings::Plot::
Plot(const Plot& masterThread): _vtkWriter(10)
{
  logTraceIn("Plot(Plot)");
  // @todo Insert your code here
  logTraceOut("Plot(Plot)");
}

void
mappings::Plot::
mergeWithWorkerThread(const Plot& workerThread)
{
  logTraceIn("mergeWithWorkerThread(Plot)");
  // @todo Insert your code here
  logTraceOut("mergeWithWorkerThread(Plot)");
}
#endif

void
mappings::Plot::
createHangingVertex(
  Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                fineGridH,
  Vertex* const   coarseGridVertices,
  const peano::grid::VertexEnumerator&      coarseGridVerticesEnumerator,
  Cell&       coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&                   fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("createHangingVertex(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);

  fineGridVertex.setAggregateWithArbitraryCardinality(
    latticeboltzmann::Vertex::getVertexIndex(fineGridX,
                                             coarseGridVerticesEnumerator
                                             .getLevel() + 1));

  logTraceOutWith1Argument("createHangingVertex(...)", fineGridVertex);
}

void
mappings::Plot::
destroyHangingVertex(
  const Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridH,
  Vertex* const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  Cell&           coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&                       fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("destroyHangingVertex(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("destroyHangingVertex(...)", fineGridVertex);
}

void
mappings::Plot::
createInnerVertex(
  Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>& fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>& fineGridH,
  Vertex* const                    coarseGridVertices,
  const peano::grid::VertexEnumerator&
  coarseGridVerticesEnumerator,
  Cell&                            coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>& fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("createInnerVertex(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);

  logTraceOutWith1Argument("createInnerVertex(...)", fineGridVertex);
}

void
mappings::Plot::
createBoundaryVertex(
  Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>& fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>& fineGridH,
  Vertex* const                    coarseGridVertices,
  const peano::grid::VertexEnumerator&
  coarseGridVerticesEnumerator,
  Cell&                            coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&    fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("createBoundaryVertex(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);

  logTraceOutWith1Argument("createBoundaryVertex(...)", fineGridVertex);
}

void
mappings::Plot::
destroyVertex(
  const Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridH,
  Vertex* const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  Cell&           coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&                       fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("destroyVertex(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("destroyVertex(...)", fineGridVertex);
}

void
mappings::Plot::
createCell(
  Cell&                                fineGridCell,
  Vertex* const                        fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  Vertex* const                        coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  Cell&                                coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&        fineGridPositionOfCell)
{
  logTraceInWith4Arguments("createCell(...)",
                           fineGridCell,
                           fineGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfCell);
  // @todo Insert your code here
  logTraceOutWith1Argument("createCell(...)", fineGridCell);
}

void
mappings::Plot::
destroyCell(
  const Cell&                          fineGridCell,
  Vertex* const                        fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  Vertex* const                        coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  Cell&                                coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&        fineGridPositionOfCell)
{
  logTraceInWith4Arguments("destroyCell(...)",
                           fineGridCell,
                           fineGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfCell);
  // @todo Insert your code here
  logTraceOutWith1Argument("destroyCell(...)", fineGridCell);
}

#ifdef Parallel
void
mappings::Plot::
mergeWithNeighbour(
  Vertex& vertex,
  const Vertex& neighbour,
  int fromRank,
  const tarch::la::Vector<DIMENSIONS, double>&   fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&   fineGridH,
  int                                           level)
{
  logTraceInWith6Arguments("mergeWithNeighbour(...)",
                           vertex,
                           neighbour,
                           fromRank,
                           fineGridX,
                           fineGridH,
                           level);
  // @todo Insert your code here
  logTraceOut("mergeWithNeighbour(...)");
}

void
mappings::Plot::
prepareSendToNeighbour(
  Vertex& vertex,
  int toRank,
  const tarch::la::Vector<DIMENSIONS, double>&   x,
  const tarch::la::Vector<DIMENSIONS, double>&   h,
  int                                           level)
{
  logTraceInWith3Arguments("prepareSendToNeighbour(...)",
                           vertex,
                           toRank,
                           level);
  // @todo Insert your code here
  logTraceOut("prepareSendToNeighbour(...)");
}

void
mappings::Plot::
prepareCopyToRemoteNode(
  Vertex& localVertex,
  int toRank,
  const tarch::la::Vector<DIMENSIONS, double>&   x,
  const tarch::la::Vector<DIMENSIONS, double>&   h,
  int                                           level)
{
  logTraceInWith5Arguments("prepareCopyToRemoteNode(...)",
                           localVertex,
                           toRank,
                           x,
                           h,
                           level);
  // @todo Insert your code here
  logTraceOut("prepareCopyToRemoteNode(...)");
}

void
mappings::Plot::
prepareCopyToRemoteNode(
  Cell& localCell,
  int toRank,
  const tarch::la::Vector<DIMENSIONS, double>&   cellCentre,
  const tarch::la::Vector<DIMENSIONS, double>&   cellSize,
  int                                           level)
{
  logTraceInWith5Arguments("prepareCopyToRemoteNode(...)",
                           localCell,
                           toRank,
                           cellCentre,
                           cellSize,
                           level);
  // @todo Insert your code here
  logTraceOut("prepareCopyToRemoteNode(...)");
}

void
mappings::Plot::
mergeWithRemoteDataDueToForkOrJoin(
  Vertex& localVertex,
  const Vertex& masterOrWorkerVertex,
  int fromRank,
  const tarch::la::Vector<DIMENSIONS, double>&  x,
  const tarch::la::Vector<DIMENSIONS, double>&  h,
  int                                       level)
{
  logTraceInWith6Arguments("mergeWithRemoteDataDueToForkOrJoin(...)",
                           localVertex,
                           masterOrWorkerVertex,
                           fromRank,
                           x,
                           h,
                           level);
  // @todo Insert your code here
  logTraceOut("mergeWithRemoteDataDueToForkOrJoin(...)");
}

void
mappings::Plot::
mergeWithRemoteDataDueToForkOrJoin(
  Cell& localCell,
  const Cell& masterOrWorkerCell,
  int fromRank,
  const tarch::la::Vector<DIMENSIONS, double>&  cellCentre,
  const tarch::la::Vector<DIMENSIONS, double>&  cellSize,
  int                                       level)
{
  logTraceInWith3Arguments("mergeWithRemoteDataDueToForkOrJoin(...)",
                           localCell,
                           masterOrWorkerCell,
                           fromRank);
  // @todo Insert your code here
  logTraceOut("mergeWithRemoteDataDueToForkOrJoin(...)");
}

void
mappings::Plot::
prepareSendToWorker(
  Cell&                                fineGridCell,
  Vertex* const                        fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  Vertex* const                        coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  Cell&                                coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&        fineGridPositionOfCell,
  int                                  worker)
{
  logTraceIn("prepareSendToWorker(...)");
  // @todo Insert your code here
  logTraceOut("prepareSendToWorker(...)");
}

void
mappings::Plot::
prepareSendToMaster(
  Cell& localCell,
  Vertex* vertices,
  const peano::grid::VertexEnumerator& verticesEnumerator,
  const Vertex* const coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  const Cell& coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&   fineGridPositionOfCell)
{
  logTraceInWith2Arguments("prepareSendToMaster(...)",
                           localCell,
                           verticesEnumerator.toString());
  // @todo Insert your code here
  logTraceOut("prepareSendToMaster(...)");
}

void
mappings::Plot::
mergeWithMaster(
  const Cell&                          workerGridCell,
  Vertex* const                        workerGridVertices,
  const peano::grid::VertexEnumerator& workerEnumerator,
  Cell&                                fineGridCell,
  Vertex* const                        fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  Vertex* const                        coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  Cell&                                coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&        fineGridPositionOfCell,
  int                                  worker,
  const State&                         workerState,
  State&                               masterState)
{
  logTraceIn("mergeWithMaster(...)");
  // @todo Insert your code here
  logTraceOut("mergeWithMaster(...)");
}

void
mappings::Plot::
receiveDataFromMaster(
  Cell& receivedCell,
  Vertex* receivedVertices,
  const peano::grid::VertexEnumerator& receivedVerticesEnumerator,
  Vertex* const receivedCoarseGridVertices,
  const peano::grid::VertexEnumerator& receivedCoarseGridVerticesEnumerator,
  Cell& receivedCoarseGridCell,
  Vertex* const workersCoarseGridVertices,
  const peano::grid::VertexEnumerator& workersCoarseGridVerticesEnumerator,
  Cell& workersCoarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&    fineGridPositionOfCell)
{
  logTraceIn("receiveDataFromMaster(...)");
  // @todo Insert your code here
  logTraceOut("receiveDataFromMaster(...)");
}

void
mappings::Plot::
mergeWithWorker(
  Cell& localCell,
  const Cell& receivedMasterCell,
  const tarch::la::Vector<DIMENSIONS, double>&  cellCentre,
  const tarch::la::Vector<DIMENSIONS, double>&  cellSize,
  int                                          level)
{
  logTraceInWith2Arguments("mergeWithWorker(...)",
                           localCell.toString(), receivedMasterCell.toString());
  // @todo Insert your code here
  logTraceOutWith1Argument("mergeWithWorker(...)", localCell.toString());
}

void
mappings::Plot::
mergeWithWorker(
  Vertex& localVertex,
  const Vertex& receivedMasterVertex,
  const tarch::la::Vector<DIMENSIONS, double>&   x,
  const tarch::la::Vector<DIMENSIONS, double>&   h,
  int                                           level)
{
  logTraceInWith2Arguments("mergeWithWorker(...)",
                           localVertex.toString(),
                           receivedMasterVertex.toString());
  // @todo Insert your code here
  logTraceOutWith1Argument("mergeWithWorker(...)", localVertex.toString());
}
#endif

void
mappings::Plot::
touchVertexFirstTime(
  Vertex&                          fineGridVertex,
  const tarch::la::Vector<DIMENSIONS,
                          double>& fineGridX,
  const tarch::la::Vector<DIMENSIONS,
                          double>& fineGridH,
  Vertex* const                    coarseGridVertices,
  const peano::grid::VertexEnumerator&
  coarseGridVerticesEnumerator,
  Cell&                            coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&    fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("touchVertexFirstTime(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("touchVertexFirstTime(...)", fineGridVertex);
}

void
mappings::Plot::
touchVertexLastTime(
  Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridH,
  Vertex* const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  Cell&           coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&                       fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("touchVertexLastTime(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("touchVertexLastTime(...)", fineGridVertex);
}

// --------------------------------------------------------------------------------

void
mappings::Plot::
enterCell(Cell& fineGridCell,
          Vertex* const fineGridVertices,
          const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
          Vertex* const coarseGridVertices,
          const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
          Cell& coarseGridCell,
          const tarch::la::Vector<DIMENSIONS, int>&  fineGridPositionOfCell)
{
  logTraceInWith4Arguments("enterCell(...)",
                           fineGridCell,
                           fineGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfCell);

  int level = fineGridVerticesEnumerator.getLevel();

  if (level >= COARSE_REFINEMENT_LEVEL &&
      level <= FINE_REFINEMENT_LEVEL &&
      !fineGridCell.isRefined()
      ) {
    tarch::la::Vector<DIMENSIONS, double> currentVertexPosition(0.0);

    int vertexIndex[TWO_POWER_D];

    // small step between points inside PEANO CELL (LB BLOCK) double

    double peanoCellSize = 1.0 / pow(3.0, level - 1);
    double step          = peanoCellSize / double(LB_DIMENSION - 1);

    //    if (level > COARSE_REFINEMENT_LEVEL)
    //      step = peanoCellSize / double(LB_DIMENSION - 1);

    // vertex plotting
    // take heap data structure
    std::vector<CellHeap>& cellData =
      peano::heap::Heap<CellHeap>::getInstance().getData(
        fineGridCell.getAggregateWithArbitraryCardinality());

    // make a copy of an array to modify it and print
    CellHeap array = cellData[0];

    int vertexArrayIndex = State::lastArray[level];

    // since we process just the central part of a cell,
    // before printing we need to complete dirs along the boundaries
    CellSteps::completeDirs(vertexArrayIndex,
                            array,
                            fineGridVertices,
                            fineGridVerticesEnumerator);

    Vector<CELL_BLOCK_SIZE, double> density(0.0);

    latticeboltzmann::CellSteps::computeDensity(array, density);

    Vector<CELL_BLOCK_SIZE, Vector<DIMENSIONS, double> > velocity;
    latticeboltzmann::CellSteps::computeVelocity(array, density, velocity);

    tarch::la::Vector<DIMENSIONS, double> fineGrid0VerPos =
      fineGridVerticesEnumerator.getVertexPosition(0);

#ifdef Dim3

    // here we find absolute position of a point
    for (int k = 0; k < LB_DIMENSION - 1; ++k) {
      double dz = fineGrid0VerPos(2) + k * step;
      currentVertexPosition(2) = dz;

#endif

    // here we find absolute position of a point
    for (int j = 0; j < LB_DIMENSION - 1; ++j) {
      double dy = fineGrid0VerPos(1) + j * step;
      currentVertexPosition(1) = dy;

      for (int i = 0; i < LB_DIMENSION - 1; ++i) {
        double dx = fineGrid0VerPos(0) + i * step;
        currentVertexPosition(0) = dx;

        // now we make CELLS

        int                          vertexCounter = 0;
        tarch::la::Vector<3, double> position(0.0);

        // z-direction
#ifdef Dim2
        position(2) = 0.0 + 0.0 * (fineGridVerticesEnumerator.getLevel() - COARSE_REFINEMENT_LEVEL);
#endif
#ifdef Dim3

        for (int kk =  0; kk < 2; ++kk) {
          position(2) = currentVertexPosition(2) + kk * step;
#endif

        for (int jj =  0; jj < 2; ++jj) {
          position(1) = currentVertexPosition(1) + jj * step;

          for (int ii = 0; ii < 2; ++ii) {
            position(0) = currentVertexPosition(0) + ii * step;

            if (_vertexToIndexMap.find(position) == _vertexToIndexMap.end()) {
              int vertexNumber = _vertexWriter->plotVertex(position);

              int index =
#ifdef Dim3
                (k + kk) * LB_DIMENSION * LB_DIMENSION +
#endif
                (j + jj) * LB_DIMENSION  + (i + ii);

              {
                // write density to VTK
                _densityWriter->plotVertex(vertexNumber, density[index]);
              }

              {
                tarch::la::Vector<3, double> velocityBuffer(0.0, 0.0, 0.0);

                for (int d = 0; d < DIMENSIONS; ++d) {
                  int index =
#ifdef Dim3
                    (k + kk) * LB_DIMENSION * LB_DIMENSION +
#endif
                    (j + jj) * LB_DIMENSION + (i + ii);

                  // negative velocity because distributions are inverted
                  velocityBuffer[d] = -velocity[index][d];
                }

                _velocityWriter->plotVertex(vertexNumber, velocityBuffer);
              }

              _vertexToIndexMap[position] = vertexNumber;
              vertexIndex[vertexCounter]  = vertexNumber;
            } else
              vertexIndex[vertexCounter] = _vertexToIndexMap.find(position)->second;

            ++vertexCounter;
          }
        }

#ifdef Dim3
      }

#endif

#ifdef Dim2
        _cellWriter->plotQuadrangle(vertexIndex);
#endif
#ifdef Dim3
        _cellWriter->plotHexahedron(vertexIndex);
#endif
      }
    }
  }

#ifdef Dim3
}
#endif

  logTraceOutWith1Argument("enterCell(...)", fineGridCell);
}

// -----------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------

void
mappings::Plot::
leaveCell(
  Cell& fineGridCell,
  Vertex* const fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  Vertex* const coarseGridVertices,
  const peano::grid::VertexEnumerator&
  coarseGridVerticesEnumerator,
  Cell& coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>& fineGridPositionOfCell)
{
  logTraceInWith4Arguments("leaveCell(...)",
                           fineGridCell,
                           fineGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfCell);
  // @todo Insert your  code here

  logTraceOutWith1Argument("leaveCell(...)", fineGridCell);
}

void
mappings::Plot::
beginIteration(State& solverState)
{
  logTraceInWith1Argument("beginIteration(State)", solverState);

  _vtkWriter.clear();
  _vertexToIndexMap.clear();

  _vertexWriter = _vtkWriter.createVertexWriter();
  _cellWriter   = _vtkWriter.createCellWriter();

  _densityWriter = _vtkWriter.createVertexDataWriter("density", 1);

  //  _swWriter = _vtkWriter.createVertexDataWriter("sw", 1);
  //  _sWriter  = _vtkWriter.createVertexDataWriter("s", 1);
  //  _seWriter = _vtkWriter.createVertexDataWriter("se", 1);

  //  _wWriter = _vtkWriter.createVertexDataWriter("w", 1);
  //  _cWriter = _vtkWriter.createVertexDataWriter("c", 1);
  //  _eWriter = _vtkWriter.createVertexDataWriter("e", 1);

  //  _nwWriter = _vtkWriter.createVertexDataWriter("nw", 1);
  //  _nWriter  = _vtkWriter.createVertexDataWriter("n", 1);
  //  _neWriter = _vtkWriter.createVertexDataWriter("ne", 1);

  _velocityWriter = _vtkWriter.createVertexDataWriter("velocity", 3);

  logTraceOutWith1Argument("beginIteration(State)", solverState);
}

void
mappings::Plot::
endIteration(State& solverState)
{
  logTraceInWith1Argument("endIteration(State)", solverState);
  // @todo Insert your
  // code here

  std::ostringstream newFilename;

  newFilename << solverState.getIteration()
              << "_"
              << "test.vtk";

  _vertexWriter->close();
  _cellWriter->close();

  _densityWriter->close();

  _velocityWriter->close();

  //  _swWriter->close();
  //  _sWriter->close();
  //  _seWriter->close();

  //  _wWriter->close();
  //  _cWriter->close();
  //  _eWriter->close();

  //  _nwWriter->close();
  //  _nWriter->close();
  //  _neWriter->close();

  delete _vertexWriter;
  delete _cellWriter;
  delete _densityWriter;
  delete _velocityWriter;

  //  delete _swWriter;
  //  delete _sWriter;
  //  delete _seWriter;

  //  delete _wWriter;
  //  delete _cWriter;
  //  delete _eWriter;

  //  delete _nwWriter;
  //  delete _nWriter;
  //  delete _neWriter;

  _vtkWriter.writeToFile(newFilename.str());

  logTraceOutWith1Argument("endIteration(State)", solverState);
}

void
mappings::Plot::
descend(Cell* const                          fineGridCells,
        Vertex* const                        fineGridVertices,
        const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
        Vertex* const                        coarseGridVertices,
        const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
        Cell&                                coarseGridCell)
{
  logTraceInWith2Arguments("descend(...)",
                           coarseGridCell.toString(),
                           coarseGridVerticesEnumerator.toString());
  // @todo Insert your
  // code here
  logTraceOut(
    "descend(...)");
}

void
mappings::Plot::
ascend(Cell* const                          fineGridCells,
       Vertex* const                        fineGridVertices,
       const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
       Vertex* const                        coarseGridVertices,
       const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
       Cell&                                coarseGridCell)
{
  logTraceInWith2Arguments("ascend(...)",
                           coarseGridCell.toString(),
                           coarseGridVerticesEnumerator.toString());
  // @todo Insert your
  // code here
  logTraceOut("ascend(...)");
}
}
