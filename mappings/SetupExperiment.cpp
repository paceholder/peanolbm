#include "latticeboltzmann/mappings/SetupExperiment.h"

#include "tarch/logging/CommandLineLogger.h"

#include "peano/heap/Heap.h"

#include "latticeboltzmann/LBDefinitions.h"

#ifdef Dim2
  #include "latticeboltzmann/heap2D/CellHeap.h"
  #include "latticeboltzmann/heap2D/VertexHeap.h"
#endif

#ifdef Dim3
  #include "latticeboltzmann/heap3D/CellHeap.h"
  #include "latticeboltzmann/heap3D/VertexHeap.h"
#endif

#include "latticeboltzmann/lbmsteps/CellSteps.h"
#include "latticeboltzmann/lbmsteps/VertexSteps.h"

using namespace peano;
using namespace tarch::la;

typedef Vector<DIMENSIONS, int>    DimVectorInt;
typedef Vector<DIMENSIONS, double> DimVectorDouble;

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
MappingSpecification
latticeboltzmann::mappings::SetupExperiment::
touchVertexLastTimeSpecification()
{
  return MappingSpecification(
    MappingSpecification::WholeTree,
    MappingSpecification::
    AvoidFineGridRaces,
    false);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
MappingSpecification
latticeboltzmann::mappings::SetupExperiment::
touchVertexFirstTimeSpecification()
{
  return MappingSpecification(
    MappingSpecification::WholeTree,
    MappingSpecification::
    AvoidFineGridRaces,
    false);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
MappingSpecification
latticeboltzmann::mappings::SetupExperiment::
enterCellSpecification()
{
  return MappingSpecification(
    MappingSpecification::WholeTree,
    MappingSpecification::
    AvoidFineGridRaces,
    false);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
MappingSpecification
latticeboltzmann::mappings::SetupExperiment::
leaveCellSpecification()
{
  return MappingSpecification(
    MappingSpecification::WholeTree,
    MappingSpecification::
    AvoidFineGridRaces,
    false);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
MappingSpecification
latticeboltzmann::mappings::SetupExperiment::
ascendSpecification()
{
  return MappingSpecification(
    MappingSpecification::WholeTree,
    MappingSpecification::
    AvoidFineGridRaces,
    false);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
MappingSpecification
latticeboltzmann::mappings::SetupExperiment::
descendSpecification()
{
  return MappingSpecification(
    MappingSpecification::WholeTree,
    MappingSpecification::
    AvoidFineGridRaces,
    false);
}

tarch::logging::Log latticeboltzmann::mappings::
SetupExperiment::
_log("latticeboltzmann::mappings::SetupExperiment");

latticeboltzmann::mappings::SetupExperiment::
SetupExperiment():
  _density(1.0),
  _v(INIT_VEL_X, INIT_VEL_Y),
  _velocity(_v)
{
  logTraceIn("SetupExperiment()");
  // @todo Insert your code here
  logTraceOut("SetupExperiment()");
}

latticeboltzmann::mappings::SetupExperiment::~SetupExperiment()
{
  logTraceIn("~SetupExperiment()");
  // @todo Insert your code here
  logTraceOut("~SetupExperiment()");
}

#if defined (SharedMemoryParallelisation)
latticeboltzmann::mappings::SetupExperiment::
SetupExperiment(const SetupExperiment& masterThread)
{
  logTraceIn("SetupExperiment(SetupExperiment)");
  // @todo Insert your code here
  logTraceOut("SetupExperiment(SetupExperiment)");
}

void
latticeboltzmann::mappings::SetupExperiment::
mergeWithWorkerThread(const SetupExperiment& workerThread)
{
  logTraceIn("mergeWithWorkerThread(SetupExperiment)");
  // @todo Insert your code here
  logTraceOut("mergeWithWorkerThread(SetupExperiment)");
}
#endif

// -------------------------------------------------------------------------------------

void
latticeboltzmann::mappings::SetupExperiment::
createHangingVertex(latticeboltzmann::Vertex&       fineGridVertex,
                    const DimVectorDouble&          fineGridX,
                    const DimVectorDouble&          fineGridH,
                    latticeboltzmann::Vertex* const coarseGridVertices,
                    const  grid::VertexEnumerator&
                    coarseGridVerticesEnumerator,
                    latticeboltzmann::Cell&         coarseGridCell,
                    const DimVectorInt&             fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("createHangingVertex(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);

  // we do not proecess vertices which are outside of the computational domain
  if (fineGridX(0) < -0.0001 || fineGridX(0) > 1.0001 ||
      fineGridX(1) < -0.0001 || fineGridX(1) > 1.0001)
    return;

  assert(fineGridX(0) > -0.0001 &&  fineGridX(0) < 1.0001);
  assert(fineGridX(1) > -0.0001 &&  fineGridX(1) < 1.0001);

  int level = coarseGridVerticesEnumerator.getLevel() + 1;

  // function creates a vertex "index" and adds it to the map:  coordinates -> index
  // for later use
  int vertexIndex = latticeboltzmann::Vertex::getVertexIndex(fineGridX, level);
  fineGridVertex.setAggregateWithArbitraryCardinality(vertexIndex);

  // set initial values here
  std::vector<latticeboltzmann::VertexHeap>& vertexData =
    peano::heap::Heap<latticeboltzmann::VertexHeap>::getInstance().getData(vertexIndex);

#ifdef Dim2
  Vector<DIMENSIONS, double> v(INIT_VEL_X, INIT_VEL_Y);

#endif   //  Dim2
#ifdef Dim3
  Vector<DIMENSIONS, double> v(INIT_VEL_X, INIT_VEL_Y, INIT_VEL_Z);

#endif   //  Dim2

  VertexSteps::initializeVertexArray(vertexData, v);

  fineGridVertex.setComputed(false);

  logTraceOutWith1Argument("createHangingVertex(...)", fineGridVertex);
}

void
latticeboltzmann::mappings::SetupExperiment::
destroyHangingVertex(
  const latticeboltzmann::Vertex& fineGridVertex,
  const DimVectorDouble&          fineGridX,
  const DimVectorDouble&          fineGridH,
  latticeboltzmann::Vertex* const coarseGridVertices,
  const  grid::VertexEnumerator&  coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&         coarseGridCell,
  const DimVectorInt&             fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("destroyHangingVertex(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("destroyHangingVertex(...)", fineGridVertex);
}

// -------------------------------------------------------------------------------------

void
refinementCriteria(latticeboltzmann::Vertex&      fineGridVertex,
                   const DimVectorDouble&         fineGridX,
                   const  grid::VertexEnumerator& coarseGridVerticesEnumerator)
{
  int level   = coarseGridVerticesEnumerator.getLevel() + 1;
  int refines = 0;

  if (coarseGridVerticesEnumerator.getLevel() >= COARSE_REFINEMENT_LEVEL - 1 &&
      coarseGridVerticesEnumerator.getLevel() < FINE_REFINEMENT_LEVEL - 1) {
    assert(level < FINE_REFINEMENT_LEVEL);

    // if (level < FINE_REFINEMENT_LEVEL - 1)
    // if (fineGridX(1) > 0.34) {
    // fineGridVertex.refine();
    // ++refines;
    // }

    if (fineGridX(1) < 0.3 && (fineGridX(0) < 0.3 || fineGridX(0) > 0.67)) {
      fineGridVertex.refine();
      ++refines;
    }

    //    if (fineGridX(1) > 0.89 && coarseGridVerticesEnumerator.getLevel() < FINE_REFINEMENT_LEVEL - 1)
    //      fineGridVertex.refine();

    // refine until coarse level
  } else if (coarseGridVerticesEnumerator.getLevel() < COARSE_REFINEMENT_LEVEL - 1) {
    assert(level < FINE_REFINEMENT_LEVEL);
    fineGridVertex.refine();
    ++refines;
  }

  // assert(refines < 2);
}

void
vertexInitialization(latticeboltzmann::Vertex& fineGridVertex,
                     int                       level)
{
  int b = peano::heap::Heap<latticeboltzmann::VertexHeap>::getInstance().createData(2);
  fineGridVertex.setAggregateWithArbitraryCardinality(b);
  std::vector<latticeboltzmann::VertexHeap>& vertexData =
    peano::heap::Heap<latticeboltzmann::VertexHeap>::getInstance().getData(b);

#ifdef Dim2
  double y = INIT_VEL_Y;
  //  if (level = COARSE_REFINEMENT_LEVEL)
  //    y = 0.01;
  Vector<DIMENSIONS, double> v(INIT_VEL_X, y);

#endif   //  Dim2
#ifdef Dim3
  Vector<DIMENSIONS, double> v(INIT_VEL_X, INIT_VEL_Y, INIT_VEL_Z);

#endif   //  Dim2

  latticeboltzmann::VertexSteps::initializeVertexArray(vertexData, v);

  fineGridVertex.setComputed(false);
}

void
latticeboltzmann::mappings::SetupExperiment::
createInnerVertex(latticeboltzmann::Vertex&       fineGridVertex,
                  const DimVectorDouble&          fineGridX,
                  const DimVectorDouble&          fineGridH,
                  latticeboltzmann::Vertex* const coarseGridVertices,
                  const  grid::VertexEnumerator&  coarseGridVerticesEnumerator,
                  latticeboltzmann::Cell&         coarseGridCell,
                  const DimVectorInt&             fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("createInnerVertex(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);

  refinementCriteria(fineGridVertex, fineGridX, coarseGridVerticesEnumerator);

  int level =  coarseGridVerticesEnumerator.getLevel() + 1;

  vertexInitialization(fineGridVertex, level);

  logTraceOutWith1Argument("createInnerVertex(...)", fineGridVertex);
}

void
latticeboltzmann::mappings::SetupExperiment::
createBoundaryVertex(latticeboltzmann::Vertex&       fineGridVertex,
                     const DimVectorDouble&          fineGridX,
                     const DimVectorDouble&          fineGridH,
                     latticeboltzmann::Vertex* const coarseGridVertices,
                     const  grid::VertexEnumerator&
                     coarseGridVerticesEnumerator,
                     latticeboltzmann::Cell&         coarseGridCell,
                     const DimVectorInt&             fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("createBoundaryVertex(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);

  refinementCriteria(fineGridVertex, fineGridX, coarseGridVerticesEnumerator);

  int level = coarseGridVerticesEnumerator.getLevel() + 1;
  vertexInitialization(fineGridVertex, level);

  logTraceOutWith1Argument("createBoundaryVertex(...)", fineGridVertex);
}

void
latticeboltzmann::mappings::SetupExperiment::
destroyVertex(const latticeboltzmann::Vertex& fineGridVertex,
              const DimVectorDouble&          fineGridX,
              const DimVectorDouble&          fineGridH,
              latticeboltzmann::Vertex* const coarseGridVertices,
              const  grid::VertexEnumerator&  coarseGridVerticesEnumerator,
              latticeboltzmann::Cell&         coarseGridCell,
              const DimVectorInt&             fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("destroyVertex(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("destroyVertex(...)", fineGridVertex);
}

void
latticeboltzmann::mappings::SetupExperiment::
createCell(latticeboltzmann::Cell&         fineGridCell,
           latticeboltzmann::Vertex* const fineGridVertices,
           const  grid::VertexEnumerator&  fineGridVerticesEnumerator,
           latticeboltzmann::Vertex* const coarseGridVertices,
           const  grid::VertexEnumerator&  coarseGridVerticesEnumerator,
           latticeboltzmann::Cell&         coarseGridCell,
           const DimVectorInt&             fineGridPositionOfCell)
{
  logTraceInWith4Arguments("createCell(...)",
                           fineGridCell,
                           fineGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfCell);

  int b = peano::heap::Heap<latticeboltzmann::CellHeap>::getInstance().createData(1);

  fineGridCell.setAggregateWithArbitraryCardinality(b);

  logTraceOutWith1Argument("createCell(...)", fineGridCell);
}

void
latticeboltzmann::mappings::SetupExperiment::
destroyCell(
  const latticeboltzmann::Cell&   fineGridCell,
  latticeboltzmann::Vertex* const fineGridVertices,
  const  grid::VertexEnumerator&  fineGridVerticesEnumerator,
  latticeboltzmann::Vertex* const coarseGridVertices,
  const  grid::VertexEnumerator&  coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&         coarseGridCell,
  const DimVectorInt&             fineGridPositionOfCell)
{
  logTraceInWith4Arguments("destroyCell(...)",
                           fineGridCell,
                           fineGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfCell);
  logTraceOutWith1Argument("destroyCell(...)", fineGridCell);
}

#ifdef Parallel
void
latticeboltzmann::mappings::SetupExperiment::
mergeWithNeighbour(latticeboltzmann::Vertex&       vertex,
                   const latticeboltzmann::Vertex& neighbour,
                   int                             fromRank,
                   const DimVectorDouble&          fineGridX,
                   const DimVectorDouble&          fineGridH,
                   int                             level)
{
  logTraceInWith6Arguments("mergeWithNeighbour(...)",
                           vertex,
                           neighbour,
                           fromRank,
                           fineGridX,
                           fineGridH,
                           level);
  // @todo Insert your code here
  logTraceOut("mergeWithNeighbour(...)");
}

void
latticeboltzmann::mappings::SetupExperiment::
prepareSendToNeighbour(latticeboltzmann::Vertex& vertex,
                       int                       toRank,
                       const DimVectorDouble&    x,
                       const DimVectorDouble&    h,
                       int                       level)
{
  logTraceInWith3Arguments("prepareSendToNeighbour(...)",
                           vertex,
                           toRank,
                           level);
  // @todo Insert your code here
  logTraceOut("prepareSendToNeighbour(...)");
}

void
latticeboltzmann::mappings::SetupExperiment::
prepareCopyToRemoteNode(latticeboltzmann::Vertex& localVertex,
                        int                       toRank,
                        const DimVectorDouble&    x,
                        const DimVectorDouble&    h,
                        int                       level)
{
  logTraceInWith5Arguments("prepareCopyToRemoteNode(...)",
                           localVertex,
                           toRank,
                           x,
                           h,
                           level);
  // @todo Insert your code here
  logTraceOut("prepareCopyToRemoteNode(...)");
}

void
latticeboltzmann::mappings::SetupExperiment::
prepareCopyToRemoteNode(latticeboltzmann::Cell& localCell,
                        int                     toRank,
                        const DimVectorDouble&  cellCentre,
                        const DimVectorDouble&  cellSize,
                        int                     level)
{
  logTraceInWith5Arguments("prepareCopyToRemoteNode(...)",
                           localCell,
                           toRank,
                           cellCentre,
                           cellSize,
                           level);
  // @todo Insert your code here
  logTraceOut("prepareCopyToRemoteNode(...)");
}

void
latticeboltzmann::mappings::SetupExperiment::
mergeWithRemoteDataDueToForkOrJoin(latticeboltzmann::Vertex& localVertex,
                                   const latticeboltzmann::Vertex&
                                   masterOrWorkerVertex,
                                   int                       fromRank,
                                   const DimVectorDouble&    x,
                                   const DimVectorDouble&    h,
                                   int                       level)
{
  logTraceInWith6Arguments("mergeWithRemoteDataDueToForkOrJoin(...)",
                           localVertex,
                           masterOrWorkerVertex,
                           fromRank,
                           x,
                           h,
                           level);
  // @todo Insert your code here
  logTraceOut("mergeWithRemoteDataDueToForkOrJoin(...)");
}

void
latticeboltzmann::mappings::SetupExperiment::
mergeWithRemoteDataDueToForkOrJoin(latticeboltzmann::Cell& localCell,
                                   cohttp : //
                                            //
                                            //
                                            // www.rian.ru/images/55399/45/553994596.jpgnst
                                            // latticeboltzmann::Cell&
                                   masterOrWorkerCell, int fromRank,
                                   const DimVectorDouble&  cellCentre,
                                   const DimVectorDouble&  cellSize,
                                   int                     level)
{
  logTraceInWith3Arguments("mergeWithRemoteDataDueToForkOrJoin(...)",
                           localCell,
                           masterOrWorkerCell,
                           fromRank);
  // @todo Insert your code here
  logTraceOut("mergeWithRemoteDataDueToForkOrJoin(...)");
}

void
latticeboltzmann::mappings::SetupExperiment::
prepareSendToWorker(latticeboltzmann::Cell&         fineGridCell,
                    latticeboltzmann::Vertex* const fineGridVertices,
                    const  grid::VertexEnumerator&  fineGridVerticesEnumerator,
                    latticeboltzmann::Vertex* const coarseGridVertices,
                    const  grid::VertexEnumerator&
                    coarseGridVerticesEnumerator,
                    latticeboltzmann::Cell&         coarseGridCell,
                    const DimVectorInt&             fineGridPositionOfCell,
                    int                             worker)
{
  logTraceIn("prepareSendToWorker(...)");
  // @todo Insert your code here
  logTraceOut("prepareSendToWorker(...)");
}

void
latticeboltzmann::mappings::SetupExperiment::
prepareSendToMaster(
  latticeboltzmann::Cell&               localCell,
  latticeboltzmann::Vertex*             vertices,
  const  grid::VertexEnumerator&        verticesEnumerator,
  const latticeboltzmann::Vertex* const coarseGridVertices,
  const  grid::VertexEnumerator&        coarseGridVerticesEnumerator,
  const latticeboltzmann::Cell&         coarseGridCell,
  const DimVectorInt&                   fineGridPositionOfCell)
{
  logTraceInWith2Arguments("prepareSendToMaster(...)",
                           localCell,
                           verticesEnumerator.toString());
  // @todo Insert your code here
  logTraceOut("prepareSendToMaster(...)");
}

void
latticeboltzmann::mappings::SetupExperiment::
mergeWithMaster(
  const latticeboltzmann::Cell&   workerGridCell,
  latticeboltzmann::Vertex* const workerGridVertices,
  const  grid::VertexEnumerator&  workerEnumerator,
  latticeboltzmann::Cell&         fineGridCell,
  latticeboltzmann::Vertex* const fineGridVertices,
  const  grid::VertexEnumerator&  fineGridVerticesEnumerator,
  latticeboltzmann::Vertex* const coarseGridVertices,
  const  grid::VertexEnumerator&  coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&         coarseGridCell,
  const DimVectorInt&             fineGridPositionOfCell,
  int                             worker,
  const latticeboltzmann::State&  workerState,
  latticeboltzmann::State&        masterState)
{
  logTraceIn("mergeWithMaster(...)");
  // @todo Insert your code here
  logTraceOut("mergeWithMaster(...)");
}

void
latticeboltzmann::mappings::SetupExperiment::
receiveDataFromMaster(
  latticeboltzmann::Cell&         receivedCell,
  latticeboltzmann::Vertex*       receivedVertices,
  const  grid::VertexEnumerator&  receivedVerticesEnumerator,
  latticeboltzmann::Vertex* const receivedCoarseGridVertices,
  const  grid::VertexEnumerator&  receivedCoarseGridVerticesEnumerator,
  latticeboltzmann::Cell&         receivedCoarseGridCell,
  latticeboltzmann::Vertex* const workersCoarseGridVertices,
  const  grid::VertexEnumerator&  workersCoarseGridVerticesEnumerator,
  latticeboltzmann::Cell&         workersCoarseGridCell,
  const DimVectorInt&             fineGridPositionOfCell)
{
  logTraceIn("receiveDataFromMaster(...)");
  // @todo Insert your code here
  logTraceOut("receiveDataFromMaster(...)");
}

void
latticeboltzmann::mappings::SetupExperiment::
mergeWithWorker(
  latticeboltzmann::Cell&       localCell,
  const latticeboltzmann::Cell& receivedMasterCell,
  const DimVectorDouble&        cellCentre,
  const DimVectorDouble&        cellSize,
  int                           level)
{
  logTraceInWith2Arguments("mergeWithWorker(...)",
                           localCell.toString(), receivedMasterCell.toString());
  // @todo Insert your code here
  logTraceOutWith1Argument("mergeWithWorker(...)", localCell.toString());
}

void
latticeboltzmann::mappings::SetupExperiment::
mergeWithWorker(
  latticeboltzmann::Vertex&       localVertex,
  const latticeboltzmann::Vertex& receivedMasterVertex,
  const DimVectorDouble&          x,
  const DimVectorDouble&          h,
  int                             level)
{
  logTraceInWith2Arguments("mergeWithWorker(...)",
                           localVertex.toString(),
                           receivedMasterVertex.toString());
  // @todo Insert your code here
  logTraceOutWith1Argument("mergeWithWorker(...)", localVertex.toString());
}
#endif

void
latticeboltzmann::mappings::SetupExperiment::
touchVertexFirstTime(latticeboltzmann::Vertex&       fineGridVertex,
                     const DimVectorDouble&          fineGridX,
                     const DimVectorDouble&          fineGridH,
                     latticeboltzmann::Vertex* const coarseGridVertices,
                     const  grid::VertexEnumerator&
                     coarseGridVerticesEnumerator,
                     latticeboltzmann::Cell&         coarseGridCell,
                     const DimVectorInt&             fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("touchVertexFirstTime(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("touchVertexFirstTime(...)", fineGridVertex);
}

void
latticeboltzmann::mappings::SetupExperiment::
touchVertexLastTime(
  latticeboltzmann::Vertex&       fineGridVertex,
  const DimVectorDouble&          fineGridX,
  const DimVectorDouble&          fineGridH,
  latticeboltzmann::Vertex* const coarseGridVertices,
  const  grid::VertexEnumerator&  coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&         coarseGridCell,
  const Vector<DIMENSIONS,
               int>&
  fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("touchVertexLastTime(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("touchVertexLastTime(...)", fineGridVertex);
}

// -----------------------------------------------------------------------------------------------------------

void
latticeboltzmann::mappings::SetupExperiment::
enterCell(latticeboltzmann::Cell&         fineGridCell,
          latticeboltzmann::Vertex* const fineGridVertices,
          const  grid::VertexEnumerator&  fineGridVerticesEnumerator,
          latticeboltzmann::Vertex* const coarseGridVertices,
          const  grid::VertexEnumerator&  coarseGridVerticesEnumerator,
          latticeboltzmann::Cell&         coarseGridCell,
          const DimVectorInt&             fineGridPositionOfCell)
{
  logTraceInWith4Arguments("enterCell(...)",
                           fineGridCell,
                           fineGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfCell);

  if (fineGridVerticesEnumerator.getLevel() >= COARSE_REFINEMENT_LEVEL) {
    CellSteps::computeFeq(_density, _velocity, _feq);

    std::vector<latticeboltzmann::CellHeap>& cellData =
      peano::heap::Heap<latticeboltzmann::CellHeap>::getInstance().getData(
        fineGridCell.getAggregateWithArbitraryCardinality());

    latticeboltzmann::CellHeap& array = cellData[0];

    for (int i = 0; i < LB_DIR; ++i)
      array.getDir(i) = _feq[i];
  }

  logTraceOutWith1Argument("enterCell(...)",
                           fineGridCell);
}

void
latticeboltzmann::mappings::SetupExperiment::
leaveCell(latticeboltzmann::Cell&         fineGridCell,
          latticeboltzmann::Vertex* const fineGridVertices,
          const  grid::VertexEnumerator&  fineGridVerticesEnumerator,
          latticeboltzmann::Vertex* const coarseGridVertices,
          const  grid::VertexEnumerator&  coarseGridVerticesEnumerator,
          latticeboltzmann::Cell&         coarseGridCell,
          const DimVectorInt&
          fineGridPositionOfCell)
{
  logTraceInWith4Arguments("leaveCell(...)",
                           fineGridCell,
                           fineGridVerticesEnumerator
                           .toString(),
                           coarseGridCell,
                           fineGridPositionOfCell);
  // @todo Insert your code here
  logTraceOutWith1Argument("leaveCell(...)",
                           fineGridCell);
}

void
latticeboltzmann::mappings::SetupExperiment::
beginIteration(latticeboltzmann::State& solverState)
{
  logTraceInWith1Argument("beginIteration(State)", solverState);

  printf("Setup Experiment\n");

  logTraceOutWith1Argument("beginIteration(State)", solverState);
}

void
latticeboltzmann::mappings::SetupExperiment::
endIteration(
  latticeboltzmann::State& solverState)
{
  logTraceInWith1Argument("endIteration(State)",
                          solverState);
  // @todo Insert your code here
  logTraceOutWith1Argument(
    "endIteration(State)", solverState);
}

void
latticeboltzmann::mappings::SetupExperiment::
descend(latticeboltzmann::Cell* const   fineGridCells,
        latticeboltzmann::Vertex* const fineGridVertices,
        const  grid::VertexEnumerator&  fineGridVerticesEnumerator,
        latticeboltzmann::Vertex* const coarseGridVertices,
        const  grid::VertexEnumerator&  coarseGridVerticesEnumerator,
        latticeboltzmann::Cell&         coarseGridCell)
{
  logTraceInWith2Arguments("descend(...)",
                           coarseGridCell.
                           toString(),
                           coarseGridVerticesEnumerator
                           .toString());
  // @todo Insert your code here
  logTraceOut("descend(...)");
}

void
latticeboltzmann::mappings::SetupExperiment::
ascend(latticeboltzmann::Cell* const   fineGridCells,
       latticeboltzmann::Vertex* const fineGridVertices,
       const  grid::VertexEnumerator&  fineGridVerticesEnumerator,
       latticeboltzmann::Vertex* const coarseGridVertices,
       const  grid::VertexEnumerator&  coarseGridVerticesEnumerator,
       latticeboltzmann::Cell&         coarseGridCell)
{
  logTraceInWith2Arguments("ascend(...)",
                           coarseGridCell.
                           toString(),
                           coarseGridVerticesEnumerator
                           .toString());
  // @todo Insert your code here
  logTraceOut("ascend(...)");
}
