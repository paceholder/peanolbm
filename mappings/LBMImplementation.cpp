#include "latticeboltzmann/mappings/LBMImplementation.h"

#include "latticeboltzmann/LBDefinitions.h"

#ifdef ISPC_BUILD
  #include "latticeboltzmann/ispc_sources/VertexSteps_ispc.h"
#endif

#ifdef Dim2
  #include "latticeboltzmann/heap2D/CellHeap.h"
  #include "latticeboltzmann/heap2D/VertexHeap.h"
#endif

#ifdef Dim3
  #include "latticeboltzmann/heap3D/CellHeap.h"
  #include "latticeboltzmann/heap3D/VertexHeap.h"
#endif

#include "latticeboltzmann/State.h"
#include "peano/heap/Heap.h"

#include "latticeboltzmann/lbmsteps/CellSteps.h"
#include "latticeboltzmann/lbmsteps/Interpolation.h"
#include "latticeboltzmann/lbmsteps/PointSteps.h"
#include "latticeboltzmann/lbmsteps/VertexSteps.h"

#include <tuple>

namespace latticeboltzmann {
namespace mappings {
/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
LBMImplementation::
touchVertexLastTimeSpecification()
{
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::AvoidFineGridRaces,
                                     false);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
LBMImplementation::
touchVertexFirstTimeSpecification()
{
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::AvoidFineGridRaces,
                                     false);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
LBMImplementation::
enterCellSpecification()
{
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::AvoidFineGridRaces,
                                     false);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
LBMImplementation::
leaveCellSpecification()
{
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::AvoidFineGridRaces,
                                     false);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
LBMImplementation::
ascendSpecification()
{
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::AvoidFineGridRaces,
                                     false);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
LBMImplementation::
descendSpecification()
{
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::AvoidFineGridRaces,
                                     false);
}

tarch::logging::Log LBMImplementation::
_log("LBMImplementation");

LBMImplementation::
LBMImplementation()
{
  logTraceIn("LBMImplementation()");
  // @todo Insert your code here
  logTraceOut("LBMImplementation()");
}

LBMImplementation::~LBMImplementation()
{
  logTraceIn("~LBMImplementation()");
  // @todo Insert your code here
  logTraceOut("~LBMImplementation()");
}

#if defined (SharedMemoryParallelisation)
LBMImplementation::
LBMImplementation(const LBMImplementation& masterThread)
{
  logTraceIn("LBMImplementation(LBMImplementation)");
  // @todo Insert your code here
  logTraceOut("LBMImplementation(LBMImplementation)");
}

void
LBMImplementation::
mergeWithWorkerThread(const LBMImplementation& workerThread)
{
  logTraceIn("mergeWithWorkerThread(LBMImplementation)");
  // @todo Insert your code here
  logTraceOut("mergeWithWorkerThread(LBMImplementation)");
}
#endif

void
LBMImplementation::
createHangingVertex(
  latticeboltzmann::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                fineGridH,
  latticeboltzmann::Vertex* const   coarseGridVertices,
  const peano::grid::VertexEnumerator&      coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&       coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&                   fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("createHangingVertex(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);

  int level = coarseGridVerticesEnumerator.getLevel() + 1;

  int newIndex = Vertex::getVertexIndex(fineGridX, level);

  // extract data structure from the heap
  fineGridVertex.setAggregateWithArbitraryCardinality(newIndex);

  if (level >= COARSE_REFINEMENT_LEVEL &&
      State::isIterationWithComputing(level))

    // if we have not processed this hanging vertex on current iteration

    if (Vertex::processedHangingVertices.find(fineGridX) == Vertex::processedHangingVertices.end() ||
        Vertex::processedHangingVertices[fineGridX] != State::getIteration()) {
      VertexSteps::computeVertex(fineGridVertex,
                                 fineGridX,
                                 fineGridH,
                                 coarseGridVertices,
                                 coarseGridVerticesEnumerator,
                                 coarseGridCell,
                                 fineGridPositionOfVertex);
      // remember that this vertex is processed on current interation
      Vertex::processedHangingVertices[fineGridX] = State::getIteration();
    }

  logTraceOutWith1Argument("createHangingVertex(...)", fineGridVertex);
}

void
LBMImplementation::
destroyHangingVertex(
  const latticeboltzmann::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridH,
  latticeboltzmann::Vertex* const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&           coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&                       fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("destroyHangingVertex(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("destroyHangingVertex(...)", fineGridVertex);
}

void
LBMImplementation::
createInnerVertex(
  latticeboltzmann::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                          fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                          fineGridH,
  latticeboltzmann::Vertex* const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&                             fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("createInnerVertex(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("createInnerVertex(...)", fineGridVertex);
}

void
LBMImplementation::
createBoundaryVertex(
  latticeboltzmann::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                          fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                          fineGridH,
  latticeboltzmann::Vertex* const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&                             fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("createBoundaryVertex(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("createBoundaryVertex(...)", fineGridVertex);
}

void
LBMImplementation::
destroyVertex(
  const latticeboltzmann::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridH,
  latticeboltzmann::Vertex* const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&           coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&                       fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("destroyVertex(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("destroyVertex(...)", fineGridVertex);
}

void
LBMImplementation::
createCell(
  latticeboltzmann::Cell& fineGridCell,
  latticeboltzmann::Vertex* const fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  latticeboltzmann::Vertex* const coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  latticeboltzmann::Cell& coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&                             fineGridPositionOfCell)
{
  logTraceInWith4Arguments("createCell(...)", fineGridCell,
                           fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell);
  // @todo Insert your code here
  logTraceOutWith1Argument("createCell(...)", fineGridCell);
}

void
LBMImplementation::
destroyCell(
  const latticeboltzmann::Cell& fineGridCell,
  latticeboltzmann::Vertex* const fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  latticeboltzmann::Vertex* const coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  latticeboltzmann::Cell& coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&                             fineGridPositionOfCell)
{
  logTraceInWith4Arguments("destroyCell(...)", fineGridCell,
                           fineGridVerticesEnumerator.toString(), coarseGridCell, fineGridPositionOfCell);
  // @todo Insert your code here
  logTraceOutWith1Argument("destroyCell(...)", fineGridCell);
}

#ifdef Parallel
void
LBMImplementation::
mergeWithNeighbour(
  latticeboltzmann::Vertex& vertex,
  const latticeboltzmann::Vertex& neighbour,
  int fromRank,
  const tarch::la::Vector<DIMENSIONS, double>&   fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&   fineGridH,
  int                                           level)
{
  logTraceInWith6Arguments("mergeWithNeighbour(...)", vertex, neighbour, fromRank, fineGridX, fineGridH, level);
  // @todo Insert your code here
  logTraceOut("mergeWithNeighbour(...)");
}

void
LBMImplementation::
prepareSendToNeighbour(
  latticeboltzmann::Vertex& vertex,
  int toRank,
  const tarch::la::Vector<DIMENSIONS, double>&   x,
  const tarch::la::Vector<DIMENSIONS, double>&   h,
  int                                           level)
{
  logTraceInWith3Arguments("prepareSendToNeighbour(...)", vertex, toRank, level);
  // @todo Insert your code here
  logTraceOut("prepareSendToNeighbour(...)");
}

void
LBMImplementation::
prepareCopyToRemoteNode(
  latticeboltzmann::Vertex& localVertex,
  int toRank,
  const tarch::la::Vector<DIMENSIONS, double>&   x,
  const tarch::la::Vector<DIMENSIONS, double>&   h,
  int                                           level)
{
  logTraceInWith5Arguments("prepareCopyToRemoteNode(...)", localVertex, toRank, x, h, level);
  // @todo Insert your code here
  logTraceOut("prepareCopyToRemoteNode(...)");
}

void
LBMImplementation::
prepareCopyToRemoteNode(
  latticeboltzmann::Cell& localCell,
  int toRank,
  const tarch::la::Vector<DIMENSIONS, double>&   cellCentre,
  const tarch::la::Vector<DIMENSIONS, double>&   cellSize,
  int                                           level)
{
  logTraceInWith5Arguments("prepareCopyToRemoteNode(...)", localCell, toRank, cellCentre, cellSize, level);
  // @todo Insert your code here
  logTraceOut("prepareCopyToRemoteNode(...)");
}

void
LBMImplementation::
mergeWithRemoteDataDueToForkOrJoin(
  latticeboltzmann::Vertex& localVertex,
  const latticeboltzmann::Vertex& masterOrWorkerVertex,
  int fromRank,
  const tarch::la::Vector<DIMENSIONS, double>&  x,
  const tarch::la::Vector<DIMENSIONS, double>&  h,
  int                                       level)
{
  logTraceInWith6Arguments("mergeWithRemoteDataDueToForkOrJoin(...)",
                           localVertex,
                           masterOrWorkerVertex,
                           fromRank,
                           x,
                           h,
                           level);
  // @todo Insert your code here
  logTraceOut("mergeWithRemoteDataDueToForkOrJoin(...)");
}

void
LBMImplementation::
mergeWithRemoteDataDueToForkOrJoin(
  latticeboltzmann::Cell& localCell,
  const latticeboltzmann::Cell& masterOrWorkerCell,
  int fromRank,
  const tarch::la::Vector<DIMENSIONS, double>&  cellCentre,
  const tarch::la::Vector<DIMENSIONS, double>&  cellSize,
  int                                       level)
{
  logTraceInWith3Arguments("mergeWithRemoteDataDueToForkOrJoin(...)", localCell, masterOrWorkerCell, fromRank);
  // @todo Insert your code here
  logTraceOut("mergeWithRemoteDataDueToForkOrJoin(...)");
}

void
LBMImplementation::
prepareSendToWorker(
  latticeboltzmann::Cell& fineGridCell,
  latticeboltzmann::Vertex* const fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  latticeboltzmann::Vertex* const coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  latticeboltzmann::Cell& coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&                             fineGridPositionOfCell,
  int                                                                  worker)
{
  logTraceIn("prepareSendToWorker(...)");
  // @todo Insert your code here
  logTraceOut("prepareSendToWorker(...)");
}

void
LBMImplementation::
prepareSendToMaster(
  latticeboltzmann::Cell& localCell,
  latticeboltzmann::Vertex* vertices,
  const peano::grid::VertexEnumerator& verticesEnumerator,
  const latticeboltzmann::Vertex* const coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  const latticeboltzmann::Cell& coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&   fineGridPositionOfCell)
{
  logTraceInWith2Arguments("prepareSendToMaster(...)", localCell, verticesEnumerator.toString());
  // @todo Insert your code here
  logTraceOut("prepareSendToMaster(...)");
}

void
LBMImplementation::
mergeWithMaster(
  const latticeboltzmann::Cell& workerGridCell,
  latticeboltzmann::Vertex* const workerGridVertices,
  const peano::grid::VertexEnumerator& workerEnumerator,
  latticeboltzmann::Cell& fineGridCell,
  latticeboltzmann::Vertex* const fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  latticeboltzmann::Vertex* const coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  latticeboltzmann::Cell& coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&                             fineGridPositionOfCell,
  int                                                                  worker,
  const latticeboltzmann::State&          workerState,
  latticeboltzmann::State&                masterState)
{
  logTraceIn("mergeWithMaster(...)");
  // @todo Insert your code here
  logTraceOut("mergeWithMaster(...)");
}

void
LBMImplementation::
receiveDataFromMaster(
  latticeboltzmann::Cell& receivedCell,
  latticeboltzmann::Vertex* receivedVertices,
  const peano::grid::VertexEnumerator& receivedVerticesEnumerator,
  latticeboltzmann::Vertex* const receivedCoarseGridVertices,
  const peano::grid::VertexEnumerator& receivedCoarseGridVerticesEnumerator,
  latticeboltzmann::Cell& receivedCoarseGridCell,
  latticeboltzmann::Vertex* const workersCoarseGridVertices,
  const peano::grid::VertexEnumerator& workersCoarseGridVerticesEnumerator,
  latticeboltzmann::Cell& workersCoarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&    fineGridPositionOfCell)
{
  logTraceIn("receiveDataFromMaster(...)");
  // @todo Insert your code here
  logTraceOut("receiveDataFromMaster(...)");
}

void
LBMImplementation::
mergeWithWorker(
  latticeboltzmann::Cell& localCell,
  const latticeboltzmann::Cell& receivedMasterCell,
  const tarch::la::Vector<DIMENSIONS, double>&  cellCentre,
  const tarch::la::Vector<DIMENSIONS, double>&  cellSize,
  int                                          level)
{
  logTraceInWith2Arguments("mergeWithWorker(...)", localCell.toString(), receivedMasterCell.toString());
  // @todo Insert your code here
  logTraceOutWith1Argument("mergeWithWorker(...)", localCell.toString());
}

void
LBMImplementation::
mergeWithWorker(
  latticeboltzmann::Vertex& localVertex,
  const latticeboltzmann::Vertex& receivedMasterVertex,
  const tarch::la::Vector<DIMENSIONS, double>&   x,
  const tarch::la::Vector<DIMENSIONS, double>&   h,
  int                                           level)
{
  logTraceInWith2Arguments("mergeWithWorker(...)", localVertex.toString(), receivedMasterVertex.toString());
  // @todo Insert your code here
  logTraceOutWith1Argument("mergeWithWorker(...)", localVertex.toString());
}
#endif

void
LBMImplementation::
touchVertexFirstTime(latticeboltzmann::Vertex& fineGridVertex,
                     const tarch::la::Vector<DIMENSIONS, double>&      fineGridX,
                     const tarch::la::Vector<DIMENSIONS, double>&      fineGridH,
                     latticeboltzmann::Vertex* const        coarseGridVertices,
                     const peano::grid::VertexEnumerator&  coarseGridVerticesEnumerator,
                     latticeboltzmann::Cell&                 coarseGridCell,
                     const tarch::la::Vector<DIMENSIONS, int>& fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("touchVertexFirstTime(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);

  int level = coarseGridVerticesEnumerator.getLevel() + 1;

  if (level >= COARSE_REFINEMENT_LEVEL &&
      State::isIterationWithComputing(level))

    VertexSteps::computeVertex(fineGridVertex,
                               fineGridX,
                               fineGridH,
                               coarseGridVertices,
                               coarseGridVerticesEnumerator,
                               coarseGridCell,
                               fineGridPositionOfVertex);

  logTraceOutWith1Argument("touchVertexFirstTime(...)", fineGridVertex);
}

void
LBMImplementation::
touchVertexLastTime(
  latticeboltzmann::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridH,
  latticeboltzmann::Vertex* const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&           coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&                       fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("touchVertexLastTime(...)",
                           fineGridVertex,
                           fineGridX,
                           fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);

  fineGridVertex.setComputed(false);

  // @todo Insert your code here
  logTraceOutWith1Argument("touchVertexLastTime(...)", fineGridVertex);
}

void
LBMImplementation::
enterCell(latticeboltzmann::Cell& fineGridCell,
          latticeboltzmann::Vertex* const fineGridVertices,
          const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
          latticeboltzmann::Vertex* const coarseGridVertices,
          const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
          latticeboltzmann::Cell& coarseGridCell,
          const tarch::la::Vector<DIMENSIONS, int>& fineGridPositionOfCell)
{
  logTraceInWith4Arguments("enterCell(...)", fineGridCell,
                           fineGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfCell);

  // if cell is refined then we need to compute here just in case this cell overlaps with finer level
  if (fineGridCell.isRefined() && !CellSteps::isOverlappingCell(fineGridVertices,
                                                                fineGridVerticesEnumerator))
    return;

  int level = fineGridVerticesEnumerator.getLevel();

  int vertexArrayIndex = (State::lastArray[level] + 1) % 2;

  if (level >= COARSE_REFINEMENT_LEVEL &&
      level <= FINE_REFINEMENT_LEVEL && State::isIterationWithComputing(level)) {
    std::vector<CellHeap>& cellData =
      peano::heap::Heap<CellHeap>::getInstance().getData(
        fineGridCell.getAggregateWithArbitraryCardinality());

    CellHeap& cell = cellData[0];

    // fill border knots in dirs  ***
    CellSteps::fillCellBoundary(vertexArrayIndex,
                                cell,
                                fineGridVertices,
                                fineGridVerticesEnumerator);

    //    // swap inner part of the cell
    double tau = State::getTau(fineGridVerticesEnumerator.getLevel());
    CellSteps::swapCollisionStreamCell(cell, tau);

#ifdef Dim2
    CellSteps::streamCellTop(cell);

    //    stream everywhere(innner part + boundary)
    CellSteps::streamCellRight(cell);
#endif
#ifdef Dim3
    CellSteps::streamCellTop3D(cell);
    CellSteps::streamCellRight3D(cell);
    CellSteps::streamCellBack3D(cell);
#endif

    //     fill vertex with updated values
    CellSteps::fillVertices(vertexArrayIndex,
                            cell,
                            fineGridVertices,
                            fineGridVerticesEnumerator);
  }

  logTraceOutWith1Argument("enterCell(...)", fineGridCell);
}

void
LBMImplementation::
leaveCell(
  latticeboltzmann::Cell& fineGridCell,
  latticeboltzmann::Vertex* const fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  latticeboltzmann::Vertex* const coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  latticeboltzmann::Cell& coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>& fineGridPositionOfCell)
{
  logTraceInWith4Arguments("leaveCell(...)", fineGridCell,
                           fineGridVerticesEnumerator.toString(), coarseGridCell,
                           fineGridPositionOfCell);
  // @todo Insert your code here
  logTraceOutWith1Argument("leaveCell(...)", fineGridCell);
}

void
LBMImplementation::
beginIteration(
  latticeboltzmann::State& solverState)
{
  logTraceInWith1Argument("beginIteration(State)", solverState);

#ifdef ISPC_BUILD
  float vin[] = { 1, 2, 3, 4 };
  float vout[4];

  // ispc::simple(&vin[0], &vout[0], 4);

  // for (int i = 0; i < 4; ++i)
  // std::cout << i << " " << vout[i] << std::endl;

#endif

  logTraceOutWith1Argument("beginIteration(State)", solverState);
}

void
LBMImplementation::
endIteration(
  latticeboltzmann::State& solverState)
{
  logTraceInWith1Argument("endIteration(State)", solverState);

  // Vertex::processedHangingVertices.clear();

  logTraceOutWith1Argument("endIteration(State)", solverState);
}

void
LBMImplementation::
descend(
  latticeboltzmann::Cell* const        fineGridCells,
  latticeboltzmann::Vertex* const      fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  latticeboltzmann::Vertex* const      coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&              coarseGridCell)
{
  logTraceInWith2Arguments("descend(...)", coarseGridCell.toString(),
                           coarseGridVerticesEnumerator.toString());
  // @todo Insert your code here
  logTraceOut("descend(...)");
}

void
LBMImplementation::
ascend(
  latticeboltzmann::Cell* const        fineGridCells,
  latticeboltzmann::Vertex* const      fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  latticeboltzmann::Vertex* const      coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  latticeboltzmann::Cell&              coarseGridCell)
{
  logTraceInWith2Arguments("ascend(...)", coarseGridCell.toString(),
                           coarseGridVerticesEnumerator.toString());
  // @todo Insert your code here
  logTraceOut("ascend(...)");
}
}
}
