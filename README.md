Adaptive LBM in Peano
===

-------------------------------------------------------------------------------


###Deploying in the peano repository

    cd $PEANO_ROOT
    cd projects
    git clone _this_repo_
    cp peanolbm/main_CMakeLists.txt ../../CMakeLists.txt

###Compilation

    cd $PEANO_ROOT
    cd build
    cmake ..
    make -j4

### Tuning simulation parameters

The main parameters are placed into the `LBDefinitions.h`.

The control of grid refinement is done with hard-coded statements in function

    refinementCriteria()

in the file

    $PEANO_ROOT/projects/peanolbm/mappings/SetupExperiment.cpp


### Settings in the CMakeLists.txt

    # makes use of experimental code compiled with ISPC. Better not to touch :)
    #set( ISPC_BUILD 1 )

    self-esplaining
    set(DIMENSIONS 2)

### LBM code

I have tried to move all LBM-related code to the folder `lbmsteps`.
There are several files

    CellSteps.*   // All operations with CellHeap class

    VertexSteps.*   // All operations with VertexHeap class

    PointSteps.*  // It could be either CellHeap or VertexHeap, but when we change just one node in the array

    Interpolation.* // interpolation routines

    Restriction.* // Restriction routines

### Dependencies

For measuring the performance the PAPI library was used.
It is linked to the executable in the `CMakeLists.txt` file and
included as a header in `main.cpp`


### Known problems.

Interpolation of 3rd order is currently switched off.
I apparently made a mistake while planning vertex structure layout.
With the current configuration I can not access certain LB-nodes from
neighbouring cell which is needed for building the spline based on four nodes.

Besides, I think that it is better to use a spline of 3rd degree
based on all available nodes rather than on four nodes.
It will guarantee the continuity between segments.

### Plotting

It is possible to plot all the refinement levels or to plot just the fines one at current point.
I did switched it manually in `mappings/Plot.cpp` in the function `enterCell`

    600    !fineGridCell.isRefined()

    667    position(2) = 0.0 + 0.0 * (fineGridVerticesEnumerator.getLevel() - COARSE_REFINEMENT_LEVEL);
                                \
                                visual offset between refinement levels

Another feature is showing gaps between Peano cells.

    // with gaps
    609    double step          = peanoCellSize / double(LB_DIMENSION - 0);

    // without gaps
    609    double step          = peanoCellSize / double(LB_DIMENSION - 1);

