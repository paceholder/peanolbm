#include "VertexHeap.h"
latticeboltzmann::VertexHeap::PersistentRecords::
PersistentRecords() {}

latticeboltzmann::VertexHeap::PersistentRecords::
PersistentRecords(const tarch::la::Vector<VERTEX_BLOCK_SIZE,
                                          double>& SE,
                  const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& S,
                  const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& SW,
                  const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& E,
                  const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& C,
                  const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& W,
                  const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& NE,
                  const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& N,
                  const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& NW):
  _SE(SE),
  _S(S),
  _SW(SW),
  _E(E),
  _C(C),
  _W(W),
  _NE(NE),
  _N(N),
  _NW(NW) {}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeap::PersistentRecords::
getSE() const
{
  return _SE;
}

void
latticeboltzmann::VertexHeap::PersistentRecords::
setSE(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& SE)
{
  _SE = (SE);
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeap::PersistentRecords::
getS() const
{
  return _S;
}

void
latticeboltzmann::VertexHeap::PersistentRecords::
setS(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& S)
{
  _S = (S);
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeap::PersistentRecords::
getSW() const
{
  return _SW;
}

void
latticeboltzmann::VertexHeap::PersistentRecords::
setSW(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& SW)
{
  _SW = (SW);
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeap::PersistentRecords::
getE() const
{
  return _E;
}

void
latticeboltzmann::VertexHeap::PersistentRecords::
setE(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& E)
{
  _E = (E);
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeap::PersistentRecords::
getC() const
{
  return _C;
}

void
latticeboltzmann::VertexHeap::PersistentRecords::
setC(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& C)
{
  _C = (C);
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeap::PersistentRecords::
getW() const
{
  return _W;
}

void
latticeboltzmann::VertexHeap::PersistentRecords::
setW(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& W)
{
  _W = (W);
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeap::PersistentRecords::
getNE() const
{
  return _NE;
}

void
latticeboltzmann::VertexHeap::PersistentRecords::
setNE(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& NE)
{
  _NE = (NE);
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeap::PersistentRecords::
getN() const
{
  return _N;
}

void
latticeboltzmann::VertexHeap::PersistentRecords::
setN(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& N)
{
  _N = (N);
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeap::PersistentRecords::
getNW() const
{
  return _NW;
}

void
latticeboltzmann::VertexHeap::PersistentRecords::
setNW(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& NW)
{
  _NW = (NW);
}

latticeboltzmann::VertexHeap::
VertexHeap() {}

latticeboltzmann::VertexHeap::
VertexHeap(const PersistentRecords& persistentRecords):
  _persistentRecords(persistentRecords._SE,
                     persistentRecords._S,
                     persistentRecords._SW,
                     persistentRecords._E,
                     persistentRecords._C,
                     persistentRecords._W,
                     persistentRecords._NE,
                     persistentRecords._N,
                     persistentRecords._NW) {}

latticeboltzmann::VertexHeap::
VertexHeap(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& SE,
           const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& S,
           const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& SW,
           const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& E,
           const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& C,
           const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& W,
           const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& NE,
           const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& N,
           const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& NW):
  _persistentRecords(SE, S, SW, E, C, W, NE, N, NW) {}

latticeboltzmann::VertexHeap::~VertexHeap() {}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeap::
getSE() const
{
  return _persistentRecords._SE;
}

void
latticeboltzmann::VertexHeap::
setSE(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& SE)
{
  _persistentRecords._SE = (SE);
}

double
latticeboltzmann::VertexHeap::
getSE(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  return _persistentRecords._SE[elementIndex];
}

void
latticeboltzmann::VertexHeap::
setSE(int elementIndex, const double& SE)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  _persistentRecords._SE[elementIndex] = SE;
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeap::
getS() const
{
  return _persistentRecords._S;
}

void
latticeboltzmann::VertexHeap::
setS(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& S)
{
  _persistentRecords._S = (S);
}

double
latticeboltzmann::VertexHeap::
getS(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  return _persistentRecords._S[elementIndex];
}

void
latticeboltzmann::VertexHeap::
setS(int elementIndex, const double& S)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  _persistentRecords._S[elementIndex] = S;
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeap::
getSW() const
{
  return _persistentRecords._SW;
}

void
latticeboltzmann::VertexHeap::
setSW(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& SW)
{
  _persistentRecords._SW = (SW);
}

double
latticeboltzmann::VertexHeap::
getSW(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  return _persistentRecords._SW[elementIndex];
}

void
latticeboltzmann::VertexHeap::
setSW(int elementIndex, const double& SW)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  _persistentRecords._SW[elementIndex] = SW;
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeap::
getE() const
{
  return _persistentRecords._E;
}

void
latticeboltzmann::VertexHeap::
setE(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& E)
{
  _persistentRecords._E = (E);
}

double
latticeboltzmann::VertexHeap::
getE(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  return _persistentRecords._E[elementIndex];
}

void
latticeboltzmann::VertexHeap::
setE(int elementIndex, const double& E)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  _persistentRecords._E[elementIndex] = E;
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeap::
getC() const
{
  return _persistentRecords._C;
}

void
latticeboltzmann::VertexHeap::
setC(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& C)
{
  _persistentRecords._C = (C);
}

double
latticeboltzmann::VertexHeap::
getC(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  return _persistentRecords._C[elementIndex];
}

void
latticeboltzmann::VertexHeap::
setC(int elementIndex, const double& C)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  _persistentRecords._C[elementIndex] = C;
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeap::
getW() const
{
  return _persistentRecords._W;
}

void
latticeboltzmann::VertexHeap::
setW(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& W)
{
  _persistentRecords._W = (W);
}

double
latticeboltzmann::VertexHeap::
getW(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  return _persistentRecords._W[elementIndex];
}

void
latticeboltzmann::VertexHeap::
setW(int elementIndex, const double& W)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  _persistentRecords._W[elementIndex] = W;
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeap::
getNE() const
{
  return _persistentRecords._NE;
}

void
latticeboltzmann::VertexHeap::
setNE(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& NE)
{
  _persistentRecords._NE = (NE);
}

double
latticeboltzmann::VertexHeap::
getNE(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  return _persistentRecords._NE[elementIndex];
}

void
latticeboltzmann::VertexHeap::
setNE(int elementIndex, const double& NE)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  _persistentRecords._NE[elementIndex] = NE;
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeap::
getN() const
{
  return _persistentRecords._N;
}

void
latticeboltzmann::VertexHeap::
setN(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& N)
{
  _persistentRecords._N = (N);
}

double
latticeboltzmann::VertexHeap::
getN(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  return _persistentRecords._N[elementIndex];
}

void
latticeboltzmann::VertexHeap::
setN(int elementIndex, const double& N)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  _persistentRecords._N[elementIndex] = N;
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeap::
getNW() const
{
  return _persistentRecords._NW;
}

void
latticeboltzmann::VertexHeap::
setNW(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& NW)
{
  _persistentRecords._NW = (NW);
}

double
latticeboltzmann::VertexHeap::
getNW(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  return _persistentRecords._NW[elementIndex];
}

void
latticeboltzmann::VertexHeap::
setNW(int elementIndex, const double& NW)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  _persistentRecords._NW[elementIndex] = NW;
}

std::string
latticeboltzmann::VertexHeap::
toString() const
{
  std::ostringstream stringstr;
  toString(stringstr);
  return stringstr.str();
}

void
latticeboltzmann::VertexHeap::
toString(std::ostream& out) const
{
  out << "(";
  out << "SE:[";

  for (int i = 0; i < VERTEX_BLOCK_SIZE - 1; i++)
    out << getSE(i) << ",";

  out << getSE(VERTEX_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "S:[";

  for (int i = 0; i < VERTEX_BLOCK_SIZE - 1; i++)
    out << getS(i) << ",";

  out << getS(VERTEX_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "SW:[";

  for (int i = 0; i < VERTEX_BLOCK_SIZE - 1; i++)
    out << getSW(i) << ",";

  out << getSW(VERTEX_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "E:[";

  for (int i = 0; i < VERTEX_BLOCK_SIZE - 1; i++)
    out << getE(i) << ",";

  out << getE(VERTEX_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "C:[";

  for (int i = 0; i < VERTEX_BLOCK_SIZE - 1; i++)
    out << getC(i) << ",";

  out << getC(VERTEX_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "W:[";

  for (int i = 0; i < VERTEX_BLOCK_SIZE - 1; i++)
    out << getW(i) << ",";

  out << getW(VERTEX_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "NE:[";

  for (int i = 0; i < VERTEX_BLOCK_SIZE - 1; i++)
    out << getNE(i) << ",";

  out << getNE(VERTEX_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "N:[";

  for (int i = 0; i < VERTEX_BLOCK_SIZE - 1; i++)
    out << getN(i) << ",";

  out << getN(VERTEX_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "NW:[";

  for (int i = 0; i < VERTEX_BLOCK_SIZE - 1; i++)
    out << getNW(i) << ",";

  out << getNW(VERTEX_BLOCK_SIZE - 1) << "]";
  out <<  ")";
}

latticeboltzmann::VertexHeap::PersistentRecords
latticeboltzmann::VertexHeap::
getPersistentRecords() const
{
  return _persistentRecords;
}

latticeboltzmann::VertexHeapPacked
latticeboltzmann::VertexHeap::
convert() const
{
  return VertexHeapPacked(
    getSE(),
    getS(),
    getSW(),
    getE(),
    getC(),
    getW(),
    getNE(),
    getN(),
    getNW()
    );
}

#ifdef Parallel
tarch::logging::Log latticeboltzmann::VertexHeap::
_log("latticeboltzmann::VertexHeap");

MPI_Datatype latticeboltzmann::VertexHeap::Datatype     = 0;
MPI_Datatype latticeboltzmann::VertexHeap::FullDatatype = 0;

void
latticeboltzmann::VertexHeap::
initDatatype()
{
  {
    VertexHeap dummyVertexHeap[2];

    const int    Attributes           = 1;
    MPI_Datatype subtypes[Attributes] = {
      MPI_UB           // end/displacement flag
    };

    int blocklen[Attributes] = {
      1          // end/displacement flag
    };

    MPI_Aint disp[Attributes];

    MPI_Aint base;
    MPI_Address(const_cast<void*>(static_cast<const void*>(&(dummyVertexHeap[0]))), &base);

    for (int i = 1; i < Attributes; i++)
      assertion1(disp[i] > disp[i - 1], i);

    for (int i = 0; i < Attributes; i++)
      disp[i] -= base;

    MPI_Type_struct(Attributes, blocklen, disp, subtypes, &VertexHeap::Datatype);
    MPI_Type_commit(&VertexHeap::Datatype);
  }
  {
    VertexHeap dummyVertexHeap[2];

    const int    Attributes           = 10;
    MPI_Datatype subtypes[Attributes] = {
      MPI_DOUBLE,          // SE
      MPI_DOUBLE,          // S
      MPI_DOUBLE,          // SW
      MPI_DOUBLE,          // E
      MPI_DOUBLE,          // C
      MPI_DOUBLE,          // W
      MPI_DOUBLE,          // NE
      MPI_DOUBLE,          // N
      MPI_DOUBLE,          // NW
      MPI_UB           // end/displacement flag
    };

    int blocklen[Attributes] = {
      VERTEX_BLOCK_SIZE,           // SE
      VERTEX_BLOCK_SIZE,           // S
      VERTEX_BLOCK_SIZE,           // SW
      VERTEX_BLOCK_SIZE,           // E
      VERTEX_BLOCK_SIZE,           // C
      VERTEX_BLOCK_SIZE,           // W
      VERTEX_BLOCK_SIZE,           // NE
      VERTEX_BLOCK_SIZE,           // N
      VERTEX_BLOCK_SIZE,           // NW
      1          // end/displacement flag
    };

    MPI_Aint disp[Attributes];

    MPI_Aint base;
    MPI_Address(const_cast<void*>(static_cast<const void*>(&(dummyVertexHeap[0]))), &base);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyVertexHeap[0]._persistentRecords._SE[0]))),     &disp[0]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyVertexHeap[0]._persistentRecords._S[0]))),    &disp[1]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyVertexHeap[0]._persistentRecords._SW[0]))),     &disp[2]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyVertexHeap[0]._persistentRecords._E[0]))),    &disp[3]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyVertexHeap[0]._persistentRecords._C[0]))),    &disp[4]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyVertexHeap[0]._persistentRecords._W[0]))),    &disp[5]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyVertexHeap[0]._persistentRecords._NE[0]))),     &disp[6]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyVertexHeap[0]._persistentRecords._N[0]))),    &disp[7]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyVertexHeap[0]._persistentRecords._NW[0]))),     &disp[8]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &dummyVertexHeap[1]._persistentRecords._SE[0])),     &disp[9]);

    for (int i = 1; i < Attributes; i++)
      assertion1(disp[i] > disp[i - 1], i);

    for (int i = 0; i < Attributes; i++)
      disp[i] -= base;

    MPI_Type_struct(Attributes, blocklen, disp, subtypes, &VertexHeap::FullDatatype);
    MPI_Type_commit(&VertexHeap::FullDatatype);
  }
}

void
latticeboltzmann::VertexHeap::
shutdownDatatype()
{
  MPI_Type_free(&VertexHeap::Datatype);
  MPI_Type_free(&VertexHeap::FullDatatype);
}

void
latticeboltzmann::VertexHeap::
send(int destination, int tag, bool exchangeOnlyAttributesMarkedWithParallelise)
{
  MPI_Request* sendRequestHandle = new MPI_Request();

  MPI_Status status;
  int        flag = 0;
  int        result;

  clock_t timeOutWarning          = -1;
  clock_t timeOutShutdown         = -1;
  bool    triggeredTimeoutWarning = false;

  if (exchangeOnlyAttributesMarkedWithParallelise)
    result = MPI_Isend(
      this, 1, Datatype, destination,
      tag, tarch::parallel::Node::getInstance().getCommunicator(),
      sendRequestHandle
      );

  else
    result = MPI_Isend(
      this, 1, FullDatatype, destination,
      tag, tarch::parallel::Node::getInstance().getCommunicator(),
      sendRequestHandle
      );

  if  (result != MPI_SUCCESS) {
    std::ostringstream msg;
    msg << "was not able to send message latticeboltzmann::VertexHeap "
        << toString()
        << " to node " << destination
        << ": " << tarch::parallel::MPIReturnValueToString(result);
    _log.error("send(int)", msg.str());
  }

  result = MPI_Test(sendRequestHandle, &flag, &status);

  while (!flag) {
    if (timeOutWarning == -1)
      timeOutWarning = tarch::parallel::Node::getInstance().getDeadlockWarningTimeStamp();

    if (timeOutShutdown == -1)
      timeOutShutdown = tarch::parallel::Node::getInstance().getDeadlockTimeOutTimeStamp();

    result = MPI_Test(sendRequestHandle, &flag, &status);

    if (result != MPI_SUCCESS) {
      std::ostringstream msg;
      msg << "testing for finished send task for latticeboltzmann::VertexHeap "
          << toString()
          << " sent to node " << destination
          << " failed: " << tarch::parallel::MPIReturnValueToString(result);
      _log.error("send(int)", msg.str());
    }

    // deadlock aspect
    if (
      tarch::parallel::Node::getInstance().isTimeOutWarningEnabled() &&
      (clock() > timeOutWarning) &&
      (!triggeredTimeoutWarning)
      ) {
      tarch::parallel::Node::getInstance().writeTimeOutWarning(
        "latticeboltzmann::VertexHeap",
        "send(int)", destination, tag, 1
        );
      triggeredTimeoutWarning = true;
    }

    if (
      tarch::parallel::Node::getInstance().isTimeOutDeadlockEnabled() &&
      (clock() > timeOutShutdown)
      )
      tarch::parallel::Node::getInstance().triggerDeadlockTimeOut(
        "latticeboltzmann::VertexHeap",
        "send(int)", destination, tag, 1
        );

    tarch::parallel::Node::getInstance().receiveDanglingMessages();
  }

  delete sendRequestHandle;
  #ifdef Debug
  _log.debug("send(int,int)", "sent " + toString());
  #endif
}

void
latticeboltzmann::VertexHeap::
receive(int source, int tag, bool exchangeOnlyAttributesMarkedWithParallelise)
{
  MPI_Request* sendRequestHandle = new MPI_Request();

  MPI_Status status;
  int        flag = 0;
  int        result;

  clock_t timeOutWarning          = -1;
  clock_t timeOutShutdown         = -1;
  bool    triggeredTimeoutWarning = false;

  if (exchangeOnlyAttributesMarkedWithParallelise)
    result = MPI_Irecv(
      this, 1, Datatype, source, tag,
      tarch::parallel::Node::getInstance().getCommunicator(), sendRequestHandle
      );

  else
    result = MPI_Irecv(
      this, 1, FullDatatype, source, tag,
      tarch::parallel::Node::getInstance().getCommunicator(), sendRequestHandle
      );

  if (result != MPI_SUCCESS) {
    std::ostringstream msg;
    msg << "failed to start to receive latticeboltzmann::VertexHeap from node "
        << source << ": " << tarch::parallel::MPIReturnValueToString(result);
    _log.error("receive(int)", msg.str());
  }

  result = MPI_Test(sendRequestHandle, &flag, &status);

  while (!flag) {
    if (timeOutWarning == -1)
      timeOutWarning = tarch::parallel::Node::getInstance().getDeadlockWarningTimeStamp();

    if (timeOutShutdown == -1)
      timeOutShutdown = tarch::parallel::Node::getInstance().getDeadlockTimeOutTimeStamp();

    result = MPI_Test(sendRequestHandle, &flag, &status);

    if (result != MPI_SUCCESS) {
      std::ostringstream msg;
      msg << "testing for finished receive task for latticeboltzmann::VertexHeap failed: "
          << tarch::parallel::MPIReturnValueToString(result);
      _log.error("receive(int)", msg.str());
    }

    // deadlock aspect
    if (
      tarch::parallel::Node::getInstance().isTimeOutWarningEnabled() &&
      (clock() > timeOutWarning) &&
      (!triggeredTimeoutWarning)
      ) {
      tarch::parallel::Node::getInstance().writeTimeOutWarning(
        "latticeboltzmann::VertexHeap",
        "receive(int)", source, tag, 1
        );
      triggeredTimeoutWarning = true;
    }

    if (
      tarch::parallel::Node::getInstance().isTimeOutDeadlockEnabled() &&
      (clock() > timeOutShutdown)
      )
      tarch::parallel::Node::getInstance().triggerDeadlockTimeOut(
        "latticeboltzmann::VertexHeap",
        "receive(int)", source, tag, 1
        );

    tarch::parallel::Node::getInstance().receiveDanglingMessages();
  }

  delete sendRequestHandle;

  #ifdef Debug
  _log.debug("receive(int,int)", "received " + toString());
  #endif
}

bool
latticeboltzmann::VertexHeap::
isMessageInQueue(int tag, bool exchangeOnlyAttributesMarkedWithParallelise)
{
  MPI_Status status;
  int        flag = 0;
  MPI_Iprobe(
    MPI_ANY_SOURCE, tag,
    tarch::parallel::Node::getInstance().getCommunicator(), &flag, &status
    );

  if (flag) {
    int messageCounter;

    if (exchangeOnlyAttributesMarkedWithParallelise)
      MPI_Get_count(&status, Datatype, &messageCounter);
    else
      MPI_Get_count(&status, FullDatatype, &messageCounter);

    return messageCounter > 0;
  } else
    return false;
}

#endif
latticeboltzmann::VertexHeapPacked::PersistentRecords::
PersistentRecords() {}

latticeboltzmann::VertexHeapPacked::PersistentRecords::
PersistentRecords(const tarch::la::Vector<VERTEX_BLOCK_SIZE,
                                          double>& SE,
                  const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& S,
                  const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& SW,
                  const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& E,
                  const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& C,
                  const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& W,
                  const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& NE,
                  const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& N,
                  const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& NW):
  _SE(SE),
  _S(S),
  _SW(SW),
  _E(E),
  _C(C),
  _W(W),
  _NE(NE),
  _N(N),
  _NW(NW) {}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeapPacked::PersistentRecords::
getSE() const
{
  return _SE;
}

void
latticeboltzmann::VertexHeapPacked::PersistentRecords::
setSE(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& SE)
{
  _SE = (SE);
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeapPacked::PersistentRecords::
getS() const
{
  return _S;
}

void
latticeboltzmann::VertexHeapPacked::PersistentRecords::
setS(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& S)
{
  _S = (S);
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeapPacked::PersistentRecords::
getSW() const
{
  return _SW;
}

void
latticeboltzmann::VertexHeapPacked::PersistentRecords::
setSW(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& SW)
{
  _SW = (SW);
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeapPacked::PersistentRecords::
getE() const
{
  return _E;
}

void
latticeboltzmann::VertexHeapPacked::PersistentRecords::
setE(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& E)
{
  _E = (E);
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeapPacked::PersistentRecords::
getC() const
{
  return _C;
}

void
latticeboltzmann::VertexHeapPacked::PersistentRecords::
setC(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& C)
{
  _C = (C);
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeapPacked::PersistentRecords::
getW() const
{
  return _W;
}

void
latticeboltzmann::VertexHeapPacked::PersistentRecords::
setW(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& W)
{
  _W = (W);
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeapPacked::PersistentRecords::
getNE() const
{
  return _NE;
}

void
latticeboltzmann::VertexHeapPacked::PersistentRecords::
setNE(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& NE)
{
  _NE = (NE);
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeapPacked::PersistentRecords::
getN() const
{
  return _N;
}

void
latticeboltzmann::VertexHeapPacked::PersistentRecords::
setN(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& N)
{
  _N = (N);
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeapPacked::PersistentRecords::
getNW() const
{
  return _NW;
}

void
latticeboltzmann::VertexHeapPacked::PersistentRecords::
setNW(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& NW)
{
  _NW = (NW);
}

latticeboltzmann::VertexHeapPacked::
VertexHeapPacked() {}

latticeboltzmann::VertexHeapPacked::
VertexHeapPacked(const PersistentRecords& persistentRecords):
  _persistentRecords(persistentRecords._SE,
                     persistentRecords._S,
                     persistentRecords._SW,
                     persistentRecords._E,
                     persistentRecords._C,
                     persistentRecords._W,
                     persistentRecords._NE,
                     persistentRecords._N,
                     persistentRecords._NW) {}

latticeboltzmann::VertexHeapPacked::
VertexHeapPacked(const tarch::la::Vector<VERTEX_BLOCK_SIZE,
                                         double>& SE,
                 const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& S,
                 const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& SW,
                 const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& E,
                 const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& C,
                 const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& W,
                 const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& NE,
                 const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& N,
                 const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& NW):
  _persistentRecords(SE, S, SW, E, C, W, NE, N, NW) {}

latticeboltzmann::VertexHeapPacked::~VertexHeapPacked() {}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeapPacked::
getSE() const
{
  return _persistentRecords._SE;
}

void
latticeboltzmann::VertexHeapPacked::
setSE(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& SE)
{
  _persistentRecords._SE = (SE);
}

double
latticeboltzmann::VertexHeapPacked::
getSE(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  return _persistentRecords._SE[elementIndex];
}

void
latticeboltzmann::VertexHeapPacked::
setSE(int elementIndex, const double& SE)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  _persistentRecords._SE[elementIndex] = SE;
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeapPacked::
getS() const
{
  return _persistentRecords._S;
}

void
latticeboltzmann::VertexHeapPacked::
setS(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& S)
{
  _persistentRecords._S = (S);
}

double
latticeboltzmann::VertexHeapPacked::
getS(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  return _persistentRecords._S[elementIndex];
}

void
latticeboltzmann::VertexHeapPacked::
setS(int elementIndex, const double& S)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  _persistentRecords._S[elementIndex] = S;
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeapPacked::
getSW() const
{
  return _persistentRecords._SW;
}

void
latticeboltzmann::VertexHeapPacked::
setSW(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& SW)
{
  _persistentRecords._SW = (SW);
}

double
latticeboltzmann::VertexHeapPacked::
getSW(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  return _persistentRecords._SW[elementIndex];
}

void
latticeboltzmann::VertexHeapPacked::
setSW(int elementIndex, const double& SW)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  _persistentRecords._SW[elementIndex] = SW;
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeapPacked::
getE() const
{
  return _persistentRecords._E;
}

void
latticeboltzmann::VertexHeapPacked::
setE(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& E)
{
  _persistentRecords._E = (E);
}

double
latticeboltzmann::VertexHeapPacked::
getE(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  return _persistentRecords._E[elementIndex];
}

void
latticeboltzmann::VertexHeapPacked::
setE(int elementIndex, const double& E)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  _persistentRecords._E[elementIndex] = E;
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeapPacked::
getC() const
{
  return _persistentRecords._C;
}

void
latticeboltzmann::VertexHeapPacked::
setC(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& C)
{
  _persistentRecords._C = (C);
}

double
latticeboltzmann::VertexHeapPacked::
getC(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  return _persistentRecords._C[elementIndex];
}

void
latticeboltzmann::VertexHeapPacked::
setC(int elementIndex, const double& C)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  _persistentRecords._C[elementIndex] = C;
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeapPacked::
getW() const
{
  return _persistentRecords._W;
}

void
latticeboltzmann::VertexHeapPacked::
setW(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& W)
{
  _persistentRecords._W = (W);
}

double
latticeboltzmann::VertexHeapPacked::
getW(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  return _persistentRecords._W[elementIndex];
}

void
latticeboltzmann::VertexHeapPacked::
setW(int elementIndex, const double& W)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  _persistentRecords._W[elementIndex] = W;
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeapPacked::
getNE() const
{
  return _persistentRecords._NE;
}

void
latticeboltzmann::VertexHeapPacked::
setNE(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& NE)
{
  _persistentRecords._NE = (NE);
}

double
latticeboltzmann::VertexHeapPacked::
getNE(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  return _persistentRecords._NE[elementIndex];
}

void
latticeboltzmann::VertexHeapPacked::
setNE(int elementIndex, const double& NE)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  _persistentRecords._NE[elementIndex] = NE;
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeapPacked::
getN() const
{
  return _persistentRecords._N;
}

void
latticeboltzmann::VertexHeapPacked::
setN(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& N)
{
  _persistentRecords._N = (N);
}

double
latticeboltzmann::VertexHeapPacked::
getN(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  return _persistentRecords._N[elementIndex];
}

void
latticeboltzmann::VertexHeapPacked::
setN(int elementIndex, const double& N)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  _persistentRecords._N[elementIndex] = N;
}

tarch::la::Vector<VERTEX_BLOCK_SIZE, double> latticeboltzmann::VertexHeapPacked::
getNW() const
{
  return _persistentRecords._NW;
}

void
latticeboltzmann::VertexHeapPacked::
setNW(const tarch::la::Vector<VERTEX_BLOCK_SIZE, double>& NW)
{
  _persistentRecords._NW = (NW);
}

double
latticeboltzmann::VertexHeapPacked::
getNW(int elementIndex) const
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  return _persistentRecords._NW[elementIndex];
}

void
latticeboltzmann::VertexHeapPacked::
setNW(int elementIndex, const double& NW)
{
  assertion(elementIndex >= 0);
  assertion(elementIndex < VERTEX_BLOCK_SIZE);
  _persistentRecords._NW[elementIndex] = NW;
}

std::string
latticeboltzmann::VertexHeapPacked::
toString() const
{
  std::ostringstream stringstr;
  toString(stringstr);
  return stringstr.str();
}

void
latticeboltzmann::VertexHeapPacked::
toString(std::ostream& out) const
{
  out << "(";
  out << "SE:[";

  for (int i = 0; i < VERTEX_BLOCK_SIZE - 1; i++)
    out << getSE(i) << ",";

  out << getSE(VERTEX_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "S:[";

  for (int i = 0; i < VERTEX_BLOCK_SIZE - 1; i++)
    out << getS(i) << ",";

  out << getS(VERTEX_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "SW:[";

  for (int i = 0; i < VERTEX_BLOCK_SIZE - 1; i++)
    out << getSW(i) << ",";

  out << getSW(VERTEX_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "E:[";

  for (int i = 0; i < VERTEX_BLOCK_SIZE - 1; i++)
    out << getE(i) << ",";

  out << getE(VERTEX_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "C:[";

  for (int i = 0; i < VERTEX_BLOCK_SIZE - 1; i++)
    out << getC(i) << ",";

  out << getC(VERTEX_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "W:[";

  for (int i = 0; i < VERTEX_BLOCK_SIZE - 1; i++)
    out << getW(i) << ",";

  out << getW(VERTEX_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "NE:[";

  for (int i = 0; i < VERTEX_BLOCK_SIZE - 1; i++)
    out << getNE(i) << ",";

  out << getNE(VERTEX_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "N:[";

  for (int i = 0; i < VERTEX_BLOCK_SIZE - 1; i++)
    out << getN(i) << ",";

  out << getN(VERTEX_BLOCK_SIZE - 1) << "]";
  out << ",";
  out << "NW:[";

  for (int i = 0; i < VERTEX_BLOCK_SIZE - 1; i++)
    out << getNW(i) << ",";

  out << getNW(VERTEX_BLOCK_SIZE - 1) << "]";
  out <<  ")";
}

latticeboltzmann::VertexHeapPacked::PersistentRecords
latticeboltzmann::VertexHeapPacked::
getPersistentRecords() const
{
  return _persistentRecords;
}

latticeboltzmann::VertexHeap
latticeboltzmann::VertexHeapPacked::
convert() const
{
  return VertexHeap(
    getSE(),
    getS(),
    getSW(),
    getE(),
    getC(),
    getW(),
    getNE(),
    getN(),
    getNW()
    );
}

#ifdef Parallel
tarch::logging::Log latticeboltzmann::VertexHeapPacked::
_log("latticeboltzmann::VertexHeapPacked");

MPI_Datatype latticeboltzmann::VertexHeapPacked::Datatype     = 0;
MPI_Datatype latticeboltzmann::VertexHeapPacked::FullDatatype = 0;

void
latticeboltzmann::VertexHeapPacked::
initDatatype()
{
  {
    VertexHeapPacked dummyVertexHeapPacked[2];

    const int    Attributes           = 1;
    MPI_Datatype subtypes[Attributes] = {
      MPI_UB           // end/displacement flag
    };

    int blocklen[Attributes] = {
      1          // end/displacement flag
    };

    MPI_Aint disp[Attributes];

    MPI_Aint base;
    MPI_Address(const_cast<void*>(static_cast<const void*>(&(dummyVertexHeapPacked[0]))), &base);

    for (int i = 1; i < Attributes; i++)
      assertion1(disp[i] > disp[i - 1], i);

    for (int i = 0; i < Attributes; i++)
      disp[i] -= base;

    MPI_Type_struct(Attributes, blocklen, disp, subtypes, &VertexHeapPacked::Datatype);
    MPI_Type_commit(&VertexHeapPacked::Datatype);
  }
  {
    VertexHeapPacked dummyVertexHeapPacked[2];

    const int    Attributes           = 10;
    MPI_Datatype subtypes[Attributes] = {
      MPI_DOUBLE,          // SE
      MPI_DOUBLE,          // S
      MPI_DOUBLE,          // SW
      MPI_DOUBLE,          // E
      MPI_DOUBLE,          // C
      MPI_DOUBLE,          // W
      MPI_DOUBLE,          // NE
      MPI_DOUBLE,          // N
      MPI_DOUBLE,          // NW
      MPI_UB           // end/displacement flag
    };

    int blocklen[Attributes] = {
      VERTEX_BLOCK_SIZE,           // SE
      VERTEX_BLOCK_SIZE,           // S
      VERTEX_BLOCK_SIZE,           // SW
      VERTEX_BLOCK_SIZE,           // E
      VERTEX_BLOCK_SIZE,           // C
      VERTEX_BLOCK_SIZE,           // W
      VERTEX_BLOCK_SIZE,           // NE
      VERTEX_BLOCK_SIZE,           // N
      VERTEX_BLOCK_SIZE,           // NW
      1          // end/displacement flag
    };

    MPI_Aint disp[Attributes];

    MPI_Aint base;
    MPI_Address(const_cast<void*>(static_cast<const void*>(&(dummyVertexHeapPacked[0]))), &base);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyVertexHeapPacked[0]._persistentRecords._SE[0]))),     &disp[0]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyVertexHeapPacked[0]._persistentRecords._S[0]))),    &disp[1]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyVertexHeapPacked[0]._persistentRecords._SW[0]))),     &disp[2]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyVertexHeapPacked[0]._persistentRecords._E[0]))),    &disp[3]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyVertexHeapPacked[0]._persistentRecords._C[0]))),    &disp[4]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyVertexHeapPacked[0]._persistentRecords._W[0]))),    &disp[5]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyVertexHeapPacked[0]._persistentRecords._NE[0]))),     &disp[6]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyVertexHeapPacked[0]._persistentRecords._N[0]))),    &disp[7]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &(dummyVertexHeapPacked[0]._persistentRecords._NW[0]))),     &disp[8]);
    MPI_Address(const_cast<void*>(static_cast<const void*>(
                                    &dummyVertexHeapPacked[1]._persistentRecords._SE[0])),     &disp[9]);

    for (int i = 1; i < Attributes; i++)
      assertion1(disp[i] > disp[i - 1], i);

    for (int i = 0; i < Attributes; i++)
      disp[i] -= base;

    MPI_Type_struct(Attributes, blocklen, disp, subtypes, &VertexHeapPacked::FullDatatype);
    MPI_Type_commit(&VertexHeapPacked::FullDatatype);
  }
}

void
latticeboltzmann::VertexHeapPacked::
shutdownDatatype()
{
  MPI_Type_free(&VertexHeapPacked::Datatype);
  MPI_Type_free(&VertexHeapPacked::FullDatatype);
}

void
latticeboltzmann::VertexHeapPacked::
send(int destination, int tag, bool exchangeOnlyAttributesMarkedWithParallelise)
{
  MPI_Request* sendRequestHandle = new MPI_Request();

  MPI_Status status;
  int        flag = 0;
  int        result;

  clock_t timeOutWarning          = -1;
  clock_t timeOutShutdown         = -1;
  bool    triggeredTimeoutWarning = false;

  if (exchangeOnlyAttributesMarkedWithParallelise)
    result = MPI_Isend(
      this, 1, Datatype, destination,
      tag, tarch::parallel::Node::getInstance().getCommunicator(),
      sendRequestHandle
      );

  else
    result = MPI_Isend(
      this, 1, FullDatatype, destination,
      tag, tarch::parallel::Node::getInstance().getCommunicator(),
      sendRequestHandle
      );

  if  (result != MPI_SUCCESS) {
    std::ostringstream msg;
    msg << "was not able to send message latticeboltzmann::VertexHeapPacked "
        << toString()
        << " to node " << destination
        << ": " << tarch::parallel::MPIReturnValueToString(result);
    _log.error("send(int)", msg.str());
  }

  result = MPI_Test(sendRequestHandle, &flag, &status);

  while (!flag) {
    if (timeOutWarning == -1)
      timeOutWarning = tarch::parallel::Node::getInstance().getDeadlockWarningTimeStamp();

    if (timeOutShutdown == -1)
      timeOutShutdown = tarch::parallel::Node::getInstance().getDeadlockTimeOutTimeStamp();

    result = MPI_Test(sendRequestHandle, &flag, &status);

    if (result != MPI_SUCCESS) {
      std::ostringstream msg;
      msg << "testing for finished send task for latticeboltzmann::VertexHeapPacked "
          << toString()
          << " sent to node " << destination
          << " failed: " << tarch::parallel::MPIReturnValueToString(result);
      _log.error("send(int)", msg.str());
    }

    // deadlock aspect
    if (
      tarch::parallel::Node::getInstance().isTimeOutWarningEnabled() &&
      (clock() > timeOutWarning) &&
      (!triggeredTimeoutWarning)
      ) {
      tarch::parallel::Node::getInstance().writeTimeOutWarning(
        "latticeboltzmann::VertexHeapPacked",
        "send(int)", destination, tag, 1
        );
      triggeredTimeoutWarning = true;
    }

    if (
      tarch::parallel::Node::getInstance().isTimeOutDeadlockEnabled() &&
      (clock() > timeOutShutdown)
      )
      tarch::parallel::Node::getInstance().triggerDeadlockTimeOut(
        "latticeboltzmann::VertexHeapPacked",
        "send(int)", destination, tag, 1
        );

    tarch::parallel::Node::getInstance().receiveDanglingMessages();
  }

  delete sendRequestHandle;
  #ifdef Debug
  _log.debug("send(int,int)", "sent " + toString());
  #endif
}

void
latticeboltzmann::VertexHeapPacked::
receive(int source, int tag, bool exchangeOnlyAttributesMarkedWithParallelise)
{
  MPI_Request* sendRequestHandle = new MPI_Request();

  MPI_Status status;
  int        flag = 0;
  int        result;

  clock_t timeOutWarning          = -1;
  clock_t timeOutShutdown         = -1;
  bool    triggeredTimeoutWarning = false;

  if (exchangeOnlyAttributesMarkedWithParallelise)
    result = MPI_Irecv(
      this, 1, Datatype, source, tag,
      tarch::parallel::Node::getInstance().getCommunicator(), sendRequestHandle
      );

  else
    result = MPI_Irecv(
      this, 1, FullDatatype, source, tag,
      tarch::parallel::Node::getInstance().getCommunicator(), sendRequestHandle
      );

  if (result != MPI_SUCCESS) {
    std::ostringstream msg;
    msg << "failed to start to receive latticeboltzmann::VertexHeapPacked from node "
        << source << ": " << tarch::parallel::MPIReturnValueToString(result);
    _log.error("receive(int)", msg.str());
  }

  result = MPI_Test(sendRequestHandle, &flag, &status);

  while (!flag) {
    if (timeOutWarning == -1)
      timeOutWarning = tarch::parallel::Node::getInstance().getDeadlockWarningTimeStamp();

    if (timeOutShutdown == -1)
      timeOutShutdown = tarch::parallel::Node::getInstance().getDeadlockTimeOutTimeStamp();

    result = MPI_Test(sendRequestHandle, &flag, &status);

    if (result != MPI_SUCCESS) {
      std::ostringstream msg;
      msg << "testing for finished receive task for latticeboltzmann::VertexHeapPacked failed: "
          << tarch::parallel::MPIReturnValueToString(result);
      _log.error("receive(int)", msg.str());
    }

    // deadlock aspect
    if (
      tarch::parallel::Node::getInstance().isTimeOutWarningEnabled() &&
      (clock() > timeOutWarning) &&
      (!triggeredTimeoutWarning)
      ) {
      tarch::parallel::Node::getInstance().writeTimeOutWarning(
        "latticeboltzmann::VertexHeapPacked",
        "receive(int)", source, tag, 1
        );
      triggeredTimeoutWarning = true;
    }

    if (
      tarch::parallel::Node::getInstance().isTimeOutDeadlockEnabled() &&
      (clock() > timeOutShutdown)
      )
      tarch::parallel::Node::getInstance().triggerDeadlockTimeOut(
        "latticeboltzmann::VertexHeapPacked",
        "receive(int)", source, tag, 1
        );

    tarch::parallel::Node::getInstance().receiveDanglingMessages();
  }

  delete sendRequestHandle;

  #ifdef Debug
  _log.debug("receive(int,int)", "received " + toString());
  #endif
}

bool
latticeboltzmann::VertexHeapPacked::
isMessageInQueue(int tag, bool exchangeOnlyAttributesMarkedWithParallelise)
{
  MPI_Status status;
  int        flag = 0;
  MPI_Iprobe(
    MPI_ANY_SOURCE, tag,
    tarch::parallel::Node::getInstance().getCommunicator(), &flag, &status
    );

  if (flag) {
    int messageCounter;

    if (exchangeOnlyAttributesMarkedWithParallelise)
      MPI_Get_count(&status, Datatype, &messageCounter);
    else
      MPI_Get_count(&status, FullDatatype, &messageCounter);

    return messageCounter > 0;
  } else
    return false;
}

#endif
