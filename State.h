// This file is part of the Peano project. For conditions of distribution and
// use, please see the copyright notice at www.peano-framework.org
#ifndef _LATTICEBOLTZMANN_STATE_H_
#define _LATTICEBOLTZMANN_STATE_H_

#include "latticeboltzmann/records/State.h"
#include "peano/grid/State.h"

#include "peano/grid/Checkpoint.h"

namespace latticeboltzmann {
class State;

/**
 * Forward declaration
 */
class Vertex;

/**
 * Forward declaration
 */
class Cell;

namespace repositories {
/**
 * Forward declaration
 */
class RepositoryArrayStack;
class RepositorySTDStack;
}
}

/**
 * Blueprint for solver state.
 *
 * This file has originally been created by the PDT and may be manually extended to
 * the needs of your application. We do not recommend to remove anything!
 */
class latticeboltzmann::State: public peano::grid::State<latticeboltzmann::records::State> {
private:
  typedef class peano::grid::State<latticeboltzmann::records::State> Base;

  /**
   * Needed for checkpointing.
   */
  friend class latticeboltzmann::repositories::RepositoryArrayStack;
  friend class latticeboltzmann::repositories::RepositorySTDStack;

  void
  writeToCheckpoint(peano::grid::Checkpoint<Vertex, Cell>&  checkpoint) const;

  void
  readFromCheckpoint(const peano::grid::Checkpoint<Vertex, Cell>&  checkpoint);

  static int _iteration;

public:
  /**
   * Default Constructor
   *
   * This constructor is required by the framework's data container. Do not
   * remove it.
   */
  State();

  /**
   * Constructor
   *
   * This constructor is required by the framework's data container. Do not
   * remove it. It is kind of a copy constructor that converts an object which
   * comprises solely persistent attributes into a full attribute. This very
   * functionality is implemented within the super type, i.e. this constructor
   * has to invoke the correponsing super type's constructor and not the super
   * type standard constructor.
   */
  State(const Base::PersistentState& argument);

public:
  static
  int
  getIteration();

  static
  void
  setIteration(const int iteration);

  int
  getCoarseRefinementLevel() const;

  void
  setCoarseRefinementLevel(const int refinementLevel);

  int
  getFineRefinementLevel() const;

  void
  setFineRefinementLevel(const int fineRefinementLevel);

  // we need to store two steps on each refinement level
  // to avoid data copying we remember number of last touch array (either 0 or 1)
  // the array is of length  FINE_REFINEMENT_LEVEL
  static std::vector<int> lastArray;

  static double tauOnTheCoarsestLevel;

public:
  static
  double
  getTau(int level);

  static
  double
  getTimeStep(int level);

  // due to different scales of simulation
  // we need to do tree iterations on fine level
  // and skip these three iterations on coarser level
  static
  bool
  isIterationWithComputing(int level, int iterationOffset  = 0);
};

#endif
