#include "latticeboltzmann/Cell.h"

namespace latticeboltzmann {
Cell::
Cell():
  Base()
{
  // @todo Insert your code here
}

Cell::
Cell(const Base::DoNotCallStandardConstructor& value):
  Base(value)
{
  // Please do not insert anything here
}

Cell::
Cell(const Base::PersistentCell& argument):
  Base(argument)
{
  // @todo Insert your code here
}

// -------------------------------------------------

int
Cell::
getAggregateWithArbitraryCardinality() const
{
  return Base::_cellData.getAggregateWithArbitraryCardinality();
}

void
Cell::
setAggregateWithArbitraryCardinality(const int cardinality)
{
  Base::_cellData.setAggregateWithArbitraryCardinality(cardinality);
}

}
