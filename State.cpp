#include "latticeboltzmann/Cell.h"
#include "latticeboltzmann/State.h"
#include "latticeboltzmann/Vertex.h"

#include "peano/grid/Checkpoint.h"

#include "LBDefinitions.h"

#include <vector>

namespace latticeboltzmann {
int State::_iteration = 0;

// +1 because we want address directly finest level
// for example 3 , in this case array must have lenght 3 + 1 to have
// last cell with #3
std::vector<int> State::
lastArray(FINE_REFINEMENT_LEVEL + 1, 0);

double State::tauOnTheCoarsestLevel = TAU_ON_COARSEST_LEVEL;

State::
State():
  Base()
{
  Base::_stateData.setIteration(0);
}

State::
State(const Base::PersistentState& argument):
  Base(argument)
{
  // @todo Insert your code here
}

void
State::
writeToCheckpoint(peano::grid::Checkpoint<Vertex,
                                          Cell>& checkpoint)
const
{
  // @todo Insert your code here
}

void
State::
readFromCheckpoint(const peano::grid::Checkpoint<Vertex,
                                                 Cell>&
                   checkpoint)
{
  // @todo Insert your code here
}

int
State::
getIteration()
{
  return State::_iteration;
}

void
State::
setIteration(const int iteration)
{
  State::_iteration = iteration;
}

double
State::
getTau(int level)
{
  int    levelOffset = level - COARSE_REFINEMENT_LEVEL;
  double tau         = State::tauOnTheCoarsestLevel;

  for (int i = 0; i < levelOffset; ++i)
    tau = 3.0 * (tau - 0.5) + 0.5;

  return tau;
}

double
State::
getTimeStep(int level)
{
  // ----------------
  // how many iterations we must skip
  // (we need 9 : 3 : 1  iterations on finest : fine : coarse levels)
  int skipIterationsOnCurrentLevel = 1;

  for (int i = 0; i < FINE_REFINEMENT_LEVEL - level; ++i)
    skipIterationsOnCurrentLevel *= 3;

  int skipIterationsOnCoarseLevel = skipIterationsOnCurrentLevel * 3;

  // --------------

  // which iteration is currently on the level above
  int iterationOnCoarseLevel =  State::getIteration() / (skipIterationsOnCoarseLevel) * skipIterationsOnCoarseLevel;

  // it could be 0, 1, 2, which correspond to 0, 1/3, 2/3 time interpolation
  double timestep = (State::getIteration() - iterationOnCoarseLevel) / (double)skipIterationsOnCoarseLevel;

  assert(timestep < 1.011);

  timestep += 1.0 / 3.0;

  return timestep;
}

bool
State::
isIterationWithComputing(int level, int iterationOffset)
{
  // "distance" from current refinement level to the finest one
  int levelCounter = FINE_REFINEMENT_LEVEL - level;

  // how much iterations we must skip
  // (we need 9 : 3 : 1  iterations on finest : fine : coarse levels)
  int skipIterations = 1;

  for (int i = 0; i < levelCounter; ++i)
    skipIterations *= 3;

  iterationOffset = iterationOffset * skipIterations / 3;

  bool b = (State::getIteration() + iterationOffset) % skipIterations == 0;

  return b;
}
}
