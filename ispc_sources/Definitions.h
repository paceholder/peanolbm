#ifdef Dim2

  #define SOUTH_WEST 0
  #define SOUTH      1
  #define SOUTH_EAST 2
  #define WEST       3
  #define CENTER     4
  #define EAST       5
  #define NORTH_WEST 6
  #define NORTH      7
  #define NORTH_EAST 8

#else

  #define BOTTOM_SOUTH 0
  #define BOTTOM_WEST  1
  #define BOTTOM       2
  #define BOTTOM_EAST  3
  #define BOTTOM_NORTH 4

  #define SOUTH_WEST 5
  #define SOUTH      6
  #define SOUTH_EAST 7
  #define WEST       8
  #define CENTER     9
  #define EAST       10
  #define NORTH_WEST 11
  #define NORTH      12
  #define NORTH_EAST 13

  #define TOP_SOUTH 14
  #define TOP_WEST  15
  #define TOP       16
  #define TOP_EAST  17
  #define TOP_NORTH 18

#endif
